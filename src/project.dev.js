window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  1: [ function(require, module, exports) {
    "use strict";
    exports.byteLength = byteLength;
    exports.toByteArray = toByteArray;
    exports.fromByteArray = fromByteArray;
    var lookup = [];
    var revLookup = [];
    var Arr = "undefined" !== typeof Uint8Array ? Uint8Array : Array;
    var code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    for (var i = 0, len = code.length; i < len; ++i) {
      lookup[i] = code[i];
      revLookup[code.charCodeAt(i)] = i;
    }
    revLookup["-".charCodeAt(0)] = 62;
    revLookup["_".charCodeAt(0)] = 63;
    function getLens(b64) {
      var len = b64.length;
      if (len % 4 > 0) throw new Error("Invalid string. Length must be a multiple of 4");
      var validLen = b64.indexOf("=");
      -1 === validLen && (validLen = len);
      var placeHoldersLen = validLen === len ? 0 : 4 - validLen % 4;
      return [ validLen, placeHoldersLen ];
    }
    function byteLength(b64) {
      var lens = getLens(b64);
      var validLen = lens[0];
      var placeHoldersLen = lens[1];
      return 3 * (validLen + placeHoldersLen) / 4 - placeHoldersLen;
    }
    function _byteLength(b64, validLen, placeHoldersLen) {
      return 3 * (validLen + placeHoldersLen) / 4 - placeHoldersLen;
    }
    function toByteArray(b64) {
      var tmp;
      var lens = getLens(b64);
      var validLen = lens[0];
      var placeHoldersLen = lens[1];
      var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen));
      var curByte = 0;
      var len = placeHoldersLen > 0 ? validLen - 4 : validLen;
      for (var i = 0; i < len; i += 4) {
        tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
        arr[curByte++] = tmp >> 16 & 255;
        arr[curByte++] = tmp >> 8 & 255;
        arr[curByte++] = 255 & tmp;
      }
      if (2 === placeHoldersLen) {
        tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
        arr[curByte++] = 255 & tmp;
      }
      if (1 === placeHoldersLen) {
        tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
        arr[curByte++] = tmp >> 8 & 255;
        arr[curByte++] = 255 & tmp;
      }
      return arr;
    }
    function tripletToBase64(num) {
      return lookup[num >> 18 & 63] + lookup[num >> 12 & 63] + lookup[num >> 6 & 63] + lookup[63 & num];
    }
    function encodeChunk(uint8, start, end) {
      var tmp;
      var output = [];
      for (var i = start; i < end; i += 3) {
        tmp = (uint8[i] << 16 & 16711680) + (uint8[i + 1] << 8 & 65280) + (255 & uint8[i + 2]);
        output.push(tripletToBase64(tmp));
      }
      return output.join("");
    }
    function fromByteArray(uint8) {
      var tmp;
      var len = uint8.length;
      var extraBytes = len % 3;
      var parts = [];
      var maxChunkLength = 16383;
      for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
      if (1 === extraBytes) {
        tmp = uint8[len - 1];
        parts.push(lookup[tmp >> 2] + lookup[tmp << 4 & 63] + "==");
      } else if (2 === extraBytes) {
        tmp = (uint8[len - 2] << 8) + uint8[len - 1];
        parts.push(lookup[tmp >> 10] + lookup[tmp >> 4 & 63] + lookup[tmp << 2 & 63] + "=");
      }
      return parts.join("");
    }
  }, {} ],
  2: [ function(require, module, exports) {
    (function(global) {
      "use strict";
      var base64 = require("base64-js");
      var ieee754 = require("ieee754");
      var isArray = require("isarray");
      exports.Buffer = Buffer;
      exports.SlowBuffer = SlowBuffer;
      exports.INSPECT_MAX_BYTES = 50;
      Buffer.TYPED_ARRAY_SUPPORT = void 0 !== global.TYPED_ARRAY_SUPPORT ? global.TYPED_ARRAY_SUPPORT : typedArraySupport();
      exports.kMaxLength = kMaxLength();
      function typedArraySupport() {
        try {
          var arr = new Uint8Array(1);
          arr.__proto__ = {
            __proto__: Uint8Array.prototype,
            foo: function() {
              return 42;
            }
          };
          return 42 === arr.foo() && "function" === typeof arr.subarray && 0 === arr.subarray(1, 1).byteLength;
        } catch (e) {
          return false;
        }
      }
      function kMaxLength() {
        return Buffer.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
      }
      function createBuffer(that, length) {
        if (kMaxLength() < length) throw new RangeError("Invalid typed array length");
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          that = new Uint8Array(length);
          that.__proto__ = Buffer.prototype;
        } else {
          null === that && (that = new Buffer(length));
          that.length = length;
        }
        return that;
      }
      function Buffer(arg, encodingOrOffset, length) {
        if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) return new Buffer(arg, encodingOrOffset, length);
        if ("number" === typeof arg) {
          if ("string" === typeof encodingOrOffset) throw new Error("If encoding is specified then the first argument must be a string");
          return allocUnsafe(this, arg);
        }
        return from(this, arg, encodingOrOffset, length);
      }
      Buffer.poolSize = 8192;
      Buffer._augment = function(arr) {
        arr.__proto__ = Buffer.prototype;
        return arr;
      };
      function from(that, value, encodingOrOffset, length) {
        if ("number" === typeof value) throw new TypeError('"value" argument must not be a number');
        if ("undefined" !== typeof ArrayBuffer && value instanceof ArrayBuffer) return fromArrayBuffer(that, value, encodingOrOffset, length);
        if ("string" === typeof value) return fromString(that, value, encodingOrOffset);
        return fromObject(that, value);
      }
      Buffer.from = function(value, encodingOrOffset, length) {
        return from(null, value, encodingOrOffset, length);
      };
      if (Buffer.TYPED_ARRAY_SUPPORT) {
        Buffer.prototype.__proto__ = Uint8Array.prototype;
        Buffer.__proto__ = Uint8Array;
        "undefined" !== typeof Symbol && Symbol.species && Buffer[Symbol.species] === Buffer && Object.defineProperty(Buffer, Symbol.species, {
          value: null,
          configurable: true
        });
      }
      function assertSize(size) {
        if ("number" !== typeof size) throw new TypeError('"size" argument must be a number');
        if (size < 0) throw new RangeError('"size" argument must not be negative');
      }
      function alloc(that, size, fill, encoding) {
        assertSize(size);
        if (size <= 0) return createBuffer(that, size);
        if (void 0 !== fill) return "string" === typeof encoding ? createBuffer(that, size).fill(fill, encoding) : createBuffer(that, size).fill(fill);
        return createBuffer(that, size);
      }
      Buffer.alloc = function(size, fill, encoding) {
        return alloc(null, size, fill, encoding);
      };
      function allocUnsafe(that, size) {
        assertSize(size);
        that = createBuffer(that, size < 0 ? 0 : 0 | checked(size));
        if (!Buffer.TYPED_ARRAY_SUPPORT) for (var i = 0; i < size; ++i) that[i] = 0;
        return that;
      }
      Buffer.allocUnsafe = function(size) {
        return allocUnsafe(null, size);
      };
      Buffer.allocUnsafeSlow = function(size) {
        return allocUnsafe(null, size);
      };
      function fromString(that, string, encoding) {
        "string" === typeof encoding && "" !== encoding || (encoding = "utf8");
        if (!Buffer.isEncoding(encoding)) throw new TypeError('"encoding" must be a valid string encoding');
        var length = 0 | byteLength(string, encoding);
        that = createBuffer(that, length);
        var actual = that.write(string, encoding);
        actual !== length && (that = that.slice(0, actual));
        return that;
      }
      function fromArrayLike(that, array) {
        var length = array.length < 0 ? 0 : 0 | checked(array.length);
        that = createBuffer(that, length);
        for (var i = 0; i < length; i += 1) that[i] = 255 & array[i];
        return that;
      }
      function fromArrayBuffer(that, array, byteOffset, length) {
        array.byteLength;
        if (byteOffset < 0 || array.byteLength < byteOffset) throw new RangeError("'offset' is out of bounds");
        if (array.byteLength < byteOffset + (length || 0)) throw new RangeError("'length' is out of bounds");
        array = void 0 === byteOffset && void 0 === length ? new Uint8Array(array) : void 0 === length ? new Uint8Array(array, byteOffset) : new Uint8Array(array, byteOffset, length);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          that = array;
          that.__proto__ = Buffer.prototype;
        } else that = fromArrayLike(that, array);
        return that;
      }
      function fromObject(that, obj) {
        if (Buffer.isBuffer(obj)) {
          var len = 0 | checked(obj.length);
          that = createBuffer(that, len);
          if (0 === that.length) return that;
          obj.copy(that, 0, 0, len);
          return that;
        }
        if (obj) {
          if ("undefined" !== typeof ArrayBuffer && obj.buffer instanceof ArrayBuffer || "length" in obj) {
            if ("number" !== typeof obj.length || isnan(obj.length)) return createBuffer(that, 0);
            return fromArrayLike(that, obj);
          }
          if ("Buffer" === obj.type && isArray(obj.data)) return fromArrayLike(that, obj.data);
        }
        throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
      }
      function checked(length) {
        if (length >= kMaxLength()) throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + kMaxLength().toString(16) + " bytes");
        return 0 | length;
      }
      function SlowBuffer(length) {
        +length != length && (length = 0);
        return Buffer.alloc(+length);
      }
      Buffer.isBuffer = function isBuffer(b) {
        return !!(null != b && b._isBuffer);
      };
      Buffer.compare = function compare(a, b) {
        if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) throw new TypeError("Arguments must be Buffers");
        if (a === b) return 0;
        var x = a.length;
        var y = b.length;
        for (var i = 0, len = Math.min(x, y); i < len; ++i) if (a[i] !== b[i]) {
          x = a[i];
          y = b[i];
          break;
        }
        if (x < y) return -1;
        if (y < x) return 1;
        return 0;
      };
      Buffer.isEncoding = function isEncoding(encoding) {
        switch (String(encoding).toLowerCase()) {
         case "hex":
         case "utf8":
         case "utf-8":
         case "ascii":
         case "latin1":
         case "binary":
         case "base64":
         case "ucs2":
         case "ucs-2":
         case "utf16le":
         case "utf-16le":
          return true;

         default:
          return false;
        }
      };
      Buffer.concat = function concat(list, length) {
        if (!isArray(list)) throw new TypeError('"list" argument must be an Array of Buffers');
        if (0 === list.length) return Buffer.alloc(0);
        var i;
        if (void 0 === length) {
          length = 0;
          for (i = 0; i < list.length; ++i) length += list[i].length;
        }
        var buffer = Buffer.allocUnsafe(length);
        var pos = 0;
        for (i = 0; i < list.length; ++i) {
          var buf = list[i];
          if (!Buffer.isBuffer(buf)) throw new TypeError('"list" argument must be an Array of Buffers');
          buf.copy(buffer, pos);
          pos += buf.length;
        }
        return buffer;
      };
      function byteLength(string, encoding) {
        if (Buffer.isBuffer(string)) return string.length;
        if ("undefined" !== typeof ArrayBuffer && "function" === typeof ArrayBuffer.isView && (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) return string.byteLength;
        "string" !== typeof string && (string = "" + string);
        var len = string.length;
        if (0 === len) return 0;
        var loweredCase = false;
        for (;;) switch (encoding) {
         case "ascii":
         case "latin1":
         case "binary":
          return len;

         case "utf8":
         case "utf-8":
         case void 0:
          return utf8ToBytes(string).length;

         case "ucs2":
         case "ucs-2":
         case "utf16le":
         case "utf-16le":
          return 2 * len;

         case "hex":
          return len >>> 1;

         case "base64":
          return base64ToBytes(string).length;

         default:
          if (loweredCase) return utf8ToBytes(string).length;
          encoding = ("" + encoding).toLowerCase();
          loweredCase = true;
        }
      }
      Buffer.byteLength = byteLength;
      function slowToString(encoding, start, end) {
        var loweredCase = false;
        (void 0 === start || start < 0) && (start = 0);
        if (start > this.length) return "";
        (void 0 === end || end > this.length) && (end = this.length);
        if (end <= 0) return "";
        end >>>= 0;
        start >>>= 0;
        if (end <= start) return "";
        encoding || (encoding = "utf8");
        while (true) switch (encoding) {
         case "hex":
          return hexSlice(this, start, end);

         case "utf8":
         case "utf-8":
          return utf8Slice(this, start, end);

         case "ascii":
          return asciiSlice(this, start, end);

         case "latin1":
         case "binary":
          return latin1Slice(this, start, end);

         case "base64":
          return base64Slice(this, start, end);

         case "ucs2":
         case "ucs-2":
         case "utf16le":
         case "utf-16le":
          return utf16leSlice(this, start, end);

         default:
          if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
          encoding = (encoding + "").toLowerCase();
          loweredCase = true;
        }
      }
      Buffer.prototype._isBuffer = true;
      function swap(b, n, m) {
        var i = b[n];
        b[n] = b[m];
        b[m] = i;
      }
      Buffer.prototype.swap16 = function swap16() {
        var len = this.length;
        if (len % 2 !== 0) throw new RangeError("Buffer size must be a multiple of 16-bits");
        for (var i = 0; i < len; i += 2) swap(this, i, i + 1);
        return this;
      };
      Buffer.prototype.swap32 = function swap32() {
        var len = this.length;
        if (len % 4 !== 0) throw new RangeError("Buffer size must be a multiple of 32-bits");
        for (var i = 0; i < len; i += 4) {
          swap(this, i, i + 3);
          swap(this, i + 1, i + 2);
        }
        return this;
      };
      Buffer.prototype.swap64 = function swap64() {
        var len = this.length;
        if (len % 8 !== 0) throw new RangeError("Buffer size must be a multiple of 64-bits");
        for (var i = 0; i < len; i += 8) {
          swap(this, i, i + 7);
          swap(this, i + 1, i + 6);
          swap(this, i + 2, i + 5);
          swap(this, i + 3, i + 4);
        }
        return this;
      };
      Buffer.prototype.toString = function toString() {
        var length = 0 | this.length;
        if (0 === length) return "";
        if (0 === arguments.length) return utf8Slice(this, 0, length);
        return slowToString.apply(this, arguments);
      };
      Buffer.prototype.equals = function equals(b) {
        if (!Buffer.isBuffer(b)) throw new TypeError("Argument must be a Buffer");
        if (this === b) return true;
        return 0 === Buffer.compare(this, b);
      };
      Buffer.prototype.inspect = function inspect() {
        var str = "";
        var max = exports.INSPECT_MAX_BYTES;
        if (this.length > 0) {
          str = this.toString("hex", 0, max).match(/.{2}/g).join(" ");
          this.length > max && (str += " ... ");
        }
        return "<Buffer " + str + ">";
      };
      Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
        if (!Buffer.isBuffer(target)) throw new TypeError("Argument must be a Buffer");
        void 0 === start && (start = 0);
        void 0 === end && (end = target ? target.length : 0);
        void 0 === thisStart && (thisStart = 0);
        void 0 === thisEnd && (thisEnd = this.length);
        if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) throw new RangeError("out of range index");
        if (thisStart >= thisEnd && start >= end) return 0;
        if (thisStart >= thisEnd) return -1;
        if (start >= end) return 1;
        start >>>= 0;
        end >>>= 0;
        thisStart >>>= 0;
        thisEnd >>>= 0;
        if (this === target) return 0;
        var x = thisEnd - thisStart;
        var y = end - start;
        var len = Math.min(x, y);
        var thisCopy = this.slice(thisStart, thisEnd);
        var targetCopy = target.slice(start, end);
        for (var i = 0; i < len; ++i) if (thisCopy[i] !== targetCopy[i]) {
          x = thisCopy[i];
          y = targetCopy[i];
          break;
        }
        if (x < y) return -1;
        if (y < x) return 1;
        return 0;
      };
      function bidirectionalIndexOf(buffer, val, byteOffset, encoding, dir) {
        if (0 === buffer.length) return -1;
        if ("string" === typeof byteOffset) {
          encoding = byteOffset;
          byteOffset = 0;
        } else byteOffset > 2147483647 ? byteOffset = 2147483647 : byteOffset < -2147483648 && (byteOffset = -2147483648);
        byteOffset = +byteOffset;
        isNaN(byteOffset) && (byteOffset = dir ? 0 : buffer.length - 1);
        byteOffset < 0 && (byteOffset = buffer.length + byteOffset);
        if (byteOffset >= buffer.length) {
          if (dir) return -1;
          byteOffset = buffer.length - 1;
        } else if (byteOffset < 0) {
          if (!dir) return -1;
          byteOffset = 0;
        }
        "string" === typeof val && (val = Buffer.from(val, encoding));
        if (Buffer.isBuffer(val)) {
          if (0 === val.length) return -1;
          return arrayIndexOf(buffer, val, byteOffset, encoding, dir);
        }
        if ("number" === typeof val) {
          val &= 255;
          if (Buffer.TYPED_ARRAY_SUPPORT && "function" === typeof Uint8Array.prototype.indexOf) return dir ? Uint8Array.prototype.indexOf.call(buffer, val, byteOffset) : Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset);
          return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir);
        }
        throw new TypeError("val must be string, number or Buffer");
      }
      function arrayIndexOf(arr, val, byteOffset, encoding, dir) {
        var indexSize = 1;
        var arrLength = arr.length;
        var valLength = val.length;
        if (void 0 !== encoding) {
          encoding = String(encoding).toLowerCase();
          if ("ucs2" === encoding || "ucs-2" === encoding || "utf16le" === encoding || "utf-16le" === encoding) {
            if (arr.length < 2 || val.length < 2) return -1;
            indexSize = 2;
            arrLength /= 2;
            valLength /= 2;
            byteOffset /= 2;
          }
        }
        function read(buf, i) {
          return 1 === indexSize ? buf[i] : buf.readUInt16BE(i * indexSize);
        }
        var i;
        if (dir) {
          var foundIndex = -1;
          for (i = byteOffset; i < arrLength; i++) if (read(arr, i) === read(val, -1 === foundIndex ? 0 : i - foundIndex)) {
            -1 === foundIndex && (foundIndex = i);
            if (i - foundIndex + 1 === valLength) return foundIndex * indexSize;
          } else {
            -1 !== foundIndex && (i -= i - foundIndex);
            foundIndex = -1;
          }
        } else {
          byteOffset + valLength > arrLength && (byteOffset = arrLength - valLength);
          for (i = byteOffset; i >= 0; i--) {
            var found = true;
            for (var j = 0; j < valLength; j++) if (read(arr, i + j) !== read(val, j)) {
              found = false;
              break;
            }
            if (found) return i;
          }
        }
        return -1;
      }
      Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
        return -1 !== this.indexOf(val, byteOffset, encoding);
      };
      Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
        return bidirectionalIndexOf(this, val, byteOffset, encoding, true);
      };
      Buffer.prototype.lastIndexOf = function lastIndexOf(val, byteOffset, encoding) {
        return bidirectionalIndexOf(this, val, byteOffset, encoding, false);
      };
      function hexWrite(buf, string, offset, length) {
        offset = Number(offset) || 0;
        var remaining = buf.length - offset;
        if (length) {
          length = Number(length);
          length > remaining && (length = remaining);
        } else length = remaining;
        var strLen = string.length;
        if (strLen % 2 !== 0) throw new TypeError("Invalid hex string");
        length > strLen / 2 && (length = strLen / 2);
        for (var i = 0; i < length; ++i) {
          var parsed = parseInt(string.substr(2 * i, 2), 16);
          if (isNaN(parsed)) return i;
          buf[offset + i] = parsed;
        }
        return i;
      }
      function utf8Write(buf, string, offset, length) {
        return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
      }
      function asciiWrite(buf, string, offset, length) {
        return blitBuffer(asciiToBytes(string), buf, offset, length);
      }
      function latin1Write(buf, string, offset, length) {
        return asciiWrite(buf, string, offset, length);
      }
      function base64Write(buf, string, offset, length) {
        return blitBuffer(base64ToBytes(string), buf, offset, length);
      }
      function ucs2Write(buf, string, offset, length) {
        return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
      }
      Buffer.prototype.write = function write(string, offset, length, encoding) {
        if (void 0 === offset) {
          encoding = "utf8";
          length = this.length;
          offset = 0;
        } else if (void 0 === length && "string" === typeof offset) {
          encoding = offset;
          length = this.length;
          offset = 0;
        } else {
          if (!isFinite(offset)) throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
          offset |= 0;
          if (isFinite(length)) {
            length |= 0;
            void 0 === encoding && (encoding = "utf8");
          } else {
            encoding = length;
            length = void 0;
          }
        }
        var remaining = this.length - offset;
        (void 0 === length || length > remaining) && (length = remaining);
        if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) throw new RangeError("Attempt to write outside buffer bounds");
        encoding || (encoding = "utf8");
        var loweredCase = false;
        for (;;) switch (encoding) {
         case "hex":
          return hexWrite(this, string, offset, length);

         case "utf8":
         case "utf-8":
          return utf8Write(this, string, offset, length);

         case "ascii":
          return asciiWrite(this, string, offset, length);

         case "latin1":
         case "binary":
          return latin1Write(this, string, offset, length);

         case "base64":
          return base64Write(this, string, offset, length);

         case "ucs2":
         case "ucs-2":
         case "utf16le":
         case "utf-16le":
          return ucs2Write(this, string, offset, length);

         default:
          if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
          encoding = ("" + encoding).toLowerCase();
          loweredCase = true;
        }
      };
      Buffer.prototype.toJSON = function toJSON() {
        return {
          type: "Buffer",
          data: Array.prototype.slice.call(this._arr || this, 0)
        };
      };
      function base64Slice(buf, start, end) {
        return 0 === start && end === buf.length ? base64.fromByteArray(buf) : base64.fromByteArray(buf.slice(start, end));
      }
      function utf8Slice(buf, start, end) {
        end = Math.min(buf.length, end);
        var res = [];
        var i = start;
        while (i < end) {
          var firstByte = buf[i];
          var codePoint = null;
          var bytesPerSequence = firstByte > 239 ? 4 : firstByte > 223 ? 3 : firstByte > 191 ? 2 : 1;
          if (i + bytesPerSequence <= end) {
            var secondByte, thirdByte, fourthByte, tempCodePoint;
            switch (bytesPerSequence) {
             case 1:
              firstByte < 128 && (codePoint = firstByte);
              break;

             case 2:
              secondByte = buf[i + 1];
              if (128 === (192 & secondByte)) {
                tempCodePoint = (31 & firstByte) << 6 | 63 & secondByte;
                tempCodePoint > 127 && (codePoint = tempCodePoint);
              }
              break;

             case 3:
              secondByte = buf[i + 1];
              thirdByte = buf[i + 2];
              if (128 === (192 & secondByte) && 128 === (192 & thirdByte)) {
                tempCodePoint = (15 & firstByte) << 12 | (63 & secondByte) << 6 | 63 & thirdByte;
                tempCodePoint > 2047 && (tempCodePoint < 55296 || tempCodePoint > 57343) && (codePoint = tempCodePoint);
              }
              break;

             case 4:
              secondByte = buf[i + 1];
              thirdByte = buf[i + 2];
              fourthByte = buf[i + 3];
              if (128 === (192 & secondByte) && 128 === (192 & thirdByte) && 128 === (192 & fourthByte)) {
                tempCodePoint = (15 & firstByte) << 18 | (63 & secondByte) << 12 | (63 & thirdByte) << 6 | 63 & fourthByte;
                tempCodePoint > 65535 && tempCodePoint < 1114112 && (codePoint = tempCodePoint);
              }
            }
          }
          if (null === codePoint) {
            codePoint = 65533;
            bytesPerSequence = 1;
          } else if (codePoint > 65535) {
            codePoint -= 65536;
            res.push(codePoint >>> 10 & 1023 | 55296);
            codePoint = 56320 | 1023 & codePoint;
          }
          res.push(codePoint);
          i += bytesPerSequence;
        }
        return decodeCodePointsArray(res);
      }
      var MAX_ARGUMENTS_LENGTH = 4096;
      function decodeCodePointsArray(codePoints) {
        var len = codePoints.length;
        if (len <= MAX_ARGUMENTS_LENGTH) return String.fromCharCode.apply(String, codePoints);
        var res = "";
        var i = 0;
        while (i < len) res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
        return res;
      }
      function asciiSlice(buf, start, end) {
        var ret = "";
        end = Math.min(buf.length, end);
        for (var i = start; i < end; ++i) ret += String.fromCharCode(127 & buf[i]);
        return ret;
      }
      function latin1Slice(buf, start, end) {
        var ret = "";
        end = Math.min(buf.length, end);
        for (var i = start; i < end; ++i) ret += String.fromCharCode(buf[i]);
        return ret;
      }
      function hexSlice(buf, start, end) {
        var len = buf.length;
        (!start || start < 0) && (start = 0);
        (!end || end < 0 || end > len) && (end = len);
        var out = "";
        for (var i = start; i < end; ++i) out += toHex(buf[i]);
        return out;
      }
      function utf16leSlice(buf, start, end) {
        var bytes = buf.slice(start, end);
        var res = "";
        for (var i = 0; i < bytes.length; i += 2) res += String.fromCharCode(bytes[i] + 256 * bytes[i + 1]);
        return res;
      }
      Buffer.prototype.slice = function slice(start, end) {
        var len = this.length;
        start = ~~start;
        end = void 0 === end ? len : ~~end;
        if (start < 0) {
          start += len;
          start < 0 && (start = 0);
        } else start > len && (start = len);
        if (end < 0) {
          end += len;
          end < 0 && (end = 0);
        } else end > len && (end = len);
        end < start && (end = start);
        var newBuf;
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          newBuf = this.subarray(start, end);
          newBuf.__proto__ = Buffer.prototype;
        } else {
          var sliceLen = end - start;
          newBuf = new Buffer(sliceLen, void 0);
          for (var i = 0; i < sliceLen; ++i) newBuf[i] = this[i + start];
        }
        return newBuf;
      };
      function checkOffset(offset, ext, length) {
        if (offset % 1 !== 0 || offset < 0) throw new RangeError("offset is not uint");
        if (offset + ext > length) throw new RangeError("Trying to access beyond buffer length");
      }
      Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
        offset |= 0;
        byteLength |= 0;
        noAssert || checkOffset(offset, byteLength, this.length);
        var val = this[offset];
        var mul = 1;
        var i = 0;
        while (++i < byteLength && (mul *= 256)) val += this[offset + i] * mul;
        return val;
      };
      Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
        offset |= 0;
        byteLength |= 0;
        noAssert || checkOffset(offset, byteLength, this.length);
        var val = this[offset + --byteLength];
        var mul = 1;
        while (byteLength > 0 && (mul *= 256)) val += this[offset + --byteLength] * mul;
        return val;
      };
      Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
        noAssert || checkOffset(offset, 1, this.length);
        return this[offset];
      };
      Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
        noAssert || checkOffset(offset, 2, this.length);
        return this[offset] | this[offset + 1] << 8;
      };
      Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
        noAssert || checkOffset(offset, 2, this.length);
        return this[offset] << 8 | this[offset + 1];
      };
      Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
        noAssert || checkOffset(offset, 4, this.length);
        return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + 16777216 * this[offset + 3];
      };
      Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
        noAssert || checkOffset(offset, 4, this.length);
        return 16777216 * this[offset] + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
      };
      Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
        offset |= 0;
        byteLength |= 0;
        noAssert || checkOffset(offset, byteLength, this.length);
        var val = this[offset];
        var mul = 1;
        var i = 0;
        while (++i < byteLength && (mul *= 256)) val += this[offset + i] * mul;
        mul *= 128;
        val >= mul && (val -= Math.pow(2, 8 * byteLength));
        return val;
      };
      Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
        offset |= 0;
        byteLength |= 0;
        noAssert || checkOffset(offset, byteLength, this.length);
        var i = byteLength;
        var mul = 1;
        var val = this[offset + --i];
        while (i > 0 && (mul *= 256)) val += this[offset + --i] * mul;
        mul *= 128;
        val >= mul && (val -= Math.pow(2, 8 * byteLength));
        return val;
      };
      Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
        noAssert || checkOffset(offset, 1, this.length);
        if (!(128 & this[offset])) return this[offset];
        return -1 * (255 - this[offset] + 1);
      };
      Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
        noAssert || checkOffset(offset, 2, this.length);
        var val = this[offset] | this[offset + 1] << 8;
        return 32768 & val ? 4294901760 | val : val;
      };
      Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
        noAssert || checkOffset(offset, 2, this.length);
        var val = this[offset + 1] | this[offset] << 8;
        return 32768 & val ? 4294901760 | val : val;
      };
      Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
        noAssert || checkOffset(offset, 4, this.length);
        return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
      };
      Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
        noAssert || checkOffset(offset, 4, this.length);
        return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
      };
      Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
        noAssert || checkOffset(offset, 4, this.length);
        return ieee754.read(this, offset, true, 23, 4);
      };
      Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
        noAssert || checkOffset(offset, 4, this.length);
        return ieee754.read(this, offset, false, 23, 4);
      };
      Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
        noAssert || checkOffset(offset, 8, this.length);
        return ieee754.read(this, offset, true, 52, 8);
      };
      Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
        noAssert || checkOffset(offset, 8, this.length);
        return ieee754.read(this, offset, false, 52, 8);
      };
      function checkInt(buf, value, offset, ext, max, min) {
        if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
        if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
        if (offset + ext > buf.length) throw new RangeError("Index out of range");
      }
      Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
        value = +value;
        offset |= 0;
        byteLength |= 0;
        if (!noAssert) {
          var maxBytes = Math.pow(2, 8 * byteLength) - 1;
          checkInt(this, value, offset, byteLength, maxBytes, 0);
        }
        var mul = 1;
        var i = 0;
        this[offset] = 255 & value;
        while (++i < byteLength && (mul *= 256)) this[offset + i] = value / mul & 255;
        return offset + byteLength;
      };
      Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
        value = +value;
        offset |= 0;
        byteLength |= 0;
        if (!noAssert) {
          var maxBytes = Math.pow(2, 8 * byteLength) - 1;
          checkInt(this, value, offset, byteLength, maxBytes, 0);
        }
        var i = byteLength - 1;
        var mul = 1;
        this[offset + i] = 255 & value;
        while (--i >= 0 && (mul *= 256)) this[offset + i] = value / mul & 255;
        return offset + byteLength;
      };
      Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 1, 255, 0);
        Buffer.TYPED_ARRAY_SUPPORT || (value = Math.floor(value));
        this[offset] = 255 & value;
        return offset + 1;
      };
      function objectWriteUInt16(buf, value, offset, littleEndian) {
        value < 0 && (value = 65535 + value + 1);
        for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) buf[offset + i] = (value & 255 << 8 * (littleEndian ? i : 1 - i)) >>> 8 * (littleEndian ? i : 1 - i);
      }
      Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 2, 65535, 0);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset] = 255 & value;
          this[offset + 1] = value >>> 8;
        } else objectWriteUInt16(this, value, offset, true);
        return offset + 2;
      };
      Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 2, 65535, 0);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset] = value >>> 8;
          this[offset + 1] = 255 & value;
        } else objectWriteUInt16(this, value, offset, false);
        return offset + 2;
      };
      function objectWriteUInt32(buf, value, offset, littleEndian) {
        value < 0 && (value = 4294967295 + value + 1);
        for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) buf[offset + i] = value >>> 8 * (littleEndian ? i : 3 - i) & 255;
      }
      Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 4, 4294967295, 0);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset + 3] = value >>> 24;
          this[offset + 2] = value >>> 16;
          this[offset + 1] = value >>> 8;
          this[offset] = 255 & value;
        } else objectWriteUInt32(this, value, offset, true);
        return offset + 4;
      };
      Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 4, 4294967295, 0);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset] = value >>> 24;
          this[offset + 1] = value >>> 16;
          this[offset + 2] = value >>> 8;
          this[offset + 3] = 255 & value;
        } else objectWriteUInt32(this, value, offset, false);
        return offset + 4;
      };
      Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
        value = +value;
        offset |= 0;
        if (!noAssert) {
          var limit = Math.pow(2, 8 * byteLength - 1);
          checkInt(this, value, offset, byteLength, limit - 1, -limit);
        }
        var i = 0;
        var mul = 1;
        var sub = 0;
        this[offset] = 255 & value;
        while (++i < byteLength && (mul *= 256)) {
          value < 0 && 0 === sub && 0 !== this[offset + i - 1] && (sub = 1);
          this[offset + i] = (value / mul >> 0) - sub & 255;
        }
        return offset + byteLength;
      };
      Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
        value = +value;
        offset |= 0;
        if (!noAssert) {
          var limit = Math.pow(2, 8 * byteLength - 1);
          checkInt(this, value, offset, byteLength, limit - 1, -limit);
        }
        var i = byteLength - 1;
        var mul = 1;
        var sub = 0;
        this[offset + i] = 255 & value;
        while (--i >= 0 && (mul *= 256)) {
          value < 0 && 0 === sub && 0 !== this[offset + i + 1] && (sub = 1);
          this[offset + i] = (value / mul >> 0) - sub & 255;
        }
        return offset + byteLength;
      };
      Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 1, 127, -128);
        Buffer.TYPED_ARRAY_SUPPORT || (value = Math.floor(value));
        value < 0 && (value = 255 + value + 1);
        this[offset] = 255 & value;
        return offset + 1;
      };
      Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 2, 32767, -32768);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset] = 255 & value;
          this[offset + 1] = value >>> 8;
        } else objectWriteUInt16(this, value, offset, true);
        return offset + 2;
      };
      Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 2, 32767, -32768);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset] = value >>> 8;
          this[offset + 1] = 255 & value;
        } else objectWriteUInt16(this, value, offset, false);
        return offset + 2;
      };
      Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 4, 2147483647, -2147483648);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset] = 255 & value;
          this[offset + 1] = value >>> 8;
          this[offset + 2] = value >>> 16;
          this[offset + 3] = value >>> 24;
        } else objectWriteUInt32(this, value, offset, true);
        return offset + 4;
      };
      Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
        value = +value;
        offset |= 0;
        noAssert || checkInt(this, value, offset, 4, 2147483647, -2147483648);
        value < 0 && (value = 4294967295 + value + 1);
        if (Buffer.TYPED_ARRAY_SUPPORT) {
          this[offset] = value >>> 24;
          this[offset + 1] = value >>> 16;
          this[offset + 2] = value >>> 8;
          this[offset + 3] = 255 & value;
        } else objectWriteUInt32(this, value, offset, false);
        return offset + 4;
      };
      function checkIEEE754(buf, value, offset, ext, max, min) {
        if (offset + ext > buf.length) throw new RangeError("Index out of range");
        if (offset < 0) throw new RangeError("Index out of range");
      }
      function writeFloat(buf, value, offset, littleEndian, noAssert) {
        noAssert || checkIEEE754(buf, value, offset, 4, 3.4028234663852886e38, -3.4028234663852886e38);
        ieee754.write(buf, value, offset, littleEndian, 23, 4);
        return offset + 4;
      }
      Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
        return writeFloat(this, value, offset, true, noAssert);
      };
      Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
        return writeFloat(this, value, offset, false, noAssert);
      };
      function writeDouble(buf, value, offset, littleEndian, noAssert) {
        noAssert || checkIEEE754(buf, value, offset, 8, 1.7976931348623157e308, -1.7976931348623157e308);
        ieee754.write(buf, value, offset, littleEndian, 52, 8);
        return offset + 8;
      }
      Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
        return writeDouble(this, value, offset, true, noAssert);
      };
      Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
        return writeDouble(this, value, offset, false, noAssert);
      };
      Buffer.prototype.copy = function copy(target, targetStart, start, end) {
        start || (start = 0);
        end || 0 === end || (end = this.length);
        targetStart >= target.length && (targetStart = target.length);
        targetStart || (targetStart = 0);
        end > 0 && end < start && (end = start);
        if (end === start) return 0;
        if (0 === target.length || 0 === this.length) return 0;
        if (targetStart < 0) throw new RangeError("targetStart out of bounds");
        if (start < 0 || start >= this.length) throw new RangeError("sourceStart out of bounds");
        if (end < 0) throw new RangeError("sourceEnd out of bounds");
        end > this.length && (end = this.length);
        target.length - targetStart < end - start && (end = target.length - targetStart + start);
        var len = end - start;
        var i;
        if (this === target && start < targetStart && targetStart < end) for (i = len - 1; i >= 0; --i) target[i + targetStart] = this[i + start]; else if (len < 1e3 || !Buffer.TYPED_ARRAY_SUPPORT) for (i = 0; i < len; ++i) target[i + targetStart] = this[i + start]; else Uint8Array.prototype.set.call(target, this.subarray(start, start + len), targetStart);
        return len;
      };
      Buffer.prototype.fill = function fill(val, start, end, encoding) {
        if ("string" === typeof val) {
          if ("string" === typeof start) {
            encoding = start;
            start = 0;
            end = this.length;
          } else if ("string" === typeof end) {
            encoding = end;
            end = this.length;
          }
          if (1 === val.length) {
            var code = val.charCodeAt(0);
            code < 256 && (val = code);
          }
          if (void 0 !== encoding && "string" !== typeof encoding) throw new TypeError("encoding must be a string");
          if ("string" === typeof encoding && !Buffer.isEncoding(encoding)) throw new TypeError("Unknown encoding: " + encoding);
        } else "number" === typeof val && (val &= 255);
        if (start < 0 || this.length < start || this.length < end) throw new RangeError("Out of range index");
        if (end <= start) return this;
        start >>>= 0;
        end = void 0 === end ? this.length : end >>> 0;
        val || (val = 0);
        var i;
        if ("number" === typeof val) for (i = start; i < end; ++i) this[i] = val; else {
          var bytes = Buffer.isBuffer(val) ? val : utf8ToBytes(new Buffer(val, encoding).toString());
          var len = bytes.length;
          for (i = 0; i < end - start; ++i) this[i + start] = bytes[i % len];
        }
        return this;
      };
      var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g;
      function base64clean(str) {
        str = stringtrim(str).replace(INVALID_BASE64_RE, "");
        if (str.length < 2) return "";
        while (str.length % 4 !== 0) str += "=";
        return str;
      }
      function stringtrim(str) {
        if (str.trim) return str.trim();
        return str.replace(/^\s+|\s+$/g, "");
      }
      function toHex(n) {
        if (n < 16) return "0" + n.toString(16);
        return n.toString(16);
      }
      function utf8ToBytes(string, units) {
        units = units || Infinity;
        var codePoint;
        var length = string.length;
        var leadSurrogate = null;
        var bytes = [];
        for (var i = 0; i < length; ++i) {
          codePoint = string.charCodeAt(i);
          if (codePoint > 55295 && codePoint < 57344) {
            if (!leadSurrogate) {
              if (codePoint > 56319) {
                (units -= 3) > -1 && bytes.push(239, 191, 189);
                continue;
              }
              if (i + 1 === length) {
                (units -= 3) > -1 && bytes.push(239, 191, 189);
                continue;
              }
              leadSurrogate = codePoint;
              continue;
            }
            if (codePoint < 56320) {
              (units -= 3) > -1 && bytes.push(239, 191, 189);
              leadSurrogate = codePoint;
              continue;
            }
            codePoint = 65536 + (leadSurrogate - 55296 << 10 | codePoint - 56320);
          } else leadSurrogate && (units -= 3) > -1 && bytes.push(239, 191, 189);
          leadSurrogate = null;
          if (codePoint < 128) {
            if ((units -= 1) < 0) break;
            bytes.push(codePoint);
          } else if (codePoint < 2048) {
            if ((units -= 2) < 0) break;
            bytes.push(codePoint >> 6 | 192, 63 & codePoint | 128);
          } else if (codePoint < 65536) {
            if ((units -= 3) < 0) break;
            bytes.push(codePoint >> 12 | 224, codePoint >> 6 & 63 | 128, 63 & codePoint | 128);
          } else {
            if (!(codePoint < 1114112)) throw new Error("Invalid code point");
            if ((units -= 4) < 0) break;
            bytes.push(codePoint >> 18 | 240, codePoint >> 12 & 63 | 128, codePoint >> 6 & 63 | 128, 63 & codePoint | 128);
          }
        }
        return bytes;
      }
      function asciiToBytes(str) {
        var byteArray = [];
        for (var i = 0; i < str.length; ++i) byteArray.push(255 & str.charCodeAt(i));
        return byteArray;
      }
      function utf16leToBytes(str, units) {
        var c, hi, lo;
        var byteArray = [];
        for (var i = 0; i < str.length; ++i) {
          if ((units -= 2) < 0) break;
          c = str.charCodeAt(i);
          hi = c >> 8;
          lo = c % 256;
          byteArray.push(lo);
          byteArray.push(hi);
        }
        return byteArray;
      }
      function base64ToBytes(str) {
        return base64.toByteArray(base64clean(str));
      }
      function blitBuffer(src, dst, offset, length) {
        for (var i = 0; i < length; ++i) {
          if (i + offset >= dst.length || i >= src.length) break;
          dst[i + offset] = src[i];
        }
        return i;
      }
      function isnan(val) {
        return val !== val;
      }
    }).call(this, "undefined" !== typeof global ? global : "undefined" !== typeof self ? self : "undefined" !== typeof window ? window : {});
  }, {
    "base64-js": 1,
    ieee754: 4,
    isarray: 3
  } ],
  3: [ function(require, module, exports) {
    var toString = {}.toString;
    module.exports = Array.isArray || function(arr) {
      return "[object Array]" == toString.call(arr);
    };
  }, {} ],
  4: [ function(require, module, exports) {
    exports.read = function(buffer, offset, isLE, mLen, nBytes) {
      var e, m;
      var eLen = 8 * nBytes - mLen - 1;
      var eMax = (1 << eLen) - 1;
      var eBias = eMax >> 1;
      var nBits = -7;
      var i = isLE ? nBytes - 1 : 0;
      var d = isLE ? -1 : 1;
      var s = buffer[offset + i];
      i += d;
      e = s & (1 << -nBits) - 1;
      s >>= -nBits;
      nBits += eLen;
      for (;nBits > 0; e = 256 * e + buffer[offset + i], i += d, nBits -= 8) ;
      m = e & (1 << -nBits) - 1;
      e >>= -nBits;
      nBits += mLen;
      for (;nBits > 0; m = 256 * m + buffer[offset + i], i += d, nBits -= 8) ;
      if (0 === e) e = 1 - eBias; else {
        if (e === eMax) return m ? NaN : Infinity * (s ? -1 : 1);
        m += Math.pow(2, mLen);
        e -= eBias;
      }
      return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
    };
    exports.write = function(buffer, value, offset, isLE, mLen, nBytes) {
      var e, m, c;
      var eLen = 8 * nBytes - mLen - 1;
      var eMax = (1 << eLen) - 1;
      var eBias = eMax >> 1;
      var rt = 23 === mLen ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
      var i = isLE ? 0 : nBytes - 1;
      var d = isLE ? 1 : -1;
      var s = value < 0 || 0 === value && 1 / value < 0 ? 1 : 0;
      value = Math.abs(value);
      if (isNaN(value) || Infinity === value) {
        m = isNaN(value) ? 1 : 0;
        e = eMax;
      } else {
        e = Math.floor(Math.log(value) / Math.LN2);
        if (value * (c = Math.pow(2, -e)) < 1) {
          e--;
          c *= 2;
        }
        value += e + eBias >= 1 ? rt / c : rt * Math.pow(2, 1 - eBias);
        if (value * c >= 2) {
          e++;
          c /= 2;
        }
        if (e + eBias >= eMax) {
          m = 0;
          e = eMax;
        } else if (e + eBias >= 1) {
          m = (value * c - 1) * Math.pow(2, mLen);
          e += eBias;
        } else {
          m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
          e = 0;
        }
      }
      for (;mLen >= 8; buffer[offset + i] = 255 & m, i += d, m /= 256, mLen -= 8) ;
      e = e << mLen | m;
      eLen += mLen;
      for (;eLen > 0; buffer[offset + i] = 255 & e, i += d, e /= 256, eLen -= 8) ;
      buffer[offset + i - d] |= 128 * s;
    };
  }, {} ],
  5: [ function(require, module, exports) {
    function EventLite() {
      if (!(this instanceof EventLite)) return new EventLite();
    }
    (function(EventLite) {
      "undefined" !== typeof module && (module.exports = EventLite);
      var LISTENERS = "listeners";
      var methods = {
        on: on,
        once: once,
        off: off,
        emit: emit
      };
      mixin(EventLite.prototype);
      EventLite.mixin = mixin;
      function mixin(target) {
        for (var key in methods) target[key] = methods[key];
        return target;
      }
      function on(type, func) {
        getListeners(this, type).push(func);
        return this;
      }
      function once(type, func) {
        var that = this;
        wrap.originalListener = func;
        getListeners(that, type).push(wrap);
        return that;
        function wrap() {
          off.call(that, type, wrap);
          func.apply(this, arguments);
        }
      }
      function off(type, func) {
        var that = this;
        var listners;
        if (arguments.length) if (func) {
          listners = getListeners(that, type, true);
          if (listners) {
            listners = listners.filter(ne);
            if (!listners.length) return off.call(that, type);
            that[LISTENERS][type] = listners;
          }
        } else {
          listners = that[LISTENERS];
          if (listners) {
            delete listners[type];
            if (!Object.keys(listners).length) return off.call(that);
          }
        } else delete that[LISTENERS];
        return that;
        function ne(test) {
          return test !== func && test.originalListener !== func;
        }
      }
      function emit(type, value) {
        var that = this;
        var listeners = getListeners(that, type, true);
        if (!listeners) return false;
        var arglen = arguments.length;
        if (1 === arglen) listeners.forEach(zeroarg); else if (2 === arglen) listeners.forEach(onearg); else {
          var args = Array.prototype.slice.call(arguments, 1);
          listeners.forEach(moreargs);
        }
        return !!listeners.length;
        function zeroarg(func) {
          func.call(that);
        }
        function onearg(func) {
          func.call(that, value);
        }
        function moreargs(func) {
          func.apply(that, args);
        }
      }
      function getListeners(that, type, readonly) {
        if (readonly && !that[LISTENERS]) return;
        var listeners = that[LISTENERS] || (that[LISTENERS] = {});
        return listeners[type] || (listeners[type] = []);
      }
    })(EventLite);
  }, {} ],
  6: [ function(require, module, exports) {
    arguments[4][4][0].apply(exports, arguments);
  }, {
    dup: 4
  } ],
  7: [ function(require, module, exports) {
    (function(Buffer) {
      var Uint64BE, Int64BE, Uint64LE, Int64LE;
      !function(exports) {
        var UNDEFINED = "undefined";
        var BUFFER = UNDEFINED !== typeof Buffer && Buffer;
        var UINT8ARRAY = UNDEFINED !== typeof Uint8Array && Uint8Array;
        var ARRAYBUFFER = UNDEFINED !== typeof ArrayBuffer && ArrayBuffer;
        var ZERO = [ 0, 0, 0, 0, 0, 0, 0, 0 ];
        var isArray = Array.isArray || _isArray;
        var BIT32 = 4294967296;
        var BIT24 = 16777216;
        var storage;
        Uint64BE = factory("Uint64BE", true, true);
        Int64BE = factory("Int64BE", true, false);
        Uint64LE = factory("Uint64LE", false, true);
        Int64LE = factory("Int64LE", false, false);
        function factory(name, bigendian, unsigned) {
          var posH = bigendian ? 0 : 4;
          var posL = bigendian ? 4 : 0;
          var pos0 = bigendian ? 0 : 3;
          var pos1 = bigendian ? 1 : 2;
          var pos2 = bigendian ? 2 : 1;
          var pos3 = bigendian ? 3 : 0;
          var fromPositive = bigendian ? fromPositiveBE : fromPositiveLE;
          var fromNegative = bigendian ? fromNegativeBE : fromNegativeLE;
          var proto = Int64.prototype;
          var isName = "is" + name;
          var _isInt64 = "_" + isName;
          proto.buffer = void 0;
          proto.offset = 0;
          proto[_isInt64] = true;
          proto.toNumber = toNumber;
          proto.toString = toString;
          proto.toJSON = toNumber;
          proto.toArray = toArray;
          BUFFER && (proto.toBuffer = toBuffer);
          UINT8ARRAY && (proto.toArrayBuffer = toArrayBuffer);
          Int64[isName] = isInt64;
          exports[name] = Int64;
          return Int64;
          function Int64(buffer, offset, value, raddix) {
            if (!(this instanceof Int64)) return new Int64(buffer, offset, value, raddix);
            return init(this, buffer, offset, value, raddix);
          }
          function isInt64(b) {
            return !!(b && b[_isInt64]);
          }
          function init(that, buffer, offset, value, raddix) {
            if (UINT8ARRAY && ARRAYBUFFER) {
              buffer instanceof ARRAYBUFFER && (buffer = new UINT8ARRAY(buffer));
              value instanceof ARRAYBUFFER && (value = new UINT8ARRAY(value));
            }
            if (!buffer && !offset && !value && !storage) {
              that.buffer = newArray(ZERO, 0);
              return;
            }
            if (!isValidBuffer(buffer, offset)) {
              var _storage = storage || Array;
              raddix = offset;
              value = buffer;
              offset = 0;
              buffer = new _storage(8);
            }
            that.buffer = buffer;
            that.offset = offset |= 0;
            if (UNDEFINED === typeof value) return;
            if ("string" === typeof value) fromString(buffer, offset, value, raddix || 10); else if (isValidBuffer(value, raddix)) fromArray(buffer, offset, value, raddix); else if ("number" === typeof raddix) {
              writeInt32(buffer, offset + posH, value);
              writeInt32(buffer, offset + posL, raddix);
            } else value > 0 ? fromPositive(buffer, offset, value) : value < 0 ? fromNegative(buffer, offset, value) : fromArray(buffer, offset, ZERO, 0);
          }
          function fromString(buffer, offset, str, raddix) {
            var pos = 0;
            var len = str.length;
            var high = 0;
            var low = 0;
            "-" === str[0] && pos++;
            var sign = pos;
            while (pos < len) {
              var chr = parseInt(str[pos++], raddix);
              if (!(chr >= 0)) break;
              low = low * raddix + chr;
              high = high * raddix + Math.floor(low / BIT32);
              low %= BIT32;
            }
            if (sign) {
              high = ~high;
              low ? low = BIT32 - low : high++;
            }
            writeInt32(buffer, offset + posH, high);
            writeInt32(buffer, offset + posL, low);
          }
          function toNumber() {
            var buffer = this.buffer;
            var offset = this.offset;
            var high = readInt32(buffer, offset + posH);
            var low = readInt32(buffer, offset + posL);
            unsigned || (high |= 0);
            return high ? high * BIT32 + low : low;
          }
          function toString(radix) {
            var buffer = this.buffer;
            var offset = this.offset;
            var high = readInt32(buffer, offset + posH);
            var low = readInt32(buffer, offset + posL);
            var str = "";
            var sign = !unsigned && 2147483648 & high;
            if (sign) {
              high = ~high;
              low = BIT32 - low;
            }
            radix = radix || 10;
            while (1) {
              var mod = high % radix * BIT32 + low;
              high = Math.floor(high / radix);
              low = Math.floor(mod / radix);
              str = (mod % radix).toString(radix) + str;
              if (!high && !low) break;
            }
            sign && (str = "-" + str);
            return str;
          }
          function writeInt32(buffer, offset, value) {
            buffer[offset + pos3] = 255 & value;
            value >>= 8;
            buffer[offset + pos2] = 255 & value;
            value >>= 8;
            buffer[offset + pos1] = 255 & value;
            value >>= 8;
            buffer[offset + pos0] = 255 & value;
          }
          function readInt32(buffer, offset) {
            return buffer[offset + pos0] * BIT24 + (buffer[offset + pos1] << 16) + (buffer[offset + pos2] << 8) + buffer[offset + pos3];
          }
        }
        function toArray(raw) {
          var buffer = this.buffer;
          var offset = this.offset;
          storage = null;
          if (false !== raw && 0 === offset && 8 === buffer.length && isArray(buffer)) return buffer;
          return newArray(buffer, offset);
        }
        function toBuffer(raw) {
          var buffer = this.buffer;
          var offset = this.offset;
          storage = BUFFER;
          if (false !== raw && 0 === offset && 8 === buffer.length && Buffer.isBuffer(buffer)) return buffer;
          var dest = new BUFFER(8);
          fromArray(dest, 0, buffer, offset);
          return dest;
        }
        function toArrayBuffer(raw) {
          var buffer = this.buffer;
          var offset = this.offset;
          var arrbuf = buffer.buffer;
          storage = UINT8ARRAY;
          if (false !== raw && 0 === offset && arrbuf instanceof ARRAYBUFFER && 8 === arrbuf.byteLength) return arrbuf;
          var dest = new UINT8ARRAY(8);
          fromArray(dest, 0, buffer, offset);
          return dest.buffer;
        }
        function isValidBuffer(buffer, offset) {
          var len = buffer && buffer.length;
          offset |= 0;
          return len && offset + 8 <= len && "string" !== typeof buffer[offset];
        }
        function fromArray(destbuf, destoff, srcbuf, srcoff) {
          destoff |= 0;
          srcoff |= 0;
          for (var i = 0; i < 8; i++) destbuf[destoff++] = 255 & srcbuf[srcoff++];
        }
        function newArray(buffer, offset) {
          return Array.prototype.slice.call(buffer, offset, offset + 8);
        }
        function fromPositiveBE(buffer, offset, value) {
          var pos = offset + 8;
          while (pos > offset) {
            buffer[--pos] = 255 & value;
            value /= 256;
          }
        }
        function fromNegativeBE(buffer, offset, value) {
          var pos = offset + 8;
          value++;
          while (pos > offset) {
            buffer[--pos] = 255 & -value ^ 255;
            value /= 256;
          }
        }
        function fromPositiveLE(buffer, offset, value) {
          var end = offset + 8;
          while (offset < end) {
            buffer[offset++] = 255 & value;
            value /= 256;
          }
        }
        function fromNegativeLE(buffer, offset, value) {
          var end = offset + 8;
          value++;
          while (offset < end) {
            buffer[offset++] = 255 & -value ^ 255;
            value /= 256;
          }
        }
        function _isArray(val) {
          return !!val && "[object Array]" == Object.prototype.toString.call(val);
        }
      }("object" === typeof exports && "string" !== typeof exports.nodeName ? exports : this || {});
    }).call(this, require("buffer").Buffer);
  }, {
    buffer: 2
  } ],
  8: [ function(require, module, exports) {
    arguments[4][3][0].apply(exports, arguments);
  }, {
    dup: 3
  } ],
  9: [ function(require, module, exports) {
    exports.encode = require("./encode").encode;
    exports.decode = require("./decode").decode;
    exports.Encoder = require("./encoder").Encoder;
    exports.Decoder = require("./decoder").Decoder;
    exports.createCodec = require("./ext").createCodec;
    exports.codec = require("./codec").codec;
  }, {
    "./codec": 18,
    "./decode": 20,
    "./decoder": 21,
    "./encode": 23,
    "./encoder": 24,
    "./ext": 28
  } ],
  10: [ function(require, module, exports) {
    (function(Buffer) {
      module.exports = c("undefined" !== typeof Buffer && Buffer) || c(this.Buffer) || c("undefined" !== typeof window && window.Buffer) || this.Buffer;
      function c(B) {
        return B && B.isBuffer && B;
      }
    }).call(this, require("buffer").Buffer);
  }, {
    buffer: 2
  } ],
  11: [ function(require, module, exports) {
    var MAXBUFLEN = 8192;
    exports.copy = copy;
    exports.toString = toString;
    exports.write = write;
    function write(string, offset) {
      var buffer = this;
      var index = offset || (offset |= 0);
      var length = string.length;
      var chr = 0;
      var i = 0;
      while (i < length) {
        chr = string.charCodeAt(i++);
        if (chr < 128) buffer[index++] = chr; else if (chr < 2048) {
          buffer[index++] = 192 | chr >>> 6;
          buffer[index++] = 128 | 63 & chr;
        } else if (chr < 55296 || chr > 57343) {
          buffer[index++] = 224 | chr >>> 12;
          buffer[index++] = 128 | chr >>> 6 & 63;
          buffer[index++] = 128 | 63 & chr;
        } else {
          chr = 65536 + (chr - 55296 << 10 | string.charCodeAt(i++) - 56320);
          buffer[index++] = 240 | chr >>> 18;
          buffer[index++] = 128 | chr >>> 12 & 63;
          buffer[index++] = 128 | chr >>> 6 & 63;
          buffer[index++] = 128 | 63 & chr;
        }
      }
      return index - offset;
    }
    function toString(encoding, start, end) {
      var buffer = this;
      var index = 0 | start;
      end || (end = buffer.length);
      var string = "";
      var chr = 0;
      while (index < end) {
        chr = buffer[index++];
        if (chr < 128) {
          string += String.fromCharCode(chr);
          continue;
        }
        192 === (224 & chr) ? chr = (31 & chr) << 6 | 63 & buffer[index++] : 224 === (240 & chr) ? chr = (15 & chr) << 12 | (63 & buffer[index++]) << 6 | 63 & buffer[index++] : 240 === (248 & chr) && (chr = (7 & chr) << 18 | (63 & buffer[index++]) << 12 | (63 & buffer[index++]) << 6 | 63 & buffer[index++]);
        if (chr >= 65536) {
          chr -= 65536;
          string += String.fromCharCode(55296 + (chr >>> 10), 56320 + (1023 & chr));
        } else string += String.fromCharCode(chr);
      }
      return string;
    }
    function copy(target, targetStart, start, end) {
      var i;
      start || (start = 0);
      end || 0 === end || (end = this.length);
      targetStart || (targetStart = 0);
      var len = end - start;
      if (target === this && start < targetStart && targetStart < end) for (i = len - 1; i >= 0; i--) target[i + targetStart] = this[i + start]; else for (i = 0; i < len; i++) target[i + targetStart] = this[i + start];
      return len;
    }
  }, {} ],
  12: [ function(require, module, exports) {
    var Bufferish = require("./bufferish");
    var exports = module.exports = alloc(0);
    exports.alloc = alloc;
    exports.concat = Bufferish.concat;
    exports.from = from;
    function alloc(size) {
      return new Array(size);
    }
    function from(value) {
      if (!Bufferish.isBuffer(value) && Bufferish.isView(value)) value = Bufferish.Uint8Array.from(value); else if (Bufferish.isArrayBuffer(value)) value = new Uint8Array(value); else {
        if ("string" === typeof value) return Bufferish.from.call(exports, value);
        if ("number" === typeof value) throw new TypeError('"value" argument must not be a number');
      }
      return Array.prototype.slice.call(value);
    }
  }, {
    "./bufferish": 16
  } ],
  13: [ function(require, module, exports) {
    var Bufferish = require("./bufferish");
    var Buffer = Bufferish.global;
    var exports = module.exports = Bufferish.hasBuffer ? alloc(0) : [];
    exports.alloc = Bufferish.hasBuffer && Buffer.alloc || alloc;
    exports.concat = Bufferish.concat;
    exports.from = from;
    function alloc(size) {
      return new Buffer(size);
    }
    function from(value) {
      if (!Bufferish.isBuffer(value) && Bufferish.isView(value)) value = Bufferish.Uint8Array.from(value); else if (Bufferish.isArrayBuffer(value)) value = new Uint8Array(value); else {
        if ("string" === typeof value) return Bufferish.from.call(exports, value);
        if ("number" === typeof value) throw new TypeError('"value" argument must not be a number');
      }
      return Buffer.from && 1 !== Buffer.from.length ? Buffer.from(value) : new Buffer(value);
    }
  }, {
    "./bufferish": 16
  } ],
  14: [ function(require, module, exports) {
    var BufferLite = require("./buffer-lite");
    exports.copy = copy;
    exports.slice = slice;
    exports.toString = toString;
    exports.write = gen("write");
    var Bufferish = require("./bufferish");
    var Buffer = Bufferish.global;
    var isBufferShim = Bufferish.hasBuffer && "TYPED_ARRAY_SUPPORT" in Buffer;
    var brokenTypedArray = isBufferShim && !Buffer.TYPED_ARRAY_SUPPORT;
    function copy(target, targetStart, start, end) {
      var thisIsBuffer = Bufferish.isBuffer(this);
      var targetIsBuffer = Bufferish.isBuffer(target);
      if (thisIsBuffer && targetIsBuffer) return this.copy(target, targetStart, start, end);
      if (brokenTypedArray || thisIsBuffer || targetIsBuffer || !Bufferish.isView(this) || !Bufferish.isView(target)) return BufferLite.copy.call(this, target, targetStart, start, end);
      var buffer = start || null != end ? slice.call(this, start, end) : this;
      target.set(buffer, targetStart);
      return buffer.length;
    }
    function slice(start, end) {
      var f = this.slice || !brokenTypedArray && this.subarray;
      if (f) return f.call(this, start, end);
      var target = Bufferish.alloc.call(this, end - start);
      copy.call(this, target, 0, start, end);
      return target;
    }
    function toString(encoding, start, end) {
      var f = !isBufferShim && Bufferish.isBuffer(this) ? this.toString : BufferLite.toString;
      return f.apply(this, arguments);
    }
    function gen(method) {
      return wrap;
      function wrap() {
        var f = this[method] || BufferLite[method];
        return f.apply(this, arguments);
      }
    }
  }, {
    "./buffer-lite": 11,
    "./bufferish": 16
  } ],
  15: [ function(require, module, exports) {
    var Bufferish = require("./bufferish");
    var exports = module.exports = Bufferish.hasArrayBuffer ? alloc(0) : [];
    exports.alloc = alloc;
    exports.concat = Bufferish.concat;
    exports.from = from;
    function alloc(size) {
      return new Uint8Array(size);
    }
    function from(value) {
      if (Bufferish.isView(value)) {
        var byteOffset = value.byteOffset;
        var byteLength = value.byteLength;
        value = value.buffer;
        if (value.byteLength !== byteLength) if (value.slice) value = value.slice(byteOffset, byteOffset + byteLength); else {
          value = new Uint8Array(value);
          value.byteLength !== byteLength && (value = Array.prototype.slice.call(value, byteOffset, byteOffset + byteLength));
        }
      } else {
        if ("string" === typeof value) return Bufferish.from.call(exports, value);
        if ("number" === typeof value) throw new TypeError('"value" argument must not be a number');
      }
      return new Uint8Array(value);
    }
  }, {
    "./bufferish": 16
  } ],
  16: [ function(require, module, exports) {
    var Buffer = exports.global = require("./buffer-global");
    var hasBuffer = exports.hasBuffer = Buffer && !!Buffer.isBuffer;
    var hasArrayBuffer = exports.hasArrayBuffer = "undefined" !== typeof ArrayBuffer;
    var isArray = exports.isArray = require("isarray");
    exports.isArrayBuffer = hasArrayBuffer ? isArrayBuffer : _false;
    var isBuffer = exports.isBuffer = hasBuffer ? Buffer.isBuffer : _false;
    var isView = exports.isView = hasArrayBuffer ? ArrayBuffer.isView || _is("ArrayBuffer", "buffer") : _false;
    exports.alloc = alloc;
    exports.concat = concat;
    exports.from = from;
    var BufferArray = exports.Array = require("./bufferish-array");
    var BufferBuffer = exports.Buffer = require("./bufferish-buffer");
    var BufferUint8Array = exports.Uint8Array = require("./bufferish-uint8array");
    var BufferProto = exports.prototype = require("./bufferish-proto");
    function from(value) {
      return "string" === typeof value ? fromString.call(this, value) : auto(this).from(value);
    }
    function alloc(size) {
      return auto(this).alloc(size);
    }
    function concat(list, length) {
      if (!length) {
        length = 0;
        Array.prototype.forEach.call(list, dryrun);
      }
      var ref = this !== exports && this || list[0];
      var result = alloc.call(ref, length);
      var offset = 0;
      Array.prototype.forEach.call(list, append);
      return result;
      function dryrun(buffer) {
        length += buffer.length;
      }
      function append(buffer) {
        offset += BufferProto.copy.call(buffer, result, offset);
      }
    }
    var _isArrayBuffer = _is("ArrayBuffer");
    function isArrayBuffer(value) {
      return value instanceof ArrayBuffer || _isArrayBuffer(value);
    }
    function fromString(value) {
      var expected = 3 * value.length;
      var that = alloc.call(this, expected);
      var actual = BufferProto.write.call(that, value);
      expected !== actual && (that = BufferProto.slice.call(that, 0, actual));
      return that;
    }
    function auto(that) {
      return isBuffer(that) ? BufferBuffer : isView(that) ? BufferUint8Array : isArray(that) ? BufferArray : hasBuffer ? BufferBuffer : hasArrayBuffer ? BufferUint8Array : BufferArray;
    }
    function _false() {
      return false;
    }
    function _is(name, key) {
      name = "[object " + name + "]";
      return function(value) {
        return null != value && {}.toString.call(key ? value[key] : value) === name;
      };
    }
  }, {
    "./buffer-global": 10,
    "./bufferish-array": 12,
    "./bufferish-buffer": 13,
    "./bufferish-proto": 14,
    "./bufferish-uint8array": 15,
    isarray: 8
  } ],
  17: [ function(require, module, exports) {
    var IS_ARRAY = require("isarray");
    exports.createCodec = createCodec;
    exports.install = install;
    exports.filter = filter;
    var Bufferish = require("./bufferish");
    function Codec(options) {
      if (!(this instanceof Codec)) return new Codec(options);
      this.options = options;
      this.init();
    }
    Codec.prototype.init = function() {
      var options = this.options;
      options && options.uint8array && (this.bufferish = Bufferish.Uint8Array);
      return this;
    };
    function install(props) {
      for (var key in props) Codec.prototype[key] = add(Codec.prototype[key], props[key]);
    }
    function add(a, b) {
      return a && b ? ab : a || b;
      function ab() {
        a.apply(this, arguments);
        return b.apply(this, arguments);
      }
    }
    function join(filters) {
      filters = filters.slice();
      return function(value) {
        return filters.reduce(iterator, value);
      };
      function iterator(value, filter) {
        return filter(value);
      }
    }
    function filter(filter) {
      return IS_ARRAY(filter) ? join(filter) : filter;
    }
    function createCodec(options) {
      return new Codec(options);
    }
    exports.preset = createCodec({
      preset: true
    });
  }, {
    "./bufferish": 16,
    isarray: 8
  } ],
  18: [ function(require, module, exports) {
    require("./read-core");
    require("./write-core");
    exports.codec = {
      preset: require("./codec-base").preset
    };
  }, {
    "./codec-base": 17,
    "./read-core": 30,
    "./write-core": 33
  } ],
  19: [ function(require, module, exports) {
    exports.DecodeBuffer = DecodeBuffer;
    var preset = require("./read-core").preset;
    var FlexDecoder = require("./flex-buffer").FlexDecoder;
    FlexDecoder.mixin(DecodeBuffer.prototype);
    function DecodeBuffer(options) {
      if (!(this instanceof DecodeBuffer)) return new DecodeBuffer(options);
      if (options) {
        this.options = options;
        if (options.codec) {
          var codec = this.codec = options.codec;
          codec.bufferish && (this.bufferish = codec.bufferish);
        }
      }
    }
    DecodeBuffer.prototype.codec = preset;
    DecodeBuffer.prototype.fetch = function() {
      return this.codec.decode(this);
    };
  }, {
    "./flex-buffer": 29,
    "./read-core": 30
  } ],
  20: [ function(require, module, exports) {
    exports.decode = decode;
    var DecodeBuffer = require("./decode-buffer").DecodeBuffer;
    function decode(input, options) {
      var decoder = new DecodeBuffer(options);
      decoder.write(input);
      return decoder.read();
    }
  }, {
    "./decode-buffer": 19
  } ],
  21: [ function(require, module, exports) {
    exports.Decoder = Decoder;
    var EventLite = require("event-lite");
    var DecodeBuffer = require("./decode-buffer").DecodeBuffer;
    function Decoder(options) {
      if (!(this instanceof Decoder)) return new Decoder(options);
      DecodeBuffer.call(this, options);
    }
    Decoder.prototype = new DecodeBuffer();
    EventLite.mixin(Decoder.prototype);
    Decoder.prototype.decode = function(chunk) {
      arguments.length && this.write(chunk);
      this.flush();
    };
    Decoder.prototype.push = function(chunk) {
      this.emit("data", chunk);
    };
    Decoder.prototype.end = function(chunk) {
      this.decode(chunk);
      this.emit("end");
    };
  }, {
    "./decode-buffer": 19,
    "event-lite": 5
  } ],
  22: [ function(require, module, exports) {
    exports.EncodeBuffer = EncodeBuffer;
    var preset = require("./write-core").preset;
    var FlexEncoder = require("./flex-buffer").FlexEncoder;
    FlexEncoder.mixin(EncodeBuffer.prototype);
    function EncodeBuffer(options) {
      if (!(this instanceof EncodeBuffer)) return new EncodeBuffer(options);
      if (options) {
        this.options = options;
        if (options.codec) {
          var codec = this.codec = options.codec;
          codec.bufferish && (this.bufferish = codec.bufferish);
        }
      }
    }
    EncodeBuffer.prototype.codec = preset;
    EncodeBuffer.prototype.write = function(input) {
      this.codec.encode(this, input);
    };
  }, {
    "./flex-buffer": 29,
    "./write-core": 33
  } ],
  23: [ function(require, module, exports) {
    exports.encode = encode;
    var EncodeBuffer = require("./encode-buffer").EncodeBuffer;
    function encode(input, options) {
      var encoder = new EncodeBuffer(options);
      encoder.write(input);
      return encoder.read();
    }
  }, {
    "./encode-buffer": 22
  } ],
  24: [ function(require, module, exports) {
    exports.Encoder = Encoder;
    var EventLite = require("event-lite");
    var EncodeBuffer = require("./encode-buffer").EncodeBuffer;
    function Encoder(options) {
      if (!(this instanceof Encoder)) return new Encoder(options);
      EncodeBuffer.call(this, options);
    }
    Encoder.prototype = new EncodeBuffer();
    EventLite.mixin(Encoder.prototype);
    Encoder.prototype.encode = function(chunk) {
      this.write(chunk);
      this.emit("data", this.read());
    };
    Encoder.prototype.end = function(chunk) {
      arguments.length && this.encode(chunk);
      this.flush();
      this.emit("end");
    };
  }, {
    "./encode-buffer": 22,
    "event-lite": 5
  } ],
  25: [ function(require, module, exports) {
    exports.ExtBuffer = ExtBuffer;
    var Bufferish = require("./bufferish");
    function ExtBuffer(buffer, type) {
      if (!(this instanceof ExtBuffer)) return new ExtBuffer(buffer, type);
      this.buffer = Bufferish.from(buffer);
      this.type = type;
    }
  }, {
    "./bufferish": 16
  } ],
  26: [ function(require, module, exports) {
    exports.setExtPackers = setExtPackers;
    var Bufferish = require("./bufferish");
    var Buffer = Bufferish.global;
    var packTypedArray = Bufferish.Uint8Array.from;
    var _encode;
    var ERROR_COLUMNS = {
      name: 1,
      message: 1,
      stack: 1,
      columnNumber: 1,
      fileName: 1,
      lineNumber: 1
    };
    function setExtPackers(codec) {
      codec.addExtPacker(14, Error, [ packError, encode ]);
      codec.addExtPacker(1, EvalError, [ packError, encode ]);
      codec.addExtPacker(2, RangeError, [ packError, encode ]);
      codec.addExtPacker(3, ReferenceError, [ packError, encode ]);
      codec.addExtPacker(4, SyntaxError, [ packError, encode ]);
      codec.addExtPacker(5, TypeError, [ packError, encode ]);
      codec.addExtPacker(6, URIError, [ packError, encode ]);
      codec.addExtPacker(10, RegExp, [ packRegExp, encode ]);
      codec.addExtPacker(11, Boolean, [ packValueOf, encode ]);
      codec.addExtPacker(12, String, [ packValueOf, encode ]);
      codec.addExtPacker(13, Date, [ Number, encode ]);
      codec.addExtPacker(15, Number, [ packValueOf, encode ]);
      if ("undefined" !== typeof Uint8Array) {
        codec.addExtPacker(17, Int8Array, packTypedArray);
        codec.addExtPacker(18, Uint8Array, packTypedArray);
        codec.addExtPacker(19, Int16Array, packTypedArray);
        codec.addExtPacker(20, Uint16Array, packTypedArray);
        codec.addExtPacker(21, Int32Array, packTypedArray);
        codec.addExtPacker(22, Uint32Array, packTypedArray);
        codec.addExtPacker(23, Float32Array, packTypedArray);
        "undefined" !== typeof Float64Array && codec.addExtPacker(24, Float64Array, packTypedArray);
        "undefined" !== typeof Uint8ClampedArray && codec.addExtPacker(25, Uint8ClampedArray, packTypedArray);
        codec.addExtPacker(26, ArrayBuffer, packTypedArray);
        codec.addExtPacker(29, DataView, packTypedArray);
      }
      Bufferish.hasBuffer && codec.addExtPacker(27, Buffer, Bufferish.from);
    }
    function encode(input) {
      _encode || (_encode = require("./encode").encode);
      return _encode(input);
    }
    function packValueOf(value) {
      return value.valueOf();
    }
    function packRegExp(value) {
      value = RegExp.prototype.toString.call(value).split("/");
      value.shift();
      var out = [ value.pop() ];
      out.unshift(value.join("/"));
      return out;
    }
    function packError(value) {
      var out = {};
      for (var key in ERROR_COLUMNS) out[key] = value[key];
      return out;
    }
  }, {
    "./bufferish": 16,
    "./encode": 23
  } ],
  27: [ function(require, module, exports) {
    exports.setExtUnpackers = setExtUnpackers;
    var Bufferish = require("./bufferish");
    var Buffer = Bufferish.global;
    var _decode;
    var ERROR_COLUMNS = {
      name: 1,
      message: 1,
      stack: 1,
      columnNumber: 1,
      fileName: 1,
      lineNumber: 1
    };
    function setExtUnpackers(codec) {
      codec.addExtUnpacker(14, [ decode, unpackError(Error) ]);
      codec.addExtUnpacker(1, [ decode, unpackError(EvalError) ]);
      codec.addExtUnpacker(2, [ decode, unpackError(RangeError) ]);
      codec.addExtUnpacker(3, [ decode, unpackError(ReferenceError) ]);
      codec.addExtUnpacker(4, [ decode, unpackError(SyntaxError) ]);
      codec.addExtUnpacker(5, [ decode, unpackError(TypeError) ]);
      codec.addExtUnpacker(6, [ decode, unpackError(URIError) ]);
      codec.addExtUnpacker(10, [ decode, unpackRegExp ]);
      codec.addExtUnpacker(11, [ decode, unpackClass(Boolean) ]);
      codec.addExtUnpacker(12, [ decode, unpackClass(String) ]);
      codec.addExtUnpacker(13, [ decode, unpackClass(Date) ]);
      codec.addExtUnpacker(15, [ decode, unpackClass(Number) ]);
      if ("undefined" !== typeof Uint8Array) {
        codec.addExtUnpacker(17, unpackClass(Int8Array));
        codec.addExtUnpacker(18, unpackClass(Uint8Array));
        codec.addExtUnpacker(19, [ unpackArrayBuffer, unpackClass(Int16Array) ]);
        codec.addExtUnpacker(20, [ unpackArrayBuffer, unpackClass(Uint16Array) ]);
        codec.addExtUnpacker(21, [ unpackArrayBuffer, unpackClass(Int32Array) ]);
        codec.addExtUnpacker(22, [ unpackArrayBuffer, unpackClass(Uint32Array) ]);
        codec.addExtUnpacker(23, [ unpackArrayBuffer, unpackClass(Float32Array) ]);
        "undefined" !== typeof Float64Array && codec.addExtUnpacker(24, [ unpackArrayBuffer, unpackClass(Float64Array) ]);
        "undefined" !== typeof Uint8ClampedArray && codec.addExtUnpacker(25, unpackClass(Uint8ClampedArray));
        codec.addExtUnpacker(26, unpackArrayBuffer);
        codec.addExtUnpacker(29, [ unpackArrayBuffer, unpackClass(DataView) ]);
      }
      Bufferish.hasBuffer && codec.addExtUnpacker(27, unpackClass(Buffer));
    }
    function decode(input) {
      _decode || (_decode = require("./decode").decode);
      return _decode(input);
    }
    function unpackRegExp(value) {
      return RegExp.apply(null, value);
    }
    function unpackError(Class) {
      return function(value) {
        var out = new Class();
        for (var key in ERROR_COLUMNS) out[key] = value[key];
        return out;
      };
    }
    function unpackClass(Class) {
      return function(value) {
        return new Class(value);
      };
    }
    function unpackArrayBuffer(value) {
      return new Uint8Array(value).buffer;
    }
  }, {
    "./bufferish": 16,
    "./decode": 20
  } ],
  28: [ function(require, module, exports) {
    require("./read-core");
    require("./write-core");
    exports.createCodec = require("./codec-base").createCodec;
  }, {
    "./codec-base": 17,
    "./read-core": 30,
    "./write-core": 33
  } ],
  29: [ function(require, module, exports) {
    exports.FlexDecoder = FlexDecoder;
    exports.FlexEncoder = FlexEncoder;
    var Bufferish = require("./bufferish");
    var MIN_BUFFER_SIZE = 2048;
    var MAX_BUFFER_SIZE = 65536;
    var BUFFER_SHORTAGE = "BUFFER_SHORTAGE";
    function FlexDecoder() {
      if (!(this instanceof FlexDecoder)) return new FlexDecoder();
    }
    function FlexEncoder() {
      if (!(this instanceof FlexEncoder)) return new FlexEncoder();
    }
    FlexDecoder.mixin = mixinFactory(getDecoderMethods());
    FlexDecoder.mixin(FlexDecoder.prototype);
    FlexEncoder.mixin = mixinFactory(getEncoderMethods());
    FlexEncoder.mixin(FlexEncoder.prototype);
    function getDecoderMethods() {
      return {
        bufferish: Bufferish,
        write: write,
        fetch: fetch,
        flush: flush,
        push: push,
        pull: pull,
        read: read,
        reserve: reserve,
        offset: 0
      };
      function write(chunk) {
        var prev = this.offset ? Bufferish.prototype.slice.call(this.buffer, this.offset) : this.buffer;
        this.buffer = prev ? chunk ? this.bufferish.concat([ prev, chunk ]) : prev : chunk;
        this.offset = 0;
      }
      function flush() {
        while (this.offset < this.buffer.length) {
          var start = this.offset;
          var value;
          try {
            value = this.fetch();
          } catch (e) {
            if (e && e.message != BUFFER_SHORTAGE) throw e;
            this.offset = start;
            break;
          }
          this.push(value);
        }
      }
      function reserve(length) {
        var start = this.offset;
        var end = start + length;
        if (end > this.buffer.length) throw new Error(BUFFER_SHORTAGE);
        this.offset = end;
        return start;
      }
    }
    function getEncoderMethods() {
      return {
        bufferish: Bufferish,
        write: write,
        fetch: fetch,
        flush: flush,
        push: push,
        pull: pull,
        read: read,
        reserve: reserve,
        send: send,
        maxBufferSize: MAX_BUFFER_SIZE,
        minBufferSize: MIN_BUFFER_SIZE,
        offset: 0,
        start: 0
      };
      function fetch() {
        var start = this.start;
        if (start < this.offset) {
          var end = this.start = this.offset;
          return Bufferish.prototype.slice.call(this.buffer, start, end);
        }
      }
      function flush() {
        while (this.start < this.offset) {
          var value = this.fetch();
          value && this.push(value);
        }
      }
      function pull() {
        var buffers = this.buffers || (this.buffers = []);
        var chunk = buffers.length > 1 ? this.bufferish.concat(buffers) : buffers[0];
        buffers.length = 0;
        return chunk;
      }
      function reserve(length) {
        var req = 0 | length;
        if (this.buffer) {
          var size = this.buffer.length;
          var start = 0 | this.offset;
          var end = start + req;
          if (end < size) {
            this.offset = end;
            return start;
          }
          this.flush();
          length = Math.max(length, Math.min(2 * size, this.maxBufferSize));
        }
        length = Math.max(length, this.minBufferSize);
        this.buffer = this.bufferish.alloc(length);
        this.start = 0;
        this.offset = req;
        return 0;
      }
      function send(buffer) {
        var length = buffer.length;
        if (length > this.minBufferSize) {
          this.flush();
          this.push(buffer);
        } else {
          var offset = this.reserve(length);
          Bufferish.prototype.copy.call(buffer, this.buffer, offset);
        }
      }
    }
    function write() {
      throw new Error("method not implemented: write()");
    }
    function fetch() {
      throw new Error("method not implemented: fetch()");
    }
    function read() {
      var length = this.buffers && this.buffers.length;
      if (!length) return this.fetch();
      this.flush();
      return this.pull();
    }
    function push(chunk) {
      var buffers = this.buffers || (this.buffers = []);
      buffers.push(chunk);
    }
    function pull() {
      var buffers = this.buffers || (this.buffers = []);
      return buffers.shift();
    }
    function mixinFactory(source) {
      return mixin;
      function mixin(target) {
        for (var key in source) target[key] = source[key];
        return target;
      }
    }
  }, {
    "./bufferish": 16
  } ],
  30: [ function(require, module, exports) {
    var ExtBuffer = require("./ext-buffer").ExtBuffer;
    var ExtUnpacker = require("./ext-unpacker");
    var readUint8 = require("./read-format").readUint8;
    var ReadToken = require("./read-token");
    var CodecBase = require("./codec-base");
    CodecBase.install({
      addExtUnpacker: addExtUnpacker,
      getExtUnpacker: getExtUnpacker,
      init: init
    });
    exports.preset = init.call(CodecBase.preset);
    function getDecoder(options) {
      var readToken = ReadToken.getReadToken(options);
      return decode;
      function decode(decoder) {
        var type = readUint8(decoder);
        var func = readToken[type];
        if (!func) throw new Error("Invalid type: " + (type ? "0x" + type.toString(16) : type));
        return func(decoder);
      }
    }
    function init() {
      var options = this.options;
      this.decode = getDecoder(options);
      options && options.preset && ExtUnpacker.setExtUnpackers(this);
      return this;
    }
    function addExtUnpacker(etype, unpacker) {
      var unpackers = this.extUnpackers || (this.extUnpackers = []);
      unpackers[etype] = CodecBase.filter(unpacker);
    }
    function getExtUnpacker(type) {
      var unpackers = this.extUnpackers || (this.extUnpackers = []);
      return unpackers[type] || extUnpacker;
      function extUnpacker(buffer) {
        return new ExtBuffer(buffer, type);
      }
    }
  }, {
    "./codec-base": 17,
    "./ext-buffer": 25,
    "./ext-unpacker": 27,
    "./read-format": 31,
    "./read-token": 32
  } ],
  31: [ function(require, module, exports) {
    var ieee754 = require("ieee754");
    var Int64Buffer = require("int64-buffer");
    var Uint64BE = Int64Buffer.Uint64BE;
    var Int64BE = Int64Buffer.Int64BE;
    exports.getReadFormat = getReadFormat;
    exports.readUint8 = uint8;
    var Bufferish = require("./bufferish");
    var BufferProto = require("./bufferish-proto");
    var HAS_MAP = "undefined" !== typeof Map;
    var NO_ASSERT = true;
    function getReadFormat(options) {
      var binarraybuffer = Bufferish.hasArrayBuffer && options && options.binarraybuffer;
      var int64 = options && options.int64;
      var usemap = HAS_MAP && options && options.usemap;
      var readFormat = {
        map: usemap ? map_to_map : map_to_obj,
        array: array,
        str: str,
        bin: binarraybuffer ? bin_arraybuffer : bin_buffer,
        ext: ext,
        uint8: uint8,
        uint16: uint16,
        uint32: uint32,
        uint64: read(8, int64 ? readUInt64BE_int64 : readUInt64BE),
        int8: int8,
        int16: int16,
        int32: int32,
        int64: read(8, int64 ? readInt64BE_int64 : readInt64BE),
        float32: read(4, readFloatBE),
        float64: read(8, readDoubleBE)
      };
      return readFormat;
    }
    function map_to_obj(decoder, len) {
      var value = {};
      var i;
      var k = new Array(len);
      var v = new Array(len);
      var decode = decoder.codec.decode;
      for (i = 0; i < len; i++) {
        k[i] = decode(decoder);
        v[i] = decode(decoder);
      }
      for (i = 0; i < len; i++) value[k[i]] = v[i];
      return value;
    }
    function map_to_map(decoder, len) {
      var value = new Map();
      var i;
      var k = new Array(len);
      var v = new Array(len);
      var decode = decoder.codec.decode;
      for (i = 0; i < len; i++) {
        k[i] = decode(decoder);
        v[i] = decode(decoder);
      }
      for (i = 0; i < len; i++) value.set(k[i], v[i]);
      return value;
    }
    function array(decoder, len) {
      var value = new Array(len);
      var decode = decoder.codec.decode;
      for (var i = 0; i < len; i++) value[i] = decode(decoder);
      return value;
    }
    function str(decoder, len) {
      var start = decoder.reserve(len);
      var end = start + len;
      return BufferProto.toString.call(decoder.buffer, "utf-8", start, end);
    }
    function bin_buffer(decoder, len) {
      var start = decoder.reserve(len);
      var end = start + len;
      var buf = BufferProto.slice.call(decoder.buffer, start, end);
      return Bufferish.from(buf);
    }
    function bin_arraybuffer(decoder, len) {
      var start = decoder.reserve(len);
      var end = start + len;
      var buf = BufferProto.slice.call(decoder.buffer, start, end);
      return Bufferish.Uint8Array.from(buf).buffer;
    }
    function ext(decoder, len) {
      var start = decoder.reserve(len + 1);
      var type = decoder.buffer[start++];
      var end = start + len;
      var unpack = decoder.codec.getExtUnpacker(type);
      if (!unpack) throw new Error("Invalid ext type: " + (type ? "0x" + type.toString(16) : type));
      var buf = BufferProto.slice.call(decoder.buffer, start, end);
      return unpack(buf);
    }
    function uint8(decoder) {
      var start = decoder.reserve(1);
      return decoder.buffer[start];
    }
    function int8(decoder) {
      var start = decoder.reserve(1);
      var value = decoder.buffer[start];
      return 128 & value ? value - 256 : value;
    }
    function uint16(decoder) {
      var start = decoder.reserve(2);
      var buffer = decoder.buffer;
      return buffer[start++] << 8 | buffer[start];
    }
    function int16(decoder) {
      var start = decoder.reserve(2);
      var buffer = decoder.buffer;
      var value = buffer[start++] << 8 | buffer[start];
      return 32768 & value ? value - 65536 : value;
    }
    function uint32(decoder) {
      var start = decoder.reserve(4);
      var buffer = decoder.buffer;
      return 16777216 * buffer[start++] + (buffer[start++] << 16) + (buffer[start++] << 8) + buffer[start];
    }
    function int32(decoder) {
      var start = decoder.reserve(4);
      var buffer = decoder.buffer;
      return buffer[start++] << 24 | buffer[start++] << 16 | buffer[start++] << 8 | buffer[start];
    }
    function read(len, method) {
      return function(decoder) {
        var start = decoder.reserve(len);
        return method.call(decoder.buffer, start, NO_ASSERT);
      };
    }
    function readUInt64BE(start) {
      return new Uint64BE(this, start).toNumber();
    }
    function readInt64BE(start) {
      return new Int64BE(this, start).toNumber();
    }
    function readUInt64BE_int64(start) {
      return new Uint64BE(this, start);
    }
    function readInt64BE_int64(start) {
      return new Int64BE(this, start);
    }
    function readFloatBE(start) {
      return ieee754.read(this, start, false, 23, 4);
    }
    function readDoubleBE(start) {
      return ieee754.read(this, start, false, 52, 8);
    }
  }, {
    "./bufferish": 16,
    "./bufferish-proto": 14,
    ieee754: 6,
    "int64-buffer": 7
  } ],
  32: [ function(require, module, exports) {
    var ReadFormat = require("./read-format");
    exports.getReadToken = getReadToken;
    function getReadToken(options) {
      var format = ReadFormat.getReadFormat(options);
      return options && options.useraw ? init_useraw(format) : init_token(format);
    }
    function init_token(format) {
      var i;
      var token = new Array(256);
      for (i = 0; i <= 127; i++) token[i] = constant(i);
      for (i = 128; i <= 143; i++) token[i] = fix(i - 128, format.map);
      for (i = 144; i <= 159; i++) token[i] = fix(i - 144, format.array);
      for (i = 160; i <= 191; i++) token[i] = fix(i - 160, format.str);
      token[192] = constant(null);
      token[193] = null;
      token[194] = constant(false);
      token[195] = constant(true);
      token[196] = flex(format.uint8, format.bin);
      token[197] = flex(format.uint16, format.bin);
      token[198] = flex(format.uint32, format.bin);
      token[199] = flex(format.uint8, format.ext);
      token[200] = flex(format.uint16, format.ext);
      token[201] = flex(format.uint32, format.ext);
      token[202] = format.float32;
      token[203] = format.float64;
      token[204] = format.uint8;
      token[205] = format.uint16;
      token[206] = format.uint32;
      token[207] = format.uint64;
      token[208] = format.int8;
      token[209] = format.int16;
      token[210] = format.int32;
      token[211] = format.int64;
      token[212] = fix(1, format.ext);
      token[213] = fix(2, format.ext);
      token[214] = fix(4, format.ext);
      token[215] = fix(8, format.ext);
      token[216] = fix(16, format.ext);
      token[217] = flex(format.uint8, format.str);
      token[218] = flex(format.uint16, format.str);
      token[219] = flex(format.uint32, format.str);
      token[220] = flex(format.uint16, format.array);
      token[221] = flex(format.uint32, format.array);
      token[222] = flex(format.uint16, format.map);
      token[223] = flex(format.uint32, format.map);
      for (i = 224; i <= 255; i++) token[i] = constant(i - 256);
      return token;
    }
    function init_useraw(format) {
      var i;
      var token = init_token(format).slice();
      token[217] = token[196];
      token[218] = token[197];
      token[219] = token[198];
      for (i = 160; i <= 191; i++) token[i] = fix(i - 160, format.bin);
      return token;
    }
    function constant(value) {
      return function() {
        return value;
      };
    }
    function flex(lenFunc, decodeFunc) {
      return function(decoder) {
        var len = lenFunc(decoder);
        return decodeFunc(decoder, len);
      };
    }
    function fix(len, method) {
      return function(decoder) {
        return method(decoder, len);
      };
    }
  }, {
    "./read-format": 31
  } ],
  33: [ function(require, module, exports) {
    var ExtBuffer = require("./ext-buffer").ExtBuffer;
    var ExtPacker = require("./ext-packer");
    var WriteType = require("./write-type");
    var CodecBase = require("./codec-base");
    CodecBase.install({
      addExtPacker: addExtPacker,
      getExtPacker: getExtPacker,
      init: init
    });
    exports.preset = init.call(CodecBase.preset);
    function getEncoder(options) {
      var writeType = WriteType.getWriteType(options);
      return encode;
      function encode(encoder, value) {
        var func = writeType[typeof value];
        if (!func) throw new Error('Unsupported type "' + typeof value + '": ' + value);
        func(encoder, value);
      }
    }
    function init() {
      var options = this.options;
      this.encode = getEncoder(options);
      options && options.preset && ExtPacker.setExtPackers(this);
      return this;
    }
    function addExtPacker(etype, Class, packer) {
      packer = CodecBase.filter(packer);
      var name = Class.name;
      if (name && "Object" !== name) {
        var packers = this.extPackers || (this.extPackers = {});
        packers[name] = extPacker;
      } else {
        var list = this.extEncoderList || (this.extEncoderList = []);
        list.unshift([ Class, extPacker ]);
      }
      function extPacker(value) {
        packer && (value = packer(value));
        return new ExtBuffer(value, etype);
      }
    }
    function getExtPacker(value) {
      var packers = this.extPackers || (this.extPackers = {});
      var c = value.constructor;
      var e = c && c.name && packers[c.name];
      if (e) return e;
      var list = this.extEncoderList || (this.extEncoderList = []);
      var len = list.length;
      for (var i = 0; i < len; i++) {
        var pair = list[i];
        if (c === pair[0]) return pair[1];
      }
    }
  }, {
    "./codec-base": 17,
    "./ext-buffer": 25,
    "./ext-packer": 26,
    "./write-type": 35
  } ],
  34: [ function(require, module, exports) {
    var ieee754 = require("ieee754");
    var Int64Buffer = require("int64-buffer");
    var Uint64BE = Int64Buffer.Uint64BE;
    var Int64BE = Int64Buffer.Int64BE;
    var uint8 = require("./write-uint8").uint8;
    var Bufferish = require("./bufferish");
    var Buffer = Bufferish.global;
    var IS_BUFFER_SHIM = Bufferish.hasBuffer && "TYPED_ARRAY_SUPPORT" in Buffer;
    var NO_TYPED_ARRAY = IS_BUFFER_SHIM && !Buffer.TYPED_ARRAY_SUPPORT;
    var Buffer_prototype = Bufferish.hasBuffer && Buffer.prototype || {};
    exports.getWriteToken = getWriteToken;
    function getWriteToken(options) {
      return options && options.uint8array ? init_uint8array() : NO_TYPED_ARRAY || Bufferish.hasBuffer && options && options.safe ? init_safe() : init_token();
    }
    function init_uint8array() {
      var token = init_token();
      token[202] = writeN(202, 4, writeFloatBE);
      token[203] = writeN(203, 8, writeDoubleBE);
      return token;
    }
    function init_token() {
      var token = uint8.slice();
      token[196] = write1(196);
      token[197] = write2(197);
      token[198] = write4(198);
      token[199] = write1(199);
      token[200] = write2(200);
      token[201] = write4(201);
      token[202] = writeN(202, 4, Buffer_prototype.writeFloatBE || writeFloatBE, true);
      token[203] = writeN(203, 8, Buffer_prototype.writeDoubleBE || writeDoubleBE, true);
      token[204] = write1(204);
      token[205] = write2(205);
      token[206] = write4(206);
      token[207] = writeN(207, 8, writeUInt64BE);
      token[208] = write1(208);
      token[209] = write2(209);
      token[210] = write4(210);
      token[211] = writeN(211, 8, writeInt64BE);
      token[217] = write1(217);
      token[218] = write2(218);
      token[219] = write4(219);
      token[220] = write2(220);
      token[221] = write4(221);
      token[222] = write2(222);
      token[223] = write4(223);
      return token;
    }
    function init_safe() {
      var token = uint8.slice();
      token[196] = writeN(196, 1, Buffer.prototype.writeUInt8);
      token[197] = writeN(197, 2, Buffer.prototype.writeUInt16BE);
      token[198] = writeN(198, 4, Buffer.prototype.writeUInt32BE);
      token[199] = writeN(199, 1, Buffer.prototype.writeUInt8);
      token[200] = writeN(200, 2, Buffer.prototype.writeUInt16BE);
      token[201] = writeN(201, 4, Buffer.prototype.writeUInt32BE);
      token[202] = writeN(202, 4, Buffer.prototype.writeFloatBE);
      token[203] = writeN(203, 8, Buffer.prototype.writeDoubleBE);
      token[204] = writeN(204, 1, Buffer.prototype.writeUInt8);
      token[205] = writeN(205, 2, Buffer.prototype.writeUInt16BE);
      token[206] = writeN(206, 4, Buffer.prototype.writeUInt32BE);
      token[207] = writeN(207, 8, writeUInt64BE);
      token[208] = writeN(208, 1, Buffer.prototype.writeInt8);
      token[209] = writeN(209, 2, Buffer.prototype.writeInt16BE);
      token[210] = writeN(210, 4, Buffer.prototype.writeInt32BE);
      token[211] = writeN(211, 8, writeInt64BE);
      token[217] = writeN(217, 1, Buffer.prototype.writeUInt8);
      token[218] = writeN(218, 2, Buffer.prototype.writeUInt16BE);
      token[219] = writeN(219, 4, Buffer.prototype.writeUInt32BE);
      token[220] = writeN(220, 2, Buffer.prototype.writeUInt16BE);
      token[221] = writeN(221, 4, Buffer.prototype.writeUInt32BE);
      token[222] = writeN(222, 2, Buffer.prototype.writeUInt16BE);
      token[223] = writeN(223, 4, Buffer.prototype.writeUInt32BE);
      return token;
    }
    function write1(type) {
      return function(encoder, value) {
        var offset = encoder.reserve(2);
        var buffer = encoder.buffer;
        buffer[offset++] = type;
        buffer[offset] = value;
      };
    }
    function write2(type) {
      return function(encoder, value) {
        var offset = encoder.reserve(3);
        var buffer = encoder.buffer;
        buffer[offset++] = type;
        buffer[offset++] = value >>> 8;
        buffer[offset] = value;
      };
    }
    function write4(type) {
      return function(encoder, value) {
        var offset = encoder.reserve(5);
        var buffer = encoder.buffer;
        buffer[offset++] = type;
        buffer[offset++] = value >>> 24;
        buffer[offset++] = value >>> 16;
        buffer[offset++] = value >>> 8;
        buffer[offset] = value;
      };
    }
    function writeN(type, len, method, noAssert) {
      return function(encoder, value) {
        var offset = encoder.reserve(len + 1);
        encoder.buffer[offset++] = type;
        method.call(encoder.buffer, value, offset, noAssert);
      };
    }
    function writeUInt64BE(value, offset) {
      new Uint64BE(this, offset, value);
    }
    function writeInt64BE(value, offset) {
      new Int64BE(this, offset, value);
    }
    function writeFloatBE(value, offset) {
      ieee754.write(this, value, offset, false, 23, 4);
    }
    function writeDoubleBE(value, offset) {
      ieee754.write(this, value, offset, false, 52, 8);
    }
  }, {
    "./bufferish": 16,
    "./write-uint8": 36,
    ieee754: 6,
    "int64-buffer": 7
  } ],
  35: [ function(require, module, exports) {
    var IS_ARRAY = require("isarray");
    var Int64Buffer = require("int64-buffer");
    var Uint64BE = Int64Buffer.Uint64BE;
    var Int64BE = Int64Buffer.Int64BE;
    var Bufferish = require("./bufferish");
    var BufferProto = require("./bufferish-proto");
    var WriteToken = require("./write-token");
    var uint8 = require("./write-uint8").uint8;
    var ExtBuffer = require("./ext-buffer").ExtBuffer;
    var HAS_UINT8ARRAY = "undefined" !== typeof Uint8Array;
    var HAS_MAP = "undefined" !== typeof Map;
    var extmap = [];
    extmap[1] = 212;
    extmap[2] = 213;
    extmap[4] = 214;
    extmap[8] = 215;
    extmap[16] = 216;
    exports.getWriteType = getWriteType;
    function getWriteType(options) {
      var token = WriteToken.getWriteToken(options);
      var useraw = options && options.useraw;
      var binarraybuffer = HAS_UINT8ARRAY && options && options.binarraybuffer;
      var isBuffer = binarraybuffer ? Bufferish.isArrayBuffer : Bufferish.isBuffer;
      var bin = binarraybuffer ? bin_arraybuffer : bin_buffer;
      var usemap = HAS_MAP && options && options.usemap;
      var map = usemap ? map_to_map : obj_to_map;
      var writeType = {
        boolean: bool,
        function: nil,
        number: number,
        object: useraw ? object_raw : object,
        string: _string(useraw ? raw_head_size : str_head_size),
        symbol: nil,
        undefined: nil
      };
      return writeType;
      function bool(encoder, value) {
        var type = value ? 195 : 194;
        token[type](encoder, value);
      }
      function number(encoder, value) {
        var ivalue = 0 | value;
        var type;
        if (value !== ivalue) {
          type = 203;
          token[type](encoder, value);
          return;
        }
        type = -32 <= ivalue && ivalue <= 127 ? 255 & ivalue : 0 <= ivalue ? ivalue <= 255 ? 204 : ivalue <= 65535 ? 205 : 206 : -128 <= ivalue ? 208 : -32768 <= ivalue ? 209 : 210;
        token[type](encoder, ivalue);
      }
      function uint64(encoder, value) {
        var type = 207;
        token[type](encoder, value.toArray());
      }
      function int64(encoder, value) {
        var type = 211;
        token[type](encoder, value.toArray());
      }
      function str_head_size(length) {
        return length < 32 ? 1 : length <= 255 ? 2 : length <= 65535 ? 3 : 5;
      }
      function raw_head_size(length) {
        return length < 32 ? 1 : length <= 65535 ? 3 : 5;
      }
      function _string(head_size) {
        return string;
        function string(encoder, value) {
          var length = value.length;
          var maxsize = 5 + 3 * length;
          encoder.offset = encoder.reserve(maxsize);
          var buffer = encoder.buffer;
          var expected = head_size(length);
          var start = encoder.offset + expected;
          length = BufferProto.write.call(buffer, value, start);
          var actual = head_size(length);
          if (expected !== actual) {
            var targetStart = start + actual - expected;
            var end = start + length;
            BufferProto.copy.call(buffer, buffer, targetStart, start, end);
          }
          var type = 1 === actual ? 160 + length : actual <= 3 ? 215 + actual : 219;
          token[type](encoder, length);
          encoder.offset += length;
        }
      }
      function object(encoder, value) {
        if (null === value) return nil(encoder, value);
        if (isBuffer(value)) return bin(encoder, value);
        if (IS_ARRAY(value)) return array(encoder, value);
        if (Uint64BE.isUint64BE(value)) return uint64(encoder, value);
        if (Int64BE.isInt64BE(value)) return int64(encoder, value);
        var packer = encoder.codec.getExtPacker(value);
        packer && (value = packer(value));
        if (value instanceof ExtBuffer) return ext(encoder, value);
        map(encoder, value);
      }
      function object_raw(encoder, value) {
        if (isBuffer(value)) return raw(encoder, value);
        object(encoder, value);
      }
      function nil(encoder, value) {
        var type = 192;
        token[type](encoder, value);
      }
      function array(encoder, value) {
        var length = value.length;
        var type = length < 16 ? 144 + length : length <= 65535 ? 220 : 221;
        token[type](encoder, length);
        var encode = encoder.codec.encode;
        for (var i = 0; i < length; i++) encode(encoder, value[i]);
      }
      function bin_buffer(encoder, value) {
        var length = value.length;
        var type = length < 255 ? 196 : length <= 65535 ? 197 : 198;
        token[type](encoder, length);
        encoder.send(value);
      }
      function bin_arraybuffer(encoder, value) {
        bin_buffer(encoder, new Uint8Array(value));
      }
      function ext(encoder, value) {
        var buffer = value.buffer;
        var length = buffer.length;
        var type = extmap[length] || (length < 255 ? 199 : length <= 65535 ? 200 : 201);
        token[type](encoder, length);
        uint8[value.type](encoder);
        encoder.send(buffer);
      }
      function obj_to_map(encoder, value) {
        var keys = Object.keys(value);
        var length = keys.length;
        var type = length < 16 ? 128 + length : length <= 65535 ? 222 : 223;
        token[type](encoder, length);
        var encode = encoder.codec.encode;
        keys.forEach(function(key) {
          encode(encoder, key);
          encode(encoder, value[key]);
        });
      }
      function map_to_map(encoder, value) {
        if (!(value instanceof Map)) return obj_to_map(encoder, value);
        var length = value.size;
        var type = length < 16 ? 128 + length : length <= 65535 ? 222 : 223;
        token[type](encoder, length);
        var encode = encoder.codec.encode;
        value.forEach(function(val, key, m) {
          encode(encoder, key);
          encode(encoder, val);
        });
      }
      function raw(encoder, value) {
        var length = value.length;
        var type = length < 32 ? 160 + length : length <= 65535 ? 218 : 219;
        token[type](encoder, length);
        encoder.send(value);
      }
    }
  }, {
    "./bufferish": 16,
    "./bufferish-proto": 14,
    "./ext-buffer": 25,
    "./write-token": 34,
    "./write-uint8": 36,
    "int64-buffer": 7,
    isarray: 8
  } ],
  36: [ function(require, module, exports) {
    var constant = exports.uint8 = new Array(256);
    for (var i = 0; i <= 255; i++) constant[i] = write0(i);
    function write0(type) {
      return function(encoder) {
        var offset = encoder.reserve(1);
        encoder.buffer[offset] = type;
      };
    }
  }, {} ],
  37: [ function(require, module, exports) {
    var msgpack = require("msgpack-lite");
    var options = {
      codec: msgpack.createCodec({
        binarraybuffer: true,
        preset: false
      })
    };
    var compressPublishPacket = function(object) {
      if ("#publish" != object.event || null == object.data) return;
      var pubArray = [ object.data.channel, object.data.data ];
      null != object.cid && pubArray.push(object.cid);
      object.p = pubArray;
      delete object.event;
      delete object.data;
      delete object.cid;
    };
    var decompressPublishPacket = function(object) {
      if (null == object.p) return;
      object.event = "#publish";
      object.data = {
        channel: object.p[0],
        data: object.p[1]
      };
      null != object.p[2] && (object.cid = object.p[2]);
      delete object.p;
    };
    var compressEmitPacket = function(object) {
      if (null == object.event) return;
      object.e = [ object.event, object.data ];
      null != object.cid && object.e.push(object.cid);
      delete object.event;
      delete object.data;
      delete object.cid;
    };
    var decompressEmitPacket = function(object) {
      if (null == object.e) return;
      object.event = object.e[0];
      object.data = object.e[1];
      null != object.e[2] && (object.cid = object.e[2]);
      delete object.e;
    };
    var compressResponsePacket = function(object) {
      if (null == object.rid) return;
      object.r = [ object.rid, object.error, object.data ];
      delete object.rid;
      delete object.error;
      delete object.data;
    };
    var decompressResponsePacket = function(object) {
      if (null == object.r) return;
      object.rid = object.r[0];
      object.error = object.r[1];
      object.data = object.r[2];
      delete object.r;
    };
    var clonePacket = function(object) {
      var clone = {};
      for (var i in object) object.hasOwnProperty(i) && (clone[i] = object[i]);
      return clone;
    };
    var compressSinglePacket = function(object) {
      object = clonePacket(object);
      compressPublishPacket(object);
      compressEmitPacket(object);
      compressResponsePacket(object);
      return object;
    };
    var decompressSinglePacket = function(object) {
      decompressEmitPacket(object);
      decompressPublishPacket(object);
      decompressResponsePacket(object);
    };
    module.exports.encode = function(object) {
      if (object) if (Array.isArray(object)) {
        var len = object.length;
        for (var i = 0; i < len; i++) object[i] = compressSinglePacket(object[i]);
      } else null == object.event && null == object.rid || (object = compressSinglePacket(object));
      return msgpack.encode(object, options);
    };
    module.exports.decode = function(str) {
      str = new Uint8Array(str);
      var object = msgpack.decode(str, options);
      if (Array.isArray(object)) {
        var len = object.length;
        for (var i = 0; i < len; i++) decompressSinglePacket(object[i]);
      } else decompressSinglePacket(object);
      return object;
    };
  }, {
    "msgpack-lite": 9
  } ],
  AccumulatedBar1: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "70436sroTRDw6DUOZ0pjarm", "AccumulatedBar1");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AccumulatedBar_1 = require("../../../scripts/Components/AccumulatedBar");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AccumulatedBar1 = function(_super) {
      __extends(AccumulatedBar1, _super);
      function AccumulatedBar1() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AccumulatedBar1 = __decorate([ ccclass ], AccumulatedBar1);
      return AccumulatedBar1;
    }(AccumulatedBar_1.default);
    exports.default = AccumulatedBar1;
    cc._RF.pop();
  }, {
    "../../../scripts/Components/AccumulatedBar": "AccumulatedBar"
  } ],
  AccumulatedBar2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2400bjcxatI3aMiaRjnFYPJ", "AccumulatedBar2");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AccumulatedBar_1 = require("../../../scripts/Components/AccumulatedBar");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AccumulatedBar2 = function(_super) {
      __extends(AccumulatedBar2, _super);
      function AccumulatedBar2() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AccumulatedBar2 = __decorate([ ccclass ], AccumulatedBar2);
      return AccumulatedBar2;
    }(AccumulatedBar_1.default);
    exports.default = AccumulatedBar2;
    cc._RF.pop();
  }, {
    "../../../scripts/Components/AccumulatedBar": "AccumulatedBar"
  } ],
  AccumulatedBar: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d943fjlle1CPIHPsnhlogIb", "AccumulatedBar");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AccumulatedBar = function(_super) {
      __extends(AccumulatedBar, _super);
      function AccumulatedBar() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.bar = null;
        _this.maxLength = 0;
        _this.minLength = 0;
        _this.progress = 1;
        _this.lerpRatio = 5;
        _this.fps = 30;
        _this.listenner = void 0;
        _this._tempProgress = 0;
        _this._tempSetTimeout = null;
        return _this;
      }
      AccumulatedBar.prototype.onLoad = function() {
        this.updateProgress(0);
      };
      AccumulatedBar.prototype.updateProgress = function(progress) {
        var _this = this;
        if (null != progress) {
          progress > 1 && (progress = 1);
          progress < 0 && (progress = 0);
          this.progress = progress;
        }
        this.updateLength();
        if (Math.abs(this.progress - this._tempProgress) < .001) {
          this._tempProgress = this.progress;
          return true;
        }
        var delta = this.progress - this._tempProgress;
        this._tempProgress = delta * this.lerpRatio / 10 / this.fps + this._tempProgress;
        this.node.runAction(cc.sequence(cc.delayTime(1 / this.fps), cc.callFunc(function() {
          _this.updateProgress();
          _this.listenner && _this.listenner(100 * _this._tempProgress);
        })));
      };
      AccumulatedBar.prototype.setListener = function(listenner) {
        this.listenner = listenner;
      };
      AccumulatedBar.prototype.onDestroy = function() {};
      AccumulatedBar.prototype.updateLength = function() {
        this.bar.width = (this.maxLength - this.minLength) * this._tempProgress + this.minLength;
      };
      __decorate([ property(cc.Node) ], AccumulatedBar.prototype, "bar", void 0);
      __decorate([ property(cc.Float) ], AccumulatedBar.prototype, "maxLength", void 0);
      __decorate([ property(cc.Float) ], AccumulatedBar.prototype, "minLength", void 0);
      __decorate([ property({
        type: cc.Float,
        min: 0,
        max: 1,
        slide: true
      }) ], AccumulatedBar.prototype, "progress", void 0);
      __decorate([ property(cc.Integer) ], AccumulatedBar.prototype, "lerpRatio", void 0);
      __decorate([ property(cc.Integer) ], AccumulatedBar.prototype, "fps", void 0);
      AccumulatedBar = __decorate([ ccclass ], AccumulatedBar);
      return AccumulatedBar;
    }(cc.Component);
    exports.default = AccumulatedBar;
    cc._RF.pop();
  }, {} ],
  AnimationController1: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ec6025Cc+VDc6CR9njwzZi6", "AnimationController1");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var AnimationController1 = function(_super) {
      __extends(AnimationController1, _super);
      function AnimationController1() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AnimationController1 = __decorate([ ccclass ], AnimationController1);
      return AnimationController1;
    }(AnimationController_1.default);
    exports.default = AnimationController1;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d376a4AvPRFcZsBDpwXN4gl", "AnimationController2");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var AnimationController2 = function(_super) {
      __extends(AnimationController2, _super);
      function AnimationController2() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AnimationController2 = __decorate([ ccclass ], AnimationController2);
      return AnimationController2;
    }(AnimationController_1.default);
    exports.default = AnimationController2;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController3: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d08e5PKM71AlqXM2LdKQkVO", "AnimationController3");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var AnimationController3 = function(_super) {
      __extends(AnimationController3, _super);
      function AnimationController3() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AnimationController3 = __decorate([ ccclass ], AnimationController3);
      return AnimationController3;
    }(AnimationController_1.default);
    exports.default = AnimationController3;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController4: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "48afd1zIvdKjqMzMyWdrWo7", "AnimationController4");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController4 = function(_super) {
      __extends(AnimationController4, _super);
      function AnimationController4() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.boderWinItem = null;
        _this.nodeAnchor = null;
        _this.reward = 0;
        _this.ItemIds = null;
        _this._lines = [];
        return _this;
      }
      AnimationController4.prototype.onLoad = function() {
        _super.prototype.onLoad.call(this);
        this._slotController = this.node.getComponent("SlotController4");
      };
      AnimationController4.prototype.checkWay = function(data) {
        var _this = this;
        var loop = 0;
        var indexLoop = 0;
        this._listItem = this._slotController.strMatrixToArray(data.spinResult.mat[0], 5, 3);
        var totalLines = config.game.PAY_LINES[243];
        var payLines = [];
        var relsult = [];
        totalLines.forEach(function(line, index) {
          var lineItem = line.map(function(row, col) {
            return _this._listItem[col][row];
          });
          var symbol = lineItem[0];
          var counter = 1;
          lineItem.forEach(function(item, index) {
            if (0 == index) return;
            if (counter < index) return;
            item !== symbol && "w" != item && "w" != symbol || counter++;
            "w" == symbol && "w" != item && (symbol = item);
          });
          if (counter > 2) {
            relsult.push(symbol);
            payLines.push(index);
            loop = 0;
          }
        });
        this.ItemIds = Array.from(new Set(relsult));
        if (0 == this.ItemIds.length) return;
        return payLines;
      };
      AnimationController4.prototype.setAnimationData = function(data) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.reward = data.spinResult.score.reward;
              this._lines = this.checkWay(data);
              return [ 4, this.showPartical(data) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      AnimationController4.prototype.showAllLines = function(count) {
        return __awaiter(this, void 0, void 0, function() {
          var i;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (count && this._slotController.isSpin) return [ 2 ];
              for (i = 0; i < this._lines.length; i++) this.showAllBox(this._lines[i]);
              return [ 4, util.game.delay(2e3) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      AnimationController4.prototype.showEachLine = function() {
        return __awaiter(this, void 0, void 0, function() {
          var leap, i;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              leap = this._lines.length;
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < leap)) return [ 3, 4 ];
              if (this._slotController.isSpin) return [ 3, 4 ];
              this.showBox(this._lines[i]);
              return [ 4, util.game.delay(2e3) ];

             case 2:
              _a.sent();
              _a.label = 3;

             case 3:
              i++;
              return [ 3, 1 ];

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      AnimationController4.prototype.showAllBox = function(item) {
        if (null == this.ItemIds) return;
        var listItem = this._slotController.reelController[0]._listItem.slice(1);
        var payTable = config.game.PAY_LINES[this.totalPayLines];
        var line = payTable[item];
        for (var i = 0; i < this.ItemIds.length; i++) {
          var _loop_1 = function(j) {
            var temp1 = this_1.checkIndex(line[j]);
            if (listItem[temp1][j].id != this_1.ItemIds[i] && listItem[temp1][j].id != this_1.wildItemId) return "break";
            listItem[temp1][j].updateShowBorderWin();
            listItem[temp1][j].playZoomAnimation();
            setTimeout(function() {
              return listItem[temp1][j].hideBorderWin();
            }, 1400);
          };
          var this_1 = this;
          for (var j = 0; j < line.length; j++) {
            var state_1 = _loop_1(j);
            if ("break" === state_1) break;
          }
        }
      };
      AnimationController4.prototype.showBox = function(item) {
        return __awaiter(this, void 0, void 0, function() {
          var listItem, payTable, line, i, _loop_2, this_2, j, state_2;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (null == this.ItemIds) return [ 2 ];
              listItem = this._slotController.reelController[0]._listItem.slice(1);
              payTable = config.game.PAY_LINES[this.totalPayLines];
              line = payTable[item];
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < this.ItemIds.length)) return [ 3, 4 ];
              _loop_2 = function(j) {
                var temp1 = this_2.checkIndex(line[j]);
                if (listItem[temp1][j].id != this_2.ItemIds[i] && listItem[temp1][j].id != this_2.wildItemId) return "break";
                listItem[temp1][j].updateShowBorderWin();
                listItem[temp1][j].playZoomAnimation();
                setTimeout(function() {
                  return listItem[temp1][j].hideBorderWin();
                }, 1400);
              };
              this_2 = this;
              for (j = 0; j < line.length; j++) {
                state_2 = _loop_2(j);
                if ("break" === state_2) break;
              }
              return [ 4, util.game.delay(2e3) ];

             case 2:
              _a.sent();
              _a.label = 3;

             case 3:
              i++;
              return [ 3, 1 ];

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      AnimationController4.prototype.checkIndex = function(temp1) {
        var temp = temp1;
        temp = 2 == temp ? 0 : 0 == temp ? 2 : 1;
        return temp;
      };
      AnimationController4.prototype.showPartical = function(data) {
        var _this = this;
        var listItem = this._slotController.reelController[0]._listItem.slice(1);
        listItem.forEach(function(item) {
          item.forEach(function(ele) {
            if ("w" == ele.id) {
              ele.partical.active = true;
              ele.partical.zIndex = 1;
              cc.log(ele.partical.zIndex);
              setTimeout(function() {
                ele.partical.runAction(cc.moveTo(.5, _this.nodeAnchor.getPosition()).easing(cc.easeCircleActionInOut()));
              }, 1e3);
            }
          });
        });
      };
      AnimationController4.prototype.stopAllAction = function() {
        this.stopZoomLineItems();
      };
      __decorate([ property(cc.Node) ], AnimationController4.prototype, "boderWinItem", void 0);
      __decorate([ property(cc.Node) ], AnimationController4.prototype, "nodeAnchor", void 0);
      AnimationController4 = __decorate([ ccclass ], AnimationController4);
      return AnimationController4;
    }(AnimationController_1.default);
    exports.default = AnimationController4;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController5: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f9738wVjnRMMKzciCT5eApg", "AnimationController5");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var AnimationController5 = function(_super) {
      __extends(AnimationController5, _super);
      function AnimationController5() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AnimationController5 = __decorate([ ccclass ], AnimationController5);
      return AnimationController5;
    }(AnimationController_1.default);
    exports.default = AnimationController5;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController6: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9eb0bFXKA9ETYLDQu2Hy5+K", "AnimationController6");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var AnimationController6 = function(_super) {
      __extends(AnimationController6, _super);
      function AnimationController6() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AnimationController6.prototype.setAnimationData = function(data) {
        if (Object.keys(data.freeSpin).length > 0) {
          this.lines = data.features.freeReelGameResult.map(function(item) {
            return item.score.lines;
          });
          this.reward = data.spinResult.score.reward;
        } else _super.prototype.setAnimationData.call(this, data);
      };
      AnimationController6 = __decorate([ ccclass ], AnimationController6);
      return AnimationController6;
    }(AnimationController_1.default);
    exports.default = AnimationController6;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController7: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5ef01XT1HpLlp3tCOqxW2WJ", "AnimationController7");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var AnimationController7 = function(_super) {
      __extends(AnimationController7, _super);
      function AnimationController7() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.shieldSke = null;
        _this.parShield = null;
        _this.tornado = null;
        _this.birdAni = null;
        _this.dataShield = [];
        _this.listShield = [];
        _this.shield = [];
        _this.position = [];
        return _this;
      }
      AnimationController7.prototype.onLoad = function() {
        this.shieldSke.enabled = false;
        _super.prototype.onLoad.call(this);
        this._slotController7 = this.node.getComponent("SlotController7");
      };
      AnimationController7.prototype.setAnimationData = function(spinResult) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _super.prototype.setAnimationData.call(this, spinResult);
              return [ 4, this.animationShield() ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      AnimationController7.prototype.animationShield = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.listShield = [];
              this.shield = [];
              this.position = [];
              this.dataShield = this._slotController7.dataSpin.spinResult.score.sum[6].arr;
              if (!(this._slotController7.dataSpin.freeSpin.c >= 0 && this._slotController7.dataSpin.freeSpin.b > 0)) return [ 3, 4 ];
              this.dataShield.forEach(function(elem) {
                if (elem.valid) {
                  var positionShield = _this._slotController7._listItem[elem.position.col][elem.position.row + 1];
                  if (!("k" == positionShield.id)) {
                    _this.listShield.push(positionShield);
                    _this.shield.push(elem);
                  }
                }
              });
              if (!(this.listShield.length > 0)) return [ 3, 2 ];
              return [ 4, util.game.delay(500) ];

             case 1:
              _a.sent();
              _a.label = 2;

             case 2:
              this.listShield.forEach(function(item) {
                _this.position.push(item.node.getPosition());
              });
              if (!(this.position.length > 0)) return [ 3, 4 ];
              return [ 4, this.aniShield() ];

             case 3:
              _a.sent();
              _a.label = 4;

             case 4:
              return [ 4, this.shieldEffect() ];

             case 5:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      AnimationController7.prototype.aniShield = function() {
        var _this = this;
        return new Promise(function(res) {
          var position = _this._slotController7.tableView.getPosition();
          var birdSkeleton = _this.birdAni.getComponent(sp.Skeleton);
          birdSkeleton.setCompleteListener(function() {
            birdSkeleton.loop = false;
            birdSkeleton.animation = "ani_attack";
            setTimeout(function() {
              _this.position.forEach(function(item, index) {
                setTimeout(function() {
                  _this.parShield.active = true;
                  _this.parShield.runAction(cc.sequence([ cc.moveTo(1, cc.v2(item.x + position.x, item.y + position.y)).easing(cc.easeQuadraticActionIn()), cc.callFunc(function() {
                    _this.listShield[index].node.runAction(cc.sequence([ cc.callFunc(function() {
                      _this.tornado.setPosition(item.x + position.x, item.y + position.y);
                      _this.tornado.getComponent(sp.Skeleton).enabled = true;
                    }), cc.callFunc(function() {
                      _this.listShield[index].node.children[1].active = true;
                      _this.listShield[index].node.children[0].getComponent(sp.Skeleton).animation = "caikhien";
                      "number" == typeof _this.shield[index].value ? _this.listShield[index].node.children[1].getComponent(cc.Label).string = util.game.abbreviateNumber(_this.shield[index].value * _this._slotController7.dataSpin.spinResult.totalBet, 0) : _this.listShield[index].node.children[1].getComponent(cc.Label).string = "" + _this.shield[index].value;
                    }), cc.callFunc(function() {
                      setTimeout(function() {
                        _this.tornado.getComponent(sp.Skeleton).enabled = false;
                        _this.tornado.getComponent(sp.Skeleton).setBonesToSetupPose();
                      }, 1e3);
                    }) ]));
                  }) ]));
                  _this.parShield.zIndex = 1;
                }, 1200 * index);
              });
              setTimeout(function() {
                _this.parShield.setPosition(358, -135);
                _this.parShield.getComponent(cc.ParticleSystem).resetSystem();
                _this.parShield.active = false;
                res();
              }, 1200 * _this.position.length + 500);
            }, 1200);
            birdSkeleton.setCompleteListener(function() {});
            birdSkeleton.setCompleteListener(function() {
              birdSkeleton.loop = true;
              birdSkeleton.animation = "ani_bay";
              birdSkeleton.setCompleteListener(function() {});
            });
          });
        });
      };
      AnimationController7.prototype.shieldEffect = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!this.dataShield.length) return [ 2 ];
              return [ 4, util.game.delay(1500) ];

             case 1:
              _a.sent();
              this.dataShield.forEach(function(item) {
                if (item.valid) {
                  var itemNode_1 = _this._slotController7._listItem[item.position.col][item.position.row + 1].node;
                  itemNode_1.runAction(cc.sequence([ cc.callFunc(function() {
                    itemNode_1.children[2].active = true;
                    itemNode_1.children[2].getComponent(sp.Skeleton).animation = "chem_ani";
                  }), cc.scaleTo(.3, 1.2), cc.scaleTo(.2, 1) ]));
                }
              });
              return [ 4, this._slotController7.totalWin() ];

             case 2:
              _a.sent();
              return [ 4, util.game.delay(500) ];

             case 3:
              return [ 2, _a.sent() ];
            }
          });
        });
      };
      __decorate([ property(sp.Skeleton) ], AnimationController7.prototype, "shieldSke", void 0);
      __decorate([ property(cc.Node) ], AnimationController7.prototype, "parShield", void 0);
      __decorate([ property(cc.Node) ], AnimationController7.prototype, "tornado", void 0);
      __decorate([ property(cc.Node) ], AnimationController7.prototype, "birdAni", void 0);
      AnimationController7 = __decorate([ ccclass ], AnimationController7);
      return AnimationController7;
    }(AnimationController_1.default);
    exports.default = AnimationController7;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController8: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2126fiOpBJPi6tDIw5/aCCo", "AnimationController8");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController_1 = require("../../../scripts/Slots/AnimationController");
    var AnimationController8 = function(_super) {
      __extends(AnimationController8, _super);
      function AnimationController8() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AnimationController8 = __decorate([ ccclass ], AnimationController8);
      return AnimationController8;
    }(AnimationController_1.default);
    exports.default = AnimationController8;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/AnimationController": "AnimationController"
  } ],
  AnimationController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3d72bR6jslGsYSbra1OLiEg", "AnimationController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AnimationController = function(_super) {
      __extends(AnimationController, _super);
      function AnimationController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.delayTime = 2.2;
        _this.wildItemId = "w";
        _this.totalPayLines = 20;
        _this.lines = null;
        _this.reward = 0;
        _this._tempIsShowWin = false;
        _this.reelController = null;
        return _this;
      }
      AnimationController.prototype.onLoad = function() {
        this._slotController = this.node.getComponent("SlotController");
      };
      AnimationController.prototype.refreshSlotAnimation = function() {
        this.reelController = this.reelController || this._slotController.reelController;
        this._slotController.reelController.forEach(function(item) {
          return item.linesController.refreshShowLines();
        });
        this.stopZoomLineItems();
        this._tempIsShowWin = false;
      };
      AnimationController.prototype.setAnimationData = function(dataSpin) {
        this.lines = [ dataSpin.spinResult.score.lines ];
        this.reward = dataSpin.spinResult.score.reward;
        view.bar.bottom.gameBar.bottomBarEvent(define.type.BottomBar.active);
      };
      AnimationController.prototype.setLinesData = function(data) {
        var gameSize = config.game.GAME_SIZE[this._slotController.gameSizeType];
        var itemAmountInReel = gameSize.length * gameSize[0];
        cc.log(itemAmountInReel);
      };
      AnimationController.prototype.showAllLines = function(count) {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (count && this._slotController.isSpin) return [ 2 ];
              return [ 4, Promise.all(this._slotController.reelController.map(function(item, index) {
                return item.linesController.showAllLines(_this.lines[index].map(function(item) {
                  return item.l;
                }).filter(function(item) {
                  return item >= 0;
                }), _this.totalPayLines);
              })) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      AnimationController.prototype.showWinAmount = function() {
        !this._tempIsShowWin && view.screen.slot && this._slotController.uuid === view.screen.slot.SlotController.uuid && view.bar.bottom.gameBar.updateWinAmount(this.reward);
        this._tempIsShowWin = true;
      };
      AnimationController.prototype.showEachLine = function() {
        return __awaiter(this, void 0, void 0, function() {
          var lines, _loop_1, this_1, i, length, state_1;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!this || !this.lines) return [ 2 ];
              lines = this.lines;
              _loop_1 = function(i, length) {
                return __generator(this, function(_a) {
                  switch (_a.label) {
                   case 0:
                    if (this_1._slotController.isSpin) return [ 2, "break" ];
                    if (!lines || !lines[0][i] || lines[0][i].l < 0) return [ 2, "continue" ];
                    this_1.playZoomLineItems(lines[0][i]);
                    return [ 4, Promise.all(this_1._slotController.reelController.map(function(item) {
                      return item.linesController.showLine(lines[0][i].l);
                    })) ];

                   case 1:
                    _a.sent();
                    this_1.stopZoomLineItems();
                    return [ 2 ];
                  }
                });
              };
              this_1 = this;
              i = 0, length = lines[0].length;
              _a.label = 1;

             case 1:
              if (!(i < length)) return [ 3, 4 ];
              return [ 5, _loop_1(i, length) ];

             case 2:
              state_1 = _a.sent();
              if ("break" === state_1) return [ 3, 4 ];
              _a.label = 3;

             case 3:
              i++;
              return [ 3, 1 ];

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      AnimationController.prototype.playZoomLineItems = function(item) {
        var _this = this;
        var listItem = this.reelController[0]._listItem.slice(1);
        var payTable = config.game.PAY_LINES[this.totalPayLines];
        var line = payTable[item.l];
        var itemId = parseInt(item.s[0]);
        var isDone = false;
        line.forEach(function(item, i) {
          if (isDone) return;
          listItem[item][i].id == itemId || listItem[item][i].id == _this.wildItemId ? listItem[item][i].playZoomAnimation() : isDone = true;
        });
      };
      AnimationController.prototype.stopZoomLineItems = function() {
        if (!this || !this.reelController) return;
        var listItem = this.reelController.map(function(item) {
          return item._listItem.slice(1);
        });
        listItem.forEach(function(item) {
          return item.forEach(function(item) {
            return item.forEach(function(item) {
              return item.stopZoomAnimation();
            });
          });
        });
      };
      AnimationController.prototype.spinItems = function(listItem) {
        var _this = this;
        listItem.forEach(function(item) {
          item.forEach(function(item) {
            _this.node.runAction(cc.sequence([ cc.delayTime(.2 * item.x), cc.callFunc(function() {
              item.move();
            }) ]));
          });
        });
      };
      AnimationController.prototype.stopAllAction = function() {
        if (!this || !this.lines) return;
        var lines = this.lines;
        for (var i = 0, length = lines[0].length; i < length; i++) {
          if (lines[0][i].l < 0) continue;
          this._slotController.reelController.map(function(item) {
            return item.linesController.stopShowLines();
          });
        }
        lines[0] && (lines[0].length = 0);
        this.stopZoomLineItems();
      };
      __decorate([ property(cc.Float) ], AnimationController.prototype, "delayTime", void 0);
      __decorate([ property(cc.String) ], AnimationController.prototype, "wildItemId", void 0);
      __decorate([ property(cc.Integer) ], AnimationController.prototype, "totalPayLines", void 0);
      AnimationController = __decorate([ ccclass ], AnimationController);
      return AnimationController;
    }(cc.Component);
    exports.default = AnimationController;
    cc._RF.pop();
  }, {} ],
  Api: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "798c96BirVCt69SsVnT+Xy6", "Api");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var scCodecMinBin = require("sc-codec-min-bin");
    var DefineKey = require("./Define/DefineKey");
    var auth_1 = require("./lib/auth");
    var HOST = "64.120.114.208";
    var API = function() {
      function API() {
        this.key = DefineKey.api;
        return API.instance || (API.instance = this);
      }
      API.prototype.login = function() {
        return __awaiter(this, void 0, void 0, function() {
          var tokenReq, error_1;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _a.trys.push([ 0, 2, , 3 ]);
              return [ 4, this.getToken("12345678919") ];

             case 1:
              tokenReq = _a.sent();
              console.log("get token done", tokenReq.token);
              this.connectSocket(tokenReq.token);
              return [ 3, 3 ];

             case 2:
              error_1 = _a.sent();
              console.log("get token error", error_1);
              return [ 3, 3 ];

             case 3:
              return [ 2 ];
            }
          });
        });
      };
      API.prototype.getToken = function(deviceID) {
        return new Promise(function(resolve, reject) {
          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function() {
            if (4 == xhr.readyState) {
              if (xhr.status >= 200 && xhr.status < 400) {
                var responseObj = JSON.parse(xhr.responseText);
                return resolve(responseObj);
              }
              return reject(xhr.responseText);
            }
          };
          xhr.open("POST", "http://" + HOST + ":8000/v1/authdev", true);
          cc.sys.isNative && xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
          xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
          xhr.send(JSON.stringify({
            ID: deviceID
          }));
        });
      };
      API.prototype.connectSocket = function(token) {
        var _this = this;
        var TOKENKEY_STORE = "socketCluster.authToken";
        var authEngine;
        if (token) {
          authEngine = new auth_1.AuthEngine();
          authEngine.saveToken(TOKENKEY_STORE, token);
        }
        this.socket = socketCluster.connect({
          port: 8001,
          hostname: HOST,
          perMessageDeflate: true,
          codecEngine: scCodecMinBin,
          multiplex: false,
          autoReconnect: false
        });
        this.socket.on("error", function(err) {
          console.error("socket.on error", JSON.stringify(err));
        });
        this.socket.on("connect", function(status) {
          return __awaiter(_this, void 0, void 0, function() {
            var userInfo, authToken, e_1;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                _a.trys.push([ 0, 2, , 3 ]);
                console.log("Socket is connected", this.socket.authToken, this.socket.signedAuthToken, status);
                controller.ui.gotoHomeScene();
                return [ 4, this.sendGDPromise({
                  e: "regisClient",
                  device_info: "developer"
                }) ];

               case 1:
                userInfo = _a.sent();
                cc.log("User info:", userInfo);
                store.emit(store.key.UPDATE_USER_BALANCE, 0, {
                  delayTime: 0,
                  lerpRatio: 200
                });
                store.emit(store.key.UPDATE_USER_BALANCE, userInfo.coin, {
                  delayTime: 0,
                  lerpRatio: 50
                });
                store.emit(store.key.UPDATE_USER_LEVEL, userInfo.level, {
                  delayTime: 0,
                  lerpRatio: 50
                });
                if (status.isAuthenticated) {
                  authToken = this.socket.authToken;
                  this.subscribeChanel(authToken.uid);
                }
                return [ 3, 3 ];

               case 2:
                e_1 = _a.sent();
                cc.log(e_1);
                return [ 3, 3 ];

               case 3:
                return [ 2 ];
              }
            });
          });
        });
        this.socket.on("authenticate", function(signedAuthToken) {
          console.log("on.authenticate", signedAuthToken);
        });
        this.socket.on("deauthenticate", function(a, b, c) {
          console.log("deauthenticate", a, b, c);
        });
        this.socket.on("random", function(data) {
          console.log('Received "random" event with data: ' + data.number);
        });
      };
      API.prototype.subscribeChanel = function(uid) {
        var channelME = this.socket.subscribe("u:" + uid);
        channelME.unwatch();
        channelME.on("subscribeFail", function(err) {
          console.error("Failed to subscribe to the channelME channel due to error: " + err);
        });
        channelME.watch(function(data) {
          console.log("channelME channel message:", data);
          switch (data.type) {
           case api.key.SLOT_SPIN:
            var slotScene = Object.values(view.slot).find(function(item) {
              return item && item.gameId == data.spinResult.id;
            });
            slotScene && slotScene.SlotController.watchEvent(data);
            store.emit(store.key.UPDATE_USER_BALANCE, data.userInfo.coinBalance);
            var level = game.user.level;
            store.emit(store.key.UPDATE_USER_LEVEL, __assign({}, level, {
              currentExp: data.userInfo.exp
            }));
            break;

           case api.key.SLOT_BONUS_RESULT:
            game.user.balance = data.ccoin;
            if (view.screen.game) {
              var slotController = Object.values(view.slot).find(function(item) {
                return item && item.gameId == data.smid;
              });
              if (slotController) return;
            }
            store.emit(store.key.UPDATE_USER_BALANCE, data.ccoin);
            break;

           case api.key.LEVEL_UP:
            var rewards = data.rewards;
            var nextRewards = data.nextRewards;
            store.emit(store.key.UPDATE_USER_BALANCE, data.balance, {
              delayTime: 0,
              lerpRatio: 50
            });
            store.emit(store.key.UPDATE_USER_LEVEL, {
              currentExp: data.exp,
              currentLevel: data.level,
              requiredExp: data.requiredExp
            }, {
              delayTime: 0,
              lerpRatio: 50
            });
            var slot_1 = view.screen.slot;
            slot_1 && slot_1.SlotController.getStatusGame().then(function() {
              slot_1.SlotController.importData();
            });
          }
        });
      };
      API.prototype.registerClient = function() {
        this.sendGD({
          e: api.key.REGIS
        }, function(err, data) {
          if (err) return cc.log(err);
          cc.log(data);
        });
      };
      API.prototype.sendGD = function(data, cb) {
        this.socket.emit("GD", data, cb);
      };
      API.prototype.sendGDPromise = function(data) {
        var _this = this;
        return new Promise(function(res, rej) {
          var cb = function() {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) args[_i] = arguments[_i];
            if (args[0]) return rej(args[0]);
            return res(args[1]);
          };
          _this.socket.emit("GD", data, cb);
        });
      };
      API.prototype.onSceneChanged = function(sceneName) {
        cc.log("onSceneChanged", sceneName);
        this.socket.emit("clientreact", {
          data: sceneName || 1
        }, function(err, res) {
          console.log("clientreact result", err, res);
        });
      };
      return API;
    }();
    exports.default = new API();
    cc._RF.pop();
  }, {
    "./Define/DefineKey": "DefineKey",
    "./lib/auth": "auth",
    "sc-codec-min-bin": 37
  } ],
  AtributeStatic: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "150e3IRMttCLZc24LilVNrB", "AtributeStatic");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AtributeStatic = function(_super) {
      __extends(AtributeStatic, _super);
      function AtributeStatic() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      AtributeStatic_1 = AtributeStatic;
      AtributeStatic.prototype.setIdBetSelect = function(idBetSelect) {
        AtributeStatic_1.idBetSelect = idBetSelect;
      };
      var AtributeStatic_1;
      AtributeStatic.idBetSelect = null;
      AtributeStatic.bonusData = [];
      AtributeStatic.loop = 0;
      AtributeStatic.idJackpot = 0;
      AtributeStatic = AtributeStatic_1 = __decorate([ ccclass ], AtributeStatic);
      return AtributeStatic;
    }(cc.Component);
    exports.default = AtributeStatic;
    cc._RF.pop();
  }, {} ],
  AutoFitCanvas: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "47ca3UOMgpCWrc3zu85EsQe", "AutoFitCanvas");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var NewClass = function(_super) {
      __extends(NewClass, _super);
      function NewClass() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      NewClass.prototype.onLoad = function() {
        var realScreenSize = cc.view.getFrameSize();
        var realScreenRatio = realScreenSize.width / realScreenSize.height;
        var designScreenRatio = 1280 / 720;
        var canvas = this.node.getComponent(cc.Canvas);
        if (realScreenRatio < designScreenRatio) {
          canvas.fitHeight = false;
          canvas.fitWidth = true;
        } else {
          canvas.fitHeight = true;
          canvas.fitWidth = false;
        }
      };
      NewClass = __decorate([ ccclass ], NewClass);
      return NewClass;
    }(cc.Component);
    exports.default = NewClass;
    cc._RF.pop();
  }, {} ],
  BasePopup: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ab08aBub7hPBqf0+XKkL0BI", "BasePopup");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DefineType_1 = require("./Define/DefineType");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var BasePopup = function(_super) {
      __extends(BasePopup, _super);
      function BasePopup() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.popupName = "";
        _this.type = DefineType_1.POPUP_TYPE.NONE;
        _this.effect = DefineType_1.EFFECT_TYPE.BOUCE;
        _this.overlay = void 0;
        _this.popBg = void 0;
        _this.autoOpen = true;
        _this.allowClose = true;
        _this.autoRelease = true;
        _this.timeIn = .25;
        _this.timeOut = .12;
        _this.isOpen = false;
        _this.mask = void 0;
        _this.popup = void 0;
        _this.oriPos = void 0;
        _this.data = void 0;
        return _this;
      }
      BasePopup.prototype.onLoad = function() {
        this.mask = this.overlay || this.node.getChildByName("mask");
        this.popup = this.popBg || this.node.getChildByName("popup");
        this.oriPos = this.node.getPosition();
        this.popup || cc.warn("BasePopup", "Please add popup background in script for run");
        !this.popupName || (view.popup[this.popupName] = this);
        this.mask && (this.mask.active = true);
        this.type !== DefineType_1.POPUP_TYPE.NONE && (this.node.zIndex = this.type);
        this.type === DefineType_1.POPUP_TYPE.FORCE && (this.allowClose = false);
        if (!this.autoOpen) {
          util.game.hideNode(this.node);
          this.node.x = cc.winSize.width + this.oriPos.x;
        }
      };
      BasePopup.prototype.start = function() {
        this.autoOpen && this.onOpen();
      };
      BasePopup.prototype.init = function(data) {
        this.data = data;
      };
      BasePopup.prototype.reOpen = function(data) {
        this.init(data);
        this.onOpen();
      };
      BasePopup.prototype.onOpen = function(callback) {
        var _this = this;
        var _a = this, popup = _a.popup, mask = _a.mask, timeIn = _a.timeIn, node = _a.node, popupName = _a.popupName, type = _a.type, oriPos = _a.oriPos;
        if (!popup) {
          cc.warn("BasePopup", "Please add popup background in script for run");
          return;
        }
        util.game.showNode(node, type !== DefineType_1.POPUP_TYPE.NONE ? type : 1);
        node.x = oriPos.x;
        popup.opacity = 0;
        mask && (mask.opacity = 0);
        node.stopAllActions();
        popup.stopAllActions();
        mask && mask.stopAllActions();
        mask && mask.runAction(cc.fadeTo(timeIn, 125));
        var option = {
          time: timeIn,
          callback: function() {
            cc.log("BasePopup", "popup " + popupName + " open complete");
            _this.isOpen = true;
            callback && callback();
            _this.openCallback();
          }
        };
        var popEffect = effect.getEffectName(DefineType_1.EFFECT_KIND.IN, this.effect);
        popup.runAction(effect.get(popEffect, option));
      };
      BasePopup.prototype.onClose = function(callback) {
        var _this = this;
        var _a = this, popup = _a.popup, mask = _a.mask, popupName = _a.popupName, autoRelease = _a.autoRelease, timeOut = _a.timeOut, node = _a.node, oriPos = _a.oriPos;
        if (!popup) {
          cc.warn("BasePopup", "Please add popup background in script for run");
          return;
        }
        node.stopAllActions();
        popup.stopAllActions();
        mask && mask.stopAllActions();
        mask && mask.runAction(cc.fadeOut(timeOut));
        var option = {
          time: timeOut,
          callback: function() {
            cc.log("BasePopup", "popup " + popupName + " close complete");
            _this.isOpen = false;
            if (autoRelease) _this.onRelease(); else {
              util.game.hideNode(node);
              node.x = cc.winSize.width + oriPos.x;
            }
            callback && callback();
            _this.closeCallback();
          }
        };
        var popEffect = effect.getEffectName(DefineType_1.EFFECT_KIND.OUT, this.effect);
        popup.runAction(effect.get(popEffect, option));
      };
      BasePopup.prototype.openCallback = function() {};
      BasePopup.prototype.closeCallback = function() {};
      BasePopup.prototype.onClickClose = function(event) {
        if (!this.allowClose && event && ("btnClose" === event.currentTarget.name || event.currentTarget.name === this.mask.name)) {
          cc.log("BasePopup", "onClose", "popup do not allow close by close button or mask");
          return;
        }
        cc.log("BasePopup", "onClickClose");
        this.onClose();
      };
      BasePopup.prototype.onRelease = function() {
        var _a = this, node = _a.node, popupName = _a.popupName;
        node.destroy();
        !!popupName && view.popup[popupName] && delete view.popup[popupName];
      };
      __decorate([ property ], BasePopup.prototype, "popupName", void 0);
      __decorate([ property({
        type: cc.Enum(DefineType_1.POPUP_TYPE),
        displayName: "Popup Type"
      }) ], BasePopup.prototype, "type", void 0);
      __decorate([ property({
        type: cc.Enum(DefineType_1.EFFECT_TYPE)
      }) ], BasePopup.prototype, "effect", void 0);
      __decorate([ property({
        type: cc.Node
      }) ], BasePopup.prototype, "overlay", void 0);
      __decorate([ property({
        type: cc.Node,
        displayName: "Popup Background"
      }) ], BasePopup.prototype, "popBg", void 0);
      __decorate([ property ], BasePopup.prototype, "autoOpen", void 0);
      __decorate([ property ], BasePopup.prototype, "allowClose", void 0);
      __decorate([ property ], BasePopup.prototype, "autoRelease", void 0);
      __decorate([ property ], BasePopup.prototype, "timeIn", void 0);
      __decorate([ property ], BasePopup.prototype, "timeOut", void 0);
      BasePopup = __decorate([ ccclass ], BasePopup);
      return BasePopup;
    }(cc.Component);
    exports.default = BasePopup;
    cc._RF.pop();
  }, {
    "./Define/DefineType": "DefineType"
  } ],
  BottomBarController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2e8benZlMBIu42SLCLkIQOe", "BottomBarController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var BAR_TYPE;
    (function(BAR_TYPE) {
      BAR_TYPE["HOME"] = "HOME";
      BAR_TYPE["GAME"] = "GAME";
    })(BAR_TYPE || (BAR_TYPE = {}));
    var BottomBarController = function(_super) {
      __extends(BottomBarController, _super);
      function BottomBarController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.homeBar = void 0;
        _this.gameBar = void 0;
        _this.model = void 0;
        _this.barType = BAR_TYPE.HOME;
        return _this;
      }
      BottomBarController.prototype.onLoad = function() {
        this.homeBar = this.node.getChildByName("home");
        this.gameBar = this.node.getChildByName("game").getComponent("BottomGameBarController");
        this.model = this.node.getChildByName("model");
        this.updateBarView();
      };
      BottomBarController.prototype.lock = function() {
        this.model.active = true;
      };
      BottomBarController.prototype.unlock = function() {
        this.model.active = false;
      };
      BottomBarController.prototype.hide = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          _this.node.y = -360;
          _this.node.stopAllActions();
          _this.lock();
          _this.node.runAction(cc.sequence(cc.moveBy(.1, cc.v2(0, -_this.node.height)), cc.callFunc(function() {
            _this.unlock();
            res();
          })));
        });
      };
      BottomBarController.prototype.show = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          _this.node.y = -360 - _this.node.height;
          _this.node.stopAllActions();
          _this.lock();
          _this.node.runAction(cc.sequence(cc.moveBy(.2, cc.v2(0, _this.node.height)), cc.callFunc(function() {
            _this.unlock();
            res();
          })));
        });
      };
      BottomBarController.prototype.toggleEffect = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.hide() ];

             case 1:
              _a.sent();
              return [ 4, this.updateBarView() ];

             case 2:
              _a.sent();
              return [ 4, this.show() ];

             case 3:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      BottomBarController.prototype.updateBarView = function() {
        this.showNodeBar(this.barType === BAR_TYPE.HOME ? this.homeBar : this.gameBar.node);
        this.hiddenNodeBar(this.barType === BAR_TYPE.HOME ? this.gameBar.node : this.homeBar);
      };
      BottomBarController.prototype.showGameBar = function() {
        if (this.barType === BAR_TYPE.GAME) return;
        this.barType = BAR_TYPE.GAME;
        this.toggleEffect();
      };
      BottomBarController.prototype.showHomeBar = function() {
        if (this.barType === BAR_TYPE.HOME) return;
        this.barType = BAR_TYPE.HOME;
        this.toggleEffect();
      };
      BottomBarController.prototype.hiddenNodeBar = function(bar) {
        bar.x = -bar.x;
        bar.opacity = 0;
      };
      BottomBarController.prototype.showNodeBar = function(bar) {
        bar.x = 0;
        bar.opacity = 255;
      };
      BottomBarController = __decorate([ ccclass ], BottomBarController);
      return BottomBarController;
    }(cc.Component);
    exports.default = BottomBarController;
    cc._RF.pop();
  }, {} ],
  BottomGameBarController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "87074QkXOxOTI12esJd2SQF", "BottomGameBarController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var BottomGameBarController = function(_super) {
      __extends(BottomGameBarController, _super);
      function BottomGameBarController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.winLabel = null;
        _this.betLabel = null;
        _this.statusLabel = null;
        _this.spinButton = null;
        _this.holdTime = 500;
        _this.autoBet = null;
        _this.normalSpin = null;
        _this.stopAutoSpin = null;
        _this.stopNormalSpin = null;
        _this.bottomActive = [];
        _this.bottomLock = [];
        _this.spinSprite = null;
        _this.autoSpinLabel = null;
        _this.lerpRatio = 10;
        _this.winAmount = 0;
        _this.currentBet = 0;
        _this.betArray = [];
        _this.isUpdattingWinAmount = false;
        _this._tempWin = 0;
        _this._isPress = false;
        _this._setTimeout = null;
        _this._isShow = false;
        _this.container = null;
        return _this;
      }
      BottomGameBarController.prototype.onLoad = function() {
        this.initialize();
        this.resetWinAmount();
        this.addSpinButtonEvent();
      };
      BottomGameBarController.prototype.initialize = function() {
        this.container = this.node.getChildByName("container");
        this.container.getComponent(cc.Widget).updateAlignment();
        this.container.getComponent(cc.Widget).enabled = false;
        this.spinSprite = this.spinButton.node.getComponent(cc.Sprite);
        var status = this.container.getChildByName("status");
        this.statusLabel = status.getChildByName("lbStatus").getComponent(cc.Label);
        this.autoSpinLabel = this.spinButton.node.getComponentInChildren(cc.Label);
      };
      BottomGameBarController.prototype.resetWinAmount = function() {
        this.winLabel.string = "0";
      };
      BottomGameBarController.prototype.addSpinButtonEvent = function() {
        var _this = this;
        this.spinButton.node.on(cc.Node.EventType.TOUCH_START, function(e) {
          _this._isPress = true;
          _this._setTimeout = setTimeout(function() {
            if (!_this.autoBet.active) {
              _this.showAutoBet();
              _this.autoBet.zIndex = -1;
            }
            _this._isPress = false;
          }, _this.holdTime);
        });
        this.spinButton.node.on(cc.Node.EventType.TOUCH_END, function(e) {
          if (_this._isPress) {
            view.screen.game.onClickSpin();
            _this.bottomBarEvent(define.type.BottomBar.lock);
            clearTimeout(_this._setTimeout);
            _this._isPress = false;
          }
        });
        this.spinButton.node.on(cc.Node.EventType.TOUCH_CANCEL, function(e) {
          _this._isPress = false;
          clearTimeout(_this._setTimeout);
        });
      };
      BottomGameBarController.prototype.bottomBarEvent = function(type) {
        var _this = this;
        var betArea = this.betLabel.node.parent;
        var nameBottomBar = [ "btn-bet-down", "btn-bet-up" ];
        var maxBet = this.node.getChildByName("container").getChildByName("btn-maxbet");
        switch (type) {
         case define.type.BottomBar.active:
          nameBottomBar.forEach(function(item, index) {
            betArea.getChildByName(item).getComponent(cc.Sprite).spriteFrame = _this.bottomActive[index];
            betArea.getChildByName(item).getComponent(cc.Button).interactable = true;
          });
          maxBet.getComponent(cc.Sprite).spriteFrame = this.bottomActive[2];
          maxBet.getComponent(cc.Button).interactable = true;
          this.updateSpinFrame(define.type.SpinButton.normal);
          break;

         case define.type.BottomBar.lock:
          nameBottomBar.forEach(function(item, index) {
            betArea.getChildByName(item).getComponent(cc.Sprite).spriteFrame = _this.bottomLock[index];
            betArea.getChildByName(item).getComponent(cc.Button).interactable = false;
          });
          maxBet.getComponent(cc.Sprite).spriteFrame = this.bottomLock[2];
          maxBet.getComponent(cc.Button).interactable = false;
          this.updateSpinFrame(define.type.SpinButton.stopNormalSpin);
        }
      };
      BottomGameBarController.prototype.displayBetLabel = function() {
        var time = .15;
        this.betLabel.node.runAction(cc.scaleTo(time, 1.05));
        this.betLabel.string = util.string.formatMoney(this.currentBet);
        this.betLabel.node.runAction(cc.sequence(cc.delayTime(time), cc.scaleTo(time, 1)));
      };
      BottomGameBarController.prototype.autoSpin = function(e, times) {
        var _this = this;
        view.screen.slot.SlotController.autoSpin(Number(times));
        this.updateSpinFrame(define.type.SpinButton.stopAutoSpin);
        setTimeout(function() {
          return _this.hideAutoBet();
        }, 100);
      };
      BottomGameBarController.prototype.showAutoBet = function() {
        this.autoBet.active = true;
        this.autoBet.runAction(cc.moveBy(.4, cc.v2(0, this.autoBet.height - 15)));
        util.game.showNode(this.autoBet);
      };
      BottomGameBarController.prototype.hideAutoBet = function() {
        var _this = this;
        util.game.hideNode(this.autoBet);
        this.autoBet.runAction(cc.moveBy(.1, cc.v2(0, 15 - this.autoBet.height)));
        setTimeout(function() {
          _this.autoBet.active = false;
        }, 300);
      };
      BottomGameBarController.prototype.updateInfo = function() {
        var currentSlot = view.screen.slot || null;
        if (!currentSlot) return;
        this.betArray = currentSlot.SlotController.betArray;
        this.currentBet = currentSlot.SlotController.currentBet;
        this.displayBetLabel();
        this.winAmount = currentSlot.SlotController.lastWin;
        this.winLabel.string = this.winAmount.toString();
      };
      BottomGameBarController.prototype.changeBetAmount = function(e, i) {
        cc.log("BottomGameBarController", "changeBetAmount");
        i = Number(i);
        var index = this.betArray.indexOf(this.currentBet);
        0 == index && -1 == i ? this.currentBet = this.betArray[this.betArray.length - 1] : index == this.betArray.length - 1 && 1 == i ? this.currentBet = this.betArray[0] : this.currentBet = index < 0 ? this.betArray[0] : this.betArray[index + i];
        view.screen.slot && view.screen.slot.SlotController.updateCurrentBet(this.currentBet);
        this.displayBetLabel();
      };
      BottomGameBarController.prototype.updateSpinFrame = function(type) {
        switch (type) {
         case define.type.SpinButton.normal:
          this.spinSprite.spriteFrame = this.normalSpin;
          this.autoSpinLabel.node.active = false;
          break;

         case define.type.SpinButton.stopAutoSpin:
          this.spinSprite.spriteFrame = this.stopAutoSpin;
          this.autoSpinLabel.node.active = true;
          break;

         case define.type.SpinButton.stopNormalSpin:
          this.spinSprite.spriteFrame = this.stopNormalSpin;
        }
      };
      BottomGameBarController.prototype.updateAutoSpinTime = function(spinTimes) {
        try {
          this.autoSpinLabel.string = spinTimes < 0 ? "BONUS" : spinTimes.toString();
          this.updateSpinFrame(define.type.SpinButton.stopAutoSpin);
        } catch (e) {
          cc.log(e);
        }
      };
      BottomGameBarController.prototype.maxBetAmount = function() {
        this.currentBet = this.betArray[this.betArray.length - 1];
        view.screen.slot && view.screen.slot.SlotController.updateCurrentBet(this.currentBet);
        this.displayBetLabel();
      };
      BottomGameBarController.prototype.updateWinAmount = function(num) {
        this.winAmount = num;
        this.isUpdattingWinAmount = true;
      };
      BottomGameBarController.prototype.payTabel = function() {
        view.screen.slot.SlotController.showPayTable();
      };
      BottomGameBarController.prototype.update = function(dt) {
        this._updateWinAmount(dt);
      };
      BottomGameBarController.prototype._updateWinAmount = function(dt) {
        if (this.isUpdattingWinAmount) {
          var delta = this.winAmount - this._tempWin;
          this._tempWin = delta * this.lerpRatio * dt + this._tempWin;
          this.winLabel.string = util.string.formatMoney(this._tempWin);
          if (this.winAmount - this._tempWin < 1) {
            this.winLabel.string = util.string.formatMoney(this.winAmount);
            this.isUpdattingWinAmount = false;
            this._tempWin = 0;
          }
        }
      };
      BottomGameBarController.prototype.updateStatus = function(status) {
        var label = this.statusLabel;
        var showAnim = false;
        if (status.freeSpin) {
          showAnim = !label.string.includes("Freespin");
          label.string = "Freespin: " + status.freeSpin;
        } else {
          showAnim = !label.string.includes("Win");
          label.string = "Win";
        }
        if (showAnim) {
          var oriPos = cc.v2(0, 8);
          label.node.stopAllActions();
          label.node.runAction(cc.sequence(cc.moveBy(.2, cc.v2(0, -100)), cc.moveTo(.3, oriPos)));
        }
      };
      __decorate([ property(cc.Label) ], BottomGameBarController.prototype, "winLabel", void 0);
      __decorate([ property(cc.Label) ], BottomGameBarController.prototype, "betLabel", void 0);
      __decorate([ property(cc.Label) ], BottomGameBarController.prototype, "statusLabel", void 0);
      __decorate([ property(cc.Button) ], BottomGameBarController.prototype, "spinButton", void 0);
      __decorate([ property(cc.Integer) ], BottomGameBarController.prototype, "holdTime", void 0);
      __decorate([ property(cc.Node) ], BottomGameBarController.prototype, "autoBet", void 0);
      __decorate([ property(cc.SpriteFrame) ], BottomGameBarController.prototype, "normalSpin", void 0);
      __decorate([ property(cc.SpriteFrame) ], BottomGameBarController.prototype, "stopAutoSpin", void 0);
      __decorate([ property(cc.SpriteFrame) ], BottomGameBarController.prototype, "stopNormalSpin", void 0);
      __decorate([ property(cc.SpriteFrame) ], BottomGameBarController.prototype, "bottomActive", void 0);
      __decorate([ property(cc.SpriteFrame) ], BottomGameBarController.prototype, "bottomLock", void 0);
      BottomGameBarController = __decorate([ ccclass ], BottomGameBarController);
      return BottomGameBarController;
    }(cc.Component);
    exports.default = BottomGameBarController;
    cc._RF.pop();
  }, {} ],
  BottomHomeBarController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "07d6f5ZTGZNTqjNaetpjylt", "BottomHomeBarController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var BottomHomeBarController = function(_super) {
      __extends(BottomHomeBarController, _super);
      function BottomHomeBarController() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      BottomHomeBarController = __decorate([ ccclass ], BottomHomeBarController);
      return BottomHomeBarController;
    }(cc.Component);
    exports.default = BottomHomeBarController;
    cc._RF.pop();
  }, {} ],
  CoinLabel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "38713mHWmRFML2UHJo56Vtc", "CoinLabel");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CoinLabel = function() {
      function CoinLabel(coinLabel, lerpRatio, prefix, fps) {
        this.coinLabel = null;
        this.lerpRatio = 10;
        this.userBalance = 0;
        this._tempUserBalance = 0;
        this.isUpdattingBalance = false;
        this.prefix = "";
        this.fps = 30;
        this.coinLabel = coinLabel;
        lerpRatio && (this.lerpRatio = lerpRatio);
        prefix && (this.prefix = prefix);
        fps && (this.fps = fps);
      }
      CoinLabel.prototype.updateUserBalance = function(num, option) {
        return __awaiter(this, void 0, void 0, function() {
          var delta;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!option) return [ 3, 3 ];
              if (!option.delayTime) return [ 3, 2 ];
              return [ 4, util.game.delay(option.delayTime, this.coinLabel.node).catch(function(e) {}) ];

             case 1:
              _a.sent();
              _a.label = 2;

             case 2:
              option.lerpRatio && (this.lerpRatio = option.lerpRatio);
              option.prefix && (this.prefix = option.prefix);
              _a.label = 3;

             case 3:
              if (num) {
                this._tempUserBalance = this.userBalance;
                this.userBalance = num;
                this.isUpdattingBalance = true;
              }
              delta = this.userBalance - this._tempUserBalance;
              this._tempUserBalance = delta * this.lerpRatio / this.fps + this._tempUserBalance;
              this.coinLabel.string = this.prefix + util.string.formatMoney(this._tempUserBalance);
              if (Math.abs(this.userBalance - this._tempUserBalance) < 1) {
                this.coinLabel.string = this.prefix + util.string.formatMoney(this.userBalance);
                return [ 2, true ];
              }
              setTimeout(function() {
                if (!_this.coinLabel.node || !_this.coinLabel.node.isValid) return;
                _this.updateUserBalance();
              }, 1e3 / this.fps);
              return [ 2 ];
            }
          });
        });
      };
      return CoinLabel;
    }();
    exports.default = CoinLabel;
    cc._RF.pop();
  }, {} ],
  DefineColor: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "23ce2zcWh1E2rVuEE6tWton", "DefineColor");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.Green = {
      r: 137,
      g: 239,
      b: 149,
      a: 255
    };
    exports.Yellow = {
      r: 219,
      g: 176,
      b: 23,
      a: 255
    };
    exports.Orange = {
      r: 255,
      g: 192,
      b: 37,
      a: 255
    };
    exports.Red = {
      r: 188,
      g: 12,
      b: 22,
      a: 255
    };
    exports.Blue = {
      r: 6,
      g: 154,
      b: 220,
      a: 255
    };
    exports.Violet = {
      r: 147,
      g: 76,
      b: 147,
      a: 255
    };
    cc._RF.pop();
  }, {} ],
  DefineInterface: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ab72a0pQsxG05X00Uj6BcTR", "DefineInterface");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    cc._RF.pop();
  }, {} ],
  DefineKey: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3c8d9kaaFNEJ4e05E2NMq5m", "DefineKey");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.CONFIG_SOUND = "CONFIG_SOUND";
    exports.CONFIG_MUSIC = "CONFIG_MUSIC";
    exports.store = {
      UPDATE_USER_BALANCE: "UPDATE_USER_BALANCE",
      UPDATE_USER_LEVEL: "UPDATE_USER_LEVEL"
    };
    exports.api = {
      REGIS: "regisClient",
      SMINFO: "sminfo",
      SLOT_SPIN: "slotspin",
      LEVEL_UP: "levelUp",
      BUY_COIN: "buycoin",
      SLOT_GET_BONUS: "slotgetbonus",
      SLOT_BONUS_RESULT: "slotbonus",
      SLOT_FREE_REEL_GAME: "spinFreeReelGameWheel"
    };
    cc._RF.pop();
  }, {} ],
  DefineType: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "32547KClMBKu7TJmMCb7CuI", "DefineType");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DefineZIndex_1 = require("./DefineZIndex");
    var Device;
    (function(Device) {
      Device[Device["NORMAL"] = 0] = "NORMAL";
      Device[Device["IPAD"] = 1] = "IPAD";
      Device[Device["IPHONEX"] = 2] = "IPHONEX";
    })(Device = exports.Device || (exports.Device = {}));
    var SpinButton;
    (function(SpinButton) {
      SpinButton[SpinButton["normal"] = 1] = "normal";
      SpinButton[SpinButton["stopAutoSpin"] = 2] = "stopAutoSpin";
      SpinButton[SpinButton["stopNormalSpin"] = 3] = "stopNormalSpin";
    })(SpinButton = exports.SpinButton || (exports.SpinButton = {}));
    var BottomBar;
    (function(BottomBar) {
      BottomBar[BottomBar["active"] = 1] = "active";
      BottomBar[BottomBar["lock"] = -1] = "lock";
    })(BottomBar = exports.BottomBar || (exports.BottomBar = {}));
    var DIRECTION;
    (function(DIRECTION) {
      DIRECTION["LEFT"] = "LEFT";
      DIRECTION["RIGHT"] = "RIGHT";
      DIRECTION["UP"] = "UP";
      DIRECTION["DOWN"] = "DOWN";
    })(DIRECTION = exports.DIRECTION || (exports.DIRECTION = {}));
    var EFFECT;
    (function(EFFECT) {
      EFFECT[EFFECT["BOUCE_IN"] = 1] = "BOUCE_IN";
      EFFECT[EFFECT["BOUCE_OUT"] = 2] = "BOUCE_OUT";
      EFFECT[EFFECT["MOVE_IN_BY_LEFT"] = 3] = "MOVE_IN_BY_LEFT";
      EFFECT[EFFECT["MOVE_IN_BY_RIGHT"] = 4] = "MOVE_IN_BY_RIGHT";
      EFFECT[EFFECT["MOVE_IN_BY_UP"] = 5] = "MOVE_IN_BY_UP";
      EFFECT[EFFECT["MOVE_IN_BY_DOWN"] = 6] = "MOVE_IN_BY_DOWN";
      EFFECT[EFFECT["MOVE_OUT_BY_LEFT"] = 7] = "MOVE_OUT_BY_LEFT";
      EFFECT[EFFECT["MOVE_OUT_BY_RIGHT"] = 8] = "MOVE_OUT_BY_RIGHT";
      EFFECT[EFFECT["MOVE_OUT_BY_UP"] = 9] = "MOVE_OUT_BY_UP";
      EFFECT[EFFECT["MOVE_OUT_BY_DOWN"] = 10] = "MOVE_OUT_BY_DOWN";
    })(EFFECT = exports.EFFECT || (exports.EFFECT = {}));
    var EFFECT_KIND;
    (function(EFFECT_KIND) {
      EFFECT_KIND[EFFECT_KIND["IN"] = 1] = "IN";
      EFFECT_KIND[EFFECT_KIND["OUT"] = 2] = "OUT";
    })(EFFECT_KIND = exports.EFFECT_KIND || (exports.EFFECT_KIND = {}));
    var EFFECT_TYPE;
    (function(EFFECT_TYPE) {
      EFFECT_TYPE[EFFECT_TYPE["BOUCE"] = 1] = "BOUCE";
      EFFECT_TYPE[EFFECT_TYPE["MOVE_BY_LEFT"] = 2] = "MOVE_BY_LEFT";
      EFFECT_TYPE[EFFECT_TYPE["MOVE_BY_RIGHT"] = 3] = "MOVE_BY_RIGHT";
      EFFECT_TYPE[EFFECT_TYPE["MOVE_BY_UP"] = 4] = "MOVE_BY_UP";
      EFFECT_TYPE[EFFECT_TYPE["MOVE_BY_DOWN"] = 5] = "MOVE_BY_DOWN";
    })(EFFECT_TYPE = exports.EFFECT_TYPE || (exports.EFFECT_TYPE = {}));
    var POPUP_TYPE;
    (function(POPUP_TYPE) {
      POPUP_TYPE[POPUP_TYPE["NONE"] = 0] = "NONE";
      POPUP_TYPE[POPUP_TYPE["NORMAL"] = DefineZIndex_1.POPUP] = "NORMAL";
      POPUP_TYPE[POPUP_TYPE["HIGH"] = DefineZIndex_1.HIGH_POPUP] = "HIGH";
      POPUP_TYPE[POPUP_TYPE["HIGHEST"] = DefineZIndex_1.HIGHEST_POPUP] = "HIGHEST";
      POPUP_TYPE[POPUP_TYPE["FORCE"] = DefineZIndex_1.FORCE_POPUP] = "FORCE";
    })(POPUP_TYPE = exports.POPUP_TYPE || (exports.POPUP_TYPE = {}));
    var WIN_TYPE;
    (function(WIN_TYPE) {
      WIN_TYPE[WIN_TYPE["NULL"] = 0] = "NULL";
      WIN_TYPE[WIN_TYPE["NORMAL"] = 1] = "NORMAL";
      WIN_TYPE[WIN_TYPE["BIGWIN"] = 2] = "BIGWIN";
      WIN_TYPE[WIN_TYPE["MEGAWIN"] = 3] = "MEGAWIN";
    })(WIN_TYPE = exports.WIN_TYPE || (exports.WIN_TYPE = {}));
    cc._RF.pop();
  }, {
    "./DefineZIndex": "DefineZIndex"
  } ],
  DefineZIndex: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "47bf7Wr5uBLNIx61CJzDgpW", "DefineZIndex");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.HOME = 1;
    exports.GAME = 2;
    exports.MINIGAME = 3;
    exports.POPUP = 4;
    exports.MULTI = 5;
    exports.BAR = 6;
    exports.HIGH_POPUP = 7;
    exports.HIGHEST_POPUP = 8;
    exports.MODEL = 9;
    exports.FORCE_POPUP = 10;
    cc._RF.pop();
  }, {} ],
  DownloadCtr: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "02850mIi71MA6l6R9T/3e4h", "DownloadCtr");
    "use strict";
    cc.Class({
      extends: cc.Component,
      properties: {
        label: cc.Label,
        sprite: cc.Sprite,
        imgUrl: "http://www.cocos.com/wp-content/themes/cocos/img/download1.png",
        txtUrl: "https://raw.githubusercontent.com/cocos-creator/tutorial-dark-slash/master/LICENSE.md",
        tempImgUrl: "http://www.cocos.com/wp-content/uploads/2018/03/%E9%BB%98%E8%AE%A4%E6%A0%87%E9%A2%98_%E5%85%AC%E4%BC%97%E5%8F%B7%E5%BA%95%E9%83%A8%E4%BA%8C%E7%BB%B4%E7%A0%81_2018.03.08.png",
        audioUrl: "http://tools.itharbors.com/christmas/res/sounds/ss.mp3",
        _downloader: null,
        _imgTask: null,
        _txtTask: null,
        _audioTask: null,
        _storagePath: "",
        _inited: false
      },
      onLoad: function onLoad() {
        true;
        this.label.string = "Downloader is a NATIVE ONLY feature.";
        return;
      },
      onSucceed: function onSucceed(task) {
        var _this = this;
        -1 !== this._audioID && cc.audioEngine.stop(this._audioID);
        switch (task.requestURL) {
         case this.imgUrl:
          cc.loader.load(task.storagePath, function(err, tex) {
            _this.sprite.spriteFrame = new cc.SpriteFrame(tex);
            _this.sprite.node.active = true;
            _this.label.node.active = false;
          });
          break;

         case this.txtUrl:
          var content = jsb.fileUtils.getStringFromFile(task.storagePath);
          this.sprite.node.active = false;
          this.label.node.active = true;
          this.label.string = content.substr(0, 350);
          break;

         case this.audioUrl:
          this.sprite.node.active = false;
          this.label.node.active = true;
          this.label.string = "Audio Download Complete.";
          cc.loader.load(task.storagePath, function(err, clip) {
            _this._audioID = cc.audioEngine.play(clip);
          });
        }
      },
      onProgress: function onProgress(task, bytesReceived, totalBytesReceived, totalBytesExpected) {},
      onError: function onError(task, errorCode, errorCodeInternal, errorStr) {
        this.sprite.node.active = false;
        this.label.node.active = true;
        this.label.string = "Failed to download file (" + task.requestURL + "): " + errorStr + "(" + errorCode + ")";
      },
      downloadImg: function downloadImg() {
        if (!this.imgUrl || !this._inited) return;
        this.sprite.node.active = false;
        this.label.node.active = true;
        this.label.string = "Downloading image";
        this._imgTask = this._downloader.createDownloadFileTask(this.imgUrl, this._storagePath + "download1.png");
      },
      loadImg: function loadImg() {
        var _this2 = this;
        if (!this.tempImgUrl || !this._inited) return;
        cc.loader.load(this.tempImgUrl, function(error, tex) {
          if (error) console.log("Load remote image failed: " + error); else {
            _this2.sprite.spriteFrame = new cc.SpriteFrame(tex);
            _this2.sprite.node.active = true;
            _this2.label.node.active = false;
            cc.audioEngine.stop(_this2._audioID);
          }
        });
      },
      loadSource: function loadSource(url) {
        return new Promise(function(resolve, reject) {
          cc.loader.load(url, function(err, texture) {
            if (err) {
              cc.log(err);
              return reject(err);
            }
            return resolve(texture);
          });
        });
      },
      loadImgFromStorage: function loadImgFromStorage() {
        var _this3 = this;
        var filePath = this._storagePath + "download1.png";
        cc.log(jsb.fileUtils.isFileExist(filePath));
        this.loadSource(filePath).then(function(sprite) {
          cc.log(sprite);
          _this3.sprite.spriteFrame = new cc.SpriteFrame(sprite);
        }).catch(function(err) {
          cc.log("error loading banner", JSON.stringify(err));
        });
      },
      downloadTxt: function downloadTxt() {
        if (!this.txtUrl || !this._inited) return;
        this.label.node.active = true;
        this.sprite.node.active = false;
        this.label.string = "Downloading Txt";
        this._txtTask = this._downloader.createDownloadFileTask(this.txtUrl, this._storagePath + "imagine.txt");
      },
      downloadAudio: function downloadAudio() {
        if (!this.audioUrl || !this._inited) return;
        this.sprite.node.active = false;
        this.label.node.active = true;
        this.label.string = "Downloading Audio";
        this._audioTask = this._downloader.createDownloadFileTask(this.audioUrl, this._storagePath + "audioTest.mp3");
      },
      onDisable: function onDisable() {
        cc.audioEngine.stop(this._audioID);
      }
    });
    cc._RF.pop();
  }, {} ],
  Effect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1085fUXxh9AOLb35Gkb8Rog", "Effect");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DefineType_1 = require("./Define/DefineType");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Effect = function(_super) {
      __extends(Effect, _super);
      function Effect() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      Effect_1 = Effect;
      Effect.ins = function() {
        this.instance || (this.instance = new Effect_1());
        return this.instance;
      };
      Effect.prototype.get = function(name, option) {
        option = option || {};
        switch (name) {
         case DefineType_1.EFFECT.BOUCE_IN:
          return this.bouceIn(option);

         case DefineType_1.EFFECT.BOUCE_OUT:
          return this.bouceOut(option);

         case DefineType_1.EFFECT.MOVE_IN_BY_DOWN:
          option.direction = DefineType_1.DIRECTION.DOWN;
          return this.moveInBy(option);

         case DefineType_1.EFFECT.MOVE_IN_BY_UP:
          option.direction = DefineType_1.DIRECTION.UP;
          return this.moveInBy(option);

         case DefineType_1.EFFECT.MOVE_IN_BY_LEFT:
          option.direction = DefineType_1.DIRECTION.LEFT;
          return this.moveInBy(option);

         case DefineType_1.EFFECT.MOVE_IN_BY_RIGHT:
          option.direction = DefineType_1.DIRECTION.RIGHT;
          return this.moveInBy(option);

         case DefineType_1.EFFECT.MOVE_OUT_BY_DOWN:
          option.direction = DefineType_1.DIRECTION.DOWN;
          return this.moveOutBy(option);

         case DefineType_1.EFFECT.MOVE_OUT_BY_UP:
          option.direction = DefineType_1.DIRECTION.UP;
          return this.moveOutBy(option);

         case DefineType_1.EFFECT.MOVE_OUT_BY_LEFT:
          option.direction = DefineType_1.DIRECTION.LEFT;
          return this.moveOutBy(option);

         case DefineType_1.EFFECT.MOVE_OUT_BY_RIGHT:
          option.direction = DefineType_1.DIRECTION.RIGHT;
          return this.moveOutBy(option);
        }
      };
      Effect.prototype.getEffectName = function(kind, type) {
        if (kind === DefineType_1.EFFECT_KIND.IN) switch (type) {
         case DefineType_1.EFFECT_TYPE.BOUCE:
          return DefineType_1.EFFECT.BOUCE_IN;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_DOWN:
          return DefineType_1.EFFECT.MOVE_IN_BY_DOWN;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_UP:
          return DefineType_1.EFFECT.MOVE_IN_BY_UP;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_LEFT:
          return DefineType_1.EFFECT.MOVE_IN_BY_LEFT;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_RIGHT:
          return DefineType_1.EFFECT.MOVE_IN_BY_RIGHT;

         default:
          return DefineType_1.EFFECT.BOUCE_IN;
        } else switch (type) {
         case DefineType_1.EFFECT_TYPE.BOUCE:
          return DefineType_1.EFFECT.BOUCE_OUT;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_DOWN:
          return DefineType_1.EFFECT.MOVE_OUT_BY_DOWN;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_UP:
          return DefineType_1.EFFECT.MOVE_OUT_BY_UP;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_LEFT:
          return DefineType_1.EFFECT.MOVE_OUT_BY_LEFT;

         case DefineType_1.EFFECT_TYPE.MOVE_BY_RIGHT:
          return DefineType_1.EFFECT.MOVE_OUT_BY_RIGHT;

         default:
          return DefineType_1.EFFECT.BOUCE_OUT;
        }
      };
      Effect.prototype.bouceIn = function(option) {
        var time = option.time, scale = option.scale, easing = option.easing, callback = option.callback;
        time = !time ? .25 : time;
        scale = !scale ? 1 : scale;
        easing = "undefined" === typeof easing || easing;
        var pre = cc.spawn(cc.fadeOut(0), cc.scaleTo(0, 0));
        var effect = cc.spawn(cc.fadeIn(time), easing ? cc.scaleTo(time, scale).easing(cc.easeBackOut()) : cc.scaleTo(time, scale));
        var call = cc.callFunc(function() {
          callback && callback();
        });
        return cc.sequence(pre, effect, call);
      };
      Effect.prototype.bouceOut = function(option) {
        var time = option.time, scale = option.scale, easing = option.easing, callback = option.callback;
        time = !time ? .15 : time;
        scale = !scale ? 0 : scale;
        easing = "undefined" === typeof easing || easing;
        var effect = cc.spawn(cc.fadeOut(time), easing ? cc.scaleTo(time, scale).easing(cc.easeBackIn()) : cc.scaleTo(time, scale));
        var call = cc.callFunc(function() {
          callback && callback();
        });
        return cc.sequence(effect, call);
      };
      Effect.prototype.moveInBy = function(option) {
        var time = option.time, direction = option.direction, distance = option.distance, easing = option.easing, callback = option.callback;
        time = !time ? .15 : time;
        direction = !direction ? DefineType_1.DIRECTION.LEFT : direction;
        distance = !distance ? 200 : distance;
        easing = "undefined" === typeof easing || easing;
        var vec = void 0;
        direction == DefineType_1.DIRECTION.LEFT && (vec = cc.v2(distance, 0));
        direction == DefineType_1.DIRECTION.RIGHT && (vec = cc.v2(-distance, 0));
        direction == DefineType_1.DIRECTION.UP && (vec = cc.v2(0, -distance));
        direction == DefineType_1.DIRECTION.DOWN && (vec = cc.v2(0, distance));
        var pre = cc.spawn(cc.fadeOut(0), cc.moveBy(0, -vec.x, -vec.y));
        var effect = cc.spawn(cc.fadeIn(time), easing ? cc.moveBy(time, vec.x, vec.y).easing(cc.easeBackOut()) : cc.moveBy(time, vec.x, vec.y));
        var call = cc.callFunc(function() {
          callback && callback();
        });
        return cc.sequence(pre, effect, call);
      };
      Effect.prototype.moveOutBy = function(option) {
        var time = option.time, direction = option.direction, distance = option.distance, easing = option.easing, callback = option.callback;
        time = !time ? .15 : time;
        direction = !direction ? DefineType_1.DIRECTION.LEFT : direction;
        distance = !distance ? 200 : distance;
        easing = "undefined" === typeof easing || easing;
        var vec = void 0;
        direction == DefineType_1.DIRECTION.LEFT && (vec = cc.v2(-distance, 0));
        direction == DefineType_1.DIRECTION.RIGHT && (vec = cc.v2(distance, 0));
        direction == DefineType_1.DIRECTION.UP && (vec = cc.v2(0, distance));
        direction == DefineType_1.DIRECTION.DOWN && (vec = cc.v2(0, -distance));
        var effect = cc.spawn(cc.fadeOut(time), easing ? cc.moveBy(time, vec.x, vec.y).easing(cc.easeBackIn()) : cc.moveBy(time, vec.x, vec.y));
        var end = cc.moveBy(0, -vec.x, -vec.y);
        var call = cc.callFunc(function() {
          callback && callback();
        });
        return cc.sequence(effect, end, call);
      };
      var Effect_1;
      Effect.instance = void 0;
      Effect = Effect_1 = __decorate([ ccclass ], Effect);
      return Effect;
    }(cc.Component);
    exports.default = Effect;
    cc._RF.pop();
  }, {
    "./Define/DefineType": "DefineType"
  } ],
  GameConfig: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3cd21EwkSRDvL7ZGEzj9bOb", "GameConfig");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var GameConfig = function() {
      function GameConfig() {}
      GameConfig.convertPayLineForServer = function(array) {
        return array.map(function(item) {
          return item.map(function(item, index) {
            return 5 * item + index;
          });
        });
      };
      GameConfig.GAME_SIZE = {
        1: [ 3, 3, 3, 3, 3 ],
        2: [ 4, 4, 4, 4, 4 ],
        3: [ 4, 4, 4, 4, 4, 4 ],
        4: [ 1, 3, 5, 3, 1 ],
        5: [ 2, 3, 4, 3, 2 ]
      };
      GameConfig.PAY_LINES = {
        9: [ [ 1, 1, 1, 1, 1 ], [ 0, 0, 0, 0, 0 ], [ 2, 2, 2, 2, 2 ], [ 0, 1, 2, 1, 0 ], [ 2, 1, 0, 1, 2 ], [ 1, 0, 0, 0, 1 ], [ 1, 2, 2, 2, 1 ], [ 0, 0, 1, 2, 2 ], [ 2, 2, 1, 0, 0 ] ],
        20: [ [ 1, 1, 1, 1, 1 ], [ 0, 0, 0, 0, 0 ], [ 2, 2, 2, 2, 2 ], [ 0, 1, 2, 1, 0 ], [ 2, 1, 0, 1, 2 ], [ 1, 0, 0, 0, 1 ], [ 1, 2, 2, 2, 1 ], [ 0, 0, 1, 2, 2 ], [ 2, 2, 1, 0, 0 ], [ 1, 2, 1, 0, 1 ], [ 1, 0, 1, 2, 1 ], [ 0, 1, 1, 1, 0 ], [ 2, 1, 1, 1, 2 ], [ 0, 1, 0, 1, 0 ], [ 2, 1, 2, 1, 2 ], [ 1, 1, 0, 1, 1 ], [ 1, 1, 2, 1, 1 ], [ 0, 0, 2, 0, 0 ], [ 2, 2, 0, 2, 2 ], [ 0, 2, 2, 2, 0 ] ],
        25: [ [ 1, 1, 1, 1, 1 ], [ 0, 0, 0, 0, 0 ], [ 2, 2, 2, 2, 2 ], [ 0, 1, 2, 1, 0 ], [ 2, 1, 0, 1, 2 ], [ 1, 0, 1, 2, 1 ], [ 1, 2, 1, 0, 1 ], [ 0, 0, 1, 2, 2 ], [ 2, 2, 1, 0, 0 ], [ 0, 1, 0, 1, 0 ], [ 2, 1, 2, 1, 2 ], [ 1, 0, 0, 0, 1 ], [ 1, 2, 2, 2, 1 ], [ 0, 1, 1, 1, 0 ], [ 2, 1, 1, 1, 2 ], [ 1, 1, 0, 1, 1 ], [ 1, 1, 2, 1, 1 ], [ 1, 0, 1, 0, 1 ], [ 1, 2, 1, 2, 1 ], [ 0, 0, 0, 1, 2 ], [ 2, 2, 2, 1, 0 ], [ 0, 1, 2, 2, 2 ], [ 2, 1, 0, 0, 0 ], [ 1, 1, 1, 0, 1 ], [ 1, 1, 1, 2, 1 ] ],
        50: [ [ 1, 1, 1, 1, 1 ], [ 2, 2, 2, 2, 2 ], [ 0, 0, 0, 0, 0 ], [ 3, 3, 3, 3, 3 ], [ 0, 1, 2, 1, 0 ], [ 3, 2, 1, 2, 3 ], [ 1, 2, 3, 2, 1 ], [ 2, 1, 0, 1, 2 ], [ 0, 1, 0, 1, 0 ], [ 3, 2, 3, 2, 3 ], [ 1, 0, 1, 0, 1 ], [ 2, 3, 2, 3, 2 ], [ 1, 2, 1, 2, 1 ], [ 2, 1, 2, 1, 2 ], [ 0, 0, 1, 0, 0 ], [ 3, 3, 2, 3, 3 ], [ 1, 1, 2, 1, 1 ], [ 2, 2, 1, 2, 2 ], [ 2, 2, 3, 2, 2 ], [ 1, 1, 0, 1, 1 ], [ 1, 2, 2, 2, 1 ], [ 2, 1, 1, 1, 2 ], [ 1, 0, 0, 0, 1 ], [ 2, 3, 3, 3, 2 ], [ 3, 2, 2, 2, 3 ], [ 0, 1, 1, 1, 0 ], [ 2, 0, 0, 0, 2 ], [ 1, 3, 3, 3, 1 ], [ 3, 1, 1, 1, 3 ], [ 0, 2, 2, 2, 0 ], [ 2, 2, 0, 2, 2 ], [ 1, 1, 3, 1, 1 ], [ 0, 0, 2, 0, 0 ], [ 3, 3, 1, 3, 3 ], [ 3, 3, 0, 3, 3 ], [ 0, 0, 3, 0, 0 ], [ 3, 2, 1, 0, 0 ], [ 0, 1, 2, 3, 3 ], [ 0, 0, 1, 2, 3 ], [ 3, 3, 2, 1, 0 ], [ 2, 1, 0, 0, 0 ], [ 1, 2, 3, 3, 3 ], [ 0, 0, 0, 1, 2 ], [ 3, 3, 3, 2, 1 ], [ 3, 2, 1, 1, 1 ], [ 0, 1, 2, 2, 2 ], [ 1, 1, 1, 2, 3 ], [ 2, 2, 2, 1, 0 ], [ 1, 3, 0, 3, 1 ], [ 2, 0, 3, 0, 2 ] ],
        80: [ [ 1, 1, 1, 1, 1, 1 ], [ 2, 2, 2, 2, 2, 2 ], [ 0, 0, 0, 0, 0, 0 ], [ 3, 3, 3, 3, 3, 3 ], [ 0, 1, 2, 2, 1, 0 ], [ 3, 2, 1, 1, 2, 3 ], [ 1, 2, 3, 3, 2, 1 ], [ 2, 1, 0, 0, 1, 2 ], [ 0, 1, 0, 0, 1, 0 ], [ 3, 2, 3, 3, 2, 3 ], [ 1, 0, 1, 1, 0, 1 ], [ 2, 3, 2, 2, 3, 2 ], [ 1, 2, 1, 1, 2, 1 ], [ 2, 1, 2, 2, 1, 2 ], [ 0, 0, 1, 1, 0, 0 ], [ 3, 3, 2, 2, 3, 3 ], [ 1, 1, 2, 2, 1, 1 ], [ 2, 2, 1, 1, 2, 2 ], [ 2, 2, 3, 3, 2, 2 ], [ 1, 1, 0, 0, 1, 1 ], [ 1, 2, 2, 2, 2, 1 ], [ 2, 1, 1, 1, 1, 2 ], [ 1, 0, 0, 0, 0, 1 ], [ 2, 3, 3, 3, 3, 2 ], [ 3, 2, 2, 2, 2, 3 ], [ 0, 1, 1, 1, 1, 0 ], [ 2, 0, 0, 0, 0, 2 ], [ 1, 3, 3, 3, 3, 1 ], [ 3, 1, 1, 1, 1, 3 ], [ 0, 2, 2, 2, 2, 0 ], [ 2, 2, 0, 0, 2, 2 ], [ 1, 1, 3, 3, 1, 1 ], [ 0, 0, 2, 2, 0, 0 ], [ 3, 3, 1, 1, 3, 3 ], [ 3, 3, 0, 0, 3, 3 ], [ 0, 0, 3, 3, 0, 0 ], [ 3, 2, 1, 0, 0, 0 ], [ 0, 1, 2, 3, 3, 3 ], [ 0, 0, 0, 1, 2, 3 ], [ 3, 3, 3, 2, 1, 0 ], [ 2, 1, 0, 0, 0, 0 ], [ 1, 2, 3, 3, 3, 3 ], [ 0, 0, 0, 0, 1, 2 ], [ 3, 3, 3, 3, 2, 1 ], [ 3, 2, 1, 1, 1, 1 ], [ 0, 1, 2, 2, 2, 2 ], [ 1, 1, 1, 1, 2, 3 ], [ 2, 2, 2, 2, 1, 0 ], [ 1, 3, 0, 0, 3, 0 ], [ 2, 0, 3, 3, 0, 2 ], [ 0, 3, 0, 0, 3, 0 ], [ 3, 0, 3, 3, 0, 3 ], [ 0, 1, 0, 1, 0, 1 ], [ 3, 2, 3, 2, 3, 2 ], [ 1, 2, 1, 2, 1, 2 ], [ 2, 1, 2, 1, 2, 1 ], [ 2, 3, 2, 3, 2, 3 ], [ 1, 0, 1, 0, 1, 0 ], [ 0, 2, 0, 2, 0, 2 ], [ 3, 1, 3, 1, 3, 1 ], [ 1, 3, 1, 3, 1, 3 ], [ 2, 0, 2, 0, 2, 0 ], [ 0, 3, 0, 3, 3, 0 ], [ 3, 0, 3, 0, 3, 0 ], [ 0, 1, 2, 3, 2, 1 ], [ 3, 2, 1, 0, 1, 2 ], [ 1, 2, 3, 2, 1, 0 ], [ 2, 1, 0, 1, 2, 3 ], [ 0, 1, 0, 1, 2, 3 ], [ 3, 2, 3, 2, 1, 0 ], [ 1, 2, 1, 0, 1, 2 ], [ 2, 1, 2, 3, 2, 1 ], [ 0, 3, 3, 3, 3, 0 ], [ 3, 0, 0, 0, 0, 3 ], [ 1, 0, 1, 2, 1, 0 ], [ 2, 3, 2, 1, 2, 3 ], [ 0, 2, 3, 3, 2, 0 ], [ 3, 1, 0, 0, 1, 3 ], [ 1, 3, 0, 3, 0, 2 ], [ 2, 0, 3, 0, 3, 1 ] ],
        243: [ [ 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 1 ], [ 0, 0, 0, 0, 2 ], [ 0, 0, 0, 1, 0 ], [ 0, 0, 0, 1, 1 ], [ 0, 0, 0, 1, 2 ], [ 0, 0, 0, 2, 0 ], [ 0, 0, 0, 2, 1 ], [ 0, 0, 0, 2, 2 ], [ 0, 0, 1, 0, 0 ], [ 0, 0, 1, 0, 1 ], [ 0, 0, 1, 0, 2 ], [ 0, 0, 1, 1, 0 ], [ 0, 0, 1, 1, 1 ], [ 0, 0, 1, 1, 2 ], [ 0, 0, 1, 2, 0 ], [ 0, 0, 1, 2, 1 ], [ 0, 0, 1, 2, 2 ], [ 0, 0, 2, 0, 0 ], [ 0, 0, 2, 0, 1 ], [ 0, 0, 2, 0, 2 ], [ 0, 0, 2, 1, 0 ], [ 0, 0, 2, 1, 1 ], [ 0, 0, 2, 1, 2 ], [ 0, 0, 2, 2, 0 ], [ 0, 0, 2, 2, 1 ], [ 0, 0, 2, 2, 2 ], [ 0, 1, 0, 0, 0 ], [ 0, 1, 0, 0, 1 ], [ 0, 1, 0, 0, 2 ], [ 0, 1, 0, 1, 0 ], [ 0, 1, 0, 1, 1 ], [ 0, 1, 0, 1, 2 ], [ 0, 1, 0, 2, 0 ], [ 0, 1, 0, 2, 1 ], [ 0, 1, 0, 2, 2 ], [ 0, 1, 1, 0, 0 ], [ 0, 1, 1, 0, 1 ], [ 0, 1, 1, 0, 2 ], [ 0, 1, 1, 1, 0 ], [ 0, 1, 1, 1, 1 ], [ 0, 1, 1, 1, 2 ], [ 0, 1, 1, 2, 0 ], [ 0, 1, 1, 2, 1 ], [ 0, 1, 1, 2, 2 ], [ 0, 1, 2, 0, 0 ], [ 0, 1, 2, 0, 1 ], [ 0, 1, 2, 0, 2 ], [ 0, 1, 2, 1, 0 ], [ 0, 1, 2, 1, 1 ], [ 0, 1, 2, 1, 2 ], [ 0, 1, 2, 2, 0 ], [ 0, 1, 2, 2, 1 ], [ 0, 1, 2, 2, 2 ], [ 0, 2, 0, 0, 0 ], [ 0, 2, 0, 0, 1 ], [ 0, 2, 0, 0, 2 ], [ 0, 2, 0, 1, 0 ], [ 0, 2, 0, 1, 1 ], [ 0, 2, 0, 1, 2 ], [ 0, 2, 0, 2, 0 ], [ 0, 2, 0, 2, 1 ], [ 0, 2, 0, 2, 2 ], [ 0, 2, 1, 0, 0 ], [ 0, 2, 1, 0, 1 ], [ 0, 2, 1, 0, 2 ], [ 0, 2, 1, 1, 0 ], [ 0, 2, 1, 1, 1 ], [ 0, 2, 1, 1, 2 ], [ 0, 2, 1, 2, 0 ], [ 0, 2, 1, 2, 1 ], [ 0, 2, 1, 2, 2 ], [ 0, 2, 2, 0, 0 ], [ 0, 2, 2, 0, 1 ], [ 0, 2, 2, 0, 2 ], [ 0, 2, 2, 1, 0 ], [ 0, 2, 2, 1, 1 ], [ 0, 2, 2, 1, 2 ], [ 0, 2, 2, 2, 0 ], [ 0, 2, 2, 2, 1 ], [ 0, 2, 2, 2, 2 ], [ 1, 0, 0, 0, 0 ], [ 1, 0, 0, 0, 1 ], [ 1, 0, 0, 0, 2 ], [ 1, 0, 0, 1, 0 ], [ 1, 0, 0, 1, 1 ], [ 1, 0, 0, 1, 2 ], [ 1, 0, 0, 2, 0 ], [ 1, 0, 0, 2, 1 ], [ 1, 0, 0, 2, 2 ], [ 1, 0, 1, 0, 0 ], [ 1, 0, 1, 0, 1 ], [ 1, 0, 1, 0, 2 ], [ 1, 0, 1, 1, 0 ], [ 1, 0, 1, 1, 1 ], [ 1, 0, 1, 1, 2 ], [ 1, 0, 1, 2, 0 ], [ 1, 0, 1, 2, 1 ], [ 1, 0, 1, 2, 2 ], [ 1, 0, 2, 0, 0 ], [ 1, 0, 2, 0, 1 ], [ 1, 0, 2, 0, 2 ], [ 1, 0, 2, 1, 0 ], [ 1, 0, 2, 1, 1 ], [ 1, 0, 2, 1, 2 ], [ 1, 0, 2, 2, 0 ], [ 1, 0, 2, 2, 1 ], [ 1, 0, 2, 2, 2 ], [ 1, 1, 0, 0, 0 ], [ 1, 1, 0, 0, 1 ], [ 1, 1, 0, 0, 2 ], [ 1, 1, 0, 1, 0 ], [ 1, 1, 0, 1, 1 ], [ 1, 1, 0, 1, 2 ], [ 1, 1, 0, 2, 0 ], [ 1, 1, 0, 2, 1 ], [ 1, 1, 0, 2, 2 ], [ 1, 1, 1, 0, 0 ], [ 1, 1, 1, 0, 1 ], [ 1, 1, 1, 0, 2 ], [ 1, 1, 1, 1, 0 ], [ 1, 1, 1, 1, 1 ], [ 1, 1, 1, 1, 2 ], [ 1, 1, 1, 2, 0 ], [ 1, 1, 1, 2, 1 ], [ 1, 1, 1, 2, 2 ], [ 1, 1, 2, 0, 0 ], [ 1, 1, 2, 0, 1 ], [ 1, 1, 2, 0, 2 ], [ 1, 1, 2, 1, 0 ], [ 1, 1, 2, 1, 1 ], [ 1, 1, 2, 1, 2 ], [ 1, 1, 2, 2, 0 ], [ 1, 1, 2, 2, 1 ], [ 1, 1, 2, 2, 2 ], [ 1, 2, 0, 0, 0 ], [ 1, 2, 0, 0, 1 ], [ 1, 2, 0, 0, 2 ], [ 1, 2, 0, 1, 0 ], [ 1, 2, 0, 1, 1 ], [ 1, 2, 0, 1, 2 ], [ 1, 2, 0, 2, 0 ], [ 1, 2, 0, 2, 1 ], [ 1, 2, 0, 2, 2 ], [ 1, 2, 1, 0, 0 ], [ 1, 2, 1, 0, 1 ], [ 1, 2, 1, 0, 2 ], [ 1, 2, 1, 1, 0 ], [ 1, 2, 1, 1, 1 ], [ 1, 2, 1, 1, 2 ], [ 1, 2, 1, 2, 0 ], [ 1, 2, 1, 2, 1 ], [ 1, 2, 1, 2, 2 ], [ 1, 2, 2, 0, 0 ], [ 1, 2, 2, 0, 1 ], [ 1, 2, 2, 0, 2 ], [ 1, 2, 2, 1, 0 ], [ 1, 2, 2, 1, 1 ], [ 1, 2, 2, 1, 2 ], [ 1, 2, 2, 2, 0 ], [ 1, 2, 2, 2, 1 ], [ 1, 2, 2, 2, 2 ], [ 2, 0, 0, 0, 0 ], [ 2, 0, 0, 0, 1 ], [ 2, 0, 0, 0, 2 ], [ 2, 0, 0, 1, 0 ], [ 2, 0, 0, 1, 1 ], [ 2, 0, 0, 1, 2 ], [ 2, 0, 0, 2, 0 ], [ 2, 0, 0, 2, 1 ], [ 2, 0, 0, 2, 2 ], [ 2, 0, 1, 0, 0 ], [ 2, 0, 1, 0, 1 ], [ 2, 0, 1, 0, 2 ], [ 2, 0, 1, 1, 0 ], [ 2, 0, 1, 1, 1 ], [ 2, 0, 1, 1, 2 ], [ 2, 0, 1, 2, 0 ], [ 2, 0, 1, 2, 1 ], [ 2, 0, 1, 2, 2 ], [ 2, 0, 2, 0, 0 ], [ 2, 0, 2, 0, 1 ], [ 2, 0, 2, 0, 2 ], [ 2, 0, 2, 1, 0 ], [ 2, 0, 2, 1, 1 ], [ 2, 0, 2, 1, 2 ], [ 2, 0, 2, 2, 0 ], [ 2, 0, 2, 2, 1 ], [ 2, 0, 2, 2, 2 ], [ 2, 1, 0, 0, 0 ], [ 2, 1, 0, 0, 1 ], [ 2, 1, 0, 0, 2 ], [ 2, 1, 0, 1, 0 ], [ 2, 1, 0, 1, 1 ], [ 2, 1, 0, 1, 2 ], [ 2, 1, 0, 2, 0 ], [ 2, 1, 0, 2, 1 ], [ 2, 1, 0, 2, 2 ], [ 2, 1, 1, 0, 0 ], [ 2, 1, 1, 0, 1 ], [ 2, 1, 1, 0, 2 ], [ 2, 1, 1, 1, 0 ], [ 2, 1, 1, 1, 1 ], [ 2, 1, 1, 1, 2 ], [ 2, 1, 1, 2, 0 ], [ 2, 1, 1, 2, 1 ], [ 2, 1, 1, 2, 2 ], [ 2, 1, 2, 0, 0 ], [ 2, 1, 2, 0, 1 ], [ 2, 1, 2, 0, 2 ], [ 2, 1, 2, 1, 0 ], [ 2, 1, 2, 1, 1 ], [ 2, 1, 2, 1, 2 ], [ 2, 1, 2, 2, 0 ], [ 2, 1, 2, 2, 1 ], [ 2, 1, 2, 2, 2 ], [ 2, 2, 0, 0, 0 ], [ 2, 2, 0, 0, 1 ], [ 2, 2, 0, 0, 2 ], [ 2, 2, 0, 1, 0 ], [ 2, 2, 0, 1, 1 ], [ 2, 2, 0, 1, 2 ], [ 2, 2, 0, 2, 0 ], [ 2, 2, 0, 2, 1 ], [ 2, 2, 0, 2, 2 ], [ 2, 2, 1, 0, 0 ], [ 2, 2, 1, 0, 1 ], [ 2, 2, 1, 0, 2 ], [ 2, 2, 1, 1, 0 ], [ 2, 2, 1, 1, 1 ], [ 2, 2, 1, 1, 2 ], [ 2, 2, 1, 2, 0 ], [ 2, 2, 1, 2, 1 ], [ 2, 2, 1, 2, 2 ], [ 2, 2, 2, 0, 0 ], [ 2, 2, 2, 0, 1 ], [ 2, 2, 2, 0, 2 ], [ 2, 2, 2, 1, 0 ], [ 2, 2, 2, 1, 1 ], [ 2, 2, 2, 1, 2 ], [ 2, 2, 2, 2, 0 ], [ 2, 2, 2, 2, 1 ], [ 2, 2, 2, 2, 2 ] ],
        100: [ [ 1, 1, 1, 1, 1 ], [ 0, 0, 0, 0, 0 ], [ 2, 2, 2, 2, 2 ], [ 0, 1, 2, 1, 0 ], [ 2, 1, 0, 1, 2 ], [ 1, 0, 0, 0, 1 ], [ 1, 2, 2, 2, 1 ], [ 0, 0, 1, 2, 2 ], [ 2, 2, 1, 0, 0 ], [ 1, 2, 1, 0, 1 ], [ 1, 0, 1, 2, 1 ], [ 0, 1, 1, 1, 0 ], [ 2, 1, 1, 1, 2 ], [ 0, 1, 0, 1, 0 ], [ 2, 1, 2, 1, 2 ], [ 1, 1, 0, 1, 1 ], [ 1, 1, 2, 1, 1 ], [ 0, 0, 2, 0, 0 ], [ 2, 2, 0, 2, 2 ], [ 0, 2, 2, 2, 0 ], [ 2, 0, 0, 0, 2 ], [ 1, 2, 0, 2, 1 ], [ 1, 0, 2, 0, 1 ], [ 0, 2, 0, 2, 0 ], [ 2, 0, 2, 0, 2 ], [ 2, 0, 1, 2, 0 ], [ 0, 2, 1, 0, 2 ], [ 0, 2, 1, 2, 0 ], [ 2, 0, 1, 0, 2 ], [ 2, 1, 0, 0, 1 ], [ 0, 1, 2, 2, 1 ], [ 0, 0, 2, 2, 2 ], [ 2, 2, 0, 0, 0 ], [ 1, 0, 2, 1, 2 ], [ 1, 2, 0, 1, 0 ], [ 0, 1, 0, 1, 2 ], [ 2, 1, 2, 1, 0 ], [ 1, 2, 2, 0, 0 ], [ 0, 0, 1, 1, 2 ], [ 2, 2, 1, 1, 0 ], [ 2, 0, 0, 0, 0 ], [ 0, 2, 2, 2, 2 ], [ 2, 2, 2, 2, 0 ], [ 0, 0, 0, 0, 2 ], [ 1, 0, 1, 0, 1 ], [ 1, 2, 1, 2, 1 ], [ 0, 1, 2, 2, 2 ], [ 2, 1, 0, 0, 0 ], [ 0, 1, 1, 1, 1 ], [ 2, 1, 1, 1, 1 ], [ 0, 0, 0, 0, 1 ], [ 0, 0, 0, 1, 1 ], [ 0, 0, 0, 1, 2 ], [ 0, 0, 1, 0, 0 ], [ 0, 0, 1, 1, 0 ], [ 0, 0, 1, 1, 1 ], [ 0, 0, 1, 2, 1 ], [ 0, 1, 0, 0, 0 ], [ 0, 1, 0, 1, 1 ], [ 2, 0, 1, 0, 1 ], [ 0, 2, 1, 2, 1 ], [ 0, 1, 0, 2, 0 ], [ 2, 1, 2, 0, 2 ], [ 0, 1, 1, 1, 2 ], [ 2, 1, 1, 1, 0 ], [ 0, 1, 1, 2, 2 ], [ 1, 0, 0, 0, 0 ], [ 1, 0, 0, 1, 1 ], [ 1, 0, 0, 1, 2 ], [ 0, 1, 1, 0, 1 ], [ 1, 0, 1, 1, 2 ], [ 1, 0, 1, 2, 2 ], [ 1, 1, 0, 0, 0 ], [ 1, 1, 0, 0, 1 ], [ 1, 1, 0, 1, 2 ], [ 0, 0, 2, 1, 0 ], [ 1, 1, 1, 0, 0 ], [ 1, 1, 2, 2, 2 ], [ 2, 2, 2, 2, 1 ], [ 1, 1, 1, 1, 2 ], [ 0, 1, 0, 2, 1 ], [ 1, 2, 2, 2, 2 ], [ 1, 2, 1, 0, 0 ], [ 2, 2, 1, 0, 1 ], [ 2, 2, 1, 1, 1 ], [ 2, 1, 2, 1, 1 ], [ 0, 0, 0, 2, 0 ], [ 2, 2, 2, 0, 2 ], [ 2, 0, 2, 2, 2 ], [ 0, 2, 0, 0, 0 ], [ 2, 2, 0, 1, 2 ], [ 1, 0, 1, 1, 1 ], [ 1, 2, 1, 1, 1 ], [ 1, 1, 2, 1, 0 ], [ 2, 1, 2, 2, 2 ], [ 0, 2, 1, 1, 1 ], [ 2, 0, 1, 1, 1 ], [ 1, 1, 1, 2, 0 ], [ 1, 1, 1, 0, 2 ], [ 1, 1, 1, 1, 0 ] ]
      };
      GameConfig.SMID = {
        1: "sm2003",
        2: "sm2004",
        3: "sm2005",
        4: "sm2002",
        5: "sm2001",
        6: "sm2006",
        7: "sm2007",
        8: "sm2008"
      };
      return GameConfig;
    }();
    exports.default = GameConfig;
    cc._RF.pop();
  }, {} ],
  GameDefine: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c6926SdoZlB9JB+QR/dFKGc", "GameDefine");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    cc._RF.pop();
  }, {} ],
  GameScene: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a56c9oArvRLpKUSqfmJzbIc", "GameScene");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var GameScene = function(_super) {
      __extends(GameScene, _super);
      function GameScene() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.center = null;
        _this.bigBg = null;
        return _this;
      }
      GameScene.prototype.onLoad = function() {
        this.initState();
      };
      GameScene.prototype.initState = function() {
        this.center.active = true;
        this.node.getChildByName("minimize-game").zIndex = 1;
      };
      GameScene.prototype.loadBgSlot = function(id) {
        var slotScene = view.slot[id];
        this.bigBg.node.active = false;
        this.bigBg.node.y = -this.bigBg.node.height * (1 === game.data.deviceKind ? 1.5 : 1.2);
        this.bigBg.node.active = true;
        this.bigBg.spriteFrame = slotScene.SlotController.resourceController.loadRes("bg-big");
      };
      GameScene.prototype.hideBg = function(time) {
        void 0 === time && (time = .5);
        this.bigBg.node.runAction(cc.moveBy(time, cc.v2(0, -this.bigBg.node.height * (1 === game.data.deviceKind ? 1.5 : 1.2))).easing(cc.easeSineIn()));
      };
      GameScene.prototype.onClickSpin = function() {
        view.screen.slot.SlotController.clickSpin();
      };
      GameScene.prototype.onClickMinimize = function() {
        controller.ui.minimizeGameScene();
      };
      GameScene.prototype.onClickBack = function() {
        controller.ui.deleteSlot();
      };
      __decorate([ property(cc.Node) ], GameScene.prototype, "center", void 0);
      __decorate([ property(cc.Sprite) ], GameScene.prototype, "bigBg", void 0);
      GameScene = __decorate([ ccclass ], GameScene);
      return GameScene;
    }(cc.Component);
    exports.default = GameScene;
    cc._RF.pop();
  }, {} ],
  GameUtil: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "db2f28fnWxPGojlwcz9F8oR", "GameUtil");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var GameUtil = function() {
      function GameUtil() {}
      GameUtil.generateUUID = function() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
          var r = 16 * Math.random() | 0, v = "x" == c ? r : 3 & r | 8;
          return v.toString(16);
        });
      };
      GameUtil.loadRes = function(path, type) {
        void 0 === type && (type = null);
        return new Promise(function(res, rej) {
          cc.loader.loadRes(path, type, function(err, data) {
            if (err) return rej(err);
            res(data);
          });
        });
      };
      GameUtil.showNode = function(node, zIndex) {
        node.opacity = 255;
        node.zIndex = zIndex || 1;
      };
      GameUtil.hideNode = function(node) {
        node.zIndex = -1;
        node.opacity = 0;
      };
      GameUtil.delay = function(time, target, callback) {
        var canvas = cc.find("Main");
        var temp = false;
        target instanceof cc.Node && (temp = true);
        console.log(target, temp);
        return new Promise(function(res, rej) {
          canvas.runAction(cc.sequence([ cc.delayTime(time / 1e3), cc.callFunc(function() {
            if (temp && (!target || !target.isValid)) return rej("Node was detroyed before");
            callback && callback();
            return res();
          }) ]));
        });
      };
      GameUtil.abbreviateNumber = function(num, fixed) {
        if (null === num) return null;
        if (0 === num) return "0";
        fixed = !fixed || fixed < 0 ? 0 : fixed;
        var b = num.toPrecision(2).split("e");
        var k = 1 === b.length ? 0 : Math.floor(Math.min(b[1].slice(1), 14) / 3);
        var c = k < 1 ? num.toFixed(0 + fixed) : (num / Math.pow(10, 3 * k)).toFixed(1 + fixed);
        var d = (c < 0 ? c : Math.abs(c)) + [ "", "K", "M", "B", "T" ][k];
        return d;
      };
      GameUtil.getDefaultImage = function(name) {
        return name.includes(".png") || name.includes(".jpg") ? name : name + ".png";
      };
      return GameUtil;
    }();
    exports.default = GameUtil;
    cc._RF.pop();
  }, {} ],
  HomeScene: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6ff7cpxeexOW4heej6ansRg", "HomeScene");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var HomeScene = function(_super) {
      __extends(HomeScene, _super);
      function HomeScene() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listGameView = null;
        _this.listItemGame = [];
        _this.itemGame = null;
        _this.animationclip = null;
        return _this;
      }
      HomeScene.prototype.onLoad = function() {
        this.initialize();
      };
      HomeScene.prototype.initialize = function() {
        var _this = this;
        this.listItemGame.forEach(function(sprite, i) {
          var itemGame = cc.instantiate(_this.itemGame);
          itemGame.parent = _this.listGameView.content;
          itemGame.active = true;
          itemGame.getComponent("ItemGame").setData(++i, sprite);
        });
      };
      HomeScene.prototype.openSlot = function(e) {
        return __awaiter(this, void 0, void 0, function() {
          var ItemGame, id, smid;
          return __generator(this, function(_a) {
            ItemGame = e.target.getComponent("ItemGame");
            id = ItemGame.id;
            smid = ItemGame.smid;
            controller.ui.openGame(id, smid);
            ItemGame.checkRes();
            return [ 2 ];
          });
        });
      };
      HomeScene.prototype.onClickMenu = function() {
        cc.log("HomeScene", "onClickMenu");
      };
      __decorate([ property(cc.ScrollView) ], HomeScene.prototype, "listGameView", void 0);
      __decorate([ property([ cc.SpriteFrame ]) ], HomeScene.prototype, "listItemGame", void 0);
      __decorate([ property(cc.Node) ], HomeScene.prototype, "itemGame", void 0);
      __decorate([ property(cc.AnimationClip) ], HomeScene.prototype, "animationclip", void 0);
      HomeScene = __decorate([ ccclass ], HomeScene);
      return HomeScene;
    }(cc.Component);
    exports.default = HomeScene;
    cc._RF.pop();
  }, {} ],
  ImageScale: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "da9c5MQc85PTbxoX5CVhQIY", "ImageScale");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ImageScale = function(_super) {
      __extends(ImageScale, _super);
      function ImageScale() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      ImageScale.prototype.onLoad = function() {
        var realScreenSize = cc.view.getFrameSize();
        var realScreenRatio = realScreenSize.width / realScreenSize.height;
        var scaleRatio = this.node.width * realScreenSize.height / (this.node.height * realScreenSize.width);
        if (realScreenRatio < 1.5) {
          this.node.width *= scaleRatio;
          this.node.scaleY *= scaleRatio;
        }
        if (realScreenRatio > 2) {
          this.node.width /= scaleRatio;
          this.node.scaleY /= scaleRatio;
        }
      };
      ImageScale = __decorate([ ccclass ], ImageScale);
      return ImageScale;
    }(cc.Component);
    exports.default = ImageScale;
    cc._RF.pop();
  }, {} ],
  ItemGame: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5bbeb6hzUpObodJ2mdI9+U3", "ItemGame");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SlotsDownloadCtr_1 = require("../Slots/SlotsDownloadCtr");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ItemGame = function(_super) {
      __extends(ItemGame, _super);
      function ItemGame() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.image = void 0;
        _this.id = 0;
        _this.smid = "";
        return _this;
      }
      ItemGame.prototype.setData = function(id, image) {
        this.id = id;
        this.image.spriteFrame = image;
        this.smid = config.game.SMID[id] || "";
        var slotsDownloadCtr = new SlotsDownloadCtr_1.default();
        slotsDownloadCtr.getUrls(1);
      };
      ItemGame.prototype.setComponentResCtr = function() {
        var _this = this;
        var resPath = "5_Games/" + this.id + "_Slot" + this.id + "/Slot" + this.id + "ResCtr";
        cc.loader.loadRes(resPath, cc.Prefab, function(err, resCtr) {
          if (err) return;
          _this.node.addComponent(resCtr.getComponent(""));
          _this.resCtr = resCtr;
        });
      };
      ItemGame.prototype.checkRes = function() {};
      __decorate([ property(cc.Sprite) ], ItemGame.prototype, "image", void 0);
      ItemGame = __decorate([ ccclass ], ItemGame);
      return ItemGame;
    }(cc.Component);
    exports.default = ItemGame;
    cc._RF.pop();
  }, {
    "../Slots/SlotsDownloadCtr": "SlotsDownloadCtr"
  } ],
  ItemSkeleton: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2c5c63AwXNGqrfxds6qJcO7", "ItemSkeleton");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ItemSkeleton = function(_super) {
      __extends(ItemSkeleton, _super);
      function ItemSkeleton() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.skeleton = null;
        return _this;
      }
      ItemSkeleton.prototype.onLoad = function() {
        window["item"] = this;
        this.skeleton = this.node.getComponent(sp.Skeleton);
        cc.log(this.skeleton.skeletonData.skeletonJson);
      };
      ItemSkeleton = __decorate([ ccclass ], ItemSkeleton);
      return ItemSkeleton;
    }(cc.Component);
    exports.default = ItemSkeleton;
    cc._RF.pop();
  }, {} ],
  Item: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "693baJDbAJFE4FJLNuwUi2N", "Item");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Item = function(_super) {
      __extends(Item, _super);
      function Item() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.id = "";
        _this.spritesFrame = null;
        _this.skeletonData = null;
        return _this;
      }
      Item.prototype.setInfo = function(id, spriteFrame, skeletonData) {
        this.id = id;
        spriteFrame && (this.spritesFrame = spriteFrame);
        skeletonData && (this.skeletonData = skeletonData);
      };
      __decorate([ property(cc.String) ], Item.prototype, "id", void 0);
      __decorate([ property(cc.SpriteFrame) ], Item.prototype, "spritesFrame", void 0);
      __decorate([ property(sp.SkeletonData) ], Item.prototype, "skeletonData", void 0);
      Item = __decorate([ ccclass ], Item);
      return Item;
    }(cc.Component);
    exports.default = Item;
    cc._RF.pop();
  }, {} ],
  LinesController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ddc65mv5XtJr6Rcm5HohZzJ", "LinesController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var LinesController = function(_super) {
      __extends(LinesController, _super);
      function LinesController() {
        var _this_1 = null !== _super && _super.apply(this, arguments) || this;
        _this_1.tableView = null;
        _this_1.lineAnimate = null;
        _this_1.lineSprite = null;
        _this_1.lineItem = null;
        _this_1.totalLines = 20;
        _this_1.lineNode = [];
        return _this_1;
      }
      LinesController.prototype.onLoad = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.loadRes() ];

             case 1:
              _a.sent();
              this.initialize();
              this.initState();
              this.initPosition();
              this.initAnimation();
              return [ 2 ];
            }
          });
        });
      };
      LinesController.prototype.loadRes = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _a;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              return [ 4, Promise.all([ util.game.loadRes("0_animation/Slots/LineAni", cc.AnimationClip), util.game.loadRes("0_textures/Slots/Linepay", cc.SpriteFrame) ]) ];

             case 1:
              _a = _b.sent(), this.lineAnimate = _a[0], this.lineSprite = _a[1];
              return [ 2 ];
            }
          });
        });
      };
      LinesController.prototype.initState = function() {
        this._slotController = this._slotController || this.node.parent.getComponent("SlotController");
      };
      LinesController.prototype.initialize = function() {
        this.lineItem = new cc.Node("Linepay");
        var sp = this.lineItem.addComponent(cc.Sprite);
        sp.spriteFrame = this.lineSprite;
        this.lineItem.anchorX = .015;
        this.lineItem.anchorY = .5;
      };
      LinesController.prototype.initPosition = function() {
        this.node.parent = this.tableView;
        this.node.anchorX = 0;
        this.node.anchorY = 0;
        this.node.width = this.tableView.width;
        this.node.height = this.tableView.height;
        this.node.x = 0;
        this.node.y = 0;
        this.node.zIndex = 2;
      };
      LinesController.prototype.initAnimation = function() {
        this.node.addComponent(cc.Animation);
        this.animation = this.node.getComponent(cc.Animation);
        this.animation.addClip(this.lineAnimate);
        this.lineItem.height = 11;
        this.lineItem.anchorX = .015;
        this.lineItem.anchorY = .5;
      };
      LinesController.prototype.showAllLines = function(arr, totalLines) {
        var _this_1 = this;
        this.node.active = true;
        totalLines && (this.totalLines = totalLines);
        util.game.showNode(this.node, 2);
        arr.forEach(function(item) {
          _this_1.getLine(item);
        });
        this.animation.play("LineAni");
        return new Promise(function(res, rej) {
          _this_1.node.runAction(cc.sequence([ cc.delayTime(1.3), cc.callFunc(function() {
            _this_1.stopShowLines();
            res();
          }) ]));
        });
      };
      LinesController.prototype.showLine = function(item) {
        var _this_1 = this;
        this.node.active = true;
        util.game.showNode(this.node, 2);
        this.getLine(item);
        this.animation.play("LineAni");
        return new Promise(function(res, rej) {
          _this_1.node.runAction(cc.sequence([ cc.delayTime(1.5), cc.callFunc(function() {
            _this_1.stopShowLines();
            res();
          }) ]));
        });
      };
      LinesController.prototype.stopShowLines = function() {
        if (!this.node) return;
        this.animation && this.animation.stop();
        this.lineNode && this.lineNode.forEach(function(item) {
          item.active = false;
          cc.isValid(item) && item.destroy();
        });
        this.node.active = false;
      };
      LinesController.prototype.refreshShowLines = function() {
        this.animation && this.animation.stop();
        util.game.hideNode(this.node);
        this.lineNode && this.lineNode.forEach(function(item) {
          cc.isValid(item) && item.destroy();
        });
        this.lineNode = [];
      };
      LinesController.prototype.getLine = function(l) {
        var _this = this;
        var payTable = config.game.PAY_LINES[this.totalLines];
        var line = payTable[l];
        var _a = this._slotController, distanceItemX = _a.distanceItemX, distanceItemY = _a.distanceItemY;
        var gameSize = this._slotController.gameSizeType;
        var width = 3;
        3 != gameSize && 2 != gameSize || (width = 4);
        var points = line.reduce(function(result, item, index) {
          result.push({
            x: (index + .5) * distanceItemX,
            y: (width - (item + .5)) * distanceItemY
          });
          return result;
        }, []);
        points = [ {
          x: 0,
          y: points[0].y
        } ].concat(points, [ {
          x: points[points.length - 1].x + distanceItemX / 2,
          y: points[points.length - 1].y
        } ]);
        var colorLine = this.getColorLine(l);
        var totalLineNode = new cc.Node("TotalLineNode");
        totalLineNode.parent = this.node;
        this.lineNode.push(totalLineNode);
        for (var i = 0; i < points.length - 1; i++) {
          var lineNode = cc.instantiate(_this.lineItem);
          var length = 4 + Math.sqrt(Math.pow(points[i].x - points[i + 1].x, 2) + Math.pow(points[i].y - points[i + 1].y, 2));
          var angle = -(180 + 180 * Math.atan2(points[i].y - points[i + 1].y, points[i].x - points[i + 1].x) / Math.PI);
          lineNode.parent = totalLineNode;
          lineNode.active = true;
          lineNode.width = length;
          lineNode.rotation = angle;
          lineNode.color = colorLine;
          lineNode.setPosition(points[i]);
        }
      };
      LinesController.prototype.getColorLine = function(n) {
        var _a;
        var _b = define.color, Green = _b.Green, Yellow = _b.Yellow, Orange = _b.Orange, Red = _b.Red, Blue = _b.Blue, Violet = _b.Violet;
        var lineColors = [ Green, Yellow, Orange, Red, Blue, Violet ];
        n %= lineColors.length;
        return new ((_a = cc.Color).bind.apply(_a, [ void 0 ].concat(Object.values(lineColors[n]))))();
      };
      __decorate([ property(cc.Node) ], LinesController.prototype, "tableView", void 0);
      LinesController = __decorate([ ccclass ], LinesController);
      return LinesController;
    }(cc.Component);
    exports.default = LinesController;
    cc._RF.pop();
  }, {} ],
  Main: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1c171djw71FS6tvzt/Atvwj", "Main");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var GameUtil_1 = require("./Util/GameUtil");
    var StringUtil_1 = require("./Util/StringUtil");
    var Api_1 = require("./Api");
    var Store_1 = require("./Store");
    var GameConfig_1 = require("./GameConfig");
    var DefineType = require("./Define/DefineType");
    var DefineColor = require("./Define/DefineColor");
    var DefineKey = require("./Define/DefineKey");
    var DefineZIndex = require("./Define/DefineZIndex");
    var SoundController_1 = require("./SoundController");
    var Effect_1 = require("./Effect");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Main = function(_super) {
      __extends(Main, _super);
      function Main() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      Main.prototype.onLoad = function() {
        this.initGlobal();
        cc.game.on(cc.game.EVENT_HIDE, this.onGameHide, this);
        cc.game.on(cc.game.EVENT_SHOW, this.onGameShow, this);
        api.login();
        this.resiterStore();
      };
      Main.prototype.initGlobal = function() {
        window.controller = {
          ui: this.node.getComponent("UIController"),
          sound: SoundController_1.default.ins()
        };
        var bar = cc.director.getScene().getChildByName("Canvas").getChildByName("bar");
        var topBar = bar.getChildByName("topBar").getComponent("TopBarController");
        var botBar = bar.getChildByName("bottomBar").getComponent("BottomBarController");
        window.view = {
          screen: {
            home: void 0,
            game: void 0,
            slot: void 0,
            bonus: void 0,
            model: cc.director.getScene().getChildByName("Canvas").getChildByName("Model")
          },
          slot: {
            1: void 0,
            2: void 0,
            3: void 0,
            4: void 0,
            5: void 0,
            6: void 0,
            7: void 0,
            8: void 0
          },
          popup: {},
          dialog: void 0,
          progress: void 0,
          bar: {
            node: bar,
            top: topBar,
            bottom: botBar
          },
          multi: cc.director.getScene().getChildByName("Canvas").getChildByName("multiSlot").getComponent("MultiScene")
        };
        window.define = {
          type: DefineType,
          color: DefineColor,
          key: DefineKey,
          zIndex: DefineZIndex
        };
        window.config = {
          game: GameConfig_1.default
        };
        window.api = Api_1.default;
        window.store = Store_1.default;
        window.util = {
          game: GameUtil_1.default,
          string: StringUtil_1.default
        };
        window.game = {
          data: {
            deviceKind: define.type.Device.NORMAL
          },
          user: {
            balance: 0,
            level: {
              currentExp: 0,
              currentLevel: 0,
              requiredExp: 0
            }
          },
          log: function(msg) {
            var subst = [];
            for (var _i = 1; _i < arguments.length; _i++) subst[_i - 1] = arguments[_i];
            cc.log.apply(cc, [ msg ].concat(subst));
          },
          logTest: function(msg) {
            var subst = [];
            for (var _i = 1; _i < arguments.length; _i++) subst[_i - 1] = arguments[_i];
            cc.log.apply(cc, [ "%cLOGTEST", "background: #222; color: #bada55", msg ].concat(subst));
          },
          warn: function(msg) {
            var subst = [];
            for (var _i = 1; _i < arguments.length; _i++) subst[_i - 1] = arguments[_i];
            cc.warn.apply(cc, [ msg ].concat(subst));
          },
          error: function(msg) {
            var subst = [];
            for (var _i = 1; _i < arguments.length; _i++) subst[_i - 1] = arguments[_i];
            cc.error.apply(cc, [ msg ].concat(subst));
          }
        };
        window.effect = Effect_1.default.ins();
        window.controller.sound.init();
      };
      Main.prototype.resiterStore = function() {
        store.register(store.key.UPDATE_USER_BALANCE, function(amount) {
          game.user.balance = amount;
        });
        store.register(store.key.UPDATE_USER_LEVEL, function(level) {
          game.user.level = level;
        });
      };
      Main.prototype.onGameHide = function() {
        cc.log("%c GAME HIDDEN, SEE YOU AGAIN!!! ", "background: #1B5E20; color: #fff");
      };
      Main.prototype.onGameShow = function() {
        cc.log("%c GAME SHOW, NICE TO MEET YOU ^^ ", "background: #01579B; color: #fff");
      };
      Main = __decorate([ ccclass ], Main);
      return Main;
    }(cc.Component);
    exports.default = Main;
    cc._RF.pop();
  }, {
    "./Api": "Api",
    "./Define/DefineColor": "DefineColor",
    "./Define/DefineKey": "DefineKey",
    "./Define/DefineType": "DefineType",
    "./Define/DefineZIndex": "DefineZIndex",
    "./Effect": "Effect",
    "./GameConfig": "GameConfig",
    "./SoundController": "SoundController",
    "./Store": "Store",
    "./Util/GameUtil": "GameUtil",
    "./Util/StringUtil": "StringUtil"
  } ],
  MarginWithIphoneX: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4d82a3moCNLpq7LGl4XXnsC", "MarginWithIphoneX");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MarginWithIphoneX = function(_super) {
      __extends(MarginWithIphoneX, _super);
      function MarginWithIphoneX() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.marginLeft = 100;
        _this.marginRight = 100;
        return _this;
      }
      MarginWithIphoneX.prototype.onLoad = function() {
        var realScreenSize = cc.view.getFrameSize();
        var realScreenRatio = realScreenSize.width / realScreenSize.height;
        if (realScreenRatio >= 2 && this.node.getComponent(cc.Widget)) {
          this.marginLeft >= 0 && (this.node.getComponent(cc.Widget).left = this.marginLeft);
          this.marginRight >= 0 && (this.node.getComponent(cc.Widget).right = this.marginRight);
        }
      };
      __decorate([ property(cc.Integer) ], MarginWithIphoneX.prototype, "marginLeft", void 0);
      __decorate([ property(cc.Integer) ], MarginWithIphoneX.prototype, "marginRight", void 0);
      MarginWithIphoneX = __decorate([ ccclass ], MarginWithIphoneX);
      return MarginWithIphoneX;
    }(cc.Component);
    exports.default = MarginWithIphoneX;
    cc._RF.pop();
  }, {} ],
  MinigameController1: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d7e56NGG8tI3odhnrwKgR9b", "MinigameController1");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CoinLabel_1 = require("../../../../scripts/Components/CoinLabel");
    var ResourceMinigame1_1 = require("./ResourceMinigame1");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MinigameController1 = function(_super) {
      __extends(MinigameController1, _super);
      function MinigameController1() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.collectBonus = null;
        _this.itemsContainer = null;
        _this.multipleLabel = null;
        _this.baseScoreLabel = null;
        _this.gameResult = null;
        _this.mainGame = null;
        _this.scoreLabel = null;
        _this.resourceMinigame = null;
        _this._slotController = null;
        _this.listItem = [];
        _this.playArray = [];
        _this.baseScore = 0;
        _this.totalMultiple = 0;
        _this._isLock = false;
        _this._time = 0;
        return _this;
      }
      MinigameController1.prototype.startMiniGame = function(mul, mulArr, w) {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            this.playArray = mulArr;
            this.baseScore = w;
            this.baseScoreLabel.string = "$ " + util.string.formatMoney(this.baseScore);
            this.coinController = new CoinLabel_1.default(this.scoreLabel);
            this.coinController.updateUserBalance(0);
            this.listItem = this.itemsContainer.children;
            this.listItem.forEach(function(item) {
              item.once(cc.Node.EventType.TOUCH_END, function(e) {
                _this.selectItem(e.currentTarget);
              });
            });
            this.resourceMinigame.resourceController = this._slotController.resourceController;
            this.resourceMinigame.loadImages();
            return [ 2 ];
          });
        });
      };
      MinigameController1.prototype.selectItem = function(node) {
        return __awaiter(this, void 0, void 0, function() {
          var num;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (this._isLock) return [ 2 ];
              this._isLock = true;
              num = this.playArray[this._time];
              this.updateTotalMultiple(num);
              this.itemAction(node, num);
              this._time++;
              if (!(this._time == this.playArray.length)) return [ 3, 2 ];
              node.children[2].active = false;
              return [ 4, this.showEndGame() ];

             case 1:
              _a.sent();
              _a.label = 2;

             case 2:
              this._isLock = false;
              return [ 2 ];
            }
          });
        });
      };
      MinigameController1.prototype.updateTotalMultiple = function(num) {
        var _this = this;
        this.totalMultiple += num;
        this.multipleLabel.node.runAction(cc.sequence(cc.scaleTo(.2, 1.2), cc.callFunc(function() {
          _this.multipleLabel.string = _this.totalMultiple + "X";
        }), cc.scaleTo(.3, 1)));
      };
      MinigameController1.prototype.itemAction = function(node, num) {
        node.children[1].active = true;
        node.children[2].active = true;
        node.children[1].scale = .3;
        node.children[2].scale = .3;
        node.children[1].getComponent(cc.Label).string = "X" + num;
        node.children[0].runAction(cc.sequence(cc.fadeOut(.2), cc.callFunc(function() {
          node.children[1].runAction(cc.sequence(cc.scaleTo(.3, 1.3), cc.scaleTo(.3, 1)));
          node.children[2].runAction(cc.sequence(cc.scaleTo(.3, 1.3), cc.scaleTo(.3, 1)));
        })));
      };
      MinigameController1.prototype.showEndGame = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          return __awaiter(_this, void 0, void 0, function() {
            var error_1;
            var _this = this;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                _a.trys.push([ 0, 2, , 3 ]);
                this.gameResult.active = true;
                this.gameResult.opacity = 0;
                return [ 4, util.game.delay(1e3, this.node, res) ];

               case 1:
                _a.sent();
                this.mainGame.runAction(cc.fadeOut(.3));
                this.gameResult.runAction(cc.sequence([ cc.fadeIn(.5), cc.callFunc(function() {
                  _this.coinController.updateUserBalance(_this.totalMultiple * _this.baseScore);
                  api.sendGD({
                    e: api.key.SLOT_GET_BONUS,
                    gtype: config.game.SMID[1]
                  }, function(err, data) {
                    cc.log(data);
                  });
                }) ]));
                return [ 3, 3 ];

               case 2:
                error_1 = _a.sent();
                res();
                return [ 3, 3 ];

               case 3:
                return [ 2 ];
              }
            });
          });
        });
      };
      MinigameController1.prototype.backToSlot = function() {
        var _this = this;
        this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          store.emit(store.key.UPDATE_USER_BALANCE, game.user.balance, 0);
          _this.node.destroy();
          view.screen.bonus = void 0;
          controller.ui.showBar();
        }) ]));
      };
      __decorate([ property(cc.Button) ], MinigameController1.prototype, "collectBonus", void 0);
      __decorate([ property(cc.Node) ], MinigameController1.prototype, "itemsContainer", void 0);
      __decorate([ property(cc.Label) ], MinigameController1.prototype, "multipleLabel", void 0);
      __decorate([ property(cc.Label) ], MinigameController1.prototype, "baseScoreLabel", void 0);
      __decorate([ property(cc.Node) ], MinigameController1.prototype, "gameResult", void 0);
      __decorate([ property(cc.Node) ], MinigameController1.prototype, "mainGame", void 0);
      __decorate([ property(cc.Label) ], MinigameController1.prototype, "scoreLabel", void 0);
      __decorate([ property(ResourceMinigame1_1.default) ], MinigameController1.prototype, "resourceMinigame", void 0);
      MinigameController1 = __decorate([ ccclass ], MinigameController1);
      return MinigameController1;
    }(cc.Component);
    exports.default = MinigameController1;
    cc._RF.pop();
  }, {
    "../../../../scripts/Components/CoinLabel": "CoinLabel",
    "./ResourceMinigame1": "ResourceMinigame1"
  } ],
  MinigameController2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7483edFQmlMXp65QBTipnR0", "MinigameController2");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CoinLabel_1 = require("../../../../scripts/Components/CoinLabel");
    var ResourceMinigame2_1 = require("./ResourceMinigame2");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MiniGameController2 = function(_super) {
      __extends(MiniGameController2, _super);
      function MiniGameController2() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.itemContainer = null;
        _this.stopGame = null;
        _this.gameResult = null;
        _this.mainGame = null;
        _this.multipleLabel = null;
        _this.baseScoreLabel = null;
        _this.scoreLabel = null;
        _this.stopOverlay = null;
        _this.resourceMinigame = null;
        _this._slotController = null;
        _this.listItem = [];
        _this.velocity = 0;
        _this.acc = 500;
        _this.maxVelocity = 1100;
        _this.baseScore = 0;
        _this._isMove = false;
        _this._isStopGame = false;
        _this._flagStop = 0;
        _this.mul = 0;
        _this.arrayBase = [ 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 5, 8, 8, 10, 10, 20 ];
        return _this;
      }
      MiniGameController2.prototype.startMiniGame = function(mul, mulArr, w) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            this.baseScore = w;
            this.mul = mul;
            this.baseScoreLabel.string = "$ " + util.string.formatMoney(this.baseScore);
            this.listItem = this.itemContainer.children;
            this.coinController = new CoinLabel_1.default(this.scoreLabel, 11);
            this.coinController.updateUserBalance(0);
            this.resourceMinigame.resourceController = this._slotController.resourceController;
            this.resourceMinigame.loadImages();
            return [ 2 ];
          });
        });
      };
      MiniGameController2.prototype.startGame = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this._isMove = true;
              this.velocity = 0;
              this._isStopGame = false;
              this.stopGame.active = true;
              this.stopGame.getComponent(cc.Button).interactable = false;
              return [ 4, util.game.delay(3e3, this.node).catch(function(e) {}) ];

             case 1:
              _a.sent();
              this.stopGame.getComponent(cc.Button).interactable = true;
              return [ 2 ];
            }
          });
        });
      };
      MiniGameController2.prototype.stopMiniGame = function() {
        this.stopOverlay.active = true;
        this._isStopGame = true;
        this.stopGame.getComponent(cc.Button).interactable = false;
      };
      MiniGameController2.prototype.update = function(dt) {
        var _this = this;
        if (this._isMove) {
          this.velocity < this.maxVelocity ? this.velocity += this.acc * dt : this.velocity = this.maxVelocity;
          this.listItem.forEach(function(item) {
            item.x += _this.velocity * dt;
            if (item.x > 700) {
              var xMin_1 = 0;
              _this.listItem.forEach(function(item) {
                item.x < xMin_1 && (xMin_1 = item.x);
              });
              item.x = xMin_1 - 350;
              item.getComponentInChildren(cc.Label).string = "X" + _this.arrayBase[Math.floor(0 + Math.random() * _this.arrayBase.length)];
              if (_this._isStopGame) {
                _this.listItem = _this.listItem.sort(function(a, b) {
                  return a.x - b.x;
                });
                if (_this._flagStop > 3) {
                  _this._isMove = false;
                  _this._isStopGame = false;
                  _this.listItem[1].getComponentInChildren(cc.Label).string = "X" + _this.mul;
                  _this._flagStop = 0;
                  setTimeout(function() {
                    _this.listItem[1].getComponentInChildren(cc.Label).node.runAction(cc.sequence(cc.scaleTo(.2, 1.3), cc.scaleTo(.2, 1), cc.scaleTo(.3, 1.1), cc.callFunc(function() {
                      var labelNode = _this.listItem[1].getComponentInChildren(cc.Label).node;
                      var newNode = cc.instantiate(labelNode);
                      newNode.parent = labelNode.parent;
                      newNode.runAction(cc.moveTo(.3, cc.v2({
                        x: 0,
                        y: 200
                      })));
                    })));
                  }, 1700);
                  _this.endMove();
                  _this.showEndGame();
                } else _this._flagStop++;
              }
            }
          });
        }
      };
      MiniGameController2.prototype.endMove = function() {
        var _this = this;
        this.listItem.forEach(function(item, index) {
          item.runAction(cc.moveTo(.8, cc.v2({
            x: 350 * (index + 1) - 700,
            y: 0
          })).easing(cc.easeQuadraticActionOut()));
        });
        setTimeout(function() {
          _this.multipleLabel.node.runAction(cc.sequence(cc.scaleTo(.2, 1.3), cc.callFunc(function() {
            _this.multipleLabel.string = "X" + _this.mul;
          }), cc.scaleTo(.3, 1)));
        }, 2470);
      };
      MiniGameController2.prototype.showEndGame = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          return __awaiter(_this, void 0, void 0, function() {
            var _this = this;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                this.gameResult.active = true;
                this.gameResult.opacity = 0;
                return [ 4, util.game.delay(4500, this.node, res).catch(function(e) {}) ];

               case 1:
                _a.sent();
                this.mainGame.runAction(cc.fadeOut(.3));
                this.gameResult.runAction(cc.sequence([ cc.fadeIn(.5), cc.callFunc(function() {
                  _this.coinController.updateUserBalance(_this.mul * _this.baseScore);
                  api.sendGD({
                    e: api.key.SLOT_GET_BONUS,
                    gtype: config.game.SMID[2]
                  }, function(err, data) {
                    cc.log(data);
                  });
                }) ]));
                return [ 2 ];
              }
            });
          });
        });
      };
      MiniGameController2.prototype.backToSlot = function() {
        var _this = this;
        this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          store.emit(store.key.UPDATE_USER_BALANCE, game.user.balance, 0);
          _this.node.destroy();
          view.screen.bonus = void 0;
          controller.ui.showBar();
        }) ]));
      };
      __decorate([ property(cc.Node) ], MiniGameController2.prototype, "itemContainer", void 0);
      __decorate([ property(cc.Node) ], MiniGameController2.prototype, "stopGame", void 0);
      __decorate([ property(cc.Node) ], MiniGameController2.prototype, "gameResult", void 0);
      __decorate([ property(cc.Node) ], MiniGameController2.prototype, "mainGame", void 0);
      __decorate([ property(cc.Label) ], MiniGameController2.prototype, "multipleLabel", void 0);
      __decorate([ property(cc.Label) ], MiniGameController2.prototype, "baseScoreLabel", void 0);
      __decorate([ property(cc.Label) ], MiniGameController2.prototype, "scoreLabel", void 0);
      __decorate([ property(cc.Node) ], MiniGameController2.prototype, "stopOverlay", void 0);
      __decorate([ property(ResourceMinigame2_1.default) ], MiniGameController2.prototype, "resourceMinigame", void 0);
      MiniGameController2 = __decorate([ ccclass ], MiniGameController2);
      return MiniGameController2;
    }(cc.Component);
    exports.default = MiniGameController2;
    cc._RF.pop();
  }, {
    "../../../../scripts/Components/CoinLabel": "CoinLabel",
    "./ResourceMinigame2": "ResourceMinigame2"
  } ],
  MinigameController3: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "77f8aXiQAFHJZ2hdN9f6vul", "MinigameController3");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceMinigame3_1 = require("./ResourceMinigame3");
    var CoinLabel_1 = require("../../../../scripts/Components/CoinLabel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MiniGameController3 = function(_super) {
      __extends(MiniGameController3, _super);
      function MiniGameController3() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceController = null;
        _this.rowBonus = [];
        _this.total = null;
        _this.gameResult = null;
        _this.mainGame = null;
        _this.scoreLabel = null;
        _this.startMinigame = null;
        _this._slotController = null;
        _this.listItem = [];
        _this.level = 0;
        _this.winScore = 0;
        _this.arrBonus = null;
        _this.arrBase = [ [ [ 5, 10, 15, 20, 25, 50 ], [ 15, 20, 25, 30, 35, 40, 100 ], [ 35, 45, 55, 65, 75, 150 ], [ 100, 150, 200, 250, 500 ] ], [ [ 25, 50, 75, 100, 125, 150, 300 ], [ 150, 200, 250, 300, 600 ], [ 400, 500, 600, 1200 ], [ 700, 1e3, 1300, 1600, 1900, 2200, 4400 ] ], [ [ 50, 100, 150, 200, 250, 300, 600 ], [ 300, 400, 500, 600, 1200 ], [ 800, 1e3, 1200, 2400 ], [ 1400, 2e3, 2600, 3200, 3800, 4400, 8800 ] ] ];
        _this.isEvent = true;
        _this._lockRow = [];
        _this.updateTot = 0;
        return _this;
      }
      MiniGameController3.prototype.startGame = function() {
        var _this = this;
        this.startMinigame.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          _this.startMinigame.active = false;
        }) ]));
        this.mainGame.active = true;
      };
      MiniGameController3.prototype.startMiniGame = function(c, w, arr) {
        return __awaiter(this, void 0, void 0, function() {
          var arrItem;
          var _this = this;
          return __generator(this, function(_a) {
            this.level = c;
            this.winScore = w;
            this.arrBonus = arr;
            arrItem = [];
            this.rowBonus.forEach(function(item) {
              arrItem.push(item.children);
            });
            this.listItem = arrItem;
            this.itemEvent(0);
            this.coinController = new CoinLabel_1.default(this.scoreLabel, 11);
            this.coinController.updateUserBalance(0);
            this.resourceController.resourceController = this._slotController.resourceController;
            this.listItem.forEach(function(item, index) {
              item.forEach(function(item) {
                item.getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
                  if (0 == index) return item.name == _this.resourceController.listResourceName.nomal[index + 1];
                  return item.name == _this.resourceController.listResourceName.items[index + 1];
                });
              });
            });
            return [ 2 ];
          });
        });
      };
      MiniGameController3.prototype.itemEvent = function(index) {
        var _this = this;
        index < 4 && true == this.isEvent ? this.listItem[index].forEach(function(item) {
          item.once(cc.Node.EventType.TOUCH_START, function(e) {
            if (_this._lockRow.indexOf(index) >= 0) return;
            _this._lockRow.push(index);
            item.runAction(cc.sequence([ cc.fadeIn(4), cc.callFunc(function() {
              _this.activeRow(index);
            }) ]));
            _this.selectItem(e.currentTarget, index);
            setTimeout(function() {
              _this.itemEvent(index + 1);
            }, 6500);
          });
        }) : this.showEndGame();
      };
      MiniGameController3.prototype.activeRow = function(index) {
        var _this = this;
        setTimeout(function() {
          if (_this.isEvent) {
            if (3 == index) return null;
            _this.listItem[index + 1].forEach(function(item) {
              item.getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
                return item.name == _this.resourceController.listResourceName.nomal[index + 2];
              });
            });
          }
        }, 1500);
      };
      MiniGameController3.prototype.selectItem = function(node, index) {
        var _this = this;
        var nodeUnselected = [];
        this.listItem[index].forEach(function(item) {
          if (item == node) return;
          nodeUnselected.push(item);
        });
        node.getComponent(cc.Sprite).spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.nomal[index + 1];
        });
        var nodeAlpha = new cc.Node();
        nodeAlpha.parent = node.parent;
        nodeAlpha.zIndex = -1;
        nodeAlpha.setPosition(node.getPosition());
        nodeAlpha.addComponent(cc.Sprite).spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.white[index + 1];
        });
        nodeAlpha.runAction(cc.repeatForever(cc.sequence([ cc.fadeIn(.3), cc.delayTime(.1), cc.fadeOut(.3) ])));
        var labelAnimation = node.getComponentInChildren(cc.Label).node;
        labelAnimation.active = true;
        labelAnimation.opacity = 0;
        var pumpkin = this.arrBase[this.level - 1][index][this.arrBase[this.level - 1][index].length - 1];
        setTimeout(function() {
          labelAnimation.runAction(cc.sequence([ cc.callFunc(function() {
            if (_this.arrBonus[index] == pumpkin) {
              node.getComponentInChildren(cc.Label).string = "";
              node.runAction(cc.sequence([ cc.fadeOut(.3), cc.callFunc(function() {
                _this.checkYellowPumpkin(index, node);
              }) ]));
            } else labelAnimation.getComponent(cc.Label).string = "" + _this.arrBonus[index];
            if (void 0 == _this.arrBonus[index]) {
              node.runAction(cc.repeatForever(cc.sequence([ cc.fadeOut(.4), cc.fadeIn(1) ])));
              nodeAlpha.destroy();
              labelAnimation.getComponent(cc.Label).string = "";
              node.getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
                return item.name == _this.resourceController.listResourceName.snake[0];
              });
              _this.isEvent = false;
            }
          }), cc.callFunc(function() {
            return labelAnimation.runAction(cc.fadeIn(.5));
          }), cc.scaleTo(.4, 1.5), cc.scaleTo(.3, 1) ]));
        }, 2500);
        setTimeout(function() {
          nodeUnselected.forEach(function(item) {
            var itemUnselected = item.getComponentInChildren(cc.Label);
            itemUnselected.node.active = true;
            itemUnselected.string = _this.arrBase[_this.level - 1][index][Math.floor(0 + Math.random() * (_this.arrBase[_this.level - 1][index].length - 1))];
            itemUnselected.node.color = new cc.Color(150, 150, 150, 255);
            itemUnselected.node.runAction(cc.sequence([ cc.callFunc(function() {
              return itemUnselected.node.runAction(cc.fadeIn(.5));
            }), cc.scaleTo(.5, 1.3), cc.scaleTo(.5, 1) ]));
            item.getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
              return item.name == _this.resourceController.listResourceName.items[index + 1];
            });
          });
          if (index >= 1) {
            var n = 0;
            while (n < 4) {
              var i = Math.floor(0 + 5 * Math.random());
              var snake = nodeUnselected[i];
              snake.getComponentInChildren(cc.Label).string = "";
              snake.getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
                return item.name == _this.resourceController.listResourceName.snake[0];
              });
              n++;
            }
          }
          if (!(_this.arrBonus[index] == pumpkin)) {
            var pumpkin_1 = nodeUnselected[Math.round(0 + 4 * Math.random())];
            pumpkin_1.getComponentInChildren(cc.Label).string = "";
            pumpkin_1.getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
              return item.name == _this.resourceController.listResourceName.gold[index + 1];
            });
          }
          if (void 0 == _this.arrBonus[index]) {
            _this.total.string = ": " + (_this.updateTot += 0);
            _this.showEndGame();
          } else _this.total.string = ": " + (_this.updateTot += _this.arrBonus[index]);
        }, 5500);
      };
      MiniGameController3.prototype.checkYellowPumpkin = function(index, node) {
        var _this = this;
        node.runAction(cc.sequence([ cc.callFunc(function() {
          node.getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.listResourceName.gold[index + 1];
          });
        }), cc.callFunc(function() {
          return node.runAction(cc.fadeIn(.3));
        }), cc.scaleTo(0, 0), cc.scaleTo(.4, 1) ]));
      };
      MiniGameController3.prototype.showEndGame = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          return __awaiter(_this, void 0, void 0, function() {
            var _this = this;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                return [ 4, util.game.delay(3e3) ];

               case 1:
                _a.sent();
                this.gameResult.active = true;
                this.mainGame.runAction(cc.fadeOut(.3));
                this.gameResult.runAction(cc.sequence([ cc.fadeIn(.5), cc.callFunc(function() {
                  _this.coinController.updateUserBalance(_this.winScore);
                  api.sendGD({
                    e: api.key.SLOT_GET_BONUS,
                    gtype: config.game.SMID[3]
                  }, function(err, data) {
                    cc.log(data);
                  });
                }) ]));
                res();
                return [ 2 ];
              }
            });
          });
        });
      };
      MiniGameController3.prototype.backToSlot = function() {
        var _this = this;
        this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          store.emit(store.key.UPDATE_USER_BALANCE, game.user.balance, 0);
          _this.node.destroy();
          view.screen.bonus = void 0;
          controller.ui.showBar();
        }) ]));
      };
      __decorate([ property(ResourceMinigame3_1.default) ], MiniGameController3.prototype, "resourceController", void 0);
      __decorate([ property(cc.Node) ], MiniGameController3.prototype, "rowBonus", void 0);
      __decorate([ property(cc.Label) ], MiniGameController3.prototype, "total", void 0);
      __decorate([ property(cc.Node) ], MiniGameController3.prototype, "gameResult", void 0);
      __decorate([ property(cc.Node) ], MiniGameController3.prototype, "mainGame", void 0);
      __decorate([ property(cc.Label) ], MiniGameController3.prototype, "scoreLabel", void 0);
      __decorate([ property(cc.Node) ], MiniGameController3.prototype, "startMinigame", void 0);
      MiniGameController3 = __decorate([ ccclass ], MiniGameController3);
      return MiniGameController3;
    }(cc.Component);
    exports.default = MiniGameController3;
    cc._RF.pop();
  }, {
    "../../../../scripts/Components/CoinLabel": "CoinLabel",
    "./ResourceMinigame3": "ResourceMinigame3"
  } ],
  MinigameController4: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "94608N0ZWxGmJDiObfqNnTw", "MinigameController4");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceMinigame4_1 = require("./ResourceMinigame4");
    var CoinLabel_1 = require("../../../../scripts/Components/CoinLabel");
    var AtributeStatic_1 = require("../AtributeStatic");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MinigameController4 = function(_super) {
      __extends(MinigameController4, _super);
      function MinigameController4() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.itemsContainer = null;
        _this.mainGame = null;
        _this.gameResult = null;
        _this.labelGr = null;
        _this.labelMa = null;
        _this.labelMino = null;
        _this.labelMini = null;
        _this.coinLabel = null;
        _this.labelJackpot = null;
        _this.resourceController = null;
        _this._slotController = null;
        _this.bonusList = [];
        _this._isLock = false;
        _this._time = 0;
        _this.idJackpot = 0;
        _this.coinController = null;
        _this.score = 0;
        _this.listItem = [];
        _this.bgBig = null;
        _this.game = null;
        _this.result = null;
        _this.title = null;
        _this.top = null;
        return _this;
      }
      MinigameController4.prototype.startMiniGame = function(dataBonus, grand, major, minor, mini) {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              AtributeStatic_1.default.bonusData = dataBonus;
              this.bonusList = dataBonus;
              this.idJackpot = dataBonus[dataBonus.length - 1];
              this.labelGr.string = grand.string;
              this.labelMa.string = major.string;
              this.labelMino.string = minor.string;
              this.labelMini.string = mini.string;
              this.bgBig = cc.find("bg-big", this.node);
              this.top = cc.find("top", this.node);
              this.game = cc.find("game", this.node);
              this.result = cc.find("result-table", this.node);
              this.title = cc.find("title", this.game);
              this.itemsContainer = cc.find("itemsContainer", this.game);
              this.coinController = new CoinLabel_1.default(this.coinLabel.getComponent(cc.Label), 9);
              this.coinController.updateUserBalance(this.score);
              cc.log("score = ", this.score);
              this.listItem = this.itemsContainer.children;
              this.listItem.forEach(function(item) {
                item.once(cc.Node.EventType.TOUCH_END, function(e) {
                  _this.selectItem(e.currentTarget);
                });
              });
              this.resourceController.resourceController = this._slotController.resourceController;
              return [ 4, this.resourceController.loadImages() ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      MinigameController4.prototype.selectMiniGame = function() {
        var _this = this;
        this.listItem.forEach(function(itemNode, index) {
          itemNode.opacity = 0;
          var sprites = itemNode.children[1].addComponent(cc.Sprite);
          sprites.spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.listResourceName.itemNormal[0];
          });
          itemNode.children[1].once(cc.Node.EventType.TOUCH_END, function(e) {
            _this.selectItem(e.currentTarget);
          });
          setTimeout(function() {
            itemNode.runAction(cc.fadeIn(.5));
          }, 1e3 * Math.random() + 500);
        });
      };
      MinigameController4.prototype.selectItem = function(node) {
        return __awaiter(this, void 0, void 0, function() {
          var num;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (this._isLock) return [ 2 ];
              this._isLock = true;
              num = this.bonusList[this._time];
              this.itemAction(node, num);
              this._time++;
              if (!(this._time == this.bonusList.length)) return [ 3, 2 ];
              return [ 4, this.showEndGame() ];

             case 1:
              _a.sent();
              _a.label = 2;

             case 2:
              this._isLock = false;
              return [ 2 ];
            }
          });
        });
      };
      MinigameController4.prototype.itemAction = function(node, num) {
        var _this = this;
        node.children[0].scale = .3;
        node.children[0].addComponent(cc.Sprite);
        node.children[0].getComponent(cc.Sprite).spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.items[num];
        });
        node.children[1].runAction(cc.sequence(cc.fadeOut(.2), cc.callFunc(function() {
          node.children[0].active = true;
          node.children[0].runAction(cc.sequence(cc.scaleTo(.3, 1.3), cc.scaleTo(.3, 1)));
          node.opacity = 255;
        })));
      };
      MinigameController4.prototype.setWinGame = function(id) {
        var labelJP = [ "Grand Jackpot", "Major Jackpot", "Minor Jackpot", "Mini Jackpot" ];
        var coinJP = [ this.labelGr.string, this.labelMa.string, this.labelMino.string, this.labelMini.string ];
        this.coinLabel.string = coinJP[id];
        this.labelJackpot.string = labelJP[id];
      };
      MinigameController4.prototype.showEndGame = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(res, rej) {
              return __awaiter(_this, void 0, void 0, function() {
                var _this = this;
                return __generator(this, function(_a) {
                  switch (_a.label) {
                   case 0:
                    this.gameResult.active = true;
                    this.gameResult.opacity = 0;
                    this.setWinGame(this.idJackpot);
                    return [ 4, util.game.delay(1e3) ];

                   case 1:
                    _a.sent();
                    this.mainGame.runAction(cc.fadeOut(.3));
                    this.gameResult.runAction(cc.sequence([ cc.fadeIn(.5), cc.callFunc(function() {
                      var slotController = view.slot[4].SlotController;
                      _this.score = slotController.listJP[_this.idJackpot];
                      _this.coinController.updateUserBalance(_this.score);
                      api.sendGD({
                        e: api.key.SLOT_GET_BONUS,
                        gtype: config.game.SMID[4]
                      }, function(err, data) {
                        cc.log(data);
                      });
                    }) ]));
                    res();
                    return [ 2 ];
                  }
                });
              });
            }) ];
          });
        });
      };
      MinigameController4.prototype.backToSlot = function() {
        var _this = this;
        this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          store.emit(store.key.UPDATE_USER_BALANCE, game.user.balance, 0);
          _this.node.destroy();
          view.screen.bonus = void 0;
          controller.ui.showBar();
        }) ]));
      };
      __decorate([ property(cc.Node) ], MinigameController4.prototype, "itemsContainer", void 0);
      __decorate([ property(cc.Node) ], MinigameController4.prototype, "mainGame", void 0);
      __decorate([ property(cc.Node) ], MinigameController4.prototype, "gameResult", void 0);
      __decorate([ property(cc.Label) ], MinigameController4.prototype, "labelGr", void 0);
      __decorate([ property(cc.Label) ], MinigameController4.prototype, "labelMa", void 0);
      __decorate([ property(cc.Label) ], MinigameController4.prototype, "labelMino", void 0);
      __decorate([ property(cc.Label) ], MinigameController4.prototype, "labelMini", void 0);
      __decorate([ property(cc.Label) ], MinigameController4.prototype, "coinLabel", void 0);
      __decorate([ property(cc.Label) ], MinigameController4.prototype, "labelJackpot", void 0);
      __decorate([ property(ResourceMinigame4_1.default) ], MinigameController4.prototype, "resourceController", void 0);
      MinigameController4 = __decorate([ ccclass ], MinigameController4);
      return MinigameController4;
    }(cc.Component);
    exports.default = MinigameController4;
    cc._RF.pop();
  }, {
    "../../../../scripts/Components/CoinLabel": "CoinLabel",
    "../AtributeStatic": "AtributeStatic",
    "./ResourceMinigame4": "ResourceMinigame4"
  } ],
  MinigameController5: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1958fQ+oxFFpqrhzE+2a3xA", "MinigameController5");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceMiniGame5_1 = require("./ResourceMiniGame5");
    var CoinLabel_1 = require("../../../../scripts/Components/CoinLabel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MinigameController5 = function(_super) {
      __extends(MinigameController5, _super);
      function MinigameController5() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.itemsContainer = null;
        _this.scoreLabel = null;
        _this.rightBox = null;
        _this.coinResultLabel = null;
        _this.resourceController = null;
        _this._slotController = null;
        _this.selectTable = null;
        _this.mainGame = null;
        _this.gameResult = null;
        _this.coinController = null;
        _this.coinResultController = null;
        _this.playArray = [];
        _this.baseScore = 0;
        _this.listItem = [];
        _this._isLock = false;
        _this.score = 0;
        _this._time = 0;
        _this._trappedTime = 0;
        return _this;
      }
      MinigameController5.prototype.startMiniGame = function(playArray, baseScore) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            this.playArray = playArray;
            this.baseScore = baseScore;
            this.node.children.forEach(function(item) {
              return item.active = false;
            });
            this.node.children[0].active = true;
            this.selectTable = cc.find("select-table", this.node);
            this.mainGame = cc.find("main-game", this.node);
            this.gameResult = cc.find("result-table", this.node);
            this.coinController = new CoinLabel_1.default(this.scoreLabel);
            this.coinController.updateUserBalance(this.score);
            this.coinResultController = new CoinLabel_1.default(this.coinResultLabel, 15, "", 60);
            this.coinResultController.updateUserBalance(this.score);
            this.listItem = this.itemsContainer.children;
            this.selectTable.active = true;
            this.resourceController.resourceController = this._slotController.resourceController;
            this.resourceController.loadImages();
            return [ 2 ];
          });
        });
      };
      MinigameController5.prototype.selectMiniGame = function() {
        var _this = this;
        this.mainGame.active = true;
        this.mainGame.opacity = 0;
        this.mainGame.runAction(cc.sequence(cc.fadeIn(.5), cc.callFunc(function() {
          _this.selectTable.active = false;
        })));
        this.loadMainGame();
      };
      MinigameController5.prototype.loadMainGame = function() {
        var _this = this;
        this.listItem.forEach(function(itemNode, index) {
          itemNode.opacity = 0;
          var sprite = itemNode.children[0].addComponent(cc.Sprite);
          sprite.spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.listResourceName.items[index + 1].normal;
          });
          itemNode.children[0].once(cc.Node.EventType.TOUCH_END, function(e) {
            _this.selectItem(e.currentTarget);
          });
          setTimeout(function() {
            itemNode.runAction(cc.fadeIn(.5));
          }, 1e3 * Math.random() + 500);
        });
        this.updateTrap();
      };
      MinigameController5.prototype.selectItem = function(node) {
        return __awaiter(this, void 0, void 0, function() {
          var num;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (this._isLock) return [ 2 ];
              this._isLock = true;
              num = this.playArray[this._time];
              if (!(0 == num)) return [ 3, 4 ];
              this._trappedTime++;
              this.updateTrap();
              return [ 4, this.itemAction(node, new cc.Color(255, 0, 0, 255), 0) ];

             case 1:
              _a.sent();
              if (!(this._trappedTime >= 2)) return [ 3, 3 ];
              return [ 4, util.game.delay(1e3) ];

             case 2:
              _a.sent();
              this.showEndGame();
              _a.label = 3;

             case 3:
              return [ 3, 6 ];

             case 4:
              this.score += num * this.baseScore;
              setTimeout(function() {
                _this.scoreLabel.node.runAction(cc.sequence([ cc.scaleTo(.2, 1.2), cc.scaleTo(.2, 1) ]));
                cc.log(_this.score);
                _this.coinController.updateUserBalance(_this.score);
              }, 300);
              this.itemAction(node, new cc.Color(255, 255, 0, 255), num * this.baseScore);
              return [ 4, util.game.delay(150) ];

             case 5:
              _a.sent();
              _a.label = 6;

             case 6:
              this._time++;
              this._isLock = false;
              return [ 2 ];
            }
          });
        });
      };
      MinigameController5.prototype.itemAction = function(node, color, score) {
        var _this = this;
        var name = node.name;
        var index = Number(name.slice(-2)).toString();
        var shadowNode = cc.instantiate(node);
        var shadowSprite = shadowNode.getComponent(cc.Sprite);
        var scoreNode = node.parent.children[1];
        var scoreLabel = scoreNode.getComponent(cc.Label);
        node.runAction(cc.sequence([ cc.delayTime(.2), cc.fadeOut(1), cc.callFunc(function() {
          node.destroy();
        }) ]));
        shadowNode.parent = node.parent;
        shadowSprite.spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.items[index].white;
        });
        shadowNode.color = color;
        shadowNode.opacity = 0;
        shadowNode.runAction(cc.sequence([ cc.fadeTo(.4, 150), cc.fadeOut(1), cc.callFunc(function() {
          shadowNode.destroy();
        }) ]));
        if (score) {
          scoreNode.active = true;
          scoreNode.opacity = 0;
          scoreNode.setPosition(cc.v2(node.x, node.y));
          scoreLabel.fontSize = 50;
          scoreLabel.lineHeight = 60;
          scoreLabel.string = util.string.formatMoney(score);
          scoreNode.runAction(cc.sequence([ cc.delayTime(.4), cc.moveBy(1, cc.v2(1, 45)) ]));
          scoreNode.runAction(cc.sequence([ cc.delayTime(.2), cc.fadeIn(.3), cc.delayTime(.5), cc.fadeOut(.5), cc.callFunc(function() {
            scoreNode.destroy();
          }) ]));
        } else {
          var failNode = new cc.Node();
          var failSprite = failNode.addComponent(cc.Sprite);
          failSprite.spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.listResourceName.fail.active;
          });
          failNode.parent = node.parent;
          failNode.color = new cc.Color(255, 255, 255, 255);
          failNode.setPosition(cc.v2(node.x, node.y));
          failNode.runAction(cc.sequence([ cc.delayTime(.3), cc.moveBy(1, cc.v2(1, 45)) ]));
          failNode.runAction(cc.sequence([ cc.delayTime(.2), cc.fadeIn(.3), cc.delayTime(.5), cc.fadeOut(.5), cc.callFunc(function() {
            scoreNode.destroy();
          }) ]));
        }
      };
      MinigameController5.prototype.updateTrap = function() {
        var _this = this;
        var fail1 = this.rightBox.children[1].getComponent(cc.Sprite);
        var fail2 = this.rightBox.children[2].getComponent(cc.Sprite);
        var fireNormalSpriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.fail.normal;
        });
        var fireAcitveSpriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.fail.active;
        });
        switch (this._trappedTime) {
         case 1:
          fail1.spriteFrame = fireAcitveSpriteFrame;
          setTimeout(function() {
            fail1.node.children[0].active = true;
          }, 200);
          break;

         case 2:
          fail2.spriteFrame = fireAcitveSpriteFrame;
          setTimeout(function() {
            fail2.node.children[0].active = true;
          }, 200);
          break;

         default:
          fail1.spriteFrame = fireNormalSpriteFrame;
          fail2.spriteFrame = fireNormalSpriteFrame;
          fail1.node.children[0].active = false;
          fail2.node.children[0].active = false;
        }
      };
      MinigameController5.prototype.showEndGame = function() {
        var _this = this;
        controller.ui.showModel();
        this.gameResult.active = true;
        this.gameResult.opacity = 0;
        this.gameResult.runAction(cc.sequence([ cc.fadeIn(.5), cc.callFunc(function() {
          _this.coinResultController.updateUserBalance(_this.score);
          api.sendGD({
            e: api.key.SLOT_GET_BONUS,
            gtype: config.game.SMID[5]
          }, function(err, data) {
            cc.log(data);
          });
          controller.ui.hideModel();
        }) ]));
      };
      MinigameController5.prototype.backToSlot = function() {
        var _this = this;
        this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          store.emit(store.key.UPDATE_USER_BALANCE, game.user.balance, 0);
          _this.node.destroy();
          view.screen.bonus = void 0;
          controller.ui.showBar();
        }) ]));
      };
      __decorate([ property(cc.Node) ], MinigameController5.prototype, "itemsContainer", void 0);
      __decorate([ property(cc.Label) ], MinigameController5.prototype, "scoreLabel", void 0);
      __decorate([ property(cc.Node) ], MinigameController5.prototype, "rightBox", void 0);
      __decorate([ property(cc.Label) ], MinigameController5.prototype, "coinResultLabel", void 0);
      __decorate([ property(ResourceMiniGame5_1.default) ], MinigameController5.prototype, "resourceController", void 0);
      MinigameController5 = __decorate([ ccclass ], MinigameController5);
      return MinigameController5;
    }(cc.Component);
    exports.default = MinigameController5;
    cc._RF.pop();
  }, {
    "../../../../scripts/Components/CoinLabel": "CoinLabel",
    "./ResourceMiniGame5": "ResourceMiniGame5"
  } ],
  MinigameController6: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "981af1BTgZF/bp1iOEyEXSR", "MinigameController6");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceMiniGame6_1 = require("./ResourceMiniGame6");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MinigameController6 = function(_super) {
      __extends(MinigameController6, _super);
      function MinigameController6() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.startSpin = null;
        _this.mainReel1 = null;
        _this.mainReel2 = null;
        _this.backgroundSpin = null;
        _this.deactiveSpin = null;
        _this.resourceController = null;
        _this._slotController = null;
        _this.gameCount = 0;
        _this.stickyWildReel = [];
        _this.extraSpin = 0;
        _this.flagSpin = false;
        _this.readyToStop = true;
        _this.isSpin = false;
        _this.itemAmount = 8;
        _this.maxVelocity = 700;
        _this.acceleration = 300;
        _this.resultPosition = [];
        _this.originalRotation = 0;
        _this.reel1Result = -1;
        _this.reel2Result = -1;
        _this.reel1Array = [ 4, 3, 2, 3, 4, 3, 2, 5 ];
        _this.reel2Array = [ "[3,5]", "[1,3,5]", "[3,4,5]", "[1,3]", "[1,2,4,5]", "[3,4]", "[4,5]", "[1]" ];
        _this._time = 0;
        return _this;
      }
      MinigameController6.prototype.onLoad = function() {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2(0);
      };
      MinigameController6.prototype.startMinigame = function(gameCount, stickyWildReel, extraSpin) {
        var _this = this;
        setTimeout(function() {
          _this.mainReel1.node.setPosition(0, 0);
          _this.mainReel2.node.setPosition(0, 0);
        }, 100);
        this.gameCount = gameCount;
        this.stickyWildReel = stickyWildReel;
        this.extraSpin = extraSpin || 0;
        this.backgroundSpin.runAction(cc.rotateBy(1, 100).repeatForever());
        this.backgroundSpin.runAction(cc.sequence([ cc.fadeTo(1, 100), cc.fadeTo(1, 255) ]).repeatForever());
        this.resultPosition = Array(this.itemAmount).fill(0).map(function(item, index) {
          return (_this.originalRotation + index * (360 / _this.itemAmount)) % 360;
        });
      };
      MinigameController6.prototype.spin = function() {
        var _this = this;
        if (this.isSpin) return;
        var gameCount = this.gameCount;
        this.extraSpin > 0 && (gameCount = 5);
        var reel1Array = this.reel1Array.reduce(function(array, item, index) {
          return item == gameCount ? array.concat([ index ]) : array;
        }, []);
        var reel2Json = JSON.stringify(this.stickyWildReel);
        this.reel1Result = reel1Array[Math.random() * reel1Array.length | 0];
        this.reel2Result = this.reel2Array.findIndex(function(item) {
          return item === reel2Json;
        });
        this.isSpin = true;
        this.flagSpin = true;
        this.readyToStop = false;
        this.deactiveSpin.active = true;
        this.deactiveSpin.opacity = 100;
        this.deactiveSpin.runAction(cc.fadeIn(.3));
        setTimeout(function() {
          _this.flagSpin = false;
        }, 5e3);
      };
      MinigameController6.prototype.backToSlot = function() {
        var _this = this;
        this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          api.sendGD({
            e: api.key.SLOT_GET_BONUS,
            gtype: config.game.SMID[6]
          }, function(_a) {
            var status = _a.status, message = _a.message;
            if ("error" == status) return;
            cc.log(message);
            _this._slotController.dataStatus.freeReelGame = _this._slotController.dataStatus.freeReelGame || {};
            _this._slotController.dataStatus.freeReelGame.gameCount = message.gameCount;
            _this._slotController.dataStatus.freeReelGame.userSpined = true;
            _this._slotController.dataStatus.freeSpin.t = message.freeSpin + message.extraSpin | 0;
            _this._slotController.dataStatus.freeSpin.c = message.freeSpin + message.extraSpin | 0;
            _this._slotController.startSlot(true);
            _this.node.destroy();
            view.screen.bonus = void 0;
            controller.ui.showBar();
          });
        }) ]));
      };
      MinigameController6.prototype.endSpin = function(currentRotation, result, direction, node) {
        var _this = this;
        void 0 === direction && (direction = 1);
        var s = this.resultPosition[result] - currentRotation * direction + 720;
        var delta = 360 / this.itemAmount / 10;
        var time = this._time | s / 260;
        this._time = time;
        node.runAction(cc.sequence([ cc.rotateBy(time, -(s + delta) * direction).easing(cc.easeSineOut()), cc.rotateBy(.6, delta * direction).easing(cc.easeBackOut()), cc.callFunc(function() {
          setTimeout(function() {
            _this.isSpin = false;
            _this._time = 0;
          }, 200);
          _this.deactiveSpin.runAction(cc.fadeOut(.2));
          if (_this.extraSpin > 0) {
            _this.extraSpin = 0;
            return;
          }
          setTimeout(function() {
            direction > 0 && _this.backToSlot();
          }, 500);
        }) ]));
      };
      MinigameController6.prototype.update = function(dt) {
        if (this.flagSpin) {
          if (Math.abs(this.mainReel1.angularVelocity) >= this.maxVelocity) return;
          this.mainReel1.angularVelocity += this.acceleration * dt * 1;
          this.mainReel2.angularVelocity += this.acceleration * dt * -1;
        } else {
          if (this.readyToStop) return;
          if (Math.abs(this.mainReel1.angularVelocity) <= 500) {
            this.mainReel1.angularVelocity = 0;
            this.mainReel2.angularVelocity = 0;
            this.endSpin(this.mainReel1.node.rotation % 360, this.reel1Result, 1, this.mainReel1.node);
            this.endSpin(this.mainReel2.node.rotation % 360, this.reel2Result, -1, this.mainReel2.node);
            this.readyToStop = true;
            return;
          }
          this.mainReel1.angularVelocity += -this.acceleration * dt * 1;
          this.mainReel2.angularVelocity += -this.acceleration * dt * -1;
        }
      };
      __decorate([ property(cc.Node) ], MinigameController6.prototype, "startSpin", void 0);
      __decorate([ property(cc.RigidBody) ], MinigameController6.prototype, "mainReel1", void 0);
      __decorate([ property(cc.RigidBody) ], MinigameController6.prototype, "mainReel2", void 0);
      __decorate([ property(cc.Node) ], MinigameController6.prototype, "backgroundSpin", void 0);
      __decorate([ property(cc.Node) ], MinigameController6.prototype, "deactiveSpin", void 0);
      __decorate([ property(ResourceMiniGame6_1.default) ], MinigameController6.prototype, "resourceController", void 0);
      MinigameController6 = __decorate([ ccclass ], MinigameController6);
      return MinigameController6;
    }(cc.Component);
    exports.default = MinigameController6;
    cc._RF.pop();
  }, {
    "./ResourceMiniGame6": "ResourceMiniGame6"
  } ],
  MinigameController7: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9d157zJ0idPO49aCIicrPkX", "MinigameController7");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceMiniGame7_1 = require("./ResourceMiniGame7");
    var CoinLabel_1 = require("../../../../scripts/Components/CoinLabel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MinigameController7 = function(_super) {
      __extends(MinigameController7, _super);
      function MinigameController7() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.buttonThuner = null;
        _this.mainReel = null;
        _this.resourceController = null;
        _this.frostmourne = null;
        _this.meltingIce = [];
        _this.thunder = null;
        _this.buttonFalse = null;
        _this.supper = null;
        _this.labelShield = null;
        _this.alpha = null;
        _this.freeSpin = null;
        _this.reward = null;
        _this.overlay = null;
        _this.jackpotsFreeSpins = null;
        _this._slotController = null;
        _this.gameCount = null;
        _this.flagSpin = false;
        _this.readyToStop = true;
        _this.isSpin = false;
        _this.itemAmount = 10;
        _this.maxVelocity = 600;
        _this.acceleration = 180;
        _this.resultPosition = [];
        _this.originalRotation = 0;
        _this.reelResult = -1;
        _this.reelArray = [ 2, "MINI", 5, "MINOR", "f12", "MINI", 10, "MEGA", "f8", "MAJOR" ];
        _this._time = 0;
        _this.isPlayGame = false;
        return _this;
      }
      MinigameController7.prototype.onLoad = function() {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2(0);
      };
      MinigameController7.prototype.startMiniGame = function(gameCount) {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            cc.log(gameCount.result);
            "SUPPER" == gameCount.type && (this.supper.active = true);
            this.resourceController.resourceController = this._slotController.resourceController;
            setTimeout(function() {
              _this.mainReel.node.runAction(cc.moveTo(0, cc.v2(0, -820)));
              _this.mainReel.node.active = true;
              var data = 0;
              var arrResult = [ 2, 5, 10 ];
              data = _this._slotController.dataSpin ? _this._slotController.dataSpin.spinResult.totalBet : _this._slotController.dataStatus.lastBet;
              _this.labelShield.children.forEach(function(item, index) {
                item.getComponent(cc.Label).string = "" + util.game.abbreviateNumber(arrResult[index] * data, 2);
              });
            }, 100);
            this.gameCount = gameCount;
            this.resultPosition = Array(this.itemAmount).fill(0).map(function(item, index) {
              return (_this.originalRotation + index * (360 / _this.itemAmount)) % 360;
            });
            this.spin();
            this.frostmourne.runAction(cc.sequence([ cc.fadeIn(.5), cc.callFunc(function() {
              _this.mainReel.node.runAction(cc.sequence([ cc.fadeIn(.5), cc.moveTo(1.4, cc.v2(0, -420)) ]));
            }), cc.moveTo(1.5, cc.v2(0, 360)).easing(cc.easeQuadraticActionIn()), cc.moveTo(.1, cc.v2(0, 320)) ]));
            setTimeout(function() {
              _this.buttonThuner.opacity = 0;
              _this.buttonThuner.active = true;
              _this.buttonThuner.runAction(cc.sequence([ cc.scaleTo(0, .2), cc.callFunc(function() {
                _this.buttonThuner.opacity = 255;
                _this.nodeScale(_this.buttonThuner, 1.1, .3, .1);
              }) ]));
            }, 3e3);
            setTimeout(function() {
              _this.flagSpin && _this.stopMinigame();
            }, 1e4);
            return [ 2 ];
          });
        });
      };
      MinigameController7.prototype.spin = function() {
        if (this.isSpin) return;
        var gameCount = this.gameCount.result;
        var reelArray = this.reelArray.reduce(function(array, item, index) {
          return item == gameCount ? array.concat([ index ]) : array;
        }, []);
        this.reelResult = reelArray[Math.random() * reelArray.length | 0];
        this.isSpin = true;
        this.flagSpin = true;
        this.readyToStop = false;
      };
      MinigameController7.prototype.showEndGame = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          return __awaiter(_this, void 0, void 0, function() {
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                return [ 4, util.game.delay(500) ];

               case 1:
                _a.sent();
                api.sendGD({
                  e: api.key.SLOT_GET_BONUS,
                  gtype: config.game.SMID[7]
                }, function(err, data) {
                  cc.log(data);
                });
                res();
                return [ 2 ];
              }
            });
          });
        });
      };
      MinigameController7.prototype.backToSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.showEndGame() ];

             case 1:
              _a.sent();
              this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
                store.emit(store.key.UPDATE_USER_BALANCE, game.user.balance, 9);
                _this.node.destroy();
                view.screen.bonus = void 0;
                controller.ui.showBar();
              }), cc.callFunc(function() {
                if (_this.gameCount.fs > 0) {
                  var fs = void 0;
                  fs = _this._slotController.dataSpin ? _this._slotController.dataSpin.freeSpin.c : _this._slotController.dataStatus.freeSpin.c;
                  var slot = view.screen.slot.SlotController;
                  slot.freeSpin = fs + _this.gameCount.fs;
                  slot.statusGame = fs + _this.gameCount.fs;
                  view.bar.bottom.gameBar.updateStatus(slot.statusGame);
                }
              }) ]));
              return [ 2 ];
            }
          });
        });
      };
      MinigameController7.prototype.stopMinigame = function() {
        this.buttonThuner.active = false;
        this.buttonFalse.active = true;
        this.frostmourne.getComponent(cc.Animation).play();
        this.flagSpin = false;
      };
      MinigameController7.prototype.endSpin = function(currentRotation, result) {
        var _this = this;
        var s = this.resultPosition[result] - currentRotation + 720;
        var delta = 360 / this.itemAmount;
        var time = this._time | s / 150;
        this._time = time;
        this.mainReel.node.runAction(cc.sequence([ cc.callFunc(function() {
          setTimeout(function() {
            _this.frostmourne.getComponent(cc.Animation).pause();
            _this.frostmourne.runAction(cc.sequence([ cc.moveTo(.1, cc.v2(0, 360)), cc.moveTo(0, cc.v2(0, 250)), cc.moveTo(.1, cc.v2(0, 240)), cc.callFunc(function() {
              _this.meltingIce[0].active = false;
              _this.meltingIce[1].active = true;
              _this.thunder.active = true;
              _this.alphaShield(result);
              _this.mainReel.node.runAction(cc.moveTo(.1, cc.v2(0, -430)));
            }), cc.moveTo(.1, cc.v2(0, 250)), cc.callFunc(function() {
              _this.mainReel.node.runAction(cc.moveTo(.1, cc.v2(0, -420)));
            }) ]));
          }, 1e3 * time - 150);
        }), cc.rotateBy(time, s + delta), cc.callFunc(function() {
          setTimeout(function() {
            _this.isSpin = false;
            _this._time = 0;
          }, 200);
          _this.showJackpots(result);
        }) ]));
      };
      MinigameController7.prototype.showJackpots = function(rs) {
        return __awaiter(this, void 0, void 0, function() {
          var color_1, reward, flag_1;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, util.game.delay(3e3) ];

             case 1:
              _a.sent();
              if (!(this.gameCount.fs > 0)) return [ 3, 2 ];
              this.jackpotsFreeSpins.opacity = 0;
              this.jackpotsFreeSpins.active = true;
              this.jackpotsFreeSpins.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
                _this.jackpotsFreeSpins.children[2].children[1].getComponent(cc.Label).string = _this.gameCount.fs;
                _this.jackpotsFreeSpins.children[1].active = false;
                _this.overlay.active = true;
                _this.jackpotsFreeSpins.opacity = 255;
                _this.nodeScale(_this.jackpotsFreeSpins, 1.1, .3, .1);
              }), cc.callFunc(function() {
                setTimeout(function() {
                  var start = _this.jackpotsFreeSpins.children[1];
                  start.opacity = 0;
                  start.active = true;
                  start.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
                    start.opacity = 255;
                    _this.nodeScale(start, 1.1, .3, .1);
                  }) ]));
                }, 200);
              }) ]));
              return [ 3, 5 ];

             case 2:
              color_1 = {
                0: [ 253, 168, 83 ],
                1: [ 102, 208, 38 ],
                2: [ 57, 189, 252 ],
                3: [ 241, 57, 111 ],
                4: [ 253, 183, 43 ],
                5: [ 253, 183, 43 ],
                6: [ 253, 183, 43 ]
              };
              reward = [ "MINI", "MINOR", "MAJOR", "MEGA", 2, 5, 10 ];
              return [ 4, reward.forEach(function(item, index) {
                if (item == _this.reelArray[rs]) {
                  _this.reward.opacity = 0;
                  _this.overlay.active = true;
                  _this.reward.active = true;
                  _this.reward.children[0].color = cc.color(color_1[index][0], color_1[index][1], color_1[index][2]);
                  _this.reward.runAction(cc.sequence([ cc.callFunc(function() {
                    isNaN(_this.reelArray[rs]) ? _this.reward.children[2].getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
                      return item.name == _this.resourceController.listResourceName.jackpots[index + 1];
                    }) : _this.reward.children[2].getComponent(cc.Sprite).spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
                      return item.name == _this.resourceController.listResourceName.jackpots[5];
                    });
                  }), cc.scaleTo(0, .1), cc.callFunc(function() {
                    _this.reward.opacity = 255;
                    var totalWin;
                    _this.coinController = new CoinLabel_1.default(_this.reward.children[3].getComponent(cc.Label), 8);
                    totalWin = _this._slotController.dataSpin ? _this._slotController.dataSpin.spinResult.score.sum[2].w * _this._slotController.dataSpin.spinResult.totalBet : _this._slotController.dataStatus.bonus.w * _this._slotController.dataStatus.lastBet;
                    "SUPPER" === _this.gameCount.type ? _this.reward.children[3].getComponent(cc.Label).string = "" + _this.coinController.updateUserBalance(2 * totalWin) : _this.reward.children[3].getComponent(cc.Label).string = "" + _this.coinController.updateUserBalance(totalWin);
                    _this.nodeScale(_this.reward, 1.2, .3, .1);
                  }) ]));
                }
              }) ];

             case 3:
              _a.sent();
              return [ 4, util.game.delay(1e3) ];

             case 4:
              _a.sent();
              flag_1 = 1;
              this.overlay.on(cc.Node.EventType.TOUCH_START, function(event) {
                if (1 === flag_1) {
                  _this.isPlayGame = true;
                  _this.backToSlot();
                  flag_1++;
                }
              });
              setTimeout(function() {
                _this.isPlayGame || _this.backToSlot();
              }, 1e4);
              _a.label = 5;

             case 5:
              return [ 2 ];
            }
          });
        });
      };
      MinigameController7.prototype.alphaShield = function(res) {
        var color = {
          0: [ 250, 255, 255 ],
          1: [ 253, 144, 39 ],
          2: [ 250, 255, 255 ],
          3: [ 102, 227, 42 ],
          4: [ 255, 245, 55 ],
          5: [ 253, 144, 39 ],
          6: [ 250, 255, 255 ],
          7: [ 253, 98, 241 ],
          8: [ 255, 245, 55 ],
          9: [ 83, 223, 253 ]
        };
        this.alpha.active = true;
        this.alpha.color = new cc.Color(color[res][0], color[res][1], color[res][2]);
        this.alpha.runAction(cc.repeatForever(cc.sequence([ cc.fadeIn(.1), cc.fadeOut(.9) ])));
        if (4 === res || 8 === res) {
          this.freeSpin.active = true;
          4 === res ? this.freeSpin.children[0].active = true : this.freeSpin.children[1].active = true;
        }
      };
      MinigameController7.prototype.nodeScale = function(node, scale, timeStar, timeEnd) {
        node.runAction(cc.sequence([ cc.scaleTo(timeStar, scale), cc.scaleTo(timeEnd, 1) ]));
      };
      MinigameController7.prototype.update = function(dt) {
        if (this.flagSpin) {
          if (Math.abs(this.mainReel.angularVelocity) >= this.maxVelocity) return;
          this.mainReel.angularVelocity += this.acceleration * dt;
        } else {
          if (this.readyToStop) return;
          if (Math.abs(this.mainReel.angularVelocity) <= 180) {
            this.mainReel.angularVelocity = 0;
            this.endSpin(this.mainReel.node.rotation % 360, this.reelResult);
            this.readyToStop = true;
            return;
          }
          this.mainReel.angularVelocity += -this.acceleration * dt;
        }
      };
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "buttonThuner", void 0);
      __decorate([ property(cc.RigidBody) ], MinigameController7.prototype, "mainReel", void 0);
      __decorate([ property(ResourceMiniGame7_1.default) ], MinigameController7.prototype, "resourceController", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "frostmourne", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "meltingIce", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "thunder", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "buttonFalse", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "supper", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "labelShield", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "alpha", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "freeSpin", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "reward", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "overlay", void 0);
      __decorate([ property(cc.Node) ], MinigameController7.prototype, "jackpotsFreeSpins", void 0);
      MinigameController7 = __decorate([ ccclass ], MinigameController7);
      return MinigameController7;
    }(cc.Component);
    exports.default = MinigameController7;
    cc._RF.pop();
  }, {
    "../../../../scripts/Components/CoinLabel": "CoinLabel",
    "./ResourceMiniGame7": "ResourceMiniGame7"
  } ],
  MinigameController8: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "938dfVsB4lMZoA87UUNOy5C", "MinigameController8");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CoinLabel_1 = require("../../../../scripts/Components/CoinLabel");
    var ResourceMiniGame8_1 = require("./ResourceMiniGame8");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MinigameController8 = function(_super) {
      __extends(MinigameController8, _super);
      function MinigameController8() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.processBar = null;
        _this.itemsContainer = null;
        _this.rightBox = null;
        _this.coinResultLabel = null;
        _this.resourceController = null;
        _this.failNode = null;
        _this.bar = null;
        _this._slotController = null;
        _this.selectTable = null;
        _this.mainGame = null;
        _this.gameResult = null;
        _this.topLabel = null;
        _this.top = null;
        _this.bottom = null;
        _this.dataLength = [ 30, 38, 53, 72, 87, 95, 95, 95, 95, 149 ];
        _this.index = 0;
        _this.sum = 0;
        _this._tempProgress = 0;
        _this._tempSetTimeout = null;
        _this.coinResultController = null;
        _this.data = [];
        _this.coinWin = 0;
        _this.res = 0;
        _this.playArray = [];
        _this.listItem = [];
        _this.listLabel = [];
        _this._isLock = false;
        _this.score = 0;
        _this._time = 0;
        _this._trappedTime = 0;
        _this.rand = 7;
        return _this;
      }
      MinigameController8.prototype.startMiniGame = function(data, coinWin, res) {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            this.data = data;
            this.coinWin = coinWin;
            this.res = res;
            this.rand = Math.floor(3 * Math.random()) + 3;
            this.node.children.forEach(function(item) {
              return item.active = false;
            });
            this.node.children[0].active = true;
            this.selectTable = cc.find("bonus_active", this.node);
            this.mainGame = cc.find("main-game", this.node);
            this.gameResult = cc.find("result-table", this.node);
            this.top = cc.find("top", this.mainGame);
            this.topLabel = cc.find("bg_power_win-02", this.top);
            this.bottom = cc.find("bottom", this.mainGame);
            this.rightBox = cc.find("bg_fail", this.bottom);
            this.coinResultController = new CoinLabel_1.default(this.coinResultLabel, 15, "", 60);
            this.coinResultController.updateUserBalance(this.coinWin);
            this.listItem = this.itemsContainer.children;
            this.listLabel = this.topLabel.children;
            this.listLabel.forEach(function(item) {
              var index = Number(item.name.slice(-2)).toString();
              item.getComponent(cc.Label).string = data[index];
            });
            this.selectTable.active = true;
            this.selectTable.children.forEach(function(item) {
              item.once(cc.Node.EventType.TOUCH_START, function(e) {
                _this.selectBookItem(e.currentTarget);
              });
            });
            this.resourceController.resourceController = this._slotController.resourceController;
            this.resourceController.loadImages();
            this.playArray = this.getArrayResult(this.rand, res);
            this.processBar.progress = 0;
            return [ 2 ];
          });
        });
      };
      MinigameController8.prototype.getArrayResult = function(randNumber, result) {
        var arr = [];
        for (var i = 0; i < randNumber - 1; i++) arr.push(0);
        for (var i = randNumber - 1; i < result + randNumber - 1; i++) arr.push(1);
        this.shuffle(arr);
        arr.push(0);
        return arr;
      };
      MinigameController8.prototype.shuffle = function(array) {
        array.sort(function() {
          return Math.random() - .5;
        });
      };
      MinigameController8.prototype.selectMiniGame = function() {
        var _this = this;
        util.game.delay(2);
        this.mainGame.active = true;
        this.mainGame.opacity = 0;
        setTimeout(function() {
          _this.mainGame.runAction(cc.sequence(cc.fadeIn(2), cc.callFunc(function() {
            _this.selectTable.active = false;
          })));
          _this.loadMainGame();
        }, 1500);
      };
      MinigameController8.prototype.selectBookItem = function(node) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            node.children[0].active = true;
            node.children[0].getComponent(cc.Label).string = this.rand + " Fails";
            node.children[0].runAction(cc.sequence([ cc.delayTime(.3), cc.moveBy(.5, cc.v2(1, 45)) ]));
            return [ 2 ];
          });
        });
      };
      MinigameController8.prototype.loadMainGame = function() {
        var _this = this;
        this.listItem.forEach(function(itemNode, index) {
          itemNode.opacity = 0;
          var sprite = itemNode.children[0].addComponent(cc.Sprite);
          sprite.spriteFrame = _this.resourceController.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.listResourceName.itemNormal;
          });
          itemNode.children[0].once(cc.Node.EventType.TOUCH_END, function(e) {
            _this.selectItem(e.currentTarget);
          });
          setTimeout(function() {
            itemNode.runAction(cc.fadeIn(.5));
          }, 1e3 * Math.random() + 500);
        });
        this.showFails(this.rand);
      };
      MinigameController8.prototype.selectItem = function(node) {
        return __awaiter(this, void 0, void 0, function() {
          var num;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (this._isLock) return [ 2 ];
              this._isLock = true;
              num = this.playArray[this._time];
              if (!(0 == num)) return [ 3, 4 ];
              this._trappedTime++;
              this.showFails(this.rand);
              return [ 4, this.itemAction(node, new cc.Color(255, 0, 0, 255), 0) ];

             case 1:
              _a.sent();
              if (!(this._trappedTime >= this.rand)) return [ 3, 3 ];
              return [ 4, util.game.delay(1e3) ];

             case 2:
              _a.sent();
              this.showEndGame();
              _a.label = 3;

             case 3:
              return [ 3, 6 ];

             case 4:
              this.sum += this.dataLength[this.index];
              this.index++;
              cc.log("csum = ", this.sum);
              this.updateProgress(this.sum / this.processBar.totalLength);
              this.itemAction(node, new cc.Color(255, 255, 0, 255), num);
              return [ 4, util.game.delay(150) ];

             case 5:
              _a.sent();
              _a.label = 6;

             case 6:
              this._time++;
              this._isLock = false;
              return [ 2 ];
            }
          });
        });
      };
      MinigameController8.prototype.itemAction = function(node, color, num) {
        var _this = this;
        var shadowNode = cc.instantiate(node);
        var resultNode = cc.instantiate(node);
        var shadowSprite = shadowNode.getComponent(cc.Sprite);
        var resultSprite = resultNode.getComponent(cc.Sprite);
        node.runAction(cc.sequence([ cc.delayTime(.2), cc.fadeOut(1), cc.callFunc(function() {
          node.destroy();
        }) ]));
        shadowNode.parent = node.parent;
        resultNode.parent = node.parent;
        shadowSprite.spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.itemEffect;
        });
        resultSprite.spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.items[num];
        });
        shadowNode.color = color;
        shadowNode.opacity = 0;
        shadowNode.runAction(cc.sequence([ cc.fadeTo(.4, 150), cc.fadeOut(1), cc.callFunc(function() {
          shadowNode.destroy();
        }) ]));
        if (1 == num) {
          resultNode.active = true;
          resultNode.opacity = 0;
          resultNode.setPosition(cc.v2(node.x, node.y));
          resultNode.runAction(cc.sequence([ cc.delayTime(.4), cc.moveBy(1, cc.v2(1, 45)) ]));
          resultNode.runAction(cc.sequence([ cc.delayTime(.2), cc.fadeIn(.3), cc.delayTime(.5), cc.fadeOut(.5), cc.callFunc(function() {
            resultNode.destroy();
          }) ]));
        } else {
          var failNode = new cc.Node();
          var failSprite = failNode.addComponent(cc.Sprite);
          failSprite.spriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.listResourceName.fail.active;
          });
          failNode.parent = node.parent;
          failNode.color = new cc.Color(255, 255, 255, 255);
          failNode.setPosition(cc.v2(node.x, node.y));
          failNode.runAction(cc.sequence([ cc.delayTime(.3), cc.moveBy(1, cc.v2(1, 45)) ]));
          failNode.runAction(cc.sequence([ cc.delayTime(.2), cc.fadeIn(.3), cc.delayTime(.5), cc.fadeOut(.5), cc.callFunc(function() {}) ]));
        }
      };
      MinigameController8.prototype.showFails = function(num) {
        var _this = this;
        var fireAcitveSpriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.fail.active;
        });
        var fireNormalSpriteFrame = this.resourceController.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.listResourceName.fail.normal;
        });
        this.rightBox.children.forEach(function(item) {
          return item.active = false;
        });
        for (var i = 0; i < num; i++) {
          this.rightBox.children[i].getComponent(cc.Sprite).spriteFrame = fireNormalSpriteFrame;
          this.rightBox.children[i].active = true;
          this.rightBox.children[i].setPosition((i + .5) * (this.rightBox.width - 27) / num + 12, this.rightBox.height / 2);
        }
        this.showItemFailActive(this._trappedTime, fireAcitveSpriteFrame);
      };
      MinigameController8.prototype.showItemFailActive = function(num, sprite) {
        for (var i = 0; i < num; i++) this.rightBox.children[i].getComponent(cc.Sprite).spriteFrame = sprite;
      };
      MinigameController8.prototype.updateProgress = function(progress) {
        var _this = this;
        if (null != progress) {
          progress > 1 && (progress = 1);
          progress < 0 && (progress = 0);
          this.processBar.progress = progress;
        }
        this.updateLength();
        if (Math.abs(this.processBar.progress - this._tempProgress) < .001) {
          this._tempProgress = this.processBar.progress;
          return true;
        }
        var delta = this.processBar.progress - this._tempProgress;
        this._tempProgress = 5 * delta / 10 / 30 + this._tempProgress;
        this._tempSetTimeout = setTimeout(function() {
          _this.updateProgress();
        }, 1e3 / 30);
      };
      MinigameController8.prototype.onDestroy = function() {
        clearTimeout(this._tempSetTimeout);
      };
      MinigameController8.prototype.updateLength = function() {
        this.bar.width = this.processBar.totalLength * this._tempProgress;
      };
      MinigameController8.prototype.showEndGame = function() {
        var _this = this;
        controller.ui.showModel();
        this.gameResult.active = true;
        this.gameResult.opacity = 0;
        this.gameResult.runAction(cc.sequence([ cc.fadeIn(.5), cc.callFunc(function() {
          _this.coinResultController.updateUserBalance(_this.score);
          api.sendGD({
            e: api.key.SLOT_GET_BONUS,
            gtype: config.game.SMID[8]
          }, function(err, data) {
            cc.log(data);
          });
          controller.ui.hideModel();
        }) ]));
      };
      MinigameController8.prototype.backToSlot = function() {
        var _this = this;
        this.node.runAction(cc.sequence([ cc.fadeOut(.5), cc.callFunc(function() {
          store.emit(store.key.UPDATE_USER_BALANCE, game.user.balance, 0);
          _this.node.destroy();
          view.screen.bonus = void 0;
          controller.ui.showBar();
        }) ]));
      };
      __decorate([ property(cc.ProgressBar) ], MinigameController8.prototype, "processBar", void 0);
      __decorate([ property(cc.Node) ], MinigameController8.prototype, "itemsContainer", void 0);
      __decorate([ property(cc.Node) ], MinigameController8.prototype, "rightBox", void 0);
      __decorate([ property(cc.Label) ], MinigameController8.prototype, "coinResultLabel", void 0);
      __decorate([ property(ResourceMiniGame8_1.default) ], MinigameController8.prototype, "resourceController", void 0);
      __decorate([ property(cc.Node) ], MinigameController8.prototype, "failNode", void 0);
      __decorate([ property(cc.Node) ], MinigameController8.prototype, "bar", void 0);
      MinigameController8 = __decorate([ ccclass ], MinigameController8);
      return MinigameController8;
    }(cc.Component);
    exports.default = MinigameController8;
    cc._RF.pop();
  }, {
    "../../../../scripts/Components/CoinLabel": "CoinLabel",
    "./ResourceMiniGame8": "ResourceMiniGame8"
  } ],
  MultiScene: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "71368zSgR5C6Z9YinXw0fTN", "MultiScene");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MultiScene = function(_super) {
      __extends(MultiScene, _super);
      function MultiScene() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.list = null;
        _this.content = null;
        _this.overlay = null;
        _this.slotItem = null;
        _this.isShow = false;
        _this.listIcon = [];
        _this.resPath = "3_homeScene/slotIcon";
        _this._isLock = false;
        return _this;
      }
      MultiScene.prototype.onLoad = function() {
        this.initialize();
        this.loadResource();
      };
      MultiScene.prototype.loadResource = function() {
        var _this = this;
        cc.loader.loadResDir(this.resPath, cc.SpriteFrame, function(err, data) {
          _this.listIcon = data;
        });
      };
      MultiScene.prototype.initialize = function() {};
      MultiScene.prototype.loadContent = function() {
        var _this = this;
        var scenesManager = Object.values(view.slot).filter(function(o) {
          return !!o;
        });
        this.list.content.removeAllChildren();
        this.listScene = [];
        scenesManager.forEach(function(item) {
          var button = cc.instantiate(_this.slotItem);
          var scene = {
            uuid: item.uuid,
            id: item.id,
            button: button,
            smid: item.gameId
          };
          button.parent = _this.list.content;
          button.active = true;
          var gameId = scene.id;
          var icon = button.getChildByName("icon").getChildByName("ic").getComponent(cc.Sprite);
          var titleLabel = button.getChildByName("title-label").getComponent(cc.Label);
          var betLabel = button.getChildByName("bet-label").getComponent(cc.Label);
          var autoBetLabel = button.getChildByName("auto-label").getComponent(cc.Label);
          icon.spriteFrame = _this.listIcon.find(function(item) {
            return item.name === gameId.toString();
          });
          titleLabel.string = item.name;
          betLabel.string = "Bet: " + util.game.abbreviateNumber(item.SlotController.currentBet);
          item.SlotController.autoSpinTimes < 0 ? autoBetLabel.string = "Auto: ++" : autoBetLabel.string = "Auto: " + item.SlotController.autoSpinTimes;
          _this.listScene.push(scene);
        });
        this.addEventButton();
      };
      MultiScene.prototype.toggleButton = function() {
        var _this = this;
        if (this._isLock) return;
        this._isLock = true;
        this.isShow ? this.content.runAction(cc.moveBy(.2, cc.v2(0, this.content.height))) : this.content.runAction(cc.moveBy(.2, cc.v2(0, -this.content.height)));
        this.isShow = !this.isShow;
        this.overlay.active = this.isShow;
        setTimeout(function() {
          _this._isLock = false;
        }, 400);
      };
      MultiScene.prototype.addEventButton = function() {
        cc.log("On click item");
        this.listScene.forEach(function(item) {
          item.button.on("click", function() {
            if (view.screen.slot) if (view.screen.slot.uuid != item.uuid) {
              view.screen.slot.SlotController.hideContent();
              controller.ui.switchSlot(item.uuid, item.id);
            } else cc.log("You currently on this game"); else controller.ui.resumeSlot(item.uuid, item.id);
          });
        });
      };
      __decorate([ property(cc.ScrollView) ], MultiScene.prototype, "list", void 0);
      __decorate([ property(cc.Node) ], MultiScene.prototype, "content", void 0);
      __decorate([ property(cc.Node) ], MultiScene.prototype, "overlay", void 0);
      __decorate([ property(cc.Node) ], MultiScene.prototype, "slotItem", void 0);
      MultiScene = __decorate([ ccclass ], MultiScene);
      return MultiScene;
    }(cc.Component);
    exports.default = MultiScene;
    cc._RF.pop();
  }, {} ],
  ResourceController1: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ccd21d0I/FHWbNRD2GcSIuP", "ResourceController1");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("../../../scripts/Slots/ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController1 = function(_super) {
      __extends(ResourceController1, _super);
      function ResourceController1() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceListName = {
          item: {
            s: "img/item/item-egypt-2",
            s2: "img/item/item-egypt-1",
            c: "img/item/egypt-coin-2",
            w: "img/item/item-egypt-3",
            sw: "img/item/item-egypt-3",
            0: "img/item/item-egypt-7",
            1: "img/item/item-egypt-8",
            2: "img/item/item-egypt-9",
            3: "img/item/item-egypt-4",
            4: "img/item/item-egypt-5",
            5: "img/item/item-egypt-6",
            6: "img/item/item-egypt-11",
            7: "img/item/item-egypt-12",
            8: "img/item/item-egypt-13",
            9: "img/item/item-egypt-14"
          },
          subItem: {
            coinGray: "img/item/egypt-coin-3",
            coin: "img/item/egypt-coin-1",
            wildBorder: "img/item/wild-border",
            bg: "/img/bg-big"
          },
          payTable: {}
        };
        _this.storageBase = "game-slot/1_Slot1/";
        _this.resourcePath = "4_gameScene/1_Slot1/";
        return _this;
      }
      ResourceController1 = __decorate([ ccclass ], ResourceController1);
      return ResourceController1;
    }(ResourceController_1.default);
    exports.default = ResourceController1;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/ResourceController": "ResourceController"
  } ],
  ResourceController2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "342b50tHJZPUIUrOPLwjhVW", "ResourceController2");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("../../../scripts/Slots/ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController2 = function(_super) {
      __extends(ResourceController2, _super);
      function ResourceController2() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceListName = {
          item: {
            s: "img/item/game-dra-item-free-spin-1",
            s2: "img/item/game-dra-item-freespin-2",
            c: "img/item/game-dra-item-coin-gold-mon",
            w: "img/item/game-dra-item-dra-wild-update",
            sw: "img/item/game-dra-item-dra-wild-update",
            0: "img/item/game-dra-item-1",
            1: "img/item/game-dra-item-2",
            2: "img/item/game-dra-item-3",
            3: "img/item/game-dra-item-4",
            4: "img/item/game-dra-item-5",
            5: "img/item/game-dra-item-6",
            6: "img/item/game-dra-item-7",
            7: "img/item/game-dra-item-special-1",
            8: "img/item/game-dra-item-special-2",
            9: "img/item/game-dra-item-special-3"
          },
          itemBorder: {
            sw: "img/item/game-dra-item-wild-bg",
            w: "img/item/game-dra-item-wild-bg",
            7: "img/item/game-dra-item-special-1-bg",
            8: "img/item/game-dra-item-special-2-bg",
            9: "img/item/game-dra-item-special-3-bg",
            bg: "/img/bg-big"
          },
          subItem: {
            coinGray: "img/item/game-dra-item-coin-gray-mon",
            coin: "img/item/game-dra-item-coin-gold",
            wildBorder: "img/item/game-dra-item-wild-bg"
          },
          payTable: {}
        };
        _this.storageBase = "game-slot/2_Slot2/";
        _this.resourcePath = "4_gameScene/2_Slot2/";
        return _this;
      }
      ResourceController2.prototype.getListItemBorder = function() {
        var _this = this;
        var listBorderName = Object.keys(this.resourceListName.itemBorder);
        return listBorderName.reduce(function(res, i) {
          return res.concat([ {
            id: i,
            img: _this.spriteList.find(function(x) {
              return x.name == _this.resourceListName.itemBorder[i].split("/").pop();
            })
          } ]);
        }, []);
      };
      ResourceController2 = __decorate([ ccclass ], ResourceController2);
      return ResourceController2;
    }(ResourceController_1.default);
    exports.default = ResourceController2;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/ResourceController": "ResourceController"
  } ],
  ResourceController3: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f0516qHNG1Pg6XRVf2I3Fb1", "ResourceController3");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("../../../scripts/Slots/ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController3 = function(_super) {
      __extends(ResourceController3, _super);
      function ResourceController3() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceListName = {
          item: {},
          payTable: {},
          subItem: {
            bg: "img/bg-big"
          }
        };
        _this.storageBase = "game-slot/3_Slot3/";
        _this.resourcePath = "4_gameScene/3_Slot3/";
        return _this;
      }
      ResourceController3 = __decorate([ ccclass ], ResourceController3);
      return ResourceController3;
    }(ResourceController_1.default);
    exports.default = ResourceController3;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/ResourceController": "ResourceController"
  } ],
  ResourceController4: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c35c9qovltLIZDUmx3E5FtT", "ResourceController4");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("../../../scripts/Slots/ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController4 = function(_super) {
      __extends(ResourceController4, _super);
      function ResourceController4() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.storageBase = "game-slot/4_Slot4/";
        _this.resourcePath = "4_gameScene/4_Slot4/";
        _this.resourceListName = {
          item: {
            s: "img/item/item-casino-scatter",
            w: "img/item/item-casino-wild",
            0: "img/item/item-casino-1",
            1: "img/item/item-casino-2",
            2: "img/item/item-casino-3",
            3: "img/item/item-casino-4",
            4: "img/item/item-casino-5",
            5: "img/item/item-casino-6",
            11: "img/item/item-casino-7",
            12: "img/item/item-casino-8",
            13: "img/item/item-casino-9",
            14: "img/item/item-casino-10",
            15: "img/item/item-casino-11",
            22: "img/item/item-casino-8-gold",
            23: "img/item/item-casino-9-gold",
            24: "img/item/item-casino-10-gold",
            25: "img/item/item-casino-11-gold"
          },
          payTable: {},
          itemBorder: {
            12: "img/bg-item-casino-8",
            13: "img/bg-item-casino-9",
            14: "img/bg-item-casino-10",
            15: "img/bg-item-casino-11",
            22: "img/bg-item-casino-8",
            23: "img/bg-item-casino-9",
            24: "img/bg-item-casino-10",
            25: "img/bg-item-casino-11",
            w: "img/bg-item-casino-wild",
            s: "img/bg-item-casino-scatter"
          },
          itemGold: {
            4: "img/item/item-casino-7",
            3: "img/item/item-casino-8-gold",
            2: "img/item/item-casino-9-gold",
            1: "img/item/item-casino-10-gold",
            0: "img/item/item-casino-11-gold"
          },
          subItem: {
            bg: "img/bg-big.jpg"
          },
          minigame: [ "minigame/casino-slot-coin-grand", "minigame/casino-slot-coin-major", "minigame/casino-slot-coin-minor", "minigame/casino-slot-coin-mini", "minigame/casino-slot-coin-normal" ],
          borderWinItem: [ "img/casino-slot-stroke-win" ]
        };
        return _this;
      }
      ResourceController4.prototype.getListItemBorder = function() {
        var _this = this;
        var listBorderName = Object.keys(this.resourceListName.itemBorder);
        listBorderName = listBorderName.reduce(function(res, i) {
          return res.concat([ {
            id: i,
            img: _this.spriteList.find(function(x) {
              return x.name == _this.resourceListName.itemBorder[i].split("/").pop();
            })
          } ]);
        }, []);
        return listBorderName;
      };
      ResourceController4 = __decorate([ ccclass ], ResourceController4);
      return ResourceController4;
    }(ResourceController_1.default);
    exports.default = ResourceController4;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/ResourceController": "ResourceController"
  } ],
  ResourceController5: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0cc3c7p7CpEm43DoqhIpGsS", "ResourceController5");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("../../../scripts/Slots/ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController5 = function(_super) {
      __extends(ResourceController5, _super);
      function ResourceController5() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceListName = {
          item: {
            b: "img/item/item-bonus-gem",
            s: "img/item/item-scatter-gem",
            w: "img/item/item-wild-gem",
            0: "img/item/gem-item-ingame-1",
            1: "img/item/gem-item-ingame-2",
            2: "img/item/gem-item-ingame-3",
            3: "img/item/gem-item-ingame-4",
            4: "img/item/gem-item-ingame-5",
            5: "img/item/gem-item-ingame-6",
            6: "img/item/gem-item-ingame-7",
            7: "img/item/gem-item-ingame-8",
            8: "img/item/gem-item-ingame-9"
          },
          payTable: {},
          itemBorder: {
            b: "img/item/bg-bonus-gem",
            s: "img/item/bg-scatter-gem",
            w: "img/item/bg-wild-gem"
          },
          subItem: {
            bg: "img/bg-big"
          },
          minigame: [ "minigame/gem_minigame_bonus_item_01", "minigame/gem_minigame_bonus_item_02", "minigame/gem_minigame_bonus_item_03", "minigame/gem_minigame_bonus_item_04", "minigame/gem_minigame_bonus_item_05", "minigame/gem_minigame_bonus_item_06", "minigame/gem_minigame_bonus_item_07", "minigame/gem_minigame_bonus_item_08", "minigame/gem_minigame_bonus_item_09", "minigame/gem_minigame_bonus_item_10", "minigame/gem_minigame_bonus_item_11", "minigame/gem_minigame_bonus_item_12", "minigame/gem_minigame_bonus_item_13", "minigame/gem_minigame_bonus_item_14", "minigame/gem_minigame_bonus_item_15", "minigame/gem_minigame_bonus_item_fail_01", "minigame/gem_minigame_bonus_item_fail_02", "minigame/gem_minigame_bonus_item_total", "minigame/gem_minigame_bonus_item_white_01", "minigame/gem_minigame_bonus_item_white_02", "minigame/gem_minigame_bonus_item_white_03", "minigame/gem_minigame_bonus_item_white_04", "minigame/gem_minigame_bonus_item_white_05", "minigame/gem_minigame_bonus_item_white_06", "minigame/gem_minigame_bonus_item_white_07", "minigame/gem_minigame_bonus_item_white_08", "minigame/gem_minigame_bonus_item_white_09", "minigame/gem_minigame_bonus_item_white_10", "minigame/gem_minigame_bonus_item_white_11", "minigame/gem_minigame_bonus_item_white_12", "minigame/gem_minigame_bonus_item_white_13", "minigame/gem_minigame_bonus_item_white_14", "minigame/gem_minigame_bonus_item_white_15", "minigame/gem_minigame_bonus_item_white_fail" ]
        };
        _this.storageBase = "game-slot/5_Slot5/";
        _this.resourcePath = "4_gameScene/5_Slot5/";
        return _this;
      }
      ResourceController5.prototype.getListItemBorder = function() {
        var _this = this;
        var listBorderName = Object.keys(this.resourceListName.itemBorder);
        return listBorderName.reduce(function(res, i) {
          return res.concat([ {
            id: i,
            img: _this.spriteList.find(function(x) {
              return x.name == _this.resourceListName.itemBorder[i].split("/").pop();
            })
          } ]);
        }, []);
      };
      ResourceController5 = __decorate([ ccclass ], ResourceController5);
      return ResourceController5;
    }(ResourceController_1.default);
    exports.default = ResourceController5;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/ResourceController": "ResourceController"
  } ],
  ResourceController6: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0b042bTVWBBuKjiup0pyodv", "ResourceController6");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("../../../scripts/Slots/ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController6 = function(_super) {
      __extends(ResourceController6, _super);
      function ResourceController6() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceListName = {
          item: {},
          payTable: {},
          slotComponent: {
            originalBack: "img/bg_game_02",
            bonusBack: "img/bonus_game_bg"
          },
          subItem: {
            bg: "img/bg-big"
          },
          minigame: []
        };
        _this.audioListName = {
          intro: "xinfu_welcome",
          background: "xinfu_intro_loop",
          win: "xinfu_bang_up2"
        };
        _this.storageBase = "game-slot/6_Slot6/";
        _this.resourcePath = "4_gameScene/6_Slot6/";
        return _this;
      }
      ResourceController6 = __decorate([ ccclass ], ResourceController6);
      return ResourceController6;
    }(ResourceController_1.default);
    exports.default = ResourceController6;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/ResourceController": "ResourceController"
  } ],
  ResourceController7: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3a62a3ASLRBvqlnpG/gAYhV", "ResourceController7");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController3_1 = require("../3_Slot3/ResourceController3");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController7 = function(_super) {
      __extends(ResourceController7, _super);
      function ResourceController7() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceListName = {
          item: {},
          subItem: {
            bg: "img/bg-big"
          },
          reelColor: {
            1: "img/item/khung_reels_1",
            2: "img/item/khung_reels_2",
            3: "img/item/khung_reels_3",
            4: "img/item/khung_reels_4",
            5: "img/item/khung_reels_5"
          },
          activeReels: {
            1: "img/item/active_reels_button_01",
            2: "img/item/active_reels_button_02",
            3: "img/item/active_reels_button_03",
            4: "img/item/active_reels_button_04",
            5: "img/item/active_reels_button_05"
          },
          lockReels: {
            1: "img/item/power_reels_lock_1",
            2: "img/item/power_reels_lock_2",
            3: "img/item/power_reels_lock_3",
            4: "img/item/power_reels_lock_4",
            5: "img/item/power_reels_lock_5"
          },
          reward: {
            MINI: "img/item/mini",
            MINOR: "img/item/minor",
            MAJOR: "img/item/major",
            MEGA: "img/item/mega"
          },
          minigame: [ "/minigame/reward", "/minigame/mini", "/minigame/minor", "/minigame/major", "/minigame/mega", "/minigame/total" ],
          payTable: {
            "0.0": "img/item/bg_payTabel",
            .1: "img/item/scatter-shields",
            .2: "img/item/scatter_shields_txt",
            "1.0": "img/item/bg_payTabel",
            1.1: "img/item/title_free_spins",
            1.2: "img/item/free_spins_txt",
            "2.0": "img/item/bg_payTabel",
            2.1: "img/item/title_rules",
            2.2: "img/item/rules_txt",
            "3.0": "img/item/bg_payTabel",
            3.1: "img/item/title_wheel_spins",
            3.2: "img/item/wheel_spins_txt"
          }
        };
        _this.storageBase = "game-slot/7_Slot7/";
        _this.resourcePath = "4_gameScene/7_Slot7/";
        return _this;
      }
      ResourceController7 = __decorate([ ccclass ], ResourceController7);
      return ResourceController7;
    }(ResourceController3_1.default);
    exports.default = ResourceController7;
    cc._RF.pop();
  }, {
    "../3_Slot3/ResourceController3": "ResourceController3"
  } ],
  ResourceController8: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ca95706i69GIKjY5dAWHKuj", "ResourceController8");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("../../../scripts/Slots/ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceController8 = function(_super) {
      __extends(ResourceController8, _super);
      function ResourceController8() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.resourceListName = {
          item: {
            b: "img/item/bonus",
            s: "img/item/scatter",
            w: "img/item/wild",
            sw: "img/item/wild_special",
            0: "img/item/10",
            1: "img/item/j",
            2: "img/item/q",
            3: "img/item/k",
            4: "img/item/gieng",
            5: "img/item/noi",
            6: "img/item/bingo",
            7: "img/item/daulau"
          },
          subItem: {
            bg: "img/bg-big"
          },
          minigame: [ "minigame/active_effect_white", "minigame/daulau_red", "minigame/daulau_white", "minigame/quantai_boxuong", "minigame/quantai_boxuong_gray", "minigame/quantai_off", "minigame/quantai_open", "minigame/thienthan", "minigame/sach_gray", "minigame/sach_green", "minigame/sach_pink", "minigame/sach_red" ],
          payTable: {
            0: "img/item/1",
            1: "img/item/2",
            2: "img/item/3",
            3: "img/item/4"
          }
        };
        _this.storageBase = "game-slot/8_Slot8/";
        _this.resourcePath = "4_gameScene/8_Slot8/";
        return _this;
      }
      ResourceController8 = __decorate([ ccclass ], ResourceController8);
      return ResourceController8;
    }(ResourceController_1.default);
    exports.default = ResourceController8;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/ResourceController": "ResourceController"
  } ],
  ResourceController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8344bl+dlVHd4RXGWkNuWkQ", "ResourceController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("./ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ImageItem = function() {
      function ImageItem() {
        this.image = null;
        this.customName = "";
        this.folderPath = "";
      }
      ImageItem.prototype.getName = function() {
        return this.customName || this.image.name;
      };
      ImageItem.prototype.getPath = function() {
        this.folderPath || (this.folderPath = "img");
        return this.folderPath + "/" + util.game.getDefaultImage(this.getName());
      };
      __decorate([ property({
        type: cc.Node,
        tooltip: "K\xe9o node ch\u1ee9a h\xecnh \u1ea3nh mu\u1ed1n load resource v\xe0o. H\xecnh \u1ea3nh s\u1ebd \u0111\u01b0\u1ee3c load m\u1eb7c \u0111\u1ecbnh d\u1ef1a theo t\xean c\u1ee7a node n\xe0y."
      }) ], ImageItem.prototype, "image", void 0);
      __decorate([ property({
        tooltip: "Custom name \u0111\u1ec3 load h\xecnh \u1ea3nh n\u1ebfu k mu\u1ed1n d\xf9ng t\xean c\u1ee7a node."
      }) ], ImageItem.prototype, "customName", void 0);
      __decorate([ property({
        tooltip: "Custom path h\xecnh \u1ea3nh \u0111\u1ec3 load."
      }) ], ImageItem.prototype, "folderPath", void 0);
      ImageItem = __decorate([ ccclass("ImageItem") ], ImageItem);
      return ImageItem;
    }();
    exports.ImageItem = ImageItem;
    var SkeletonItem = function() {
      function SkeletonItem() {
        this.skeleton = null;
        this.folderName = "";
        this.animationName = "";
      }
      SkeletonItem.prototype.getName = function() {
        return this.folderName || this.skeleton.node.name;
      };
      SkeletonItem.prototype.getPath = function() {
        return "skeleton/" + this.getName();
      };
      __decorate([ property({
        type: sp.Skeleton,
        tooltip: "K\xe9o th\u1ea3 skeleton c\u1ea7n load source v\xe0o \u0111\xe2y"
      }) ], SkeletonItem.prototype, "skeleton", void 0);
      __decorate([ property({
        tooltip: "T\xean folder ch\u1ee9a 3 files c\u1ee7a skeleton."
      }) ], SkeletonItem.prototype, "folderName", void 0);
      __decorate([ property({
        tooltip: "T\xean animation m\u1eb7c \u0111\u1ecbnh \u0111\u01b0\u1ee3c ch\u1ea1y khi load xong resource."
      }) ], SkeletonItem.prototype, "animationName", void 0);
      SkeletonItem = __decorate([ ccclass("SkeletonItem") ], SkeletonItem);
      return SkeletonItem;
    }();
    exports.SkeletonItem = SkeletonItem;
    var PrefabItem = function() {
      function PrefabItem() {
        this.prefab = null;
        this.customPath = "";
      }
      PrefabItem.prototype.getName = function() {
        return this.customPath || this.prefab.name;
      };
      PrefabItem.prototype.getAllSpriteName = function() {
        var _this = this;
        var resourcePrefab = this.prefab.data.getComponentInChildren(ResourcePrefab_1.default);
        if (!resourcePrefab) return [];
        var result = resourcePrefab.listImage.map(function(item) {
          var customPath = item.folderPath || _this.getName();
          return customPath + "/" + util.game.getDefaultImage(item.getName());
        });
        return result;
      };
      PrefabItem.prototype.getAllSkeletonName = function() {
        var resourcePrefab = this.prefab.data.getComponentInChildren(ResourcePrefab_1.default);
        if (!resourcePrefab) return [];
        var result = resourcePrefab.listSkeleton.map(function(item) {
          return "" + item.getName();
        });
        return result;
      };
      __decorate([ property({
        type: cc.Prefab,
        tooltip: "K\xe9o th\u1ea3 c\xe1c prefab con c\u1ea7n load resource v\xe0o \u0111\xe2y. C\xe1c prefab n\xe0y c\u1ea7n c\xf3 1 node ch\u01b0a component ResourcePrefab \u0111\u1ec3 k\xe9o th\u1ea3 v\xe0 load source"
      }) ], PrefabItem.prototype, "prefab", void 0);
      __decorate([ property({
        tooltip: "\u0110\u01b0\u1eddng d\u1eabn tu\u1ef3 ch\u1ec9nh. M\u1eb7c \u0111\u1ecbnh s\u1ebd l\u1ea5y t\xean c\u1ee7a prefab."
      }) ], PrefabItem.prototype, "customPath", void 0);
      PrefabItem = __decorate([ ccclass("PrefabItem") ], PrefabItem);
      return PrefabItem;
    }();
    exports.PrefabItem = PrefabItem;
    var ResourceController = function(_super) {
      __extends(ResourceController, _super);
      function ResourceController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.storageBase = "game-slot/1_Slot1/";
        _this.resourcePath = "4_gameScene/1_Slot1/";
        _this.resourceListName = {
          item: {},
          payTable: {}
        };
        _this.skeletonListName = null;
        _this.audioListName = null;
        _this.resourceLinkBase = "http://64.120.114.208:9009/";
        _this.spriteList = [];
        _this.skeletonList = [];
        _this.audioList = [];
        _this.listImage = [];
        _this.listImageName = [];
        _this.listSkeleton = [];
        _this.listSkeletonFolder = [];
        _this.listPrefab = [];
        _this.totalResourcePath = [];
        _this.totalSpritePath = [];
        _this.totalSkeletonPath = [];
        _this.totalAudioPath = [];
        _this.writablePath = "";
        _this.omittedItemsPath = [];
        _this.downloader = null;
        return _this;
      }
      ResourceController.prototype.onLoad = function() {
        window["resource"] = this;
        false;
        this.loadResourceName();
      };
      ResourceController.prototype.loadResourceName = function() {
        var _this = this;
        this.listImage.forEach(function(item) {
          _this.listImageName.push(item.getPath());
        });
        this.listPrefab.forEach(function(prefabItem) {
          _this.listImageName = _this.listImageName.concat(prefabItem.getAllSpriteName());
        });
        this.listSkeleton.forEach(function(item) {
          _this.listSkeletonFolder.push("" + item.getName());
        });
        this.listPrefab.forEach(function(prefabItem) {
          _this.listSkeletonFolder = _this.listSkeletonFolder.concat(prefabItem.getAllSkeletonName());
        });
      };
      ResourceController.prototype.loadRes = function(name) {
        return this.spriteList.find(function(x) {
          return x.name == name;
        });
      };
      ResourceController.prototype.startLoad = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              true;
              return [ 3, 3 ];

             case 1:
              _a.sent();
              return [ 4, this.loadNativeRes() ];

             case 2:
              _a.sent();
              return [ 3, 5 ];

             case 3:
              return [ 4, this.loadWebRes() ];

             case 4:
              _a.sent();
              _a.label = 5;

             case 5:
              this.loadImages();
              this.loadSkeleton();
              cc.log("Load end");
              return [ 2 ];
            }
          });
        });
      };
      ResourceController.prototype.getResourcePaths = function() {
        var _this = this;
        this.totalSpritePath = this.listImageName.map(function(item) {
          return _this.writablePath + _this.storageBase + item;
        });
        for (var x in this.resourceListName) for (var y in this.resourceListName[x]) {
          var name = this.resourceListName[x][y];
          name.includes(".jpg") || name.includes(".png") || (name += ".png");
          this.totalSpritePath.push(this.writablePath + this.storageBase + name);
        }
        this.listSkeletonFolder.forEach(function(item) {
          _this.totalSkeletonPath.push(_this.writablePath + _this.storageBase + "skeleton/" + item + "/skeleton.png");
          _this.totalSkeletonPath.push(_this.writablePath + _this.storageBase + "skeleton/" + item + "/skeleton.atlas");
          _this.totalSkeletonPath.push(_this.writablePath + _this.storageBase + "skeleton/" + item + "/skeleton.json");
        });
        this.totalAudioPath = Object.keys(this.audioListName).reduce(function(array, key) {
          var name = _this.audioListName[key];
          if (!name) return array;
          name.includes(".") || (name += ".ogg");
          return array.concat([ _this.writablePath + _this.storageBase + "audio/" + name ]);
        }, []);
        this.totalSpritePath = Array.from(new Set(this.totalSpritePath));
        this.totalAudioPath = Array.from(new Set(this.totalAudioPath));
        cc.log("total audio path", this.totalAudioPath);
      };
      ResourceController.prototype.checkResources = function() {
        return __awaiter(this, void 0, void 0, function() {
          var omittedItems, index;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              omittedItems = [];
              this.totalSpritePath.forEach(function(item) {
                jsb.fileUtils.isFileExist(item) || omittedItems.push(item);
                jsb.fileUtils.isFileExist(item + ".meta") || omittedItems.push(item + ".meta");
              });
              this.totalSkeletonPath.forEach(function(item) {
                jsb.fileUtils.isFileExist(item) || omittedItems.push(item);
              });
              this.totalAudioPath.forEach(function(item) {
                jsb.fileUtils.isFileExist(item) || omittedItems.push(item);
              });
              if (0 == omittedItems.length) return [ 2, true ];
              index = this.writablePath.length;
              this.omittedItemsPath = omittedItems.map(function(item) {
                return item.substr(index);
              });
              return [ 4, this.downloadResources() ];

             case 1:
              _a.sent();
              return [ 2, false ];
            }
          });
        });
      };
      ResourceController.prototype.loadWebRes = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, Promise.all([ new Promise(function(res, rej) {
                cc.loader.loadResDir(_this.resourcePath, cc.SpriteFrame, function(err, data) {
                  if (err) return rej(err);
                  _this.spriteList = data;
                  res(data);
                });
              }), Promise.all(this.listSkeletonFolder.map(function(item) {
                return __awaiter(_this, void 0, void 0, function() {
                  var baseURL, skeletonData;
                  return __generator(this, function(_a) {
                    switch (_a.label) {
                     case 0:
                      baseURL = this.resourcePath + "skeleton/" + item + "/skeleton";
                      return [ 4, new Promise(function(res, rej) {
                        cc.loader.loadRes(baseURL, sp.SkeletonData, function(err, data) {
                          err && rej(err);
                          res(data);
                        });
                      }) ];

                     case 1:
                      skeletonData = _a.sent();
                      skeletonData.name = item;
                      this.skeletonList.push(skeletonData);
                      return [ 2 ];
                    }
                  });
                });
              })), new Promise(function(res, rej) {
                cc.loader.loadResDir(_this.resourcePath, cc.AudioClip, function(err, data) {
                  if (err) return rej(err);
                  _this.audioList = data;
                  res(data);
                });
              }) ]) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      ResourceController.prototype.loadNativeRes = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              cc.log("start load native resource");
              return [ 4, Promise.all([ this.loadImageResNative(), this.loadSkeletonResNative(), this.loadAudioResNative() ]) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      ResourceController.prototype.loadImageResNative = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          var listImage = _this.totalSpritePath.filter(function(item) {
            return item.includes(".png") || item.includes(".jpg");
          });
          cc.loader.load(listImage, function(err, data) {
            return __awaiter(_this, void 0, void 0, function() {
              var _this = this;
              return __generator(this, function(_a) {
                switch (_a.label) {
                 case 0:
                  if (err) {
                    cc.log("load error", err);
                    return [ 2, rej(err) ];
                  }
                  return [ 4, Promise.all(listImage.map(function(item) {
                    return new Promise(function(res, rej) {
                      var source = data.getContent(item);
                      var index = source.url.lastIndexOf("/");
                      var name = source.url.substr(index + 1);
                      var sprite = new cc.SpriteFrame(source);
                      sprite.name = name.split(".")[0];
                      _this.spriteList.push(sprite);
                      cc.loader.load(item + ".meta", function(err, data) {
                        if (err) return rej(err);
                        _this.loadConfigToSpriteFrame(sprite, data);
                        res();
                      });
                    });
                  })) ];

                 case 1:
                  _a.sent();
                  res();
                  return [ 2 ];
                }
              });
            });
          });
        });
      };
      ResourceController.prototype.loadSkeletonResNative = function() {
        var _this = this;
        return Promise.all(this.listSkeletonFolder.map(function(item) {
          return __awaiter(_this, void 0, void 0, function() {
            var baseURL, _a, texture, atlasJson, spineJson, skeletonData;
            return __generator(this, function(_b) {
              switch (_b.label) {
               case 0:
                baseURL = this.writablePath + this.storageBase + "skeleton/" + item;
                return [ 4, Promise.all([ new Promise(function(res, rej) {
                  cc.loader.load(baseURL + "/skeleton.png", function(err, texture) {
                    return res(texture);
                  });
                }), new Promise(function(res, rej) {
                  cc.loader.load({
                    url: baseURL + "/skeleton.atlas",
                    type: "txt"
                  }, function(err, atlasJson) {
                    return res(atlasJson);
                  });
                }), new Promise(function(res, rej) {
                  cc.loader.load({
                    url: baseURL + "/skeleton.json",
                    type: "txt"
                  }, function(err, spineJson) {
                    return res(spineJson);
                  });
                }) ]) ];

               case 1:
                _a = _b.sent(), texture = _a[0], atlasJson = _a[1], spineJson = _a[2];
                skeletonData = new sp.SkeletonData();
                skeletonData.name = item;
                skeletonData.textures = [ texture ];
                skeletonData.atlasText = atlasJson;
                skeletonData.skeletonJson = JSON.parse(spineJson);
                skeletonData["textureNames"] = [ "skeleton.png" ];
                this.skeletonList.push(skeletonData);
                return [ 2 ];
              }
            });
          });
        }));
      };
      ResourceController.prototype.loadAudioResNative = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _a;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              _a = this;
              return [ 4, Promise.all(this.totalAudioPath.map(function(item) {
                return new Promise(function(res) {
                  cc.loader.load(item, function(err, data) {
                    err && cc.log(err);
                    data.name = item.split("/").pop().replace(".ogg", "");
                    res(data);
                  });
                });
              })) ];

             case 1:
              _a.audioList = _b.sent();
              return [ 2 ];
            }
          });
        });
      };
      ResourceController.prototype.downloadResources = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          cc.log("Starting download ...");
          var count = 0;
          var date = Date.now();
          var length = _this.omittedItemsPath.length;
          var newDirectories = Array.from(new Set(_this.omittedItemsPath.map(function(item) {
            return item.slice(0, item.lastIndexOf("/"));
          })));
          newDirectories.forEach(function(item) {
            jsb.fileUtils.createDirectory(_this.writablePath + item);
          });
          _this.downloader = new jsb.Downloader();
          _this.omittedItemsPath.forEach(function(item) {
            _this.downloader.createDownloadFileTask(_this.resourceLinkBase + item, _this.writablePath + item);
          });
          _this.downloader.setOnFileTaskSuccess(function(task) {
            count++;
            cc.log("download success", task, count / length * 100 + "%");
            if (count == length) {
              cc.log("Downloaded in", Date.now() - date);
              res();
            }
          });
          _this.downloader.setOnTaskError(function(task, errorCode, errorCodeInternal, errorStr) {
            cc.log("download error", task, errorStr);
            rej();
          });
        });
      };
      ResourceController.prototype.loadImages = function() {
        var _this = this;
        this.listImage.forEach(function(item) {
          if (!item.image) return;
          var sprite = _this.spriteList.find(function(ele) {
            return ele.name == item.image.name;
          });
          item.image.getComponent(cc.Sprite).spriteFrame = sprite;
        });
      };
      ResourceController.prototype.loadSkeleton = function() {
        var _this = this;
        this.listSkeleton.forEach(function(item) {
          var name = item.folderName || item.skeleton.node.name;
          item.skeleton.skeletonData = _this.skeletonList.find(function(item) {
            return item.name == name;
          });
          item.skeleton.animation = item.animationName || "";
        });
      };
      ResourceController.prototype.getlistImgItem = function() {
        var _this = this;
        return Object.keys(this.resourceListName.item).reduce(function(res, cur) {
          return res.concat([ {
            id: cur,
            img: _this.spriteList.find(function(x) {
              return x.name == _this.resourceListName.item[cur].split("/").pop();
            })
          } ]);
        }, []);
      };
      ResourceController.prototype.loadConfigToSpriteFrame = function(spriteFrame, data) {
        try {
          var meta = JSON.parse(data);
          var config = meta.subMetas[Object.keys(meta.subMetas)[0]];
          spriteFrame.insetLeft = config.borderLeft;
          spriteFrame.insetRight = config.borderRight;
          spriteFrame.insetTop = config.borderTop;
          spriteFrame.insetBottom = config.borderBottom;
        } catch (error) {
          cc.log("Load config to sprite fail:", error);
        }
      };
      ResourceController.prototype.getAudio = function(audioName) {
        return this.audioList.find(function(item) {
          return item.name == audioName;
        });
      };
      __decorate([ property({
        type: ImageItem,
        tooltip: "Nh\u1eadp s\u1ed1 l\u01b0\u1ee3ng \u1ea3nh c\u1ea7n load"
      }) ], ResourceController.prototype, "listImage", void 0);
      __decorate([ property({
        type: SkeletonItem,
        tooltip: "Nh\u1eadp s\u1ed1 l\u01b0\u1ee3ng skeleton c\u1ea7n load"
      }) ], ResourceController.prototype, "listSkeleton", void 0);
      __decorate([ property({
        type: PrefabItem,
        tooltip: "Nh\u1eadp s\u1ed1 l\u01b0\u1ee3ng prefab con c\u1ea7n load"
      }) ], ResourceController.prototype, "listPrefab", void 0);
      ResourceController = __decorate([ ccclass ], ResourceController);
      return ResourceController;
    }(cc.Component);
    exports.default = ResourceController;
    cc._RF.pop();
  }, {
    "./ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMiniGame5: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "db42duYvd5AmaX9huAw3pKj", "ResourceMiniGame5");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame5 = function(_super) {
      __extends(ResourceMinigame5, _super);
      function ResourceMinigame5() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listResourceName = {
          items: {
            1: {
              normal: "gem_minigame_bonus_item_01",
              white: "gem_minigame_bonus_item_white_01"
            },
            2: {
              normal: "gem_minigame_bonus_item_02",
              white: "gem_minigame_bonus_item_white_02"
            },
            3: {
              normal: "gem_minigame_bonus_item_03",
              white: "gem_minigame_bonus_item_white_03"
            },
            4: {
              normal: "gem_minigame_bonus_item_04",
              white: "gem_minigame_bonus_item_white_04"
            },
            5: {
              normal: "gem_minigame_bonus_item_05",
              white: "gem_minigame_bonus_item_white_05"
            },
            6: {
              normal: "gem_minigame_bonus_item_06",
              white: "gem_minigame_bonus_item_white_06"
            },
            7: {
              normal: "gem_minigame_bonus_item_07",
              white: "gem_minigame_bonus_item_white_07"
            },
            8: {
              normal: "gem_minigame_bonus_item_08",
              white: "gem_minigame_bonus_item_white_08"
            },
            9: {
              normal: "gem_minigame_bonus_item_09",
              white: "gem_minigame_bonus_item_white_09"
            },
            10: {
              normal: "gem_minigame_bonus_item_10",
              white: "gem_minigame_bonus_item_white_10"
            },
            11: {
              normal: "gem_minigame_bonus_item_11",
              white: "gem_minigame_bonus_item_white_11"
            },
            12: {
              normal: "gem_minigame_bonus_item_12",
              white: "gem_minigame_bonus_item_white_12"
            },
            13: {
              normal: "gem_minigame_bonus_item_13",
              white: "gem_minigame_bonus_item_white_13"
            },
            14: {
              normal: "gem_minigame_bonus_item_14",
              white: "gem_minigame_bonus_item_white_14"
            },
            15: {
              normal: "gem_minigame_bonus_item_15",
              white: "gem_minigame_bonus_item_white_15"
            }
          },
          fail: {
            active: "gem_minigame_bonus_item_fail_01",
            normal: "gem_minigame_bonus_item_fail_02"
          },
          labelFrame: "gem_minigame_bonus_item_total",
          selectTable: {
            1: "gem-minigame-select-bonus-1",
            2: "gem-minigame-select-bonus-2"
          }
        };
        return _this;
      }
      ResourceMinigame5 = __decorate([ ccclass ], ResourceMinigame5);
      return ResourceMinigame5;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame5;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMiniGame6: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8e1beFss1lGab9ZTBeG+VeS", "ResourceMiniGame6");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame6 = function(_super) {
      __extends(ResourceMinigame6, _super);
      function ResourceMinigame6() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listResourceName = {
          items: {
            1: {
              normal: "gem_minigame_bonus_item_01",
              white: "gem_minigame_bonus_item_white_01"
            },
            2: {
              normal: "gem_minigame_bonus_item_02",
              white: "gem_minigame_bonus_item_white_02"
            },
            3: {
              normal: "gem_minigame_bonus_item_03",
              white: "gem_minigame_bonus_item_white_03"
            },
            4: {
              normal: "gem_minigame_bonus_item_04",
              white: "gem_minigame_bonus_item_white_04"
            },
            5: {
              normal: "gem_minigame_bonus_item_05",
              white: "gem_minigame_bonus_item_white_05"
            },
            6: {
              normal: "gem_minigame_bonus_item_06",
              white: "gem_minigame_bonus_item_white_06"
            },
            7: {
              normal: "gem_minigame_bonus_item_07",
              white: "gem_minigame_bonus_item_white_07"
            },
            8: {
              normal: "gem_minigame_bonus_item_08",
              white: "gem_minigame_bonus_item_white_08"
            },
            9: {
              normal: "gem_minigame_bonus_item_09",
              white: "gem_minigame_bonus_item_white_09"
            },
            10: {
              normal: "gem_minigame_bonus_item_10",
              white: "gem_minigame_bonus_item_white_10"
            },
            11: {
              normal: "gem_minigame_bonus_item_11",
              white: "gem_minigame_bonus_item_white_11"
            },
            12: {
              normal: "gem_minigame_bonus_item_12",
              white: "gem_minigame_bonus_item_white_12"
            },
            13: {
              normal: "gem_minigame_bonus_item_13",
              white: "gem_minigame_bonus_item_white_13"
            },
            14: {
              normal: "gem_minigame_bonus_item_14",
              white: "gem_minigame_bonus_item_white_14"
            },
            15: {
              normal: "gem_minigame_bonus_item_15",
              white: "gem_minigame_bonus_item_white_15"
            }
          },
          fail: {
            active: "gem_minigame_bonus_item_fail_01",
            normal: "gem_minigame_bonus_item_fail_02"
          },
          labelFrame: "gem_minigame_bonus_item_total",
          selectTable: {
            1: "gem-minigame-select-bonus-1",
            2: "gem-minigame-select-bonus-2"
          }
        };
        return _this;
      }
      ResourceMinigame6 = __decorate([ ccclass ], ResourceMinigame6);
      return ResourceMinigame6;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame6;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMiniGame7: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "dc29aH0/9NEMYUL5CzTkZdZ", "ResourceMiniGame7");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame7 = function(_super) {
      __extends(ResourceMinigame7, _super);
      function ResourceMinigame7() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listResourceName = {
          jackpots: {
            0: "reward",
            1: "mini",
            2: "minor",
            3: "major",
            4: "mega",
            5: "total"
          }
        };
        return _this;
      }
      ResourceMinigame7 = __decorate([ ccclass ], ResourceMinigame7);
      return ResourceMinigame7;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame7;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMiniGame8: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "75951An3Q9Ks5jY1SgMt5rB", "ResourceMiniGame8");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame8 = function(_super) {
      __extends(ResourceMinigame8, _super);
      function ResourceMinigame8() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listResourceName = {
          fail: {
            active: "daulau_white",
            normal: "daulau_red"
          },
          items: {
            1: "thienthan",
            0: "quantai_boxuong"
          },
          itemNormal: "quantai_off",
          itemEffect: "active_effect_white",
          selectTable: {
            1: {
              enable: "sach_green",
              disable: "sach_gray"
            },
            2: {
              enable: "sach_pink",
              disable: "sach_gray"
            },
            3: {
              enable: "sach_red",
              disable: "sach_gray"
            }
          }
        };
        return _this;
      }
      ResourceMinigame8 = __decorate([ ccclass ], ResourceMinigame8);
      return ResourceMinigame8;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame8;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMinigame1: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4c76aTINXRMko6EeEm7vSjK", "ResourceMinigame1");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame1 = function(_super) {
      __extends(ResourceMinigame1, _super);
      function ResourceMinigame1() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      ResourceMinigame1 = __decorate([ ccclass ], ResourceMinigame1);
      return ResourceMinigame1;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame1;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMinigame2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ea71940qQ9BDLbQbaUDXGhG", "ResourceMinigame2");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame2 = function(_super) {
      __extends(ResourceMinigame2, _super);
      function ResourceMinigame2() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      ResourceMinigame2 = __decorate([ ccclass ], ResourceMinigame2);
      return ResourceMinigame2;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame2;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMinigame3: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ce8f0vjBKZBJJ1EC6oiKRxw", "ResourceMinigame3");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame3 = function(_super) {
      __extends(ResourceMinigame3, _super);
      function ResourceMinigame3() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listResourceName = {
          items: {
            1: "bap_cai_dark",
            2: "cu_hanh_dark",
            3: "huong_duong_dark",
            4: "ca_rot_dark"
          },
          nomal: {
            1: "bap_cai_final",
            2: "cu_hanh_final",
            3: "huong_duong_alpha-13",
            4: "ca_rot_final"
          },
          snake: {
            0: "snake"
          },
          gold: {
            1: "gold_bap_cai",
            2: "gold_cu_hanh",
            3: "gold_huong_duong",
            4: "gold_ca_rot"
          },
          white: {
            1: "bap_cai_alpha",
            2: "cu_hanh_alpha",
            3: "huong_duong_alpha",
            4: "ca_rot_alpha"
          }
        };
        return _this;
      }
      ResourceMinigame3 = __decorate([ ccclass ], ResourceMinigame3);
      return ResourceMinigame3;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame3;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourceMinigame4: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3030cAzQURD1ZwN5t1Kyglx", "ResourceMinigame4");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourcePrefab_1 = require("../../../../scripts/Slots/ResourcePrefab");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourceMinigame4 = function(_super) {
      __extends(ResourceMinigame4, _super);
      function ResourceMinigame4() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listResourceName = {
          items: {
            0: "casino-slot-coin-grand",
            1: "casino-slot-coin-major",
            2: "casino-slot-coin-minor",
            3: "casino-slot-coin-mini"
          },
          itemNormal: [ "casino-slot-coin-normal" ]
        };
        return _this;
      }
      ResourceMinigame4 = __decorate([ ccclass ], ResourceMinigame4);
      return ResourceMinigame4;
    }(ResourcePrefab_1.default);
    exports.default = ResourceMinigame4;
    cc._RF.pop();
  }, {
    "../../../../scripts/Slots/ResourcePrefab": "ResourcePrefab"
  } ],
  ResourcePrefab: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7a417UEGSFI/6Du/8W335dX", "ResourcePrefab");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController_1 = require("./ResourceController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ResourcePrefab = function(_super) {
      __extends(ResourcePrefab, _super);
      function ResourcePrefab() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.listImage = [];
        _this.listSkeleton = [];
        _this.resourceController = null;
        return _this;
      }
      ResourcePrefab.prototype.loadImages = function() {
        var _this = this;
        this.listImage.forEach(function(item) {
          if (!item.image) return;
          var sprite = _this.resourceController.spriteList.find(function(ele) {
            return ele.name == item.image.name;
          });
          item.image.getComponent(cc.Sprite).spriteFrame = sprite;
        });
      };
      __decorate([ property({
        type: ResourceController_1.ImageItem,
        tooltip: "K\xe9o th\u1ea3 h\xecnh \u1ea3nh v\xe0o \u0111\xe2y ^.^"
      }) ], ResourcePrefab.prototype, "listImage", void 0);
      __decorate([ property({
        type: ResourceController_1.SkeletonItem,
        tooltip: "K\xe9o th\u1ea3 skeleton v\xe0o \u0111\xe2y"
      }) ], ResourcePrefab.prototype, "listSkeleton", void 0);
      ResourcePrefab = __decorate([ ccclass ], ResourcePrefab);
      return ResourcePrefab;
    }(cc.Component);
    exports.default = ResourcePrefab;
    cc._RF.pop();
  }, {
    "./ResourceController": "ResourceController"
  } ],
  SlotController1: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0a173IQnrZAgLibwPTvUO3q", "SlotController1");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController1_1 = require("./ResourceController1");
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var AccumulatedBar1_1 = require("./AccumulatedBar1");
    var CoinLabel_1 = require("../../../scripts/Components/CoinLabel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController1 = function(_super) {
      __extends(SlotController1, _super);
      function SlotController1() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.accumilatedBar = null;
        _this.coinBonusLabel = null;
        _this.coinBonusLerp = 10;
        _this.originalCoin = null;
        _this.stickyWildContainer = null;
        _this.miniGame = null;
        _this.resourceController = null;
        _this.coinBonus = 0;
        _this.coinBonusController = null;
        _this.stickyList = [];
        return _this;
      }
      SlotController1.prototype.startSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          var error_1;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _a.trys.push([ 0, 4, , 5 ]);
              this.stickyWildContainer.parent = this.tableView;
              this.stickyWildContainer.zIndex = 1;
              this.coinBonusController = new CoinLabel_1.default(this.coinBonusLabel, this.coinBonusLerp, "$");
              _super.prototype.startSlot.call(this);
              if (!this.dataStatus.bonus) return [ 3, 2 ];
              if (!this.dataStatus.bonus.w) return [ 3, 2 ];
              controller.ui.showModel();
              cc.log("start load bonus");
              return [ 4, this.playBonus(this.dataStatus.bonus) ];

             case 1:
              _a.sent();
              controller.ui.hideModel();
              _a.label = 2;

             case 2:
              return [ 4, util.game.delay(200, this.node) ];

             case 3:
              _a.sent();
              this.dataStatus.collect = this.dataStatus.collect || {
                c: 0,
                tc: 100
              };
              this.accumilatedBar.updateProgress(this.dataStatus.collect.c / this.dataStatus.collect.tc);
              this.coinBonus = this.dataStatus.collect.w;
              this.coinBonusController.updateUserBalance(this.coinBonus);
              if (this.dataStatus.freeSpin.c > 0) {
                this.accumilatedBar.node.runAction(cc.sequence([ cc.delayTime(.2), cc.fadeOut(.4) ]));
                this.handleStickyWild(this.dataStatus.lastmat);
              }
              return [ 3, 5 ];

             case 4:
              error_1 = _a.sent();
              cc.error(error_1);
              return [ 3, 5 ];

             case 5:
              return [ 2 ];
            }
          });
        });
      };
      SlotController1.prototype.getAnimationController = function() {
        this.animationController = this.node.getComponent("AnimationController");
      };
      SlotController1.prototype.handleCollectCoin = function(_a) {
        var _this = this;
        var collect = _a.features.collect;
        return new Promise(function(res, rej) {
          return __awaiter(_this, void 0, void 0, function() {
            var listItem, listCoin_1, error_2;
            var _this = this;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                _a.trys.push([ 0, 5, , 6 ]);
                listItem = this.reelController[0]._listItem.slice(1);
                listCoin_1 = [];
                listItem.forEach(function(item) {
                  item.forEach(function(item) {
                    "c" === item.id && listCoin_1.push(item);
                  });
                });
                if (!listCoin_1.length) return [ 3, 2 ];
                listCoin_1.forEach(function(item) {
                  var coin = new cc.Node("coin");
                  var front = new cc.Node("coin");
                  var mainPosition = _this.originalCoin.parent.convertToWorldSpaceAR(cc.v2(0, 0));
                  var coinPosition = item.node.convertToWorldSpaceAR(cc.v2(0, 0));
                  coin.addComponent(cc.Sprite);
                  front.addComponent(cc.Sprite);
                  coin.getComponent(cc.Sprite).spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.subItem.coin.split("/").pop());
                  front.getComponent(cc.Sprite).spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.item.c.split("/").pop());
                  item.mainItem.getComponent(cc.Sprite).spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.subItem.coinGray.split("/").pop());
                  front.parent = item.node;
                  front.setPosition(0, 0);
                  item.node.getComponentInChildren(cc.Label).node.zIndex = 2;
                  front.runAction(cc.sequence(cc.fadeOut(.6), cc.callFunc(function() {
                    front.destroy();
                    coin.parent = _this.originalCoin.parent;
                    coin.setPosition(coinPosition.x - mainPosition.x, coinPosition.y - mainPosition.y + 2);
                    coin.opacity = 0;
                    coin.runAction(cc.fadeIn(.2));
                  }), cc.callFunc(function() {
                    coin.runAction(cc.sequence(cc.scaleTo(.5, 1.2), cc.callFunc(function() {
                      var time = .7;
                      coin.runAction(cc.moveTo(time, _this.originalCoin.getPosition()));
                      coin.runAction(cc.sequence(cc.skewTo(time / 2 - time / 5, 45, 45), cc.skewTo(time / 2 - time / 5, 0, 0)));
                      coin.runAction(cc.scaleTo(time / 2, .6));
                      coin.runAction(cc.sequence(cc.delayTime(time), cc.scaleTo(.1, .55), cc.scaleTo(.1, .6), cc.delayTime(.1), cc.callFunc(function() {
                        coin.destroy();
                      })));
                    })));
                  })));
                });
                return [ 4, util.game.delay(2e3, this.node, res) ];

               case 1:
                _a.sent();
                this.accumilatedBar.updateProgress(collect.c / collect.tc);
                this.coinBonusController.updateUserBalance(collect.w);
                this.coinBonusController.coinLabel.node.runAction(cc.sequence(cc.scaleTo(.2, 1.2), cc.scaleTo(.2, 1)));
                return [ 3, 4 ];

               case 2:
                return [ 4, util.game.delay(100, this.node, res).catch(function(e) {}) ];

               case 3:
                _a.sent();
                _a.label = 4;

               case 4:
                return [ 3, 6 ];

               case 5:
                error_2 = _a.sent();
                res();
                return [ 3, 6 ];

               case 6:
                return [ 2 ];
              }
            });
          });
        });
      };
      SlotController1.prototype.handleFreeSpin = function(dataSpin) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            if (dataSpin.spinResult.freeSpin) {
              this.freeSpin = dataSpin.spinResult.freeSpin;
              this.accumilatedBar.node.opacity && this.accumilatedBar.node.runAction(cc.fadeOut(.3));
            } else 0 === this.accumilatedBar.node.opacity && this.accumilatedBar.node.runAction(cc.fadeIn(.3));
            _super.prototype.handleFreeSpin.call(this, dataSpin);
            this.handleStickyWild(dataSpin.spinResult.mat[0]);
            dataSpin.freeSpin && 0 == dataSpin.freeSpin.c && this.destroyStickyWild();
            return [ 2 ];
          });
        });
      };
      SlotController1.prototype.handleStickyWild = function(mat) {
        var swPosition = mat.split(",").reduce(function(arr, item, index) {
          return "sw" === item ? arr.concat([ index ]) : arr;
        }, []);
        swPosition = swPosition.map(function(item, index) {
          return {
            x: item / 6 | 0,
            y: item % 6,
            index: item
          };
        });
        swPosition.length && this.showStickyWild(swPosition);
      };
      SlotController1.prototype.showStickyWild = function(swPosition) {
        var _this = this;
        var listItem = this.reelController[0]._listItem.slice(1);
        var list = swPosition.map(function(item) {
          return {
            index: item.index,
            slotItem: listItem[item.x][item.y]
          };
        });
        list.forEach(function(item) {
          if (_this.stickyList.map(function(item) {
            return item.index;
          }).indexOf(item.index) >= 0) return;
          var newNode = new cc.Node("sticky");
          var newSprite = newNode.addComponent(cc.Sprite);
          newSprite.spriteFrame = item.slotItem.mainItemSprite.spriteFrame;
          var mainNode = new cc.Node("borderSticky");
          var mainSprite = mainNode.addComponent(cc.Sprite);
          mainSprite.spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.subItem.wildBorder.split("/").pop());
          var targetPosition = _this.stickyWildContainer.convertToWorldSpaceAR(cc.v2(0, 0));
          var wildPosition = item.slotItem.node.convertToWorldSpaceAR(cc.v2(0, 0));
          mainNode.parent = _this.stickyWildContainer;
          mainNode.setPosition(wildPosition.sub(targetPosition));
          newNode.parent = mainNode;
          _this.stickyList.push({
            index: item.index,
            node: mainNode
          });
        });
        cc.log(this.stickyList);
      };
      SlotController1.prototype.destroyStickyWild = function() {
        this.stickyList.forEach(function(item) {
          item.node.runAction(cc.sequence(cc.fadeOut(.3), cc.callFunc(function() {
            item.node.destroy();
          })));
        });
        this.stickyList = [];
      };
      SlotController1.prototype.handleBonus = function(dataSpin) {
        this.playBonus(dataSpin.spinResult.score.sum[2]);
      };
      SlotController1.prototype.playBonus = function(_a) {
        var mul = _a.mul, mulArr = _a.mulArr, w = _a.w;
        return __awaiter(this, void 0, void 0, function() {
          var canvas, miniGameNode, miniGameController;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              canvas = cc.find("Canvas");
              miniGameNode = cc.instantiate(this.miniGame);
              miniGameController = miniGameNode.getComponent("MinigameController1");
              view.screen.bonus = miniGameController;
              controller.ui.hideBar();
              miniGameController._slotController = this;
              miniGameNode.opacity = 0;
              miniGameNode.zIndex = define.zIndex.MINIGAME;
              miniGameNode.parent = canvas;
              return [ 4, miniGameController.startMiniGame(mul, mulArr, w) ];

             case 1:
              _b.sent();
              miniGameNode.runAction(cc.fadeIn(.5));
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property(AccumulatedBar1_1.default) ], SlotController1.prototype, "accumilatedBar", void 0);
      __decorate([ property(cc.Label) ], SlotController1.prototype, "coinBonusLabel", void 0);
      __decorate([ property(cc.Integer) ], SlotController1.prototype, "coinBonusLerp", void 0);
      __decorate([ property(cc.Node) ], SlotController1.prototype, "originalCoin", void 0);
      __decorate([ property(cc.Node) ], SlotController1.prototype, "stickyWildContainer", void 0);
      __decorate([ property(cc.Prefab) ], SlotController1.prototype, "miniGame", void 0);
      __decorate([ property(ResourceController1_1.default) ], SlotController1.prototype, "resourceController", void 0);
      SlotController1 = __decorate([ ccclass ], SlotController1);
      return SlotController1;
    }(SlotController_1.default);
    exports.default = SlotController1;
    cc._RF.pop();
  }, {
    "../../../scripts/Components/CoinLabel": "CoinLabel",
    "../../../scripts/Slots/SlotController": "SlotController",
    "./AccumulatedBar1": "AccumulatedBar1",
    "./ResourceController1": "ResourceController1"
  } ],
  SlotController2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "fc160JMdmFJoLP26bXfNzO3", "SlotController2");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController2_1 = require("./ResourceController2");
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var AccumulatedBar2_1 = require("./AccumulatedBar2");
    var CoinLabel_1 = require("../../../scripts/Components/CoinLabel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController2 = function(_super) {
      __extends(SlotController2, _super);
      function SlotController2() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.accumilatedBar = null;
        _this.coinBonusLabel = null;
        _this.coinBonusLerp = 5;
        _this.stickyWildContainer = null;
        _this.originalCoin = null;
        _this.miniGame = null;
        _this.particleCoinBonus = null;
        _this.lightCoin = null;
        _this.resourceController = null;
        _this.coinBonus = 0;
        _this.coinBonusController = null;
        _this.stickyList = [];
        return _this;
      }
      SlotController2.prototype.startSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.coinBonusController = new CoinLabel_1.default(this.coinBonusLabel, this.coinBonusLerp, "$");
              _super.prototype.startSlot.call(this);
              if (!this.dataStatus.bonus) return [ 3, 2 ];
              if (!this.dataStatus.bonus.w) return [ 3, 2 ];
              controller.ui.showModel();
              cc.log("start load bonus");
              return [ 4, this.playBonus(this.dataStatus.bonus) ];

             case 1:
              _a.sent();
              controller.ui.hideModel();
              _a.label = 2;

             case 2:
              setTimeout(function() {
                _this.accumilatedBar.updateProgress(_this.dataStatus.collect.c / _this.dataStatus.collect.tc);
                _this.coinBonus = _this.dataStatus.collect.w;
                _this.coinBonusController.updateUserBalance(_this.coinBonus, {
                  lerpRatio: 10
                });
                if (_this.dataStatus.freeSpin.c > 0) {
                  setTimeout(function() {
                    _this.accumilatedBar.node.runAction(cc.fadeOut(.4));
                  }, 200);
                  _this.handleStickyWild(_this.dataStatus.lastmat);
                }
              }, 200);
              this.dataStatus.bonus;
              return [ 2 ];
            }
          });
        });
      };
      SlotController2.prototype.loadBorderRes = function() {
        var _this = this;
        var listBorderImg = this.resourceController.getListItemBorder();
        listBorderImg.forEach(function(res) {
          var item = {
            id: res.id,
            img: res.img
          };
          _this._listBorderImg.push(item);
        });
      };
      SlotController2.prototype.getAnimationController = function() {
        this.animationController = this.node.getComponent("AnimationController");
      };
      SlotController2.prototype.handleCollectCoin = function(_a) {
        var _this = this;
        var collect = _a.features.collect;
        return new Promise(function(res, rej) {
          var listItem = _this.reelController[0]._listItem.slice(1);
          var listCoin = [];
          listItem.forEach(function(item) {
            item.forEach(function(item) {
              "c" === item.id && listCoin.push(item);
            });
          });
          if (listCoin.length) {
            listCoin.forEach(function(item) {
              var coin = new cc.Node("coin");
              var front = new cc.Node("coin");
              var mainPosition = _this.originalCoin.parent.convertToWorldSpaceAR(cc.v2(0, 0));
              var coinPosition = item.node.convertToWorldSpaceAR(cc.v2(0, 0));
              coin.addComponent(cc.Sprite);
              front.addComponent(cc.Sprite);
              coin.getComponent(cc.Sprite).spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.subItem.coin.split("/").pop());
              front.getComponent(cc.Sprite).spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.item.c.split("/").pop());
              item.mainItem.getComponent(cc.Sprite).spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.subItem.coinGray.split("/").pop());
              front.parent = item.node;
              front.setPosition(0, 0);
              item.node.getComponentInChildren(cc.Label).node.zIndex = 2;
              front.runAction(cc.sequence(cc.fadeOut(.6), cc.callFunc(function() {
                front.destroy();
                coin.parent = _this.originalCoin.parent;
                coin.setPosition(coinPosition.x - mainPosition.x, coinPosition.y - mainPosition.y + 2);
                coin.opacity = 0;
                coin.runAction(cc.fadeIn(.2));
              }), cc.callFunc(function() {
                coin.runAction(cc.sequence(cc.scaleTo(.5, 1.2), cc.callFunc(function() {
                  var time = .7;
                  coin.runAction(cc.moveTo(time, _this.originalCoin.getPosition()));
                  coin.runAction(cc.sequence(cc.skewTo(time / 2 - time / 5, 45, 45), cc.skewTo(time / 2 - time / 5, 0, 0)));
                  coin.runAction(cc.scaleTo(time / 2, .6));
                  coin.runAction(cc.sequence(cc.delayTime(time), cc.scaleTo(.1, .55), cc.scaleTo(.1, .6), cc.delayTime(.1), cc.callFunc(function() {
                    coin.destroy();
                  })));
                })));
              })));
            });
            setTimeout(function() {
              _this.accumilatedBar.updateProgress(collect.c / collect.tc);
              _this.coinBonusController.updateUserBalance(collect.w, {
                lerpRatio: 10
              });
              _this.coinBonusController.coinLabel.node.runAction(cc.sequence(cc.callFunc(function() {
                _this.lightCoin.getComponent(cc.Animation).play();
                _this.particleCoin();
              }), cc.scaleTo(.2, 1.2), cc.scaleTo(.2, 1)));
              res();
            }, 2e3);
          } else setTimeout(function() {
            return res();
          }, 100);
        });
      };
      SlotController2.prototype.handleFreeSpin = function(dataSpin) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            if (dataSpin.spinResult.freeSpin) {
              this.freeSpin = dataSpin.spinResult.freeSpin;
              this.accumilatedBar.node.opacity && this.accumilatedBar.node.runAction(cc.fadeOut(.3));
            } else 0 === this.accumilatedBar.node.opacity && this.accumilatedBar.node.runAction(cc.fadeIn(.3));
            _super.prototype.handleFreeSpin.call(this, dataSpin);
            this.handleStickyWild(dataSpin.spinResult.mat[0]);
            dataSpin.freeSpin && 0 == dataSpin.freeSpin.c && this.destroyStickyWild();
            return [ 2 ];
          });
        });
      };
      SlotController2.prototype.handleStickyWild = function(mat) {
        var swPosition = mat.split(",").reduce(function(arr, item, index) {
          return "sw" === item ? arr.concat([ index ]) : arr;
        }, []);
        swPosition = swPosition.map(function(item, index) {
          return {
            x: item / 6 | 0,
            y: item % 6,
            index: item
          };
        });
        swPosition.length && this.showStickyWild(swPosition);
      };
      SlotController2.prototype.showStickyWild = function(swPosition) {
        var _this = this;
        var listItem = this.reelController[0]._listItem.slice(1);
        var list = swPosition.map(function(item) {
          return {
            index: item.index,
            slotItem: listItem[item.x][item.y]
          };
        });
        list.forEach(function(item) {
          if (_this.stickyList.map(function(item) {
            return item.index;
          }).indexOf(item.index) >= 0) return;
          var newNode = new cc.Node("sticky");
          newNode.addComponent(cc.Sprite);
          newNode.getComponent(cc.Sprite).spriteFrame = item.slotItem.mainItemSprite.spriteFrame;
          var mainNode = new cc.Node("borderSticky");
          mainNode.addComponent(cc.Sprite);
          mainNode.getComponent(cc.Sprite).spriteFrame = _this.resourceController.loadRes(_this.resourceController.resourceListName.subItem.wildBorder.split("/").pop());
          var targetPosition = _this.stickyWildContainer.convertToWorldSpaceAR(cc.v2(0, 0));
          var wildPosition = item.slotItem.node.convertToWorldSpaceAR(cc.v2(0, 0));
          mainNode.parent = _this.stickyWildContainer;
          mainNode.setPosition(wildPosition.sub(targetPosition));
          newNode.parent = mainNode;
          _this.stickyList.push({
            index: item.index,
            node: mainNode
          });
        });
      };
      SlotController2.prototype.destroyStickyWild = function() {
        this.stickyList.forEach(function(item) {
          item.node.runAction(cc.sequence(cc.fadeOut(.3), cc.callFunc(function() {
            item.node.destroy();
          })));
        });
        this.stickyList = [];
      };
      SlotController2.prototype.handleBonus = function(dataSpin) {
        this.playBonus(dataSpin.spinResult.score.sum[2]);
      };
      SlotController2.prototype.playBonus = function(_a) {
        var mul = _a.mul, mulArr = _a.mulArr, w = _a.w;
        return __awaiter(this, void 0, void 0, function() {
          var canvas, miniGameNode, miniGameController;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              canvas = cc.find("Canvas");
              miniGameNode = cc.instantiate(this.miniGame);
              miniGameController = miniGameNode.getComponent("MinigameController2");
              view.screen.bonus = miniGameController;
              controller.ui.hideBar();
              miniGameController._slotController = this;
              miniGameNode.opacity = 0;
              miniGameNode.zIndex = define.zIndex.MINIGAME;
              miniGameNode.parent = canvas;
              view.screen.bonus = miniGameController;
              return [ 4, miniGameController.startMiniGame(mul, mulArr, w) ];

             case 1:
              _b.sent();
              miniGameNode.runAction(cc.fadeIn(.5));
              return [ 2 ];
            }
          });
        });
      };
      SlotController2.prototype.particleCoin = function() {
        var _this = this;
        this.particleCoinBonus.active = true;
        setTimeout(function() {
          _this.particleCoinBonus.active = false;
        }, 1500);
      };
      __decorate([ property(AccumulatedBar2_1.default) ], SlotController2.prototype, "accumilatedBar", void 0);
      __decorate([ property(cc.Label) ], SlotController2.prototype, "coinBonusLabel", void 0);
      __decorate([ property(cc.Integer) ], SlotController2.prototype, "coinBonusLerp", void 0);
      __decorate([ property(cc.Node) ], SlotController2.prototype, "stickyWildContainer", void 0);
      __decorate([ property(cc.Node) ], SlotController2.prototype, "originalCoin", void 0);
      __decorate([ property(cc.Prefab) ], SlotController2.prototype, "miniGame", void 0);
      __decorate([ property(cc.Node) ], SlotController2.prototype, "particleCoinBonus", void 0);
      __decorate([ property(cc.Node) ], SlotController2.prototype, "lightCoin", void 0);
      __decorate([ property(ResourceController2_1.default) ], SlotController2.prototype, "resourceController", void 0);
      SlotController2 = __decorate([ ccclass ], SlotController2);
      return SlotController2;
    }(SlotController_1.default);
    exports.default = SlotController2;
    cc._RF.pop();
  }, {
    "../../../scripts/Components/CoinLabel": "CoinLabel",
    "../../../scripts/Slots/SlotController": "SlotController",
    "./AccumulatedBar2": "AccumulatedBar2",
    "./ResourceController2": "ResourceController2"
  } ],
  SlotController3: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "cece9ZRt9ZL/br9ZC/R4Bml", "SlotController3");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var ResourceController3_1 = require("./ResourceController3");
    var Item_1 = require("../../../scripts/Slots/Item");
    var SlotController3 = function(_super) {
      __extends(SlotController3, _super);
      function SlotController3() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.miniGamePrefab = null;
        _this.resourceController = null;
        _this.listItemSkeleton = {
          b: "Bonus",
          s: "Scatter",
          w: "Wild",
          0: "qua_bi_ngo",
          1: "qua_ca_chua",
          2: "qua_dua_hau",
          3: "qua_dua",
          4: "qua_oi",
          5: "qua_thang_long",
          6: "con_bo",
          7: "con_cho",
          8: "con_de"
        };
        return _this;
      }
      SlotController3.prototype.startSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _super.prototype.startSlot.call(this);
              if (!this.dataStatus.bonus) return [ 3, 3 ];
              if (!this.dataStatus.bonus.w) return [ 3, 3 ];
              controller.ui.showModel();
              return [ 4, util.game.delay(200) ];

             case 1:
              _a.sent();
              cc.log("start load bonus");
              return [ 4, this.playBonus(this.dataStatus.bonus) ];

             case 2:
              _a.sent();
              controller.ui.hideModel();
              _a.label = 3;

             case 3:
              return [ 2 ];
            }
          });
        });
      };
      SlotController3.prototype.loadItemRes = function() {
        var _this = this;
        Object.keys(this.listItemSkeleton).forEach(function(id) {
          var item = new Item_1.default();
          item.setInfo(id, null, null);
          _this._listItemData.push(item);
        });
      };
      SlotController3.prototype.playBonus = function(_a) {
        var c = _a.c, w = _a.w, arr = _a.arr;
        return __awaiter(this, void 0, void 0, function() {
          var canvas, miniGameNode, miniGameController;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              canvas = cc.find("Canvas");
              miniGameNode = cc.instantiate(this.miniGamePrefab);
              miniGameController = miniGameNode.getComponent("MinigameController3");
              view.screen.bonus = miniGameController;
              controller.ui.hideBar();
              miniGameController._slotController = this;
              miniGameNode.opacity = 0;
              miniGameNode.zIndex = define.zIndex.MINIGAME;
              miniGameNode.parent = canvas;
              return [ 4, miniGameController.startMiniGame(c, w, arr) ];

             case 1:
              _b.sent();
              return [ 4, util.game.delay(1500) ];

             case 2:
              _b.sent();
              miniGameNode.runAction(cc.fadeIn(.5));
              return [ 2 ];
            }
          });
        });
      };
      SlotController3.prototype.handleBonus = function(dataSpin) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              controller.ui.showModel();
              return [ 4, this.playBonus(dataSpin.spinResult.score.sum[2]) ];

             case 1:
              _a.sent();
              controller.ui.hideModel();
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property(cc.Prefab) ], SlotController3.prototype, "miniGamePrefab", void 0);
      __decorate([ property(ResourceController3_1.default) ], SlotController3.prototype, "resourceController", void 0);
      SlotController3 = __decorate([ ccclass ], SlotController3);
      return SlotController3;
    }(SlotController_1.default);
    exports.default = SlotController3;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/Item": "Item",
    "../../../scripts/Slots/SlotController": "SlotController",
    "./ResourceController3": "ResourceController3"
  } ],
  SlotController4: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7a7534/6fZKq6oRk2EwSkCp", "SlotController4");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResourceController4_1 = require("./ResourceController4");
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var AtributeStatic_1 = require("./AtributeStatic");
    var CoinLabel_1 = require("../../../scripts/Components/CoinLabel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController4 = function(_super) {
      __extends(SlotController4, _super);
      function SlotController4() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.minigamePrefab = null;
        _this.betNode = null;
        _this.itemsContainer = null;
        _this.activeBet = null;
        _this.lockBet = null;
        _this.tableGold = null;
        _this.resourceController = null;
        _this.grandLabel = null;
        _this.majorLabel = null;
        _this.miniLabel = null;
        _this.minorLabel = null;
        _this._listItemSelectData = [];
        _this._listItemSelectImg = [];
        _this._betValue = 0;
        _this.betLevels = [];
        _this.idEventBet = "";
        _this.jackpots = [];
        _this.baseJP = [];
        _this.bonus = [];
        _this._listGoldItem = [];
        _this.coinGrand = null;
        _this.coinMajor = null;
        _this.coinMinor = null;
        _this.coinMini = null;
        _this.betId = 4;
        _this.isSelect = false;
        _this.listJP = [];
        return _this;
      }
      SlotController4.prototype.startSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _super.prototype.startSlot.call(this);
              this.coinGrand = new CoinLabel_1.default(this.grandLabel.getComponent(cc.Label), 9);
              this.coinMajor = new CoinLabel_1.default(this.majorLabel.getComponent(cc.Label), 9);
              this.coinMinor = new CoinLabel_1.default(this.minorLabel.getComponent(cc.Label), 9);
              this.coinMini = new CoinLabel_1.default(this.miniLabel.getComponent(cc.Label), 9);
              this.getLableJackpot(this.currentBet, this.jackpots);
              this.getListItemGold();
              this.updateActiveGold(this.dataStatus.lastBet);
              if (!this.dataStatus.bonus) return [ 3, 2 ];
              cc.log("you get a bonus");
              return [ 4, this.playBonus(this.dataStatus.bonus) ];

             case 1:
              _a.sent();
              return [ 3, 5 ];

             case 2:
              if (!(this.dataStatus.freeSpin.c > 0)) return [ 3, 3 ];
              this.betNode.active = false;
              return [ 3, 5 ];

             case 3:
              return [ 4, this.showActiveBetNode(true) ];

             case 4:
              _a.sent();
              _a.label = 5;

             case 5:
              return [ 2 ];
            }
          });
        });
      };
      SlotController4.prototype.showBet = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              controller.ui.lockBar();
              if (!(this.dataStatus.freeSpin.c > 0)) return [ 3, 1 ];
              this.activeBet.active = false;
              this.lockBet.active = true;
              this.betNode.active = false;
              return [ 3, 3 ];

             case 1:
              return [ 4, util.game.delay(400) ];

             case 2:
              _a.sent();
              this.showActiveBetNode(true);
              _a.label = 3;

             case 3:
              return [ 2 ];
            }
          });
        });
      };
      SlotController4.prototype.showActiveBetNode = function(activeNode) {
        var _this = this;
        !activeNode ? controller.ui.unlockBar() : controller.ui.lockBar();
        this.betNode.opacity = 0;
        this.betNode.active = activeNode;
        this.betNode.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
          _this.betNode.opacity = 255;
        }), cc.scaleTo(.3, 1) ]));
      };
      SlotController4.prototype.getBetId = function(e, data) {
        var _this = this;
        this.betId = data;
        AtributeStatic_1.default.idBetSelect = this.betId.toString();
        this.itemsContainer.children[data].children[1].active = true;
        this.itemsContainer.children.forEach(function(item, index) {
          index != data && (item.children[1].active = false);
        });
        this.isSelect = true;
        setTimeout(function() {
          return _this.betNode.runAction(cc.sequence([ cc.scaleTo(.2, .1), cc.callFunc(function() {
            _this.betNode.active = false;
            _this.itemsContainer.children[data].children[1].active = false;
          }) ]));
        }, 500);
        this.activeBet.active = true;
        this.lockBet.active = false;
        controller.ui.unlockBar();
        this.getCurrentBet(data);
        this.showActiveGoldItem();
        this.updateActiveGold(this.betLevels[data]);
      };
      SlotController4.prototype.getListItemGold = function() {
        this._listGoldItem = this.tableGold.children;
      };
      SlotController4.prototype.showActiveGoldItem = function() {
        switch (this.betId) {
         case 0:
          this._listGoldItem.forEach(function(item, index) {
            item.name < "05" ? item.opacity = 128 : item.opacity = 255;
          });
          break;

         case 1:
          this._listGoldItem.forEach(function(item) {
            item.name < "04" ? item.opacity = 128 : item.opacity = 255;
          });
          break;

         case 2:
          this._listGoldItem.forEach(function(item) {
            item.name < "03" ? item.opacity = 128 : item.opacity = 255;
          });
          break;

         case 3:
          this._listGoldItem.forEach(function(item) {
            item.name < "02" ? item.opacity = 128 : item.opacity = 255;
          });
          break;

         case 4:
          this._listGoldItem.forEach(function(item) {
            item.opacity = 255;
          });
          break;

         default:
          this._listGoldItem.forEach(function(item) {
            item.opacity = 255;
          });
          cc.log("default");
          return;
        }
      };
      SlotController4.prototype.playBonus = function(dataBonus) {
        return __awaiter(this, void 0, void 0, function() {
          var canvas, miniNode, miniControllers;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              AtributeStatic_1.default.idJackpot = dataBonus[dataBonus.length - 1];
              canvas = cc.find("Canvas");
              miniNode = cc.instantiate(this.minigamePrefab);
              miniControllers = miniNode.getComponent("MinigameController4");
              view.screen.bonus = miniControllers;
              controller.ui.hideBar();
              miniControllers._slotController = this;
              miniNode.opacity = 0;
              miniNode.zIndex = define.zIndex.MINIGAME;
              miniNode.parent = canvas;
              return [ 4, miniControllers.startMiniGame(dataBonus, this.grandLabel, this.majorLabel, this.minorLabel, this.miniLabel) ];

             case 1:
              _a.sent();
              miniNode.runAction(cc.fadeIn(.5));
              return [ 2 ];
            }
          });
        });
      };
      SlotController4.prototype.loadBorderRes = function() {
        var _this = this;
        var listBorderImg = this.resourceController.getListItemBorder();
        listBorderImg.forEach(function(res) {
          var item = {
            id: res.id,
            img: res.img
          };
          _this._listBorderImg.push(item);
        });
      };
      SlotController4.prototype.importData = function() {
        this.betArray = this.dataStatus.betArr;
        this.betLevels = this.dataStatus.betLevels;
        this.jackpots = this.dataStatus.jackpots;
        this.currentBet = this.dataStatus.lastBet;
        this.baseJP = this.dataStatus.baseJP;
        this.bonus = this.dataStatus.bonus;
        view.bar.bottom.gameBar.updateInfo();
      };
      SlotController4.prototype.updateJackpot = function(baseJackpot, bet, jp) {
        var str = baseJackpot * bet + jp;
        return str;
      };
      SlotController4.prototype.getLableJackpot = function(bet, JP) {
        this.listJP = [ this.updateJackpot(this.baseJP[0], bet, JP[0]), this.updateJackpot(this.baseJP[1], bet, JP[1]), this.updateJackpot(this.baseJP[2], bet, JP[2]), this.updateJackpot(this.baseJP[3], bet, JP[3]) ];
        this.grandLabel.string = util.string.formatMoney(this.listJP[0]);
        this.majorLabel.string = util.string.formatMoney(this.listJP[1]);
        this.minorLabel.string = util.string.formatMoney(this.listJP[2]);
        this.miniLabel.string = util.string.formatMoney(this.listJP[3]);
        this.coinGrand.updateUserBalance(this.listJP[0]);
        this.coinMajor.updateUserBalance(this.listJP[1]);
        this.coinMinor.updateUserBalance(this.listJP[2]);
        this.coinMini.updateUserBalance(this.listJP[3]);
      };
      SlotController4.prototype.updateActiveGold = function(bet) {
        var atri = new AtributeStatic_1.default();
        bet >= this.betLevels[4] ? this.betId = 4 : bet >= this.betLevels[3] ? this.betId = 3 : bet >= this.betLevels[2] ? this.betId = 2 : bet >= this.betLevels[1] ? this.betId = 1 : this.betId = 0;
        this.showActiveGoldItem();
        view.bar.bottom.gameBar.updateInfo();
      };
      SlotController4.prototype.getCurrentBet = function(idBet) {
        var bet = this.betLevels[idBet];
        this.updateCurrentBet(bet);
        view.bar.bottom.gameBar.updateInfo();
      };
      SlotController4.prototype.updateCurrentBet = function(bet) {
        this.currentBet = bet;
        this.updateActiveGold(bet);
        this.getLableJackpot(bet, this.jackpots);
      };
      SlotController4.prototype.watchEvent = function(data) {
        this.lockBet.active = true;
        this.activeBet.active = false;
        data.spinResult.score.reward > 0 && (data.spinResult.score.sum[1].c = 1);
        switch (data.type) {
         case api.key.SLOT_SPIN:
          "string" === typeof data.spinResult.mat && (data.spinResult.mat = [ data.spinResult.mat ]);
          this.dataSpin = data;
          this.isReceivedData = true;
          this.reelController.forEach(function(reel, index) {
            reel._listItem.forEach(function(item) {
              item.forEach(function(item) {
                item.getComponent("SlotItem").setMatrixEnd(data.spinResult.mat[index].split(","));
              });
            });
          });
          this.isSpin || this.spin(data);
          this.lastWin = data.spinResult.score.reward;
          this.getLableJackpot(this.currentBet, this.dataSpin.features.jackpots);
        }
        this.updateStatus(data);
      };
      SlotController4.prototype.handleBonus = function(data) {
        this.playBonus(data.spinResult.score.sum[2].arr);
      };
      SlotController4.prototype.handleCollectCoin = function(dataSpin) {
        if (!(dataSpin.freeSpin.c > 0)) {
          this.activeBet.active = true;
          this.lockBet.active = false;
        }
      };
      SlotController4.prototype.hideContent = function() {
        if (true == this.betNode.active) {
          this.betNode.active = false;
          this.isSelect = false;
        }
      };
      SlotController4.prototype.showContent = function() {
        var _this = this;
        this.betNode.active = !this.isSelect;
        true == this.betNode.active && this.betNode.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
          _this.betNode.opacity = 255;
        }), cc.scaleTo(.3, 1) ]));
      };
      __decorate([ property(cc.Prefab) ], SlotController4.prototype, "minigamePrefab", void 0);
      __decorate([ property(cc.Node) ], SlotController4.prototype, "betNode", void 0);
      __decorate([ property(cc.Node) ], SlotController4.prototype, "itemsContainer", void 0);
      __decorate([ property(cc.Node) ], SlotController4.prototype, "activeBet", void 0);
      __decorate([ property(cc.Node) ], SlotController4.prototype, "lockBet", void 0);
      __decorate([ property(cc.Node) ], SlotController4.prototype, "tableGold", void 0);
      __decorate([ property(ResourceController4_1.default) ], SlotController4.prototype, "resourceController", void 0);
      __decorate([ property(cc.Label) ], SlotController4.prototype, "grandLabel", void 0);
      __decorate([ property(cc.Label) ], SlotController4.prototype, "majorLabel", void 0);
      __decorate([ property(cc.Label) ], SlotController4.prototype, "miniLabel", void 0);
      __decorate([ property(cc.Label) ], SlotController4.prototype, "minorLabel", void 0);
      SlotController4 = __decorate([ ccclass ], SlotController4);
      return SlotController4;
    }(SlotController_1.default);
    exports.default = SlotController4;
    cc._RF.pop();
  }, {
    "../../../scripts/Components/CoinLabel": "CoinLabel",
    "../../../scripts/Slots/SlotController": "SlotController",
    "./AtributeStatic": "AtributeStatic",
    "./ResourceController4": "ResourceController4"
  } ],
  SlotController5: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "eaf93LzNJxMZpSfPcUbAmxZ", "SlotController5");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var ResourceController5_1 = require("./ResourceController5");
    var SlotController5 = function(_super) {
      __extends(SlotController5, _super);
      function SlotController5() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.miniGamePrefab = null;
        _this.resourceController = null;
        return _this;
      }
      SlotController5.prototype.startSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, _super.prototype.startSlot.call(this) ];

             case 1:
              _a.sent();
              if (!this.dataStatus.bonus) return [ 3, 4 ];
              if (!this.dataStatus.bonus.arr) return [ 3, 4 ];
              controller.ui.showModel();
              return [ 4, util.game.delay(500) ];

             case 2:
              _a.sent();
              cc.log("start load bonus");
              return [ 4, this.playBonus(this.dataStatus.bonus.arr, this.dataStatus.lastBet) ];

             case 3:
              _a.sent();
              controller.ui.hideModel();
              _a.label = 4;

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      SlotController5.prototype.loadBorderRes = function() {
        var _this = this;
        var listBorderImg = this.resourceController.getListItemBorder();
        listBorderImg.forEach(function(res) {
          var item = {
            id: res.id,
            img: res.img
          };
          _this._listBorderImg.push(item);
        });
      };
      SlotController5.prototype.playBonus = function(bonusArr, lastBet) {
        return __awaiter(this, void 0, void 0, function() {
          var canvas, miniGameNode, miniGameController;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              canvas = cc.find("Canvas");
              miniGameNode = cc.instantiate(this.miniGamePrefab);
              miniGameController = miniGameNode.getComponent("MinigameController5");
              view.screen.bonus = miniGameController;
              controller.ui.hideBar();
              miniGameController._slotController = this;
              miniGameNode.opacity = 0;
              miniGameNode.zIndex = define.zIndex.MINIGAME;
              miniGameNode.parent = canvas;
              return [ 4, miniGameController.startMiniGame(bonusArr, lastBet) ];

             case 1:
              _a.sent();
              miniGameNode.runAction(cc.fadeIn(.5));
              return [ 2 ];
            }
          });
        });
      };
      SlotController5.prototype.handleBonus = function(dataSpin) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              controller.ui.showModel();
              return [ 4, this.playBonus(dataSpin.spinResult.score.sum[2].arr, dataSpin.spinResult.totalBet) ];

             case 1:
              _a.sent();
              controller.ui.hideModel();
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property(cc.Prefab) ], SlotController5.prototype, "miniGamePrefab", void 0);
      __decorate([ property(ResourceController5_1.default) ], SlotController5.prototype, "resourceController", void 0);
      SlotController5 = __decorate([ ccclass ], SlotController5);
      return SlotController5;
    }(SlotController_1.default);
    exports.default = SlotController5;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotController": "SlotController",
    "./ResourceController5": "ResourceController5"
  } ],
  SlotController6: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2e376b7L2JH7KulVgAzoW8a", "SlotController6");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var ResourceController6_1 = require("./ResourceController6");
    var MinigameController6_1 = require("./Minigame/MinigameController6");
    var LinesController_1 = require("../../../scripts/Slots/LinesController");
    var Item_1 = require("../../../scripts/Slots/Item");
    var SlotController6 = function(_super) {
      __extends(SlotController6, _super);
      function SlotController6() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.miniGamePrefab = null;
        _this.resourceController = null;
        _this.denLong = null;
        _this.originalBackground = null;
        _this.bonusBackground = null;
        _this.listItemSkeleton = {
          b: "bonus",
          w: "wild",
          0: "ngocboi",
          1: "tuitien",
          2: "binhruou",
          3: "caitrong",
          4: "ngocbua",
          5: "khobau",
          6: "a",
          7: "k",
          8: "q",
          9: "j"
        };
        return _this;
      }
      SlotController6.prototype.startSlot = function(again) {
        var _this = this;
        if (again) {
          this.tableView.children.forEach(function(item) {
            if ("LinesController" == item.name) return;
            item.destroy();
          });
          setTimeout(function() {
            _super.prototype.startSlot.call(_this);
          }, 0);
          return;
        }
        this.dataStatus.freeReelGame && !this.dataStatus.freeReelGame.userSpined && this.playBonus(this.dataStatus.freeReelGame.gameCount, this.dataStatus.freeReelGame.stickyWildReels, this.dataStatus.freeReelGame.extraSpin);
        this.originalBackground = this.resourceController.spriteList.find(function(item) {
          return "bg_game_02" == item.name;
        });
        this.bonusBackground = this.resourceController.spriteList.find(function(item) {
          return "bonus_game_bg" == item.name;
        });
        _super.prototype.startSlot.call(this);
      };
      SlotController6.prototype.loadItemRes = function() {
        var _this = this;
        var dataSkeleton = this.resourceController.skeletonList["item_spine"];
        Object.keys(this.listItemSkeleton).forEach(function(id) {
          var item = new Item_1.default();
          item.setInfo(id, null, dataSkeleton);
          _this._listItemData.push(item);
        });
      };
      SlotController6.prototype.initState = function() {
        _super.prototype.initState.call(this);
        if (this.dataStatus.freeSpin.c > 0 && this.dataStatus.freeReelGame.userSpined) {
          cc.log("init reel");
          this.initReel(this.dataStatus.freeReelGame.gameCount);
        }
      };
      SlotController6.prototype.initReel = function(reelAmount) {
        var _this = this;
        void 0 === reelAmount && (reelAmount = 1);
        this.reelController = Array(reelAmount).fill(0).map(function(item, index) {
          var tableView;
          var linesController;
          if (index > 0) {
            var tableViewParent = cc.instantiate(_this.tableView.parent);
            tableViewParent.parent = _this.tableView.parent.parent;
            tableView = tableViewParent.getChildByName("table-view");
            if (tableView.children.length) {
              linesController = tableView.getComponentInChildren(LinesController_1.default);
              linesController._slotController = _this;
            } else {
              var linesControllerNode = new cc.Node("LinesController");
              linesControllerNode.parent = _this.node;
              linesController = linesControllerNode.addComponent(LinesController_1.default);
            }
            linesController.tableView = tableView;
          } else {
            tableView = _this.tableView;
            linesController = _this.node.getComponentInChildren(LinesController_1.default);
          }
          if (reelAmount > 1) {
            var background = _this.reelController[0].tableView.parent.getChildByName("slot-background");
            background.getComponent(cc.Sprite).spriteFrame = _this.bonusBackground;
            background.y = 10;
            _this.denLong.active = false;
          }
          return {
            tableView: tableView,
            linesController: linesController
          };
        });
        switch (reelAmount) {
         case 1:
          this.reelController[0].tableView.parent.scale = 1;
          this.reelController[0].tableView.parent.setPosition(0, 15);
          break;

         case 2:
          this.reelController.forEach(function(item) {
            return item.tableView.parent.scale = .55;
          });
          this.reelController[0].tableView.parent.setPosition(-250, 10);
          this.reelController[1].tableView.parent.setPosition(250, 10);
          break;

         case 3:
          this.reelController.forEach(function(item) {
            return item.tableView.parent.scale = .43;
          });
          this.reelController[0].tableView.parent.setPosition(0, 130);
          this.reelController[1].tableView.parent.setPosition(-200, -135);
          this.reelController[2].tableView.parent.setPosition(200, -135);
          break;

         case 4:
          this.reelController.forEach(function(item) {
            return item.tableView.parent.scale = .41;
          });
          this.reelController[0].tableView.parent.setPosition(-190, 140);
          this.reelController[1].tableView.parent.setPosition(190, 140);
          this.reelController[2].tableView.parent.setPosition(-190, -135);
          this.reelController[3].tableView.parent.setPosition(190, -135);
        }
      };
      SlotController6.prototype.deleteSubReels = function() {
        this.reelController.forEach(function(item, index) {
          if (0 == index) return;
          item.tableView.parent.destroy();
          item.linesController.destroy();
        });
        this.reelController = [ this.reelController[0] ];
        this.reelController[0].tableView.parent.scale = 1;
        this.reelController[0].tableView.parent.setPosition(0, 0);
        var background = this.reelController[0].tableView.parent.getChildByName("slot-background");
        background.getComponent(cc.Sprite).spriteFrame = this.originalBackground;
        background.y = 0;
        this.denLong.active = true;
      };
      SlotController6.prototype.playBonus = function(gameCount, stickyWildReels, extraSpin) {
        var canvas = cc.find("Canvas");
        var miniGameNode = cc.instantiate(this.miniGamePrefab);
        var miniGameController = miniGameNode.getComponent(MinigameController6_1.default);
        view.screen.bonus = miniGameController;
        controller.ui.hideBar();
        miniGameController._slotController = this;
        miniGameNode.opacity = 0;
        miniGameNode.zIndex = define.zIndex.MINIGAME;
        miniGameNode.parent = canvas;
        miniGameController.startMinigame(gameCount, stickyWildReels, extraSpin);
        setTimeout(function() {
          miniGameNode.runAction(cc.fadeIn(1));
        }, 200);
      };
      SlotController6.prototype.handleBonus = function(_a) {
        var freeReelGame = _a.features.freeReelGame;
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              controller.ui.showModel();
              return [ 4, this.playBonus(freeReelGame.gameCount, freeReelGame.stickyWildReels, freeReelGame.extraSpin) ];

             case 1:
              _b.sent();
              controller.ui.hideModel();
              this.isSpin = false;
              return [ 2 ];
            }
          });
        });
      };
      SlotController6.prototype.handleFreeSpin = function(dataSpin) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, _super.prototype.handleFreeSpin.call(this, dataSpin) ];

             case 1:
              _a.sent();
              (!dataSpin.freeSpin || dataSpin.freeSpin.c < 1) && this.deleteSubReels();
              return [ 2 ];
            }
          });
        });
      };
      SlotController6.prototype.watchEvent = function(data) {
        Object.keys(data.freeSpin).length && (data.spinResult.mat = data.features.freeReelGameResult.map(function(item) {
          return item.matstr;
        }));
        _super.prototype.watchEvent.call(this, data);
      };
      __decorate([ property(cc.Prefab) ], SlotController6.prototype, "miniGamePrefab", void 0);
      __decorate([ property(ResourceController6_1.default) ], SlotController6.prototype, "resourceController", void 0);
      __decorate([ property(cc.Node) ], SlotController6.prototype, "denLong", void 0);
      SlotController6 = __decorate([ ccclass ], SlotController6);
      return SlotController6;
    }(SlotController_1.default);
    exports.default = SlotController6;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/Item": "Item",
    "../../../scripts/Slots/LinesController": "LinesController",
    "../../../scripts/Slots/SlotController": "SlotController",
    "./Minigame/MinigameController6": "MinigameController6",
    "./ResourceController6": "ResourceController6"
  } ],
  SlotController7: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ffc09fygINLyIuWrcreDebd", "SlotController7");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var ResourceController7_1 = require("./ResourceController7");
    var CoinLabel_1 = require("../../../scripts/Components/CoinLabel");
    var Item_1 = require("../../../scripts/Slots/Item");
    var SlotController7 = function(_super) {
      __extends(SlotController7, _super);
      function SlotController7() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.miniGamePrefab = null;
        _this.resourceController = null;
        _this.lbReward = [];
        _this.review = null;
        _this.activeReel = null;
        _this.reelsColor = null;
        _this.overlay = null;
        _this.reel = null;
        _this.Lockreels = null;
        _this.rewardWin = null;
        _this._listItem = [];
        _this.reels = 5;
        _this.reward = [ [ 12, 18, 50, 250 ], [ 8, 12, 30, 160 ], [ 6, 9, 25, 125 ], [ 4, 6, 15, 80 ], [ 3, 5, 12, 60 ] ];
        _this.totalB = 0;
        _this.listItemSkeleton = {
          k: "caikhien",
          s: "trungchim",
          w: "longchim",
          0: "thanhkiem",
          1: "conheongu",
          2: "chimmaudo",
          3: "khoibang",
          4: "conheongoc",
          5: "nine",
          6: "ten",
          7: "j",
          8: "q",
          9: "k",
          10: "a"
        };
        return _this;
      }
      SlotController7.prototype.startSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _super.prototype.startSlot.call(this);
              this.dataStatus.lastReels > 0 && this.activeSpriteFrame(this.dataStatus.lastReels);
              return [ 4, this.showReview() ];

             case 1:
              _a.sent();
              if (!this.dataStatus.bonus) return [ 3, 4 ];
              controller.ui.showModel();
              return [ 4, util.game.delay(200) ];

             case 2:
              _a.sent();
              cc.log("start load bonus");
              return [ 4, this.playBonus(this.dataStatus.bonus) ];

             case 3:
              _a.sent();
              controller.ui.hideModel();
              _a.label = 4;

             case 4:
              this.rewardFormat();
              return [ 4, util.game.delay(400) ];

             case 5:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      SlotController7.prototype.showReview = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!(this.dataStatus.freeSpin.c > 0)) return [ 3, 1 ];
              this.reel.active = false;
              this.Lockreels.active = true;
              return [ 3, 3 ];

             case 1:
              view.bar.bottom.lock();
              return [ 4, util.game.delay(400) ];

             case 2:
              _a.sent();
              this.overlay.active = true;
              this.review.opacity = 0;
              this.review.active = true;
              this.review.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
                return _this.review.opacity = 255;
              }), cc.scaleTo(.3, 1), cc.callFunc(function() {
                setTimeout(function() {
                  _this.review.runAction(cc.sequence([ cc.fadeOut(.2), cc.callFunc(function() {
                    _this.review.active = false;
                    _this.overlay.active = false;
                    _this.showActiveReel();
                  }) ]));
                }, 4e3);
              }) ]));
              _a.label = 3;

             case 3:
              return [ 2 ];
            }
          });
        });
      };
      SlotController7.prototype.loadItemRes = function() {
        var _this = this;
        var dataSkeleton = this.resourceController.skeletonList["item_spine"];
        Object.keys(this.listItemSkeleton).forEach(function(id) {
          var item = new Item_1.default();
          item.setInfo(id, null, dataSkeleton);
          _this._listItemData.push(item);
        });
      };
      SlotController7.prototype.watchEvent = function(data) {
        this.reel.active = false;
        this.Lockreels.active = true;
        if (data.freeSpin.c >= 0 && data.freeSpin.b > 0) {
          data.spinResult.newMat = data.spinResult.mat;
          data.spinResult.mat = data.features.oldMat;
        }
        _super.prototype.watchEvent.call(this, data);
      };
      SlotController7.prototype.listItemFormat = function() {
        var _this = this;
        this._listItem = [ [], [], [], [], [] ];
        this.reelController.forEach(function(item) {
          item._listItem.forEach(function(item) {
            for (var i = 0; i < 5; i++) _this._listItem[i].push(item[i]);
          });
        });
      };
      SlotController7.prototype.showActiveReel = function() {
        var _this = this;
        view.bar.bottom.lock();
        this.overlay.active = true;
        this.activeReel.opacity = 0;
        this.activeReel.active = true;
        this.activeReel.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
          return _this.activeReel.opacity = 255;
        }), cc.scaleTo(.3, 1) ]));
      };
      SlotController7.prototype.activeColumn = function(e, data) {
        var _this = this;
        this.reels = data;
        cc.log("Sent " + this.reels);
        this.activeReel.runAction(cc.sequence([ cc.scaleTo(.2, .1), cc.callFunc(function() {
          return _this.activeReel.active = false;
        }) ]));
        this.activeSpriteFrame(data);
        view.bar.bottom.unlock();
        this.overlay.active = false;
      };
      SlotController7.prototype.showPayTable = function() {
        var _this = this;
        _super.prototype.showPayTable.call(this);
        this.payTabel.getChildByName("container").opacity = 0;
        var button = this.payTabel.getChildByName("button");
        button.opacity = 0;
        this.payTabel.getChildByName("container").runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
          return _this.payTabel.getChildByName("container").opacity = 255;
        }), cc.scaleTo(.25, 1.1), cc.callFunc(function() {
          button.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
            return button.opacity = 255;
          }), cc.scaleTo(.1, 1.1), cc.scaleTo(.05, 1) ]));
        }), cc.scaleTo(.1, 1) ]));
      };
      SlotController7.prototype.activeSpriteFrame = function(reels) {
        var _this = this;
        this.reelsColor.getComponent(cc.Sprite).spriteFrame = this.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.resourceListName.reelColor[reels].split("/").pop();
        });
        this.reel.children[0].getComponent(cc.Sprite).spriteFrame = this.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.resourceListName.activeReels[reels].split("/").pop();
        });
        this.Lockreels.children[0].getComponent(cc.Sprite).spriteFrame = this.resourceController.spriteList.find(function(item) {
          return item.name == _this.resourceController.resourceListName.lockReels[reels].split("/").pop();
        });
        this.rewardFormat();
      };
      SlotController7.prototype.spin = function(data) {
        if (this.currentBet > game.user.balance) {
          this.autoSpinTimes = 0;
          cc.log("User do not have enough coin to spin ...");
          return;
        }
        this.isSpin = true;
        this.isReceivedData = !!data;
        data || api.sendGD({
          e: api.key.SLOT_SPIN,
          gtype: this.gameId,
          totalbet: this.currentBet,
          choosedReels: this.reels
        }, function(err, res) {
          cc.log("callback", res);
        });
        this.listItemFormat();
        this.timeManager();
        this.activeColumn(void 0, this.reels);
      };
      SlotController7.prototype.playBonus = function(c) {
        return __awaiter(this, void 0, void 0, function() {
          var canvas, miniGameNode, miniGameController;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              canvas = cc.find("Canvas");
              miniGameNode = cc.instantiate(this.miniGamePrefab);
              miniGameController = miniGameNode.getComponent("MinigameController7");
              view.screen.bonus = miniGameController;
              controller.ui.hideBar();
              miniGameController._slotController = this;
              miniGameNode.opacity = 0;
              miniGameNode.zIndex = define.zIndex.MINIGAME;
              miniGameNode.parent = canvas;
              return [ 4, miniGameController.startMiniGame(c) ];

             case 1:
              _a.sent();
              miniGameNode.zIndex = 3;
              miniGameNode.runAction(cc.fadeIn(.5));
              return [ 2 ];
            }
          });
        });
      };
      SlotController7.prototype.updateCurrentBet = function(bet) {
        this.totalB = bet;
        this.rewardFormat();
        _super.prototype.updateCurrentBet.call(this, bet);
      };
      SlotController7.prototype.rewardFormat = function() {
        var _this = this;
        0 == this.totalB && (this.totalB = this.dataStatus.lastBet);
        this.lbReward.forEach(function(item, index) {
          item.string = util.string.formatMoney(_this.reward[_this.reels - 1][index] * _this.totalB);
        });
      };
      SlotController7.prototype.handleBonus = function(data) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              controller.ui.showModel();
              return [ 4, this.playBonus(data.spinResult.score.sum[2]) ];

             case 1:
              _a.sent();
              controller.ui.hideModel();
              return [ 2 ];
            }
          });
        });
      };
      SlotController7.prototype.handleCollectCoin = function(dataSpin) {
        if (!(dataSpin.freeSpin.c > 0)) {
          this.reel.active = true;
          this.Lockreels.active = false;
        }
      };
      SlotController7.prototype.totalWin = function() {
        return __awaiter(this, void 0, void 0, function() {
          var arrReward, checkReward;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              arrReward = [];
              checkReward = this.dataSpin.spinResult.score.sum[6].arr;
              checkReward.forEach(function(elem) {
                "string" != typeof elem.value || "SUPPER" == elem.value || "WHEEL" == elem.value || true != elem.valid || arrReward.push(elem);
              });
              this.dataSpin.spinResult.score.sum[0].w > 0 && arrReward.unshift({
                value: "MEGA",
                valid: true
              });
              arrReward.forEach(function(item, index) {
                return setTimeout(function() {
                  return _this.showJackpots(item, arrReward.length);
                }, 4500 * index);
              });
              return [ 4, util.game.delay(4500 * arrReward.length) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      SlotController7.prototype.showJackpots = function(rew, arr) {
        return __awaiter(this, void 0, void 0, function() {
          var bonus, color, indexBonus;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, util.game.delay(2e3) ];

             case 1:
              _a.sent();
              bonus = [ "MINI", "MINOR", "MAJOR", "MEGA" ];
              color = {
                0: [ 253, 168, 83 ],
                1: [ 102, 208, 38 ],
                2: [ 57, 189, 252 ],
                3: [ 241, 57, 111 ]
              };
              indexBonus = bonus.indexOf(rew.value);
              if (bonus.includes(rew.value)) {
                this.rewardWin.opacity = 0;
                this.overlay.active = true;
                this.rewardWin.active = true;
                this.rewardWin.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
                  _this.rewardWin.children[0].color = cc.color(color[indexBonus][0], color[indexBonus][1], color[indexBonus][2]);
                  _this.rewardWin.children[2].getComponent(cc.Sprite).spriteFrame = _this.resourceController.spriteList.find(function(item) {
                    return item.name == _this.resourceController.resourceListName.reward[rew.value].split("/").pop();
                  });
                  _this.coinController = new CoinLabel_1.default(_this.rewardWin.children[3].getComponent(cc.Label), 8);
                  _this.rewardWin.children[3].addComponent(cc.LabelOutline);
                  _this.coinController.updateUserBalance(_this.reward[_this.reels - 1][indexBonus] * _this.totalB);
                  setTimeout(function() {
                    _this.rewardWin.active = false;
                    _this.overlay.active = false;
                  }, 4e3);
                }), cc.callFunc(function() {
                  _this.rewardWin.opacity = 255;
                }), cc.scaleTo(.3, 1.1), cc.scaleTo(.1, 1) ]));
              }
              return [ 2 ];
            }
          });
        });
      };
      SlotController7.prototype.hideContent = function() {
        _super.prototype.hideContent.call(this);
        view.bar.bottom.unlock();
      };
      SlotController7.prototype.showContent = function() {
        _super.prototype.showContent.call(this);
        cc.log("Show content", view.screen.slot.id);
        (this.activeReel.active || this.review.active) && view.bar.bottom.lock();
      };
      __decorate([ property(cc.Prefab) ], SlotController7.prototype, "miniGamePrefab", void 0);
      __decorate([ property(ResourceController7_1.default) ], SlotController7.prototype, "resourceController", void 0);
      __decorate([ property(cc.Label) ], SlotController7.prototype, "lbReward", void 0);
      __decorate([ property(cc.Node) ], SlotController7.prototype, "review", void 0);
      __decorate([ property(cc.Node) ], SlotController7.prototype, "activeReel", void 0);
      __decorate([ property(cc.Node) ], SlotController7.prototype, "reelsColor", void 0);
      __decorate([ property(cc.Node) ], SlotController7.prototype, "overlay", void 0);
      __decorate([ property(cc.Node) ], SlotController7.prototype, "reel", void 0);
      __decorate([ property(cc.Node) ], SlotController7.prototype, "Lockreels", void 0);
      __decorate([ property(cc.Node) ], SlotController7.prototype, "rewardWin", void 0);
      SlotController7 = __decorate([ ccclass ], SlotController7);
      return SlotController7;
    }(SlotController_1.default);
    exports.default = SlotController7;
    cc._RF.pop();
  }, {
    "../../../scripts/Components/CoinLabel": "CoinLabel",
    "../../../scripts/Slots/Item": "Item",
    "../../../scripts/Slots/SlotController": "SlotController",
    "./ResourceController7": "ResourceController7"
  } ],
  SlotController8: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f8b3dAjILRFLpr6beashjl8", "SlotController8");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController_1 = require("../../../scripts/Slots/SlotController");
    var ResourceController8_1 = require("./ResourceController8");
    var SlotController8 = function(_super) {
      __extends(SlotController8, _super);
      function SlotController8() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.miniGamePrefab = null;
        _this.resourceController = null;
        _this.indexSlide8 = 0;
        return _this;
      }
      SlotController8.prototype.startSlot = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, _super.prototype.startSlot.call(this) ];

             case 1:
              _a.sent();
              if (!this.dataStatus.bonus) return [ 3, 4 ];
              if (!this.dataStatus.bonus.arr) return [ 3, 4 ];
              controller.ui.showModel();
              return [ 4, util.game.delay(500) ];

             case 2:
              _a.sent();
              cc.log("start load bonus");
              return [ 4, this.playBonus(this.dataStatus.bonus.arr, this.dataStatus.bonus.w, this.dataStatus.bonus.res) ];

             case 3:
              _a.sent();
              controller.ui.hideModel();
              _a.label = 4;

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      SlotController8.prototype.playBonus = function(bonusArr, coinWin, res) {
        return __awaiter(this, void 0, void 0, function() {
          var canvas, miniGameNode, miniGameController;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              canvas = cc.find("Canvas");
              miniGameNode = cc.instantiate(this.miniGamePrefab);
              miniGameController = miniGameNode.getComponent("MinigameController8");
              view.screen.bonus = miniGameController;
              controller.ui.hideBar();
              miniGameController._slotController = this;
              miniGameNode.opacity = 0;
              miniGameNode.zIndex = define.zIndex.MINIGAME;
              miniGameNode.parent = canvas;
              return [ 4, miniGameController.startMiniGame(bonusArr, coinWin, res) ];

             case 1:
              _a.sent();
              miniGameNode.runAction(cc.fadeIn(.5));
              return [ 2 ];
            }
          });
        });
      };
      SlotController8.prototype.handleBonus = function(dataSpin) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              controller.ui.showModel();
              return [ 4, this.playBonus(dataSpin.spinResult.score.sum[2].arr, dataSpin.spinResult.score.sum[2].w, dataSpin.spinResult.score.sum[2].res) ];

             case 1:
              _a.sent();
              controller.ui.hideModel();
              return [ 2 ];
            }
          });
        });
      };
      SlotController8.prototype.showTable = function(data) {
        var _this = this;
        var container = this.payTabel.getChildByName("container");
        container.children[0].children.forEach(function(item) {
          item.getComponent(cc.Sprite).spriteFrame = _this.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.resourceListName.payTable[data].split("/").pop();
          });
        });
      };
      SlotController8.prototype.showPayTable = function() {
        var _this = this;
        _super.prototype.showPayTable.call(this);
        this.payTabel.getChildByName("container").opacity = 0;
        var button = this.payTabel.getChildByName("button");
        button.opacity = 0;
        this.payTabel.getChildByName("container").runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
          return _this.payTabel.getChildByName("container").opacity = 255;
        }), cc.scaleTo(.25, 1.1), cc.callFunc(function() {
          button.runAction(cc.sequence([ cc.scaleTo(0, .1), cc.callFunc(function() {
            return button.opacity = 255;
          }), cc.scaleTo(.1, 1.1), cc.scaleTo(.05, 1) ]));
        }), cc.scaleTo(.1, 1) ]));
      };
      __decorate([ property(cc.Prefab) ], SlotController8.prototype, "miniGamePrefab", void 0);
      __decorate([ property(ResourceController8_1.default) ], SlotController8.prototype, "resourceController", void 0);
      SlotController8 = __decorate([ ccclass ], SlotController8);
      return SlotController8;
    }(SlotController_1.default);
    exports.default = SlotController8;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotController": "SlotController",
    "./ResourceController8": "ResourceController8"
  } ],
  SlotController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8694fkUlCRAJJzy6KdYvvZS", "SlotController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Item_1 = require("./Item");
    var AnimationController_1 = require("./AnimationController");
    var SlotItem_1 = require("./SlotItem");
    var LinesController_1 = require("./LinesController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotController = function(_super) {
      __extends(SlotController, _super);
      function SlotController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.gameId = "";
        _this.gameSizeType = 1;
        _this.tableView = null;
        _this.itemSlot = null;
        _this.IPadScale = 1.2;
        _this.payTabel = null;
        _this.reelController = [];
        _this._listItemData = [];
        _this._listBorderImg = [];
        _this.listItemSkeleton = {};
        _this.betArray = [];
        _this.lastWin = 0;
        _this.isAutoSpin = false;
        _this.autoSpinTimes = 0;
        _this.dataStatus = null;
        _this.isSpin = false;
        _this.isReceivedData = false;
        _this.isReFresh = false;
        _this.dataSpin = null;
        _this.freeSpin = 0;
        _this.bonus = false;
        _this.indexSlide = 0;
        _this.statusGame = null;
        _this.statusCallback = void 0;
        return _this;
      }
      SlotController.prototype.getData = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          return __awaiter(_this, void 0, void 0, function() {
            var e_1;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                _a.trys.push([ 0, 2, , 3 ]);
                return [ 4, Promise.all([ this.resourceController.startLoad(), this.getStatusGame() ]) ];

               case 1:
                _a.sent();
                this.importData();
                this.loadItemRes();
                this.loadBorderRes();
                res();
                return [ 3, 3 ];

               case 2:
                e_1 = _a.sent();
                rej(e_1);
                return [ 3, 3 ];

               case 3:
                return [ 2 ];
              }
            });
          });
        });
      };
      SlotController.prototype.startSlot = function() {
        this.initState();
        this.initializeItem();
        this.checkFreeSpin();
      };
      SlotController.prototype.getStatusGame = function(smid) {
        var _this = this;
        return new Promise(function(res, rej) {
          api.sendGD({
            e: api.key.SMINFO,
            gtype: smid || _this.gameId
          }, function(err, data) {
            err && rej(err);
            cc.log(data);
            _this.dataStatus = data;
            _this.updateStatus(data);
            res(data);
          });
        });
      };
      SlotController.prototype.importData = function() {
        this.betArray = this.dataStatus.betArr;
        this.currentBet = this.dataStatus.lastBet;
        view.bar.bottom.gameBar.updateInfo();
      };
      SlotController.prototype.loadItemRes = function() {
        var _this = this;
        var listImgItem = this.resourceController.getlistImgItem();
        listImgItem.forEach(function(data) {
          var item = new Item_1.default();
          item.setInfo(data.id, data.img);
          _this._listItemData.push(item);
        });
      };
      SlotController.prototype.loadBorderRes = function() {};
      SlotController.prototype.loadItemSelectRes = function() {};
      SlotController.prototype.initState = function() {
        var _this = this;
        this.animationController = this.node.getComponent(AnimationController_1.default);
        var realScreenSize = cc.view.getFrameSize();
        var realScreenRatio = realScreenSize.width / realScreenSize.height;
        realScreenRatio < 1.5 && (this.node.scale *= this.IPadScale);
        this.reelController = [ 0 ].map(function() {
          return {
            tableView: _this.tableView
          };
        });
        this.reelController[0].linesController = this.node.getComponentInChildren(LinesController_1.default);
      };
      SlotController.prototype.initializeItem = function() {
        var _this = this;
        this.gameSize = config.game.GAME_SIZE[this.gameSizeType];
        this.maxCol = this.gameSize.length;
        this.maxRow = Math.max.apply(Math, this.gameSize);
        this.reelController.forEach(function(item, index) {
          var listItem = [];
          for (var x = 0; x < _this.gameSize.length; x++) for (var y = 0; y < _this.gameSize[x] + 1; y++) {
            var item_1 = _this.getRandomItem();
            var slotItem = _this.instantiateSlotItem(item_1, x, y, index);
            listItem.push(slotItem);
          }
          item._listItem = _this.sortListItem(listItem);
        });
        window["reel"] = this.reelController;
      };
      SlotController.prototype.getRandomItem = function(option) {
        return this._listItemData[Math.floor(Math.random() * this._listItemData.length)];
      };
      SlotController.prototype.instantiateSlotItem = function(itemInfo, x, y, index) {
        void 0 === index && (index = 0);
        this.distanceItemX = this.reelController[index].tableView.width / this.maxCol;
        this.distanceItemY = this.reelController[index].tableView.height / this.maxRow;
        var item = cc.instantiate(this.itemSlot);
        item.parent = this.reelController[index].tableView;
        item.setPosition((x + .5) * this.distanceItemX, (y + .5) * this.distanceItemY);
        var component = item.getComponent(SlotItem_1.default);
        component.setItemInfo(itemInfo, x, y);
        if (this.dataStatus.lastmat) {
          component.setMatrixEnd(this.dataStatus.lastmat.split(","));
          component.setItemSprite(null);
        }
        item.active = true;
        return component;
      };
      SlotController.prototype.sortListItem = function(listItem) {
        var sortedList = [ [] ];
        var a = 3;
        var b = 4;
        if (2 == this.gameSizeType) {
          a = 4;
          b = 5;
        }
        if (3 == this.gameSizeType) {
          a = 4;
          b = 5;
        }
        listItem.forEach(function(item, i) {
          var index = a - i % b;
          sortedList[index] || (sortedList[index] = []);
          sortedList[index].push(item);
        });
        return sortedList;
      };
      SlotController.prototype.checkFreeSpin = function() {
        if (this.dataStatus.freeSpin.c > 0) {
          this.freeSpin = this.dataStatus.freeSpin.c;
          this.statusGame.freeSpin = this.freeSpin;
          view.bar.bottom.gameBar.updateStatus(this.statusGame);
        }
      };
      SlotController.prototype.watchEvent = function(data) {
        switch (data.type) {
         case api.key.SLOT_SPIN:
          "string" === typeof data.spinResult.mat && (data.spinResult.mat = [ data.spinResult.mat ]);
          this.dataSpin = data;
          this.isReceivedData = true;
          this.reelController.forEach(function(reel, index) {
            reel._listItem.forEach(function(item) {
              item.forEach(function(item) {
                item.getComponent("SlotItem").setMatrixEnd(data.spinResult.mat[index].split(","));
              });
            });
          });
          this.isSpin || this.spin(data);
          this.lastWin = data.spinResult.score.reward;
          this.updateStatus(data);
        }
      };
      SlotController.prototype.timeManager = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _a, dataSpin, spinResult, error_1;
          var _this = this;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              _b.trys.push([ 0, 20, , 21 ]);
              view.multi.loadContent();
              if (!this.isReFresh) {
                this.animationController && this.animationController.refreshSlotAnimation();
                view.bar.bottom.gameBar.resetWinAmount();
                this.isReFresh = true;
              }
              this.reelController.forEach(function(item) {
                return _this.animationController.spinItems(item._listItem);
              });
              if (this.freeSpin > 0) {
                this.freeSpin--;
                if (this.gameId == view.screen.slot.SlotController.gameId) {
                  this.statusGame.freeSpin = this.freeSpin;
                  view.bar.bottom.gameBar.updateStatus(this.statusGame);
                }
              }
              this.reelController.map(function(item) {
                return _this.animationController.spinItems(item._listItem);
              });
              return [ 4, util.game.delay(3e3, this.node) ];

             case 1:
              _b.sent();
              this.statusCallback && this.statusCallback();
              if (!(this.isReceivedData && this.dataSpin && this.dataSpin.spinResult)) return [ 3, 19 ];
              _a = this, dataSpin = _a.dataSpin, spinResult = _a.dataSpin.spinResult;
              if (!this.node || !this.handleCollectCoin) return [ 2 ];
              return [ 4, this.handleCollectCoin(dataSpin) ];

             case 2:
              _b.sent();
              if (!this.node || !this.animationController) return [ 2 ];
              return [ 4, this.animationController.setAnimationData(dataSpin) ];

             case 3:
              _b.sent();
              if (!this.node) return [ 2 ];
              if (!(spinResult.score.reward > 0)) return [ 3, 5 ];
              this.animationController.showWinAmount();
              return [ 4, this.animationController.showAllLines() ];

             case 4:
              _b.sent();
              return [ 3, 6 ];

             case 5:
              view.bar.bottom.gameBar.resetWinAmount();
              _b.label = 6;

             case 6:
              if (!this.node) return [ 2 ];
              if (!(spinResult.score.sum[2] && (spinResult.score.sum[2].w > 0 || spinResult.score.sum[2].c > 0))) return [ 3, 8 ];
              return [ 4, util.game.delay(500, this.node) ];

             case 7:
              _b.sent();
              if (!this.node || !this.handleBonus) return [ 2 ];
              cc.log("You get a bonus");
              this.bonus = true;
              this.isSpin = false;
              this.isReFresh = false;
              this.handleBonus(dataSpin);
              return [ 2 ];

             case 8:
              if (!this.node || !this.handleFreeSpin) return [ 2 ];
              return [ 4, this.handleFreeSpin(dataSpin) ];

             case 9:
              _b.sent();
              this.isSpin = false;
              this.isReFresh = false;
              return [ 4, view.multi.loadContent() ];

             case 10:
              _b.sent();
              if (!this.isAutoSpin) return [ 3, 13 ];
              if (!(this.autoSpinTimes >= 0)) return [ 3, 12 ];
              return [ 4, util.game.delay(200) ];

             case 11:
              _b.sent();
              _b.label = 12;

             case 12:
              if (0 == this.autoSpinTimes) {
                this.isAutoSpin = false;
                setTimeout(function() {
                  return view.bar.bottom.gameBar.updateSpinFrame(define.type.SpinButton.normal);
                }, 100);
              } else {
                this.autoSpinTimes < 0 && true == this.bonus && view.bar.bottom.gameBar.updateSpinFrame(define.type.SpinButton.normal);
                this.autoSpin(this.autoSpinTimes);
              }
              _b.label = 13;

             case 13:
              if (!(0 == Object.keys(dataSpin.freeSpin).length)) return [ 3, 18 ];
              if (!(spinResult.score.sum[1].c > 0)) return [ 3, 17 ];
              _b.label = 14;

             case 14:
              if (!(!this.isSpin && !this.isAutoSpin && this.animationController)) return [ 3, 17 ];
              return [ 4, this.animationController.showEachLine() ];

             case 15:
              _b.sent();
              return [ 4, this.animationController.showAllLines(1) ];

             case 16:
              _b.sent();
              return [ 3, 14 ];

             case 17:
              return [ 3, 18 ];

             case 18:
              return [ 3, 19 ];

             case 19:
              return [ 3, 21 ];

             case 20:
              error_1 = _b.sent();
              this.isSpin = false;
              this.isReFresh = false;
              this.isAutoSpin = false;
              cc.log("Time manager error:", error_1);
              return [ 3, 21 ];

             case 21:
              return [ 2 ];
            }
          });
        });
      };
      SlotController.prototype.autoSpin = function(times) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.autoSpinTimes = times;
              if (this.isSpin) {
                this.isAutoSpin = true;
                return [ 2 ];
              }
              cc.log(this.autoSpinTimes);
              this.isAutoSpin = true;
              return [ 4, this.spin() ];

             case 1:
              _a.sent();
              view.screen.slot && view.screen.slot.SlotController && this.autoSpinTimes == view.screen.slot.SlotController.autoSpinTimes && view.bar.bottom.gameBar.updateAutoSpinTime(this.autoSpinTimes);
              this.autoSpinTimes--;
              return [ 2 ];
            }
          });
        });
      };
      SlotController.prototype.clickSpin = function() {
        view.bar.bottom.gameBar.autoBet.active && view.bar.bottom.gameBar.hideAutoBet();
        if (this.isAutoSpin) {
          this.isAutoSpin = false;
          view.bar.bottom.gameBar.updateSpinFrame(define.type.SpinButton.normal);
          this.autoSpinTimes = 0;
          return;
        }
        if (this.isSpin) return;
        this.spin();
      };
      SlotController.prototype.spin = function(data) {
        if (this.currentBet > game.user.balance) {
          this.autoSpinTimes = 0;
          cc.log("User do not have enough coin to spin ...");
          return;
        }
        this.isSpin = true;
        this.isReceivedData = !!data;
        data || api.sendGD({
          e: api.key.SLOT_SPIN,
          gtype: this.gameId,
          totalbet: this.currentBet
        }, function(err, res) {
          cc.log("callback", res);
        });
        this.timeManager();
      };
      SlotController.prototype.strMatrixToArray = function(str, x, y) {
        void 0 === x && (x = 5);
        void 0 === y && (y = 3);
        str = str.split(",");
        var data = [];
        for (var i = 0; i < x; i++) {
          var arr1 = [];
          for (var j = 0; j < y; j++) arr1.push(str[(y - 1 - j) * x + i]);
          data.push(arr1);
        }
        return data;
      };
      SlotController.prototype.updateCurrentBet = function(bet) {
        this.currentBet = bet;
      };
      SlotController.prototype.handleFreeSpin = function(dataSpin) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            if (dataSpin.freeSpin.c > 0 && view.screen.slot) {
              if (dataSpin.spinResult.id != view.screen.slot.SlotController.gameId) return [ 2 ];
              this.freeSpin = dataSpin.freeSpin.c;
              this.statusGame.freeSpin = this.freeSpin;
              view.bar.bottom.gameBar.updateStatus(this.statusGame);
            } else this.freeSpin = 0;
            return [ 2 ];
          });
        });
      };
      SlotController.prototype.checkAutoSpin = function() {
        cc.log(this.autoSpinTimes);
        this.autoSpinTimes > 0 ? this.autoSpin(this.autoSpinTimes) : view.bar.bottom.gameBar.updateSpinFrame(define.type.SpinButton.normal);
      };
      SlotController.prototype.showPayTable = function() {
        cc.log("Show Pay Tabel Slot", view.screen.slot.id);
        this.payTabel.active = true;
        view.screen.game.node.insertChild(this.payTabel, 1);
        this.payTabel.zIndex = 1;
        this.payTabel.runAction(cc.fadeIn(.2));
        this.showTable(0);
      };
      SlotController.prototype.buttonPayTable = function(e, data) {
        var _this = this;
        var arrSlide = Object.getOwnPropertyNames(this.resourceController.resourceListName.payTable);
        var slides = parseInt(arrSlide[arrSlide.length - 1].split(".")[0]) + 1;
        if (0 == data) this.payTabel.runAction(cc.sequence([ cc.fadeOut(.2), cc.callFunc(function() {
          _this.payTabel.removeFromParent();
          _this.payTabel.active = false;
        }) ])); else {
          if (data < 0) {
            this.indexSlide--;
            this.indexSlide < 0 && (this.indexSlide = slides - 1);
          } else {
            this.indexSlide++;
            this.indexSlide > slides - 1 && (this.indexSlide = 0);
          }
          this.showTable(this.indexSlide);
        }
      };
      SlotController.prototype.showTable = function(slide) {
        var _this = this;
        var container = this.payTabel.getChildByName("container");
        container.children.forEach(function(item, index) {
          item.getComponent(cc.Sprite).spriteFrame = _this.resourceController.spriteList.find(function(item) {
            return item.name == _this.resourceController.resourceListName.payTable[slide + "." + index].split("/").pop();
          });
        });
      };
      SlotController.prototype.hideContent = function() {};
      SlotController.prototype.showContent = function() {
        0 === this.autoSpinTimes ? view.bar.bottom.gameBar.updateSpinFrame(define.type.SpinButton.normal) : view.bar.bottom.gameBar.updateAutoSpinTime(this.autoSpinTimes);
        this.statusGame.freeSpin = this.freeSpin;
        this.freeSpin > 0 && view.bar.bottom.gameBar.updateStatus(this.statusGame);
        cc.log("FS", this.freeSpin);
        cc.log("AUTO", this.autoSpinTimes);
      };
      SlotController.prototype.handleBonus = function(data) {};
      SlotController.prototype.handleCollectCoin = function(dataSpin) {};
      SlotController.prototype.updateStatus = function(data) {
        var _this = this;
        console.log("Update status", data);
        var status = null;
        if ("slotspin" === data.type) {
          var result = data.spinResult.score;
          status = {
            win: Math.round(result.reward),
            winType: define.type.WIN_TYPE.NORMAL,
            freeSpin: data.freeSpin && data.freeSpin.c > 0 ? data.freeSpin.c : result.freeSpin ? result.freeSpin : 0,
            isBonus: !(!result.sum[2] || !(result.sum[2].w > 0 || result.sum[2].c > 0)),
            autoSpin: this.autoSpinTimes > 0 ? this.autoSpinTimes : 0
          };
          this.statusCallback = function() {
            view.screen.slot && view.screen.slot.SlotController.gameId === _this.gameId && view.bar.bottom.gameBar.updateStatus(status);
            view.bar.top.pushStatus(status, _this.gameId);
          };
        } else {
          status = {
            win: 0,
            winType: define.type.WIN_TYPE.NORMAL,
            freeSpin: data.freeSpin && data.freeSpin.c > 0 ? data.freeSpin.c : 0,
            isBonus: !!data.bonus,
            autoSpin: this.autoSpinTimes > 0 ? this.autoSpinTimes : 0
          };
          view.bar.bottom.gameBar.updateStatus(status);
        }
        this.statusGame = status;
      };
      __decorate([ property(cc.String) ], SlotController.prototype, "gameId", void 0);
      __decorate([ property(cc.Integer) ], SlotController.prototype, "gameSizeType", void 0);
      __decorate([ property(cc.Node) ], SlotController.prototype, "tableView", void 0);
      __decorate([ property(cc.Node) ], SlotController.prototype, "itemSlot", void 0);
      __decorate([ property(cc.Float) ], SlotController.prototype, "IPadScale", void 0);
      __decorate([ property(cc.Node) ], SlotController.prototype, "payTabel", void 0);
      SlotController = __decorate([ ccclass ], SlotController);
      return SlotController;
    }(cc.Component);
    exports.default = SlotController;
    cc._RF.pop();
  }, {
    "./AnimationController": "AnimationController",
    "./Item": "Item",
    "./LinesController": "LinesController",
    "./SlotItem": "SlotItem"
  } ],
  SlotItem1: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2f3e7JQJ5lPJav3AfWf9gNI", "SlotItem1");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SlotItem_1 = require("../../../scripts/Slots/SlotItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotItem1 = function(_super) {
      __extends(SlotItem1, _super);
      function SlotItem1() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.slotsController = null;
        return _this;
      }
      SlotItem1.prototype.setItemSprite = function(item, first) {
        _super.prototype.setItemSprite.call(this, item);
        var coinLabel = this.node.getComponentInChildren(cc.Label);
        if (!coinLabel) return;
        if ("c" == this.id) {
          this.node.children[2].active = true;
          this.mainItemSprite.spriteFrame = this.slotsController.resourceController.loadRes("egypt-coin-3");
        } else this.node.children[2].active = false;
        if (this.slotsController.dataSpin) {
          "c" == this.id && (this.mainItemSprite.spriteFrame = this.slotsController.resourceController.loadRes("egypt-coin-2"));
          coinLabel.string = "$" + this.slotsController.dataSpin.spinResult.totalBet / 100;
        } else coinLabel.string = "$" + this.slotsController.dataStatus.lastBet / 100;
      };
      __decorate([ property({
        override: true
      }) ], SlotItem1.prototype, "slotsController", void 0);
      SlotItem1 = __decorate([ ccclass ], SlotItem1);
      return SlotItem1;
    }(SlotItem_1.default);
    exports.default = SlotItem1;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotItem": "SlotItem"
  } ],
  SlotItem2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "18b984gERFAQYDhHRd3FjKq", "SlotItem2");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SlotItem_1 = require("../../../scripts/Slots/SlotItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotItem2 = function(_super) {
      __extends(SlotItem2, _super);
      function SlotItem2() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.slotsController = null;
        return _this;
      }
      SlotItem2.prototype.setItemSprite = function(item, first) {
        _super.prototype.setItemSprite.call(this, item);
        var coinLabel = this.node.children[2].getComponent(cc.Label);
        if ("c" == this.id) {
          this.node.children[2].active = true;
          this.mainItemSprite.spriteFrame = this.slotsController.resourceController.loadRes("game-dra-item-coin-gray-mon");
        } else this.node.children[2].active = false;
        if (this.slotsController.dataSpin) {
          "c" == this.id && (this.mainItemSprite.spriteFrame = this.slotsController.resourceController.loadRes("game-dra-item-coin-gold-mon"));
          coinLabel.string = "$" + this.slotsController.dataSpin.spinResult.totalBet / 100;
        } else coinLabel.string = "$" + this.slotsController.dataStatus.lastBet / 100;
      };
      SlotItem2.prototype.checkSpecialItem = function(itemId) {
        if ("7" == itemId || "8" == itemId || "9" == itemId || "w" == itemId || "sw" == itemId) return true;
        return false;
      };
      __decorate([ property({
        override: true
      }) ], SlotItem2.prototype, "slotsController", void 0);
      SlotItem2 = __decorate([ ccclass ], SlotItem2);
      return SlotItem2;
    }(SlotItem_1.default);
    exports.default = SlotItem2;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotItem": "SlotItem"
  } ],
  SlotItem3: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a6d3exTRQhI6auP1LrWmYDA", "SlotItem3");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotSkeleton_1 = require("../../../scripts/Slots/SlotSkeleton");
    var SlotItem3 = function(_super) {
      __extends(SlotItem3, _super);
      function SlotItem3() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      SlotItem3 = __decorate([ ccclass ], SlotItem3);
      return SlotItem3;
    }(SlotSkeleton_1.default);
    exports.default = SlotItem3;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotSkeleton": "SlotSkeleton"
  } ],
  SlotItem4: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a73fbloCxtHDIET+yNuHXbr", "SlotItem4");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SlotItem_1 = require("../../../scripts/Slots/SlotItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotItem4 = function(_super) {
      __extends(SlotItem4, _super);
      function SlotItem4() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.borderWin = null;
        _this.partical = null;
        return _this;
      }
      SlotItem4.prototype.onLoad = function() {
        _super.prototype.onLoad.call(this);
        this.borderWin.active = false;
      };
      SlotItem4.prototype.updateShowBorderWin = function() {
        var _this = this;
        this.borderWin.active = true;
        if (true == this.border.active) {
          this.border.active = false;
          setTimeout(function() {
            return _this.border.active = true;
          }, 1400);
        }
      };
      SlotItem4.prototype.hideBorderWin = function() {
        this.borderWin.active = false;
      };
      SlotItem4.prototype.checkSpecialItem = function(itemId) {
        if ("s" == itemId || "w" == itemId || "12" == itemId || "13" == itemId || "14" == itemId || "15" == itemId || "22" == itemId || "23" == itemId || "24" == itemId || "25" == itemId) return true;
        return false;
      };
      __decorate([ property(cc.Node) ], SlotItem4.prototype, "borderWin", void 0);
      __decorate([ property(cc.Node) ], SlotItem4.prototype, "partical", void 0);
      SlotItem4 = __decorate([ ccclass ], SlotItem4);
      return SlotItem4;
    }(SlotItem_1.default);
    exports.default = SlotItem4;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotItem": "SlotItem"
  } ],
  SlotItem5: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c69b03tvv9KQZLdNmcJcwHr", "SlotItem5");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotItem_1 = require("../../../scripts/Slots/SlotItem");
    var SlotItem5 = function(_super) {
      __extends(SlotItem5, _super);
      function SlotItem5() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      SlotItem5 = __decorate([ ccclass ], SlotItem5);
      return SlotItem5;
    }(SlotItem_1.default);
    exports.default = SlotItem5;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotItem": "SlotItem"
  } ],
  SlotItem6: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b28b2desdRKna+K1V1C4gvK", "SlotItem6");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotSkeleton_1 = require("../../../scripts/Slots/SlotSkeleton");
    var SlotItem6 = function(_super) {
      __extends(SlotItem6, _super);
      function SlotItem6() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      SlotItem6.prototype.setItemInfo = function(item, x, y) {
        _super.prototype.setItemInfo.call(this, item, x, y);
        if (this.slotsController.dataStatus && this.slotsController.dataStatus.freeSpin.c > 0) {
          var sticky = this.slotsController.dataStatus.freeReelGame.stickyWildReels;
          if (!sticky) return;
          sticky.indexOf(this.x + 1) >= 0 && this.checkStikyWild();
        }
      };
      SlotItem6.prototype.setItemSprite = function(item) {
        if (this.slotsController.dataSpin && this.slotsController.dataSpin.freeSpin.c > 0) {
          var sticky = this.slotsController.dataSpin.features.freeReelGame.stickyWildReels;
          if (sticky.indexOf(this.x + 1) >= 0) {
            this.checkStikyWild();
            return;
          }
        }
        _super.prototype.setItemSprite.call(this, item);
      };
      SlotItem6.prototype.checkStikyWild = function() {
        this.id = "w";
        this.setItemSkeleton("wild");
      };
      SlotItem6 = __decorate([ ccclass ], SlotItem6);
      return SlotItem6;
    }(SlotSkeleton_1.default);
    exports.default = SlotItem6;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotSkeleton": "SlotSkeleton"
  } ],
  SlotItem7: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f995aMrajBOQLo9kdlBnyOl", "SlotItem7");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotSkeleton_1 = require("../../../scripts/Slots/SlotSkeleton");
    var SlotItem7 = function(_super) {
      __extends(SlotItem7, _super);
      function SlotItem7() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.slotsController = null;
        _this.aniBorder = null;
        _this.value = [ .25, .5, .25, .5, .25, .5, .75, 1, 1.5, 2, 2.5, .75, 1, 1.5, 2, 2.5, .75, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 3, 3.5, 4, 4.5, 5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, "MINI", "MINI", "MINI", "MINI", "MINI", "MINOR", "MINOR", "MINOR", "MAJOR", "MAJOR", "WHEEL", "WHEEL", "SUPPER" ];
        _this.listItemAni = [ "0", "1", "2", "3", "4" ];
        return _this;
      }
      SlotItem7.prototype.setItemSprite = function(randomItem, first) {
        return __awaiter(this, void 0, void 0, function() {
          var shieldLabel, valueRandom, totalB, data_1;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _super.prototype.setItemSprite.call(this, randomItem);
              shieldLabel = this.node.children[1].getComponent(cc.Label);
              valueRandom = this.value[Math.floor(0 + Math.random() * this.value.length)];
              if ("k" == this.id) {
                this.node.children[1].active = true;
                if ("number" == typeof valueRandom) {
                  totalB = 0;
                  totalB = this.slotsController.dataSpin ? this.slotsController.dataSpin.spinResult.totalBet : this.slotsController.dataStatus.lastBet;
                  shieldLabel.string = "" + util.game.abbreviateNumber(valueRandom * totalB, 0);
                } else shieldLabel.string = "" + valueRandom;
              } else this.node.children[1].active = false;
              if (!this.slotsController.dataSpin) return [ 3, 2 ];
              return [ 4, util.game.delay(1e3) ];

             case 1:
              _a.sent();
              data_1 = this.slotsController.dataSpin.spinResult.score.sum[6].arr;
              data_1.forEach(function(item, index) {
                var listItemShield = _this.slotsController._listItem[item.position.col][item.position.row + 1];
                "number" == typeof data_1[index].value ? listItemShield.node.children[1].getComponent(cc.Label).string = "" + util.game.abbreviateNumber(data_1[index].value * _this.slotsController.dataSpin.spinResult.totalBet, 0) : listItemShield.node.children[1].getComponent(cc.Label).string = "" + data_1[index].value;
              });
              _a.label = 2;

             case 2:
              return [ 2 ];
            }
          });
        });
      };
      SlotItem7.prototype.playZoomAnimation = function() {
        _super.prototype.playZoomAnimation.call(this);
        this.node.children[3].active = true;
        this.listItemAni.includes(this.id) && this.node.children[0].runAction(cc.repeatForever(cc.sequence([ cc.fadeOut(.4), cc.fadeIn(.6) ])));
      };
      SlotItem7.prototype.stopZoomAnimation = function() {
        _super.prototype.stopZoomAnimation.call(this);
        this.node.children[3].active = false;
      };
      __decorate([ property({
        override: true
      }) ], SlotItem7.prototype, "slotsController", void 0);
      __decorate([ property(cc.Node) ], SlotItem7.prototype, "aniBorder", void 0);
      SlotItem7 = __decorate([ ccclass ], SlotItem7);
      return SlotItem7;
    }(SlotSkeleton_1.default);
    exports.default = SlotItem7;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotSkeleton": "SlotSkeleton"
  } ],
  SlotItem8: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a076dq++lFHt5SZnwTBtVKG", "SlotItem8");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotItem_1 = require("../../../scripts/Slots/SlotItem");
    var SlotItem8 = function(_super) {
      __extends(SlotItem8, _super);
      function SlotItem8() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      SlotItem8 = __decorate([ ccclass ], SlotItem8);
      return SlotItem8;
    }(SlotItem_1.default);
    exports.default = SlotItem8;
    cc._RF.pop();
  }, {
    "../../../scripts/Slots/SlotItem": "SlotItem"
  } ],
  SlotItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5fd32DWbaVC7rErwd21rsCa", "SlotItem");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SlotController_1 = require("./SlotController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotItem = function(_super) {
      __extends(SlotItem, _super);
      function SlotItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.mainItemSprite = null;
        _this.slotsControllerNode = null;
        _this.border = null;
        _this.zoomAni = null;
        _this.a = .5;
        _this.c = 1900;
        _this.maxCount = 5;
        _this.id = "";
        _this.x = 0;
        _this.y = 0;
        _this.count = 0;
        return _this;
      }
      SlotItem.prototype.onLoad = function() {
        this.isMove = false;
        this.time = 0;
        this.duration = 0;
        this.y0 = this.node.y;
        this.distance = 0;
        this.slotsController = this.slotsControllerNode.getComponent(SlotController_1.default);
        this.maxY = (this.slotsController.maxRow + .5) * this.slotsController.distanceItemY;
        this.minY = -.5 * this.slotsController.distanceItemY;
        this.initZoomAnimation();
      };
      SlotItem.prototype.setItemInfo = function(item, x, y) {
        this.id = item.id;
        this.mainItemSprite.spriteFrame = item.spritesFrame;
        this.x = x;
        this.y = y;
        if (this.slotsController._listBorderImg.length > 0) if (this.checkSpecialItem(item.id)) {
          var borderImg = this.slotsController._listBorderImg.find(function(x) {
            return x.id === item.id;
          });
          this.border.active = true;
          this.border.getComponent(cc.Sprite).spriteFrame = borderImg.img;
        } else this.border.active = false;
      };
      SlotItem.prototype.setItemSprite = function(randomItem, first) {
        var _this = this;
        var item = this.slotsController.getRandomItem();
        if (randomItem) {
          this.mainItemSprite.spriteFrame = item.spritesFrame;
          this.id = item.id;
        } else if (null != this.arrResult && this.y <= this.slotsController.maxRow - 1) {
          var index_1 = this.slotsController.maxCol * (this.slotsController.maxRow - this.y - 1) + this.x;
          item = this.slotsController._listItemData.find(function(x) {
            return x.id == _this.arrResult[index_1];
          });
          this.id = item.id;
          this.mainItemSprite.spriteFrame = item.spritesFrame;
        }
        if (this.slotsController._listBorderImg.length > 0) if (this.checkSpecialItem(item.id)) {
          var borderImg = this.slotsController._listBorderImg.find(function(x) {
            return x.id === item.id;
          });
          this.border.active = true;
          this.border.getComponent(cc.Sprite).spriteFrame = borderImg.img;
        } else this.border.active = false;
      };
      SlotItem.prototype.checkSpecialItem = function(itemId) {
        if ("b" == itemId || "s" == itemId || "w" == itemId) return true;
        return false;
      };
      SlotItem.prototype.move = function() {
        var _this = this;
        var self = this;
        this.node.runAction(cc.sequence(cc.moveTo(.3, cc.v2(this.node.x, (this.y + .6) * this.slotsController.distanceItemY)), cc.callFunc(function() {
          _this.isMove = true;
          _this.time = 0;
          _this.y0 = self.node.y;
          _this.count = 0;
        })));
      };
      SlotItem.prototype.setMatrixEnd = function(data) {
        this.arrResult = data;
      };
      SlotItem.prototype.stop = function() {
        this.endingMove = true;
      };
      SlotItem.prototype.scaleAnim = function(time) {
        var self = this;
        var action = cc.scaleTo(.5, 1.2);
        this.node.runAction(action);
        setTimeout(function() {
          var action = cc.scaleTo(.5, 1);
          null != self.node && self.node.runAction(action);
        }, 500);
      };
      SlotItem.prototype.initZoomAnimation = function() {
        this.mainItem = this.node.children[1] || this.node;
        this.zoomAnimation = this.mainItem.addComponent(cc.Animation);
        this.zoomAni.speed = 1.1;
        this.zoomAnimation.addClip(this.zoomAni);
      };
      SlotItem.prototype.playZoomAnimation = function() {
        this.zoomAnimation.play("ZoomItem");
      };
      SlotItem.prototype.stopZoomAnimation = function() {
        if (!this.zoomAnimation) return;
        this.zoomAnimation.setCurrentTime(0);
        this.zoomAnimation.stop("ZoomItem");
      };
      SlotItem.prototype.update = function(dt) {
        this.time += dt;
        this.duration += dt;
        if (true == this.isMove) {
          this.node.y = this.y0 - this.c * this.time;
          if (this.node.y <= this.minY) {
            this.count++;
            if (!this.reallyStop) {
              var delta = this.minY - this.node.y;
              this.node.y = this.maxY - delta;
              this.y0 = this.node.y;
              this.time = 0;
            }
            this.setItemSprite(true);
            var maxCount = this.maxCount;
            if (this.count >= maxCount) {
              this.endingMove = true;
              this.setItemSprite(false);
              this.reallyStop = true;
            }
          }
          if (this.reallyStop && this.node.y <= (this.y + .3) * this.slotsController.distanceItemY) {
            this.node.y = (this.y + .3) * this.slotsController.distanceItemY;
            this.endingMove = false;
            this.isMove = false;
            var action = cc.moveTo(.2, cc.v2(this.node.x, (this.y + .5) * this.slotsController.distanceItemY)).easing(cc.easeBackOut());
            this.node.runAction(cc.sequence([ action, cc.callFunc(function() {}) ]));
            this.reallyStop = false;
          }
        }
      };
      __decorate([ property(cc.Sprite) ], SlotItem.prototype, "mainItemSprite", void 0);
      __decorate([ property(cc.Node) ], SlotItem.prototype, "slotsControllerNode", void 0);
      __decorate([ property(cc.Node) ], SlotItem.prototype, "border", void 0);
      __decorate([ property(cc.AnimationClip) ], SlotItem.prototype, "zoomAni", void 0);
      __decorate([ property ], SlotItem.prototype, "a", void 0);
      __decorate([ property ], SlotItem.prototype, "c", void 0);
      __decorate([ property ], SlotItem.prototype, "maxCount", void 0);
      SlotItem = __decorate([ ccclass ], SlotItem);
      return SlotItem;
    }(cc.Component);
    exports.default = SlotItem;
    cc._RF.pop();
  }, {
    "./SlotController": "SlotController"
  } ],
  SlotSkeleton: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "86f79nJDm1CrrvlJ1GslRXC", "SlotSkeleton");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotItem_1 = require("./SlotItem");
    var SlotSkeleton = function(_super) {
      __extends(SlotSkeleton, _super);
      function SlotSkeleton() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.mainItemSke = null;
        _this.listItemSkeleton = {};
        return _this;
      }
      SlotSkeleton.prototype.onLoad = function() {
        _super.prototype.onLoad.call(this);
        this.listItemSkeleton = this.slotsController.listItemSkeleton;
      };
      SlotSkeleton.prototype.setItemInfo = function(item, x, y) {
        this.id = item.id;
        this.x = x;
        this.y = y;
        var keys = Object.keys(this.listItemSkeleton);
        this.setItemSkeleton(this.listItemSkeleton[keys[Math.random() * keys.length | 0]]);
      };
      SlotSkeleton.prototype.setItemSprite = function(item) {
        var _this = this;
        if (item) {
          item = this.slotsController.getRandomItem();
          this.setItemSkeleton(this.listItemSkeleton[item.id]);
          this.id = item.id;
        } else {
          item = this.slotsController.getRandomItem();
          if (null != this.arrResult) {
            if (this.y <= this.slotsController.maxRow - 1) {
              var index_1 = this.slotsController.maxCol * (this.slotsController.maxRow - this.y - 1) + this.x;
              item = this.slotsController._listItemData.find(function(x) {
                return x.id == _this.arrResult[index_1];
              });
              this.id = item.id;
              this.setItemSkeleton(this.listItemSkeleton[item.id]);
            }
          } else this.setItemSkeleton(this.listItemSkeleton[item.id]);
        }
      };
      SlotSkeleton.prototype.setItemSkeleton = function(name) {
        this.mainItemSke.animation = name;
      };
      SlotSkeleton.prototype.initZoomAnimation = function() {};
      SlotSkeleton.prototype.playZoomAnimation = function() {
        var name = this.mainItemSke.animation;
        this.mainItemSke.animation = name + "_run";
      };
      SlotSkeleton.prototype.stopZoomAnimation = function() {
        if (!this.mainItemSke) return;
        var name = this.mainItemSke.animation;
        this.mainItemSke.animation = name.includes("_run") ? name.slice(0, name.length - 4) : name;
      };
      __decorate([ property(sp.Skeleton) ], SlotSkeleton.prototype, "mainItemSke", void 0);
      SlotSkeleton = __decorate([ ccclass ], SlotSkeleton);
      return SlotSkeleton;
    }(SlotItem_1.default);
    exports.default = SlotSkeleton;
    cc._RF.pop();
  }, {
    "./SlotItem": "SlotItem"
  } ],
  SlotsDownloadCtr: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "47cb4qOft9Ak7C8Ceh0Sa9R", "SlotsDownloadCtr");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SlotsDownloadCtr = function(_super) {
      __extends(SlotsDownloadCtr, _super);
      function SlotsDownloadCtr() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      SlotsDownloadCtr.prototype.loadSrc = function() {
        cc.log("test");
      };
      SlotsDownloadCtr.prototype.getUrls = function(id) {
        var resPath = "4_gameScene/" + id + "_Slot" + id;
        cc.loader.loadResDir(resPath, function(err, resCtr, urls) {});
      };
      SlotsDownloadCtr = __decorate([ ccclass ], SlotsDownloadCtr);
      return SlotsDownloadCtr;
    }(cc.Component);
    exports.default = SlotsDownloadCtr;
    cc._RF.pop();
  }, {} ],
  SoundController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ba032ooKTBJ14H9we97QPTJ", "SoundController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SoundController = function() {
      function SoundController() {
        this.sounds = {};
        this.soundOn = false;
        this.musicOn = false;
        this.listSounds = [];
        this.buttonSFXName = "click";
        this.popupSFXName = "slide";
        this.dialogSFXName = "pop";
      }
      SoundController.ins = function() {
        this.instance || (this.instance = new SoundController());
        return this.instance;
      };
      SoundController.prototype.init = function() {
        if (null === cc.sys.localStorage.getItem(define.key.CONFIG_SOUND)) {
          this.soundOn = true;
          cc.sys.localStorage.setItem(define.key.CONFIG_SOUND, this.soundOn);
        }
        this.soundOn = "true" === cc.sys.localStorage.getItem(define.key.CONFIG_SOUND);
        if (null === cc.sys.localStorage.getItem(define.key.CONFIG_MUSIC)) {
          this.musicOn = true;
          cc.sys.localStorage.setItem(define.key.CONFIG_MUSIC, this.musicOn);
        }
        this.musicOn = "true" === cc.sys.localStorage.getItem(define.key.CONFIG_MUSIC);
        this.listSounds = [];
      };
      SoundController.prototype.toggleSound = function() {
        this.soundOn = "true" === cc.sys.localStorage.getItem(define.key.CONFIG_SOUND);
        this.soundOn = !this.soundOn;
        cc.sys.localStorage.setItem(define.key.CONFIG_SOUND, this.soundOn);
        this.soundOn || this.stopAllSounds();
        cc.log("SoundController", "toggle sound", "sound on: ", this.soundOn);
        return this.soundOn;
      };
      SoundController.prototype.toggleMusic = function() {
        this.musicOn = "true" === cc.sys.localStorage.getItem(define.key.CONFIG_MUSIC);
        this.musicOn = !this.musicOn;
        cc.sys.localStorage.setItem(define.key.CONFIG_MUSIC, this.musicOn);
        this.musicOn ? this.playMusic() : this.stopMusic();
        cc.log("SoundController", "toggle music", "music on: ", this.musicOn);
        return this.musicOn;
      };
      SoundController.prototype.getSoundStatus = function() {
        return this.soundOn;
      };
      SoundController.prototype.getMusicStatus = function() {
        return this.musicOn;
      };
      SoundController.prototype.setMusicVolume = function(volume) {
        if ("number" !== typeof volume || volume < 0 || volume > 1) {
          cc.warn("SoundController", "setMusicVolume", "volume must be number between 0 and 1", volume);
          return;
        }
        cc.audioEngine.setMusicVolume(volume);
      };
      SoundController.prototype.setSoundVolume = function(volume) {
        if ("number" !== typeof volume || volume < 0 || volume > 1) {
          cc.warn("SoundController", "setSoundVolume", "volume must be number between 0 and 1", volume);
          return;
        }
        cc.audioEngine.setEffectsVolume(volume);
      };
      SoundController.prototype.getMusicVolume = function() {
        return cc.audioEngine.getMusicVolume();
      };
      SoundController.prototype.getSoundVolume = function() {
        return cc.audioEngine.getEffectsVolume();
      };
      SoundController.prototype.playMusic = function(name, loop) {
        if (!this.musicOn) return;
        "undefined" === typeof loop && (loop = true);
        var id = void 0;
        "string" === typeof name ? this.sounds && this.sounds[name] ? id = cc.audioEngine.playMusic(this.sounds[name], loop) : cc.warn("SoundController", "playMusic", "music " + name + " not defined in resource", this.sounds) : this.sounds && this.sounds["music"] && (id = cc.audioEngine.playMusic(this.sounds["music"], loop));
        return id;
      };
      SoundController.prototype.pauseMusic = function() {
        cc.audioEngine.pauseMusic();
      };
      SoundController.prototype.resumeMusic = function() {
        if (!this.musicOn) return;
        cc.audioEngine.resumeMusic();
      };
      SoundController.prototype.stopMusic = function() {
        cc.audioEngine.stopMusic();
      };
      SoundController.prototype.isMusicPlaying = function() {
        return cc.audioEngine.isMusicPlaying();
      };
      SoundController.prototype.playSound = function(source, option) {
        if (!source) return game.warn("Audio source is not available.");
        if (!this.soundOn) return;
        option || (option = {});
        "undefined" === typeof option.loop && (option.loop = false);
        var id = void 0;
        var name = "";
        name = source instanceof cc.AudioClip ? source.name : source;
        "string" === typeof source && this.sounds && this.sounds[name] ? id = cc.audioEngine.playEffect(this.sounds[name], option.loop) : source instanceof cc.AudioClip ? id = cc.audioEngine.playEffect(source, option.loop) : cc.warn("SoundController", "playSound", "sound " + name + " not defined in resource", this.sounds);
        cc.audioEngine.setVolume(id, option.volume || 1);
        "number" === typeof id && this.listSounds.push({
          name: name,
          id: id,
          tag: option.tag || "default"
        });
        return id;
      };
      SoundController.prototype.setVolumeByTag = function(tag, volume) {
        if (volume > 1 || volume < 0) return false;
        this.listSounds.forEach(function(item) {
          item.tag === tag && cc.audioEngine.setVolume(item.id, volume);
        });
        return true;
      };
      SoundController.prototype.disableAudioByTag = function(tag) {
        return this.setVolumeByTag(tag, 0);
      };
      SoundController.prototype.pauseSound = function(name) {
        var list = this.listSounds.filter(function(o) {
          return o.name === name && cc.audioEngine.getState(o.id) === cc.audioEngine.AudioState.PLAYING;
        });
        for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
          var sound = list_1[_i];
          cc.audioEngine.pauseEffect(sound.id);
        }
      };
      SoundController.prototype.pauseAllSounds = function() {
        cc.audioEngine.pauseAllEffects();
      };
      SoundController.prototype.resumeSound = function(name) {
        if (!this.soundOn) return;
        var sound = this.listSounds.find(function(o) {
          return o.name === name && cc.audioEngine.getState(o.id) === cc.audioEngine.AudioState.PAUSED;
        });
        sound && cc.audioEngine.resumeEffect(sound.id);
      };
      SoundController.prototype.resumeAllSounds = function() {
        if (!this.soundOn) return;
        cc.audioEngine.resumeAllEffects();
      };
      SoundController.prototype.stopSound = function(name) {
        if ("string" === typeof name) {
          var list = this.listSounds.filter(function(o) {
            return o.name === name && cc.audioEngine.getState(o.id) === cc.audioEngine.AudioState.PLAYING;
          });
          for (var _i = 0, list_2 = list; _i < list_2.length; _i++) {
            var sound = list_2[_i];
            cc.audioEngine.stopEffect(sound.id);
          }
        } else "number" === typeof name && cc.audioEngine.stopEffect(name);
      };
      SoundController.prototype.stopAllSounds = function() {
        cc.audioEngine.stopAllEffects();
      };
      SoundController.prototype.isSoundsPlaying = function(name) {
        var list = this.listSounds.filter(function(o) {
          return o.name === name && cc.audioEngine.getState(o.id) === cc.audioEngine.AudioState.PLAYING;
        });
        if (list && list.length && list.length > 0) return true;
        return false;
      };
      SoundController.prototype.pauseAll = function() {
        this.pauseMusic();
        this.pauseAllSounds();
      };
      SoundController.prototype.resumeAll = function() {
        this.resumeMusic();
        this.resumeAllSounds();
      };
      SoundController.prototype.stopAll = function() {
        this.stopMusic();
        this.stopAllSounds();
      };
      SoundController.prototype.playButtonSFX = function() {
        this.isSoundsPlaying(this.popupSFXName) || this.isSoundsPlaying(this.dialogSFXName) || this.playSound(this.buttonSFXName);
      };
      SoundController.prototype.stopButtonSFX = function() {
        this.stopSound(this.buttonSFXName);
      };
      SoundController.prototype.playPopupSFX = function() {
        this.stopButtonSFX();
        this.playSound(this.popupSFXName);
      };
      SoundController.prototype.stopPopupSFX = function() {
        this.stopSound(this.popupSFXName);
      };
      SoundController.prototype.playDialogSFX = function() {
        this.stopButtonSFX();
        this.playSound(this.dialogSFXName);
      };
      SoundController.prototype.stopDialogSFX = function() {
        this.stopSound(this.dialogSFXName);
      };
      SoundController.instance = void 0;
      return SoundController;
    }();
    exports.default = SoundController;
    cc._RF.pop();
  }, {} ],
  Store: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8d5df9Kfl1LNISDiv4sLJhx", "Store");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DefineKey = require("./Define/DefineKey");
    var Store = function() {
      function Store() {
        this.eventList = {};
        this.subUid = -1;
        this.key = DefineKey.store;
        return Store._instance || (Store._instance = this);
      }
      Store.prototype.register = function(event, callback) {
        var token = (++this.subUid).toString();
        this.eventList[event] ? this.eventList[event].push({
          token: token,
          callback: callback
        }) : this.eventList[event] = [ {
          token: token,
          callback: callback
        } ];
        return token;
      };
      Store.prototype.unRegister = function(token) {
        var eventList = this.eventList;
        Object.keys(eventList).forEach(function(key) {
          if (!eventList[key]) return;
          var index = eventList[key].findIndex(function(item) {
            return item.token == token;
          });
          if (index < 0) return;
          eventList[key].splice(index, 1);
          return true;
        });
      };
      Store.prototype.emit = function(event, data, option) {
        if (!this.eventList[event]) return false;
        this.eventList[event].forEach(function(item) {
          item.callback(data, option);
        });
      };
      return Store;
    }();
    exports.default = new Store();
    cc._RF.pop();
  }, {
    "./Define/DefineKey": "DefineKey"
  } ],
  StringUtil: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ba958kkuGVE/prrUc//BcIQ", "StringUtil");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var StringUtil = function() {
      function StringUtil() {}
      StringUtil.formatMoney = function(n, c) {
        void 0 === c && (c = ",");
        return Math.floor(n).toString().replace(/\B(?=(\d{3})+(?!\d))/g, c);
      };
      return StringUtil;
    }();
    exports.default = StringUtil;
    cc._RF.pop();
  }, {} ],
  TopBarController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "388b1L76zZKf4od6mv6qXzi", "TopBarController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CoinLabel_1 = require("../Components/CoinLabel");
    var AccumulatedBar_1 = require("../Components/AccumulatedBar");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var TopBarController = function(_super) {
      __extends(TopBarController, _super);
      function TopBarController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.tag = "TopBarController";
        _this.coinController = void 0;
        _this.keyList = [];
        _this.model = void 0;
        _this.lbStatusTitle = void 0;
        _this.lbStatus = void 0;
        _this.lbCoin = void 0;
        _this.barExp = void 0;
        _this.lbLevel = void 0;
        _this.lbPercent = void 0;
        _this.nodeStatus = void 0;
        return _this;
      }
      TopBarController.prototype.onLoad = function() {
        var _this = this;
        this.model = this.node.getChildByName("model");
        this.coinController = new CoinLabel_1.default(this.lbCoin);
        this.barExp.setListener(function(percent) {
          _this.lbPercent.string = Math.floor(percent) + "%";
        });
        this.resgisterEvent();
        this.lbStatusTitle = this.nodeStatus.getChildByName("title").getComponent(cc.Label);
        this.lbStatus = this.nodeStatus.getChildByName("status").getComponent(cc.Label);
      };
      TopBarController.prototype.resgisterEvent = function() {
        var _this = this;
        var keyCoin = store.register(store.key.UPDATE_USER_BALANCE, function(amount, option) {
          return __awaiter(_this, void 0, void 0, function() {
            return __generator(this, function(_a) {
              this.coinController.updateUserBalance(amount, option);
              return [ 2 ];
            });
          });
        });
        this.keyList.push(keyCoin);
        var keyLevel = store.register(store.key.UPDATE_USER_LEVEL, function(data, option) {
          return __awaiter(_this, void 0, void 0, function() {
            var currentExp, requiredExp, currentLevel, percent;
            return __generator(this, function(_a) {
              currentExp = data.currentExp, requiredExp = data.requiredExp, currentLevel = data.currentLevel;
              percent = currentExp <= requiredExp ? currentExp / requiredExp : 1;
              this.barExp.updateProgress(percent);
              this.lbLevel.string = currentLevel;
              return [ 2 ];
            });
          });
        });
        this.keyList.push(keyLevel);
      };
      TopBarController.prototype.hide = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          _this.node.y = 360;
          _this.node.stopAllActions();
          _this.lock();
          _this.node.runAction(cc.sequence(cc.moveBy(.1, cc.v2(0, _this.node.height)), cc.callFunc(function() {
            _this.unlock();
            res();
          })));
        });
      };
      TopBarController.prototype.show = function() {
        var _this = this;
        return new Promise(function(res, rej) {
          _this.node.y = 360 + _this.node.height;
          _this.node.stopAllActions();
          _this.lock();
          _this.node.runAction(cc.sequence(cc.moveBy(.2, cc.v2(0, -_this.node.height)), cc.callFunc(function() {
            _this.unlock();
            res();
          })));
        });
      };
      TopBarController.prototype.lock = function() {
        this.model.active = true;
      };
      TopBarController.prototype.unlock = function() {
        this.model.active = false;
      };
      TopBarController.prototype.pushStatus = function(status, gameId) {
        var _a = this, lbStatus = _a.lbStatus, lbStatusTitle = _a.lbStatusTitle;
        var statusData = JSON.parse(JSON.stringify(status));
        var statusArray = [];
        statusData.isBonus && statusArray.push({
          key: "isBonus",
          data: true
        });
        !status.win || statusData.winType !== define.type.WIN_TYPE.BIGWIN && statusData.winType !== define.type.WIN_TYPE.MEGAWIN || statusArray.push({
          key: "win",
          data: status.win
        });
        status.freeSpin && statusArray.push({
          key: "freeSpin",
          data: status.freeSpin
        });
        status.win && statusData.winType === define.type.WIN_TYPE.NORMAL && statusArray.push({
          key: "win",
          data: status.win
        });
        status.autoSpin && statusArray.push({
          key: "autoSpin",
          data: status.autoSpin
        });
        if (!statusArray.length) return;
        var gameName = "Slot " + (Object.values(config.game.SMID).findIndex(function(o) {
          return o === gameId;
        }) + 1);
        var getStatus = function(object) {
          var key = object.key, data = object.data;
          if ("isBonus" === key) return "You got bonus";
          if ("freeSpin" === key) return "FreeSpin: " + data;
          if ("win" === key) return "Win: " + util.string.formatMoney(data);
          if ("autoSpin" === key) return "Auto spin: " + data;
          return "";
        };
        lbStatusTitle.node.stopAllActions();
        lbStatusTitle.string !== gameName && lbStatusTitle.node.runAction(cc.sequence(cc.moveBy(.15, cc.v2(0, 20)), cc.callFunc(function() {
          if (!lbStatusTitle) return;
          lbStatusTitle.string = gameName;
          lbStatusTitle.node.y = 37;
          lbStatusTitle.node.opacity || (lbStatusTitle.node.opacity = 255);
        }), cc.moveTo(.15, cc.v2(0, 17)), cc.delayTime(1)));
        lbStatus.node.stopAllActions();
        lbStatus.node.runAction(cc.repeat(cc.sequence(cc.moveBy(.15, cc.v2(0, -20)), cc.callFunc(function() {
          if (!lbStatus) return;
          lbStatus.string = getStatus(statusArray[0]);
          lbStatus.node.y = -20;
          statusArray = statusArray.slice(1);
          lbStatus.node.opacity || (lbStatus.node.opacity = 255);
        }), cc.moveTo(.15, cc.v2(0, -1.7)), cc.delayTime(1)), statusArray.length));
      };
      TopBarController.prototype.onClickAvatar = function() {
        game.log(this.tag, "onClickAvatar");
      };
      TopBarController.prototype.onClickBuy = function() {
        game.log(this.tag, "onClickBuy");
      };
      TopBarController.prototype.onClickDeal = function() {
        game.log(this.tag, "onClickDeal");
      };
      TopBarController.prototype.onClickMulti = function() {
        game.log(this.tag, "onClickMulti");
        view.multi.toggleButton();
      };
      TopBarController.prototype.onClickSetting = function() {
        game.log(this.tag, "onClickSetting");
      };
      TopBarController.prototype.onDestroy = function() {
        this.keyList.forEach(function(key) {
          return store.unRegister(key);
        });
      };
      __decorate([ property(cc.Label) ], TopBarController.prototype, "lbCoin", void 0);
      __decorate([ property(AccumulatedBar_1.default) ], TopBarController.prototype, "barExp", void 0);
      __decorate([ property(cc.Label) ], TopBarController.prototype, "lbLevel", void 0);
      __decorate([ property(cc.Label) ], TopBarController.prototype, "lbPercent", void 0);
      __decorate([ property(cc.Node) ], TopBarController.prototype, "nodeStatus", void 0);
      TopBarController = __decorate([ ccclass ], TopBarController);
      return TopBarController;
    }(cc.Component);
    exports.default = TopBarController;
    cc._RF.pop();
  }, {
    "../Components/AccumulatedBar": "AccumulatedBar",
    "../Components/CoinLabel": "CoinLabel"
  } ],
  UIController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b5626ARq+VCVp5u2EAHN17N", "UIController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var PopupData = function() {
      function PopupData() {
        this.name = "";
        this.prefab = void 0;
        this.componentName = "";
      }
      __decorate([ property({
        tooltip: "Name of popup, to open popup by name, view can view instance of popup in view.popup[name]"
      }) ], PopupData.prototype, "name", void 0);
      __decorate([ property({
        type: cc.Prefab,
        tooltip: "Prefab of popup with popup name"
      }) ], PopupData.prototype, "prefab", void 0);
      __decorate([ property({
        tooltip: "Component name of popup prefab"
      }) ], PopupData.prototype, "componentName", void 0);
      PopupData = __decorate([ ccclass("PopupData") ], PopupData);
      return PopupData;
    }();
    exports.PopupData = PopupData;
    var UIController = function(_super) {
      __extends(UIController, _super);
      function UIController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.homeScenePrefab = null;
        _this.gameScenePrefab = null;
        _this.popupData = [];
        _this.canvas = null;
        return _this;
      }
      UIController.prototype.onLoad = function() {
        this.canvas = cc.director.getScene().getChildByName("Canvas");
        this.initialize();
      };
      UIController.prototype.initialize = function() {
        cc.game.addPersistRootNode(this.node);
        view.multi.node.zIndex = define.zIndex.MULTI;
        view.bar.node.zIndex = define.zIndex.BAR;
        view.screen.model.zIndex = define.zIndex.MODEL;
        var _a = cc.view.getFrameSize(), width = _a.width, height = _a.height;
        var screenRatio = width / height;
        screenRatio > 2 ? game.data.deviceKind = define.type.Device.IPHONEX : screenRatio < 1.5 && (game.data.deviceKind = define.type.Device.IPAD);
      };
      UIController.prototype.gotoHomeScene = function() {
        if (!view.screen.home) {
          view.screen.home = cc.instantiate(this.homeScenePrefab).getComponent("HomeScene");
          view.screen.home.node.zIndex = define.zIndex.HOME;
          view.screen.home.node.parent = this.canvas;
        }
        view.bar.bottom.showHomeBar();
      };
      UIController.prototype.openGame = function(id, smid) {
        if (view.screen.game) {
          util.game.showNode(view.screen.game.node, define.zIndex.GAME);
          view.screen.game.node.position = cc.v2(0, 0);
        } else {
          view.screen.game = cc.instantiate(this.gameScenePrefab).getComponent("GameScene");
          view.screen.game.node.zIndex = define.zIndex.GAME;
          view.screen.game.node.parent = this.canvas;
        }
        this.openSlot(id);
        view.bar.bottom.showGameBar();
      };
      UIController.prototype.resumeGame = function() {
        if (!view.screen.game) {
          cc.warn("Please open game before open slot");
          return;
        }
        var gameView = view.screen.game;
        var slotNode = view.screen.slot.SlotNode;
        slotNode.parent = gameView.node;
        slotNode.active = true;
        slotNode.opacity = 0;
        slotNode.zIndex = 0;
        slotNode.runAction(cc.fadeIn(.5));
        view.multi.loadContent();
        view.bar.bottom.showGameBar();
      };
      UIController.prototype.minimizeGameScene = function() {
        var _this = this;
        if (!view.screen.game) {
          cc.warn("Please open game before open slot");
          return;
        }
        var gameView = view.screen.game;
        view.bar.bottom.showHomeBar();
        view.screen.slot.SlotNode.runAction(cc.sequence(cc.callFunc(function() {
          var multi = view.multi;
          gameView.node.runAction(cc.scaleTo(.2, .1));
          gameView.node.runAction(cc.moveTo(.3, cc.v2(multi.node.x, multi.node.y + multi.content.height)));
        }), cc.delayTime(.3), cc.callFunc(function() {
          util.game.hideNode(gameView.node);
          gameView.node.position = cc.v2(cc.winSize.width, 0);
          gameView.node.scale = 1;
        }), cc.delayTime(.01), cc.callFunc(function() {
          gameView.hideBg(.01);
        }), cc.delayTime(.01), cc.callFunc(function() {
          view.screen.slot.SlotController.hideContent();
          util.game.hideNode(view.screen.slot.SlotNode);
          view.screen.slot = null;
          view.multi.loadContent();
          _this.unlockBar();
        })));
      };
      UIController.prototype.openSlot = function(id) {
        return __awaiter(this, void 0, void 0, function() {
          var gameView, slotData;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!view.screen.game) {
                cc.warn("Please open game before open slot");
                return [ 2 ];
              }
              gameView = view.screen.game;
              slotData = view.slot[id];
              if (slotData) {
                this.resumeSlot(slotData.uuid, slotData.id);
                return [ 2 ];
              }
              controller.ui.showModel();
              return [ 4, Promise.all([ this.instantiateSlot(id) ]) ];

             case 1:
              _a.sent();
              gameView.loadBgSlot(id);
              util.game.showNode(gameView.node, define.zIndex.GAME);
              gameView.node.position = cc.v2(0, 0);
              gameView.bigBg.node.runAction(cc.sequence([ cc.moveBy(.5, cc.v2(0, gameView.bigBg.node.height * (1 === game.data.deviceKind ? 1.5 : 1.2))).easing(cc.easeSineOut()), cc.callFunc(function() {
                var slotController = view.screen.slot.SlotController;
                (0 == slotController.autoSpinTimes || !slotController.bonus && slotController.autoSpinTimes < 0) && view.bar.bottom.gameBar.updateSpinFrame(define.type.SpinButton.normal);
              }), cc.delayTime(.5), cc.callFunc(function() {
                return __awaiter(_this, void 0, void 0, function() {
                  return __generator(this, function(_a) {
                    view.screen.slot.SlotController.startSlot();
                    view.screen.slot.SlotNode.runAction(cc.fadeIn(.5));
                    view.multi.loadContent();
                    controller.ui.hideModel();
                    view.screen.slot.SlotController.showContent();
                    return [ 2 ];
                  });
                });
              }) ]));
              return [ 2 ];
            }
          });
        });
      };
      UIController.prototype.resumeSlot = function(uuid, id) {
        return __awaiter(this, void 0, void 0, function() {
          var gameView, statusGame;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!view.screen.game) {
                cc.warn("Please open game before open slot");
                return [ 2 ];
              }
              gameView = view.screen.game;
              util.game.hideNode(gameView.node);
              view.screen.slot = Object.values(view.slot).find(function(item) {
                return item && item.uuid == uuid;
              });
              statusGame = view.screen.slot.SlotController.statusGame;
              statusGame && view.bar.bottom.gameBar.updateStatus(statusGame);
              return [ 4, gameView.loadBgSlot(id) ];

             case 1:
              _a.sent();
              gameView.bigBg.node.runAction(cc.sequence([ cc.moveBy(.01, cc.v2(0, gameView.bigBg.node.height * (1 === game.data.deviceKind ? 1.5 : 1.2))).easing(cc.easeSineOut()), cc.callFunc(function() {
                view.bar.bottom.gameBar.updateInfo();
                _this.resumeGame();
                var multi = view.multi;
                util.game.showNode(gameView.node, define.zIndex.GAME);
                util.game.showNode(view.screen.slot.SlotNode);
                gameView.node.position = cc.v2(multi.node.x, multi.node.y + multi.content.height);
                gameView.node.scale = 0;
                gameView.node.runAction(cc.scaleTo(.1, 1));
                gameView.node.runAction(cc.moveTo(.1, cc.v2(0, 0)));
              }), cc.delayTime(.1), cc.callFunc(function() {
                view.screen.slot.SlotController.showContent();
              }) ]));
              return [ 2 ];
            }
          });
        });
      };
      UIController.prototype.switchSlot = function(uuid, id) {
        if (!view.screen.game) {
          cc.warn("Please open game before open slot");
          return;
        }
        var gameView = view.screen.game;
        var slotScene = Object.values(view.slot).find(function(item) {
          return item && item.uuid == uuid;
        });
        gameView.bigBg.node.active = true;
        gameView.bigBg.spriteFrame = slotScene.SlotController.resourceController.loadRes("bg-big");
        util.game.hideNode(view.screen.slot.SlotNode);
        view.screen.slot = slotScene;
        util.game.showNode(view.screen.slot.SlotNode);
        view.bar.bottom.gameBar.updateInfo();
        view.screen.game.node.position = cc.v2(0, 0);
        view.screen.slot.SlotNode.opacity = 0;
        view.screen.slot.SlotNode.runAction(cc.fadeIn(.5));
        view.screen.slot.SlotController.showContent();
        var statusGame = view.screen.slot.SlotController.statusGame;
        statusGame && view.bar.bottom.gameBar.updateStatus(statusGame);
      };
      UIController.prototype.deleteSlot = function() {
        if (!view.screen.game) {
          cc.warn("Please open game before open slot");
          return;
        }
        var gameView = view.screen.game;
        var id = view.screen.slot.id;
        view.slot[id] && (view.slot[id] = void 0);
        view.screen.slot.SlotController.animationController.stopAllAction();
        view.screen.slot.SlotNode.runAction(cc.sequence(cc.fadeOut(.5), cc.callFunc(function() {
          gameView.hideBg();
          view.bar.bottom.showHomeBar();
        }), cc.delayTime(.75), cc.callFunc(function() {
          util.game.hideNode(gameView.node);
          gameView.node.position = cc.v2(cc.winSize.width, 0);
          view.screen.slot.SlotNode.destroy();
          view.screen.slot = null;
          view.multi.loadContent();
        })));
      };
      UIController.prototype.instantiateSlot = function(id) {
        if (!view.screen.game) {
          cc.warn("Please open game before open slot");
          return;
        }
        var gameView = view.screen.game;
        return new Promise(function(res, rej) {
          var resPath = "5_Games/" + id + "_Slot" + id + "/Slot" + id;
          cc.loader.loadRes(resPath, function(err, prefab) {
            err && rej(err);
            if (null == prefab.name) return;
            var slotNode = cc.instantiate(prefab);
            var slotController = slotNode.getComponent("SlotController");
            var slotScene = {
              id: id,
              gameId: slotController.gameId,
              uuid: util.game.generateUUID(),
              name: "Slot " + id,
              SlotNode: slotNode,
              SlotController: slotController
            };
            slotNode.opacity = 0;
            slotNode.parent = gameView.center;
            view.slot[id] = slotScene;
            view.screen.slot = slotScene;
            slotScene.SlotController.getData().then(res).catch(rej);
          });
        });
      };
      UIController.prototype.hideBar = function() {
        view.bar.top.hide();
        view.bar.bottom.hide();
      };
      UIController.prototype.showBar = function() {
        view.bar.top.show();
        view.bar.bottom.show();
      };
      UIController.prototype.lockBar = function() {
        view.bar.top.lock();
        view.bar.bottom.lock();
      };
      UIController.prototype.unlockBar = function() {
        view.bar.top.unlock();
        view.bar.bottom.unlock();
      };
      UIController.prototype.showModel = function() {
        view.screen.model.active = true;
        view.screen.model.zIndex = define.zIndex.MODEL;
      };
      UIController.prototype.hideModel = function() {
        view.screen.model.active = false;
        view.screen.model.zIndex = 0;
      };
      UIController.prototype.showPopup = function(name, data) {
        var popup = view.popup[name];
        if (popup) popup.isOpen || popup.reOpen(data); else {
          var popData = this.popupData.find(function(o) {
            return o.name === name;
          });
          if (popData) {
            popup = cc.instantiate(popData.prefab).getComponent(popData.componentName);
            popup.popupName = name;
            popup.node.x = 0;
            popup.node.y = 0;
            popup.node.parent = this.canvas;
            popup.init(data);
          } else cc.warn("UIController", "showPopup", "Can't find popup name " + name);
        }
      };
      UIController.prototype.closePopup = function(name) {
        var popup = view.popup[name];
        popup ? popup.onClose() : cc.warn("UIController", "closePopup", "Can't find popup name " + name);
      };
      __decorate([ property(cc.Prefab) ], UIController.prototype, "homeScenePrefab", void 0);
      __decorate([ property(cc.Prefab) ], UIController.prototype, "gameScenePrefab", void 0);
      __decorate([ property({
        type: PopupData
      }) ], UIController.prototype, "popupData", void 0);
      UIController = __decorate([ ccclass ], UIController);
      return UIController;
    }(cc.Component);
    exports.default = UIController;
    cc._RF.pop();
  }, {} ],
  UserInfoController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0983ea9pZtEF4fD8mosgb6y", "UserInfoController");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CoinLabel_1 = require("../Components/CoinLabel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var UserInfoController = function(_super) {
      __extends(UserInfoController, _super);
      function UserInfoController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.coinLabel = null;
        _this.userLevel = null;
        _this.userName = null;
        _this.userClan = null;
        _this.coinController = null;
        _this._eventList = [];
        return _this;
      }
      UserInfoController.prototype.onLoad = function() {
        this.initialize();
        this.resgisterEvent();
      };
      UserInfoController.prototype.initialize = function() {
        this.coinController = new CoinLabel_1.default(this.coinLabel);
      };
      UserInfoController.prototype.resgisterEvent = function() {
        this._eventList = [ store.register(store.key.UPDATE_USER_BALANCE, this.updateUserBalance.bind(this)) ];
      };
      UserInfoController.prototype.updateUserBalance = function(amount, option) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            this.coinController.updateUserBalance(amount, option);
            return [ 2 ];
          });
        });
      };
      UserInfoController.prototype.onDestroy = function() {
        this._eventList.forEach(function(item) {
          return store.unRegister(item);
        });
      };
      __decorate([ property(cc.Label) ], UserInfoController.prototype, "coinLabel", void 0);
      __decorate([ property(cc.Label) ], UserInfoController.prototype, "userLevel", void 0);
      __decorate([ property(cc.Label) ], UserInfoController.prototype, "userName", void 0);
      __decorate([ property(cc.Label) ], UserInfoController.prototype, "userClan", void 0);
      UserInfoController = __decorate([ ccclass ], UserInfoController);
      return UserInfoController;
    }(cc.Component);
    exports.default = UserInfoController;
    cc._RF.pop();
  }, {
    "../Components/CoinLabel": "CoinLabel"
  } ],
  auth: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a2a548X5aVKa73+sUjHuuhj", "auth");
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AuthEngine = function() {
      function AuthEngine() {
        this._internalStorage = {};
        this.isLocalStorageEnabled = this._checkLocalStorageEnabled();
      }
      AuthEngine.prototype._checkLocalStorageEnabled = function() {
        var err;
        try {
          cc.sys.localStorage;
          cc.sys.localStorage.setItem("__scLocalStorageTest", 1);
          cc.sys.localStorage.removeItem("__scLocalStorageTest");
        } catch (e) {
          err = e;
        }
        return !err;
      };
      AuthEngine.prototype.saveToken = function(name, token, options, callback) {
        this.isLocalStorageEnabled && cc.sys.localStorage ? cc.sys.localStorage.setItem(name, token) : this._internalStorage[name] = token;
        callback && callback(null, token);
      };
      AuthEngine.prototype.removeToken = function(name, callback) {
        var token;
        this.loadToken(name, function(err, authToken) {
          token = authToken;
        });
        this.isLocalStorageEnabled && cc.sys.localStorage ? cc.sys.localStorage.removeItem(name) : delete this._internalStorage[name];
        callback && callback(null, token);
      };
      AuthEngine.prototype.loadToken = function(name, callback) {
        var token;
        token = this.isLocalStorageEnabled && cc.sys.localStorage ? cc.sys.localStorage.getItem(name) : this._internalStorage[name] || null;
        callback(null, token);
      };
      return AuthEngine;
    }();
    exports.AuthEngine = AuthEngine;
    cc._RF.pop();
  }, {} ]
}, {}, [ "AccumulatedBar1", "AnimationController1", "MinigameController1", "ResourceMinigame1", "ResourceController1", "SlotController1", "SlotItem1", "AccumulatedBar2", "AnimationController2", "MinigameController2", "ResourceMinigame2", "ResourceController2", "SlotController2", "SlotItem2", "AnimationController3", "ItemSkeleton", "MinigameController3", "ResourceMinigame3", "ResourceController3", "SlotController3", "SlotItem3", "AnimationController4", "AtributeStatic", "ResourceController4", "SlotController4", "SlotItem4", "MinigameController4", "ResourceMinigame4", "AnimationController5", "MinigameController5", "ResourceMiniGame5", "ResourceController5", "SlotController5", "SlotItem5", "AnimationController6", "MinigameController6", "ResourceMiniGame6", "ResourceController6", "SlotController6", "SlotItem6", "AnimationController7", "MinigameController7", "ResourceMiniGame7", "ResourceController7", "SlotController7", "SlotItem7", "AnimationController8", "MinigameController8", "ResourceMiniGame8", "ResourceController8", "SlotController8", "SlotItem8", "Api", "BottomBarController", "BottomGameBarController", "BottomHomeBarController", "MultiScene", "TopBarController", "BasePopup", "AccumulatedBar", "CoinLabel", "DefineColor", "DefineInterface", "DefineKey", "DefineType", "DefineZIndex", "DownloadCtr", "Effect", "GameConfig", "GameDefine", "GameScene", "HomeScene", "ItemGame", "UserInfoController", "Main", "AutoFitCanvas", "ImageScale", "MarginWithIphoneX", "AnimationController", "Item", "LinesController", "ResourceController", "ResourcePrefab", "SlotController", "SlotItem", "SlotSkeleton", "SlotsDownloadCtr", "SoundController", "Store", "UIController", "GameUtil", "StringUtil", "auth" ]);