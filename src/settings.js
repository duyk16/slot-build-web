window._CCSettings = {
    platform: "web-desktop",
    groupList: [
        "default",
        "popup"
    ],
    collisionMatrix: [
        [
            true
        ],
        [
            false,
            false
        ]
    ],
    rawAssets: {
        assets: {
            "51DoqXneZHrKy8ejYtU/bt": [
                "0_animation/Slots/LineAni.anim",
                "cc.AnimationClip"
            ],
            "179U4jdvVMHJU+NYYUFjtJ": [
                "0_animation/Slots/ZoomItem.anim",
                "cc.AnimationClip"
            ],
            "abb1fbmUxGXKLrIPn0s+80": [
                "0_textures/Slots/Linepay",
                "cc.SpriteFrame",
                1
            ],
            "81k/777jNLsIHEfAs6pynO": [
                "0_textures/Slots/Linepay.png",
                "cc.Texture2D"
            ],
            "50f1c1nwJK+JRWdbuMskZQ": [
                "0_textures/bg-noti",
                "cc.SpriteFrame",
                1
            ],
            "3cwLffwb5K/aH87+TLq+1k": [
                "0_textures/bg-noti.png",
                "cc.Texture2D"
            ],
            f38gzWUbZCIrJsjc0TrFDp: [
                "0_textures/overlay",
                "cc.SpriteFrame",
                1
            ],
            "46MDNNzGBC1ZktAuh11ppK": [
                "0_textures/overlay.png",
                "cc.Texture2D"
            ],
            "4eSvH7fwpOsJlCSuYnbCvU": [
                "1_gameui/bar/botbargame/slot-ingame-bet-down-active",
                "cc.SpriteFrame",
                1
            ],
            "51/XILsltLDI+KXgHNYcmi": [
                "1_gameui/bar/botbargame/slot-ingame-bet-down-active.png",
                "cc.Texture2D"
            ],
            "1523BBo3tOy6AjgdvD7AsQ": [
                "1_gameui/bar/botbargame/slot-ingame-bet-down-normal",
                "cc.SpriteFrame",
                1
            ],
            bc5mmpNwxOh5MJEKrb1fBg: [
                "1_gameui/bar/botbargame/slot-ingame-bet-down-normal.png",
                "cc.Texture2D"
            ],
            "74Ig4rlThA4q4SIaRgLvIb": [
                "1_gameui/bar/botbargame/slot-ingame-bet-max-bet-active",
                "cc.SpriteFrame",
                1
            ],
            "9cutVPmHBEtJe5zw0Uw2Xf": [
                "1_gameui/bar/botbargame/slot-ingame-bet-max-bet-active.png",
                "cc.Texture2D"
            ],
            f2V8wZFLxGJKMCsYOQO3XB: [
                "1_gameui/bar/botbargame/slot-ingame-bet-max-bet-normal",
                "cc.SpriteFrame",
                1
            ],
            "e1bGJbWKZKxr/PHVGhxMWt": [
                "1_gameui/bar/botbargame/slot-ingame-bet-max-bet-normal.png",
                "cc.Texture2D"
            ],
            "3eI+JZpBxCaax+2GX1adCg": [
                "1_gameui/bar/botbargame/slot-ingame-bet-up-active",
                "cc.SpriteFrame",
                1
            ],
            "98zTHSLlFOdbR0gTZfilg1": [
                "1_gameui/bar/botbargame/slot-ingame-bet-up-active.png",
                "cc.Texture2D"
            ],
            "06E7ssTAhKsr2u76Q4paM9": [
                "1_gameui/bar/botbargame/slot-ingame-bet-up-normal",
                "cc.SpriteFrame",
                1
            ],
            "d64VLoML5Cs7btSMVj8Q2/": [
                "1_gameui/bar/botbargame/slot-ingame-bet-up-normal.png",
                "cc.Texture2D"
            ],
            "d1FQ+vqZJM8bUdEcoBOT5e": [
                "1_gameui/bar/botbargame/slot-ingame-bg-bet",
                "cc.SpriteFrame",
                1
            ],
            "29vMvznXBJAbHjH7zFstFp": [
                "1_gameui/bar/botbargame/slot-ingame-bg-bet.png",
                "cc.Texture2D"
            ],
            "66AtMglxFM1osOZ+5FQTSf": [
                "1_gameui/bar/botbargame/slot-ingame-bg-bot-back",
                "cc.SpriteFrame",
                1
            ],
            "0eJmpCHeVLpLSEYOu9GD/W": [
                "1_gameui/bar/botbargame/slot-ingame-bg-bot-back.png",
                "cc.Texture2D"
            ],
            a4Vidk9SdLHL9QL0gEHkre: [
                "1_gameui/bar/botbargame/slot-ingame-bg-bot-front",
                "cc.SpriteFrame",
                1
            ],
            "962HOFhZVLaYLNe9khtgzC": [
                "1_gameui/bar/botbargame/slot-ingame-bg-bot-front.png",
                "cc.Texture2D"
            ],
            "42WNugKHFAoK7K+5EVisjS": [
                "1_gameui/bar/botbargame/slot-ingame-bg-line-quest",
                "cc.SpriteFrame",
                1
            ],
            "1dmfH47w5PJ4oeFmOJtEGK": [
                "1_gameui/bar/botbargame/slot-ingame-bg-line-quest.png",
                "cc.Texture2D"
            ],
            d4tX0DlVlF4KyuhzDU9ZmA: [
                "1_gameui/bar/botbargame/slot-ingame-bg-spin-stop",
                "cc.SpriteFrame",
                1
            ],
            "dawFXXvCxGE5OjEy/ybAJl": [
                "1_gameui/bar/botbargame/slot-ingame-bg-spin-stop.png",
                "cc.Texture2D"
            ],
            "eeD4YcIWxOta+8p4LOn2bX": [
                "1_gameui/bar/botbargame/slot-ingame-btn-gift",
                "cc.SpriteFrame",
                1
            ],
            "fe2Gb4JLRMO7F+KmkgFcIA": [
                "1_gameui/bar/botbargame/slot-ingame-btn-gift.png",
                "cc.Texture2D"
            ],
            "48AQN1Cc9ODbMJuc7fu/lp": [
                "1_gameui/bar/botbargame/slot-ingame-btn-spin",
                "cc.SpriteFrame",
                1
            ],
            bbHAtyT9FBs6nGVns5nESH: [
                "1_gameui/bar/botbargame/slot-ingame-btn-spin.png",
                "cc.Texture2D"
            ],
            "3eSr+J3dxE3rW9G9pU8lCQ": [
                "1_gameui/bar/botbargame/slot-ingame-btn-stop-spin",
                "cc.SpriteFrame",
                1
            ],
            "13aeHwhSJBFrP2P/nJvTIt": [
                "1_gameui/bar/botbargame/slot-ingame-btn-stop-spin.png",
                "cc.Texture2D"
            ],
            "67GdevoqBOOZSDWhJO1rgM": [
                "1_gameui/bar/botbargame/slot-ingame-line-quest",
                "cc.SpriteFrame",
                1
            ],
            "80krSyDNdEOJjaNY/GIyWz": [
                "1_gameui/bar/botbargame/slot-ingame-line-quest.png",
                "cc.Texture2D"
            ],
            "4awwR1ETpDb6dUIaNR599t": [
                "1_gameui/bar/botbarhome/slot-bg-bonnus-bottom",
                "cc.SpriteFrame",
                1
            ],
            "22rA9j9M5Ej4iZniNc/6B4": [
                "1_gameui/bar/botbarhome/slot-bg-bonnus-bottom.png",
                "cc.Texture2D"
            ],
            "010i+2VeJNLqBC4rgFc4LP": [
                "1_gameui/bar/botbarhome/slot-bg-bot-1",
                "cc.SpriteFrame",
                1
            ],
            "99XILorW1FxLIvZfHJpYIY": [
                "1_gameui/bar/botbarhome/slot-bg-bot-1.png",
                "cc.Texture2D"
            ],
            "0cYAVLV3hLOrnqdVC6m+94": [
                "1_gameui/bar/botbarhome/slot-bg-bot-2",
                "cc.SpriteFrame",
                1
            ],
            "9080S2Mm1Dso4gSoj8dlUQ": [
                "1_gameui/bar/botbarhome/slot-bg-bot-2.png",
                "cc.Texture2D"
            ],
            "b7Z0ca/OhEn5hvoY9QR9+O": [
                "1_gameui/bar/botbarhome/slot-bg-wheel",
                "cc.SpriteFrame",
                1
            ],
            eb2vewiCtGRbiXKwMcNLaV: [
                "1_gameui/bar/botbarhome/slot-bg-wheel.png",
                "cc.Texture2D"
            ],
            "53LespqTxK0rQNvqmmK5AZ": [
                "1_gameui/bar/botbarhome/slot-icon-btn-daily-gilf",
                "cc.SpriteFrame",
                1
            ],
            "43w7FVWOlKFJAIw6LQSc0C": [
                "1_gameui/bar/botbarhome/slot-icon-btn-daily-gilf.png",
                "cc.Texture2D"
            ],
            "91q3bs2NtCdpkNFObUtrcm": [
                "1_gameui/bar/botbarhome/slot-icon-btn-daily-quest",
                "cc.SpriteFrame",
                1
            ],
            "a5myBSC2ZP/rJGDNHCImrx": [
                "1_gameui/bar/botbarhome/slot-icon-btn-daily-quest.png",
                "cc.Texture2D"
            ],
            "adSFGqN7hHdbAW0r7g/4Ie": [
                "1_gameui/bar/topbar/particle_texture.plist",
                "cc.ParticleAsset"
            ],
            "69dO8fjOhCHoRVNIB/mePg": [
                "1_gameui/bar/topbar/slot-bg-btn-minimize",
                "cc.SpriteFrame",
                1
            ],
            a3CbHeUdVGh4m5QCDQ0Rit: [
                "1_gameui/bar/topbar/slot-bg-btn-minimize.png",
                "cc.Texture2D"
            ],
            "27AqCbJYtIML5b7bFAUduM": [
                "1_gameui/bar/topbar/slot-bg-buy-deal",
                "cc.SpriteFrame",
                1
            ],
            "36bGgMgT5PU7u1fZ/gMAUB": [
                "1_gameui/bar/topbar/slot-bg-buy-deal.png",
                "cc.Texture2D"
            ],
            "30fjdZnQtDrpRQa12XCcqi": [
                "1_gameui/bar/topbar/slot-bg-coin",
                "cc.SpriteFrame",
                1
            ],
            "40rAsZJ/JHh4LZxJl+SuCI": [
                "1_gameui/bar/topbar/slot-bg-coin.png",
                "cc.Texture2D"
            ],
            d7IfRsBdtHqJSy1Qh8WobL: [
                "1_gameui/bar/topbar/slot-bg-level-exp",
                "cc.SpriteFrame",
                1
            ],
            e4NZbOt9tD260Y4vqHrHVS: [
                "1_gameui/bar/topbar/slot-bg-level-exp.png",
                "cc.Texture2D"
            ],
            "38K88QR3VNKqBYD2BJBCKE": [
                "1_gameui/bar/topbar/slot-bg-level-up-exp",
                "cc.SpriteFrame",
                1
            ],
            "0fRQyckVZNiIgkSpYOsPpv": [
                "1_gameui/bar/topbar/slot-bg-level-up-exp.png",
                "cc.Texture2D"
            ],
            ebUIK6KHRPe6fmjmVwOix4: [
                "1_gameui/bar/topbar/slot-bg-show-popup-minimize",
                "cc.SpriteFrame",
                1
            ],
            e4PO0jFG5FSbsoodCteVIa: [
                "1_gameui/bar/topbar/slot-bg-show-popup-minimize.png",
                "cc.Texture2D"
            ],
            "99XNeeNdhIEaPTeChBJavl": [
                "1_gameui/bar/topbar/slot-bg-top-bar",
                "cc.SpriteFrame",
                1
            ],
            "31wWwgtBBMKq9lQFKNBk+X": [
                "1_gameui/bar/topbar/slot-bg-top-bar.png",
                "cc.Texture2D"
            ],
            "51rNQNfJBNVLyPzmMuYC9T": [
                "1_gameui/bar/topbar/slot-btn-buy",
                "cc.SpriteFrame",
                1
            ],
            e6m8VCgGpKPKjn1XjTe5iu: [
                "1_gameui/bar/topbar/slot-btn-buy.png",
                "cc.Texture2D"
            ],
            "9a7ULqD31BC59rUl49AWHW": [
                "1_gameui/bar/topbar/slot-btn-deal",
                "cc.SpriteFrame",
                1
            ],
            f9vTBXQihMU462iOVmOdz2: [
                "1_gameui/bar/topbar/slot-btn-deal.png",
                "cc.Texture2D"
            ],
            "c0GS2dpq1GOr+qeHk88bX+": [
                "1_gameui/bar/topbar/slot-btn-menu-setting",
                "cc.SpriteFrame",
                1
            ],
            "8d6faQNUhGObaLdudeXuPp": [
                "1_gameui/bar/topbar/slot-btn-menu-setting.png",
                "cc.Texture2D"
            ],
            "a9p9D+J3FCGLBKAY+LE+SQ": [
                "1_gameui/bar/topbar/slot-icon-coin",
                "cc.SpriteFrame",
                1
            ],
            "7bX3UYNiJGergCPlIUfqp8": [
                "1_gameui/bar/topbar/slot-icon-coin.png",
                "cc.Texture2D"
            ],
            "6cpy8Sop9FHrE96DvwYRBJ": [
                "1_gameui/bar/topbar/slot-icon-game-minizime-1",
                "cc.SpriteFrame",
                1
            ],
            "acKsqCo4NKgqqA3ClZ+yAx": [
                "1_gameui/bar/topbar/slot-icon-game-minizime-1.png",
                "cc.Texture2D"
            ],
            "4c1ZIqrJtAoq3pvUZW7qW4": [
                "1_gameui/bar/topbar/slot-icon-game-minizime-2",
                "cc.SpriteFrame",
                1
            ],
            "e7r5ZMmSpAEbEF/tu/roc4": [
                "1_gameui/bar/topbar/slot-icon-game-minizime-2.png",
                "cc.Texture2D"
            ],
            "95N7Eu9RxFmJWs4GMIQ1cW": [
                "1_gameui/bar/topbar/slot-icon-game-minizime-3",
                "cc.SpriteFrame",
                1
            ],
            "75zjIh6U5Eb6duH10A/mKL": [
                "1_gameui/bar/topbar/slot-icon-game-minizime-3.png",
                "cc.Texture2D"
            ],
            e59i58K6xDrZDZAg1aIaa7: [
                "1_gameui/bar/topbar/slot-icon-game-minizime-4",
                "cc.SpriteFrame",
                1
            ],
            "c2qgVe+QRFqqTAfcTHv/6v": [
                "1_gameui/bar/topbar/slot-icon-game-minizime-4.png",
                "cc.Texture2D"
            ],
            "36BdEYFttAFIwneeEBRiu7": [
                "1_gameui/bar/topbar/slot-icon-star-level",
                "cc.SpriteFrame",
                1
            ],
            "99wsjufQtHy5D52aEPZY+L": [
                "1_gameui/bar/topbar/slot-icon-star-level.png",
                "cc.Texture2D"
            ],
            "29Y5ia2TxEipapWTsqwjwn": [
                "1_gameui/bar/topbar/slot-img-avatar-demo",
                "cc.SpriteFrame",
                1
            ],
            "3aBymgHuBJLqnGlaAV1eMr": [
                "1_gameui/bar/topbar/slot-img-avatar-demo.png",
                "cc.Texture2D"
            ],
            "86D3mOsrJJHbfuA147slVM": [
                "1_gameui/bar/topbar/slot-img-bg-avatar-demo",
                "cc.SpriteFrame",
                1
            ],
            "6b8ZLwaqVMg5RFBNvP03ot": [
                "1_gameui/bar/topbar/slot-img-bg-avatar-demo.png",
                "cc.Texture2D"
            ],
            b3Pzs7eFNI95y7mDr3PBfk: [
                "1_gameui/bar/topbar/slot-line-center-btn-buy-deal",
                "cc.SpriteFrame",
                1
            ],
            "dc2JBAc+NHn4rH/tz3NByJ": [
                "1_gameui/bar/topbar/slot-line-center-btn-buy-deal.png",
                "cc.Texture2D"
            ],
            e47A0XGwdMNKIe18fAYZIu: [
                "1_gameui/bar/topbar/slot-line-level-exp",
                "cc.SpriteFrame",
                1
            ],
            "30wUtKkb9KcZJfpTj2zz3I": [
                "1_gameui/bar/topbar/slot-line-level-exp.png",
                "cc.Texture2D"
            ],
            "13pI23QWtHzJ1OTBglys+D": [
                "1_gameui/particle/bubble_particle",
                "cc.SpriteFrame",
                1
            ],
            "8b68L2P7tFUrHGXnCkSwwF": [
                "1_gameui/particle/bubble_particle.plist",
                "cc.ParticleAsset"
            ],
            "139ajMHxxOIKbI2zfCZr8Q": [
                "1_gameui/particle/bubble_particle.png",
                "cc.Texture2D"
            ],
            fabF8KyWJFiLafRYNzB6oH: [
                "2_test/Biking_Girl_Alpha.mp4",
                "cc.Asset"
            ],
            "74FGByhUBOuaw1+TJSQ2Wp": [
                "2_test/LB_0019_Layer-1",
                "cc.SpriteFrame",
                1
            ],
            fbfyDexoBC9KCkIPyNxBMW: [
                "2_test/LB_0019_Layer-1.png",
                "cc.Texture2D"
            ],
            "88rDyjjXRNNKCzDXJuxyHC": [
                "2_test/NewProject_2/NewProject_2_ske.json",
                "dragonBones.DragonBonesAsset"
            ],
            "06749II2NLrIQTNqsLyagx": [
                "2_test/NewProject_2/NewProject_2_tex",
                "cc.SpriteFrame",
                1
            ],
            "34SJOO811KupIIHtj/wxvl": [
                "2_test/NewProject_2/NewProject_2_tex.json",
                "dragonBones.DragonBonesAtlasAsset"
            ],
            "62ahVYKp1AvozTQy7cXMMf": [
                "2_test/NewProject_2/NewProject_2_tex.png",
                "cc.Texture2D"
            ],
            "59KisoI8FEKL1cgpe4FxnL": [
                "2_test/bg-big",
                "cc.SpriteFrame",
                1
            ],
            d3d9SzvctAFI92RsmojcDl: [
                "2_test/bg-big.png",
                "cc.Texture2D"
            ],
            "56lsffGS9NzKOzoyS9a0u+": [
                "2_test/bg-blue",
                "cc.SpriteFrame",
                1
            ],
            cbCSnciEREkJ5QaOp9bwhu: [
                "2_test/bg-blue.png",
                "cc.Texture2D"
            ],
            "34QclaaRJBlJsF5eIjsHK9": [
                "2_test/bg-logo-top",
                "cc.SpriteFrame",
                1
            ],
            b9GSobCydAoKkZtcueQ7gO: [
                "2_test/bg-logo-top.png",
                "cc.Texture2D"
            ],
            fcw77x0qJBXokNYDwkKRM5: [
                "2_test/free-spin-background",
                "cc.SpriteFrame",
                1
            ],
            "a6+coWhRxP8Znn2bxVihrH": [
                "2_test/free-spin-background.png",
                "cc.Texture2D"
            ],
            "f41n0/1dxFF7gO22KgTinn": [
                "2_test/pick.mp3",
                "cc.AudioClip"
            ],
            "a8H9a+N+tLvKpkPP8G0728": [
                "2_test/slot-6",
                "cc.SpriteFrame",
                1
            ],
            "7cdEf41VhNt53UU2qKc/aS": [
                "2_test/slot-6.jpg",
                "cc.Texture2D"
            ],
            "69B0n88idJIKexNXpTytxJ": [
                "3_homeScene/AutoAtlas.pac",
                "cc.SpriteAtlas"
            ],
            "c2bMG0Ot5Mop/4f4Pc2eyZ": [
                "3_homeScene/avatar-demo",
                "cc.SpriteFrame",
                1
            ],
            "92OCu0ialLV6uvzpaOhOBl": [
                "3_homeScene/avatar-top-demo-1",
                "cc.SpriteFrame",
                1
            ],
            "42BzJyY75GP6jz60dZnVkZ": [
                "3_homeScene/avatar-top-demo-2",
                "cc.SpriteFrame",
                1
            ],
            c1mPwM4kJF2YCV8qXKDiVi: [
                "3_homeScene/avatar-top-demo-3",
                "cc.SpriteFrame",
                1
            ],
            "05wzE2x7lLpoc+x24qDc+M": [
                "3_homeScene/avatar-top-demo-4",
                "cc.SpriteFrame",
                1
            ],
            dbpBbPQrFDLpXIn2yPcCMN: [
                "3_homeScene/avatar-top-demo-5",
                "cc.SpriteFrame",
                1
            ],
            "26okrYRW9PAbP7KmhmKYj7": [
                "3_homeScene/avatar-top-demo-6",
                "cc.SpriteFrame",
                1
            ],
            "76Hrs3LCdGUKI/L38myT1N": [
                "3_homeScene/avatar-top-demo-7",
                "cc.SpriteFrame",
                1
            ],
            "85VYVNEXhHGobNYi5z7NIR": [
                "3_homeScene/avatar-top-demo-8",
                "cc.SpriteFrame",
                1
            ],
            "831c1UqxpACb6oO07YIwcd": [
                "3_homeScene/bg",
                "cc.SpriteFrame",
                1
            ],
            "61YxvObDdD540zeO4ag6XR": [
                "3_homeScene/bg-icon-banner",
                "cc.SpriteFrame",
                1
            ],
            "b0+gScl99C7qEyMsay0h8F": [
                "3_homeScene/bg-noti-menu",
                "cc.SpriteFrame",
                1
            ],
            caPNqks25KgoxFqaXm9d7v: [
                "3_homeScene/bg.png",
                "cc.Texture2D"
            ],
            "8bplBnhcRDZbSZ4hOiheNg": [
                "3_homeScene/slot-bg-item-game",
                "cc.SpriteFrame",
                1
            ],
            "163ciWemNOkqZ/iPB1WcYl": [
                "3_homeScene/slot-btn-menu-left",
                "cc.SpriteFrame",
                1
            ],
            "7bydhlBlVJ2ICXKxWb6Spc": [
                "3_homeScene/slot-img-banner-demo",
                "cc.SpriteFrame",
                1
            ],
            b7hxjzixJCVIpF0zJTi7K2: [
                "3_homeScene/slot-img-demo-item-game",
                "cc.SpriteFrame",
                1
            ],
            "429SWZnyhPa51bYeKJzasc": [
                "3_homeScene/slotIcon/1",
                "cc.SpriteFrame",
                1
            ],
            a18Jj0cyJK17LF2pgR6vUl: [
                "3_homeScene/slotIcon/2",
                "cc.SpriteFrame",
                1
            ],
            c6G7Bnwc9BiI2TAa7iZ5Xk: [
                "3_homeScene/slotIcon/3",
                "cc.SpriteFrame",
                1
            ],
            "5czFmgVgBEa7XeKHjm+Skz": [
                "3_homeScene/slotIcon/4",
                "cc.SpriteFrame",
                1
            ],
            "9fLlU2MoNFA7UD5M2Wfz11": [
                "3_homeScene/slotIcon/5",
                "cc.SpriteFrame",
                1
            ],
            "a0wREYAb1GFLd5+2MsgC84": [
                "3_homeScene/slotIcon/6",
                "cc.SpriteFrame",
                1
            ],
            "8d/o6qTTFLpKYfzc7jxnJK": [
                "3_homeScene/slotIcon/7",
                "cc.SpriteFrame",
                1
            ],
            bc78wiLiVChbl88NMlNm95: [
                "3_homeScene/slotIcon/8",
                "cc.SpriteFrame",
                1
            ],
            a2kiz2G2lNroffMNw9uwdx: [
                "3_homeScene/trans-item-game-slot",
                "cc.SpriteFrame",
                1
            ],
            e2EjgpN7VMTrgJppp4UN7t: [
                "4_gameScene/0_Default/0/Demo-slot-egypt",
                "cc.SpriteFrame",
                1
            ],
            "90vBxT2CRMvrcoJtVclXd9": [
                "4_gameScene/0_Default/0/Demo-slot-egypt.png",
                "cc.Texture2D"
            ],
            "7fjr+nJOlGcr4+EWmyFusO": [
                "4_gameScene/0_Default/0/avatar-ingame",
                "cc.SpriteFrame",
                1
            ],
            "73HOyAsl5CPJnuM8ql8e/N": [
                "4_gameScene/0_Default/0/avatar-ingame.png",
                "cc.Texture2D"
            ],
            "8dFtPViaZASrFP2LDvpzvq": [
                "4_gameScene/0_Default/0/bg-bet-score-ingame",
                "cc.SpriteFrame",
                1
            ],
            "21ezNVZwlDCLwpJmx1rrzi": [
                "4_gameScene/0_Default/0/bg-bet-score-ingame.png",
                "cc.Texture2D"
            ],
            "f2I/DDFgBE75gpDgsTlK2R": [
                "4_gameScene/0_Default/0/bg-coin-deal",
                "cc.SpriteFrame",
                1
            ],
            "65EtOIoFVL4bL/XOVRFb3f": [
                "4_gameScene/0_Default/0/bg-coin-deal.png",
                "cc.Texture2D"
            ],
            "e1SU/fb4BBGJWJQ7dHJxuH": [
                "4_gameScene/0_Default/0/bg-exp-ingame",
                "cc.SpriteFrame",
                1
            ],
            "adHZS2SglPT6cOGT7UAw+4": [
                "4_gameScene/0_Default/0/bg-exp-ingame.png",
                "cc.Texture2D"
            ],
            "42B/EdA/9Ggrn8pZGYU4u7": [
                "4_gameScene/0_Default/0/bg-ingame-bot",
                "cc.SpriteFrame",
                1
            ],
            adJljPhZxPbbUctUhF0zSD: [
                "4_gameScene/0_Default/0/bg-ingame-bot.png",
                "cc.Texture2D"
            ],
            b4r2TYgT1EypJ3KJ91Tt2f: [
                "4_gameScene/0_Default/0/bg-ingame-top",
                "cc.SpriteFrame",
                1
            ],
            fbtVpapbZLOICIiotj7yYt: [
                "4_gameScene/0_Default/0/bg-ingame-top.png",
                "cc.Texture2D"
            ],
            "b9f/OIYZRCSLqIS8tIm3Nj": [
                "4_gameScene/0_Default/0/bg-line-black-exp-ingame",
                "cc.SpriteFrame",
                1
            ],
            "d0EQ6+x+lCYrupjIfuVch3": [
                "4_gameScene/0_Default/0/bg-line-black-exp-ingame.png",
                "cc.Texture2D"
            ],
            "2axtS06KxJuLnuOiiZOk73": [
                "4_gameScene/0_Default/0/bg-score-coin",
                "cc.SpriteFrame",
                1
            ],
            "5d9jfiwcRJQ7hTC6NoOLNZ": [
                "4_gameScene/0_Default/0/bg-score-coin.png",
                "cc.Texture2D"
            ],
            "40x8D/9CRBuZ7rMvvaYtud": [
                "4_gameScene/0_Default/0/bg-score-money",
                "cc.SpriteFrame",
                1
            ],
            "58FmBKOnBBv41Kbt9v48Uj": [
                "4_gameScene/0_Default/0/bg-score-money.png",
                "cc.Texture2D"
            ],
            "71meXvlT5KJKVQgvwWpKBO": [
                "4_gameScene/0_Default/0/bg-select-free-spin",
                "cc.SpriteFrame",
                1
            ],
            cd44h3qFdE9bzGtDdOAQQG: [
                "4_gameScene/0_Default/0/bg-select-free-spin.png",
                "cc.Texture2D"
            ],
            "f4fYupl2tEI6xcyo+3n8zI": [
                "4_gameScene/0_Default/0/btn-avatar-ingame",
                "cc.SpriteFrame",
                1
            ],
            "12uraOYolPyoYsYg5SajC/": [
                "4_gameScene/0_Default/0/btn-avatar-ingame.png",
                "cc.Texture2D"
            ],
            d4dX0GOZVEYKBMi92G5xMn: [
                "4_gameScene/0_Default/0/btn-back-ingame",
                "cc.SpriteFrame",
                1
            ],
            "9bv3ONbMhAdba2ui24gBFo": [
                "4_gameScene/0_Default/0/btn-back-ingame.png",
                "cc.Texture2D"
            ],
            "8emcRsKwVOMZWco0GYhadm": [
                "4_gameScene/0_Default/0/btn-bet-down",
                "cc.SpriteFrame",
                1
            ],
            "518or4kNRFrJWMz/ccgXbh": [
                "4_gameScene/0_Default/0/btn-bet-down.png",
                "cc.Texture2D"
            ],
            "60Tc7ANmZAbbTNMkMOLZ57": [
                "4_gameScene/0_Default/0/btn-bet-up",
                "cc.SpriteFrame",
                1
            ],
            "a8YiZzpKxM+r2szBANdYbW": [
                "4_gameScene/0_Default/0/btn-bet-up.png",
                "cc.Texture2D"
            ],
            "34w3aoyuxPN5+LVve+08+J": [
                "4_gameScene/0_Default/0/btn-buy-ingame",
                "cc.SpriteFrame",
                1
            ],
            "63isKmft1HhKo5dkwd4JZy": [
                "4_gameScene/0_Default/0/btn-buy-ingame.png",
                "cc.Texture2D"
            ],
            "58b46ZxF1MqqrIKcw4OaYn": [
                "4_gameScene/0_Default/0/btn-chat-ingame",
                "cc.SpriteFrame",
                1
            ],
            "94KG+IW6FPJYqI1l39j1Xv": [
                "4_gameScene/0_Default/0/btn-chat-ingame.png",
                "cc.Texture2D"
            ],
            "72T5bu1E5H3JcM7Shmqeh9": [
                "4_gameScene/0_Default/0/btn-deal-ingame",
                "cc.SpriteFrame",
                1
            ],
            c3G2mIaW1HnaLFeoRilV6J: [
                "4_gameScene/0_Default/0/btn-deal-ingame.png",
                "cc.Texture2D"
            ],
            "f39wFmgbNBEomIVstAuU+f": [
                "4_gameScene/0_Default/0/btn-help",
                "cc.SpriteFrame",
                1
            ],
            "0dC2BgMixJiZ+dojBvyck3": [
                "4_gameScene/0_Default/0/btn-help.png",
                "cc.Texture2D"
            ],
            "d82s+i0oFB2JsjjC8J7Suz": [
                "4_gameScene/0_Default/0/btn-maxbet",
                "cc.SpriteFrame",
                1
            ],
            f60x2DBT9JKYW4IqPo6bmW: [
                "4_gameScene/0_Default/0/btn-maxbet.png",
                "cc.Texture2D"
            ],
            "48/jdRtmhBBJfU7mhBm0x8": [
                "4_gameScene/0_Default/0/btn-setting-ingame",
                "cc.SpriteFrame",
                1
            ],
            "24+0SS8ORMZr1CCaMMEeLb": [
                "4_gameScene/0_Default/0/btn-setting-ingame.png",
                "cc.Texture2D"
            ],
            "37kJc9CQhIpY4Nulw5fykp": [
                "4_gameScene/0_Default/0/btn-spin",
                "cc.SpriteFrame",
                1
            ],
            "53qAh3fYJFjKQHTm0picXK": [
                "4_gameScene/0_Default/0/btn-spin.png",
                "cc.Texture2D"
            ],
            "ecDNKkdOJGd5lJ+xLyLjlI": [
                "4_gameScene/0_Default/0/btn-treasure",
                "cc.SpriteFrame",
                1
            ],
            "e4cpgk8o1Kl7KKX53NAns/": [
                "4_gameScene/0_Default/0/btn-treasure.png",
                "cc.Texture2D"
            ],
            "d4cjdU0ntLc7+phlBDRNJ0": [
                "4_gameScene/0_Default/0/coin-money-ingame",
                "cc.SpriteFrame",
                1
            ],
            "7eMllcwY1KEbk24RHnZtn9": [
                "4_gameScene/0_Default/0/coin-money-ingame.png",
                "cc.Texture2D"
            ],
            "65iVcVPx1LtKE+QzUpD16H": [
                "4_gameScene/0_Default/0/icon-rank-avatar",
                "cc.SpriteFrame",
                1
            ],
            "585P8ZvDJPUrM6MXTgqBP5": [
                "4_gameScene/0_Default/0/icon-rank-avatar.png",
                "cc.Texture2D"
            ],
            "bfQkCWxhNJ34d9X/M/EuRo": [
                "4_gameScene/0_Default/0/line-green-exp-ingame",
                "cc.SpriteFrame",
                1
            ],
            "20CCcyb7pO7K+SOHoX7QYQ": [
                "4_gameScene/0_Default/0/line-green-exp-ingame.png",
                "cc.Texture2D"
            ],
            bcrBicFttGmLj5jbg43boe: [
                "4_gameScene/0_Default/0/slot-btn-100spin",
                "cc.SpriteFrame",
                1
            ],
            "27fnkWrANNW5p4wTC8ceBW": [
                "4_gameScene/0_Default/0/slot-btn-100spin.png",
                "cc.Texture2D"
            ],
            b0w5fmdqZG7YBBwwBXdS2H: [
                "4_gameScene/0_Default/0/slot-btn-200spin",
                "cc.SpriteFrame",
                1
            ],
            "4d6UIW8VBMw7qrCUHDrC7w": [
                "4_gameScene/0_Default/0/slot-btn-200spin.png",
                "cc.Texture2D"
            ],
            "2cD+iXLkdGhof+KxG74Vhx": [
                "4_gameScene/0_Default/0/slot-btn-25spin",
                "cc.SpriteFrame",
                1
            ],
            "50cvkwlzVPEbDV+1sv8rbC": [
                "4_gameScene/0_Default/0/slot-btn-25spin.png",
                "cc.Texture2D"
            ],
            "33mcNug8tOlZ6OH8RPiNYX": [
                "4_gameScene/0_Default/0/slot-btn-50spin",
                "cc.SpriteFrame",
                1
            ],
            a45Ptgcf9O7JyB7xst0ZzU: [
                "4_gameScene/0_Default/0/slot-btn-50spin.png",
                "cc.Texture2D"
            ],
            "41YaTxk6FBT7NCdPnXks5y": [
                "4_gameScene/0_Default/0/slot-btn-minimize",
                "cc.SpriteFrame",
                1
            ],
            "55+srNvCFJR6F4mz1Rcpk7": [
                "4_gameScene/0_Default/0/slot-btn-minimize.png",
                "cc.Texture2D"
            ],
            "0f+wJP+JFBULkLYBymLVK7": [
                "4_gameScene/0_Default/0/slot-btn-spin-stop-1",
                "cc.SpriteFrame",
                1
            ],
            "41TeJbgXRDhaKHzUuaVLsD": [
                "4_gameScene/0_Default/0/slot-btn-spin-stop-1.png",
                "cc.Texture2D"
            ],
            "b6/1gA5uRKaqcKRlTO6q0c": [
                "4_gameScene/0_Default/0/slot-btn-spin-stop-2",
                "cc.SpriteFrame",
                1
            ],
            "25ulgrrGdAEJgDAJcuDAxY": [
                "4_gameScene/0_Default/0/slot-btn-spin-stop-2.png",
                "cc.Texture2D"
            ],
            acCVK2A3lL9ogy1T41eovR: [
                "4_gameScene/0_Default/0/slot-btn-ulti-bonus",
                "cc.SpriteFrame",
                1
            ],
            "81kEpCQClJcJYjDvndV8oH": [
                "4_gameScene/0_Default/0/slot-btn-ulti-bonus.png",
                "cc.Texture2D"
            ],
            "a9HT2hGqRCPLT1KXId+yWD": [
                "4_gameScene/0_Default/0/star-exp-ingame",
                "cc.SpriteFrame",
                1
            ],
            "7f2aKV8dNGm5aCS5soIhYd": [
                "4_gameScene/0_Default/0/star-exp-ingame.png",
                "cc.Texture2D"
            ],
            b8KErSdcNHU7XW5BHiLRS0: [
                "4_gameScene/0_Default/1/bg-screen-slot",
                "cc.SpriteFrame",
                1
            ],
            "1cdkkRlDZFo6XTHJ0gaBop": [
                "4_gameScene/0_Default/1/bg-screen-slot.png",
                "cc.Texture2D"
            ],
            c6MtH1ixNDz7vbyMG6fdnW: [
                "4_gameScene/0_Default/1/btn-screen-arow-hide",
                "cc.SpriteFrame",
                1
            ],
            "a2CWdR/+tBILr+gG6Dtx+7": [
                "4_gameScene/0_Default/1/btn-screen-arow-hide.png",
                "cc.Texture2D"
            ],
            "ffbYg4NMZDQJxOIyWmU6f/": [
                "4_gameScene/0_Default/1/btn-screen-arow-show",
                "cc.SpriteFrame",
                1
            ],
            "2apS7RYORK+5lLpBTvH2az": [
                "4_gameScene/0_Default/1/btn-screen-arow-show.png",
                "cc.Texture2D"
            ],
            e9YSqke7RKR6DWAr4h0ERl: [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_backtogame",
                "cc.SpriteFrame",
                1
            ],
            "75ty/AxiZM0bqDcmClfWel": [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_backtogame.png",
                "cc.Texture2D"
            ],
            "7c8AJX0e1Pz6gfa5C3AVke": [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_bg",
                "cc.SpriteFrame",
                1
            ],
            a7WGeJj8FGs6hIeAAGY50A: [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_bg.png",
                "cc.Texture2D"
            ],
            "48aI31qb9F5IQ49aCJb0VK": [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_iconshare",
                "cc.SpriteFrame",
                1
            ],
            "67B4U0gL1NZ5uxmGRkV5ZW": [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_iconshare.png",
                "cc.Texture2D"
            ],
            e1h1A5QChJSrKLpzWdzSQB: [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_treasure",
                "cc.SpriteFrame",
                1
            ],
            cakM6VR6tNFZTlJuN4xmGg: [
                "4_gameScene/0_Default/result_table/game_minigame_totalscore_treasure.png",
                "cc.Texture2D"
            ],
            "0bKgFGfu5KqZ5KOKpxrBPf": [
                "4_gameScene/1_Slot1/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "90F0whFgNIcpgGVhSbh01l": [
                "4_gameScene/1_Slot1/img/bg-big.png",
                "cc.Texture2D"
            ],
            "50bYOrUqZOH5fuqiABr1nK": [
                "4_gameScene/1_Slot1/img/bg-ingame-egypt",
                "cc.SpriteFrame",
                1
            ],
            "2eWrj7lTNNfqT0nKB17F2Q": [
                "4_gameScene/1_Slot1/img/bg-ingame-egypt.png",
                "cc.Texture2D"
            ],
            "70bTLekf5DtrHXUV0HN7/u": [
                "4_gameScene/1_Slot1/img/bg-line-game-egypt",
                "cc.SpriteFrame",
                1
            ],
            "55BBPgDGlPRbPTuWR4XHT2": [
                "4_gameScene/1_Slot1/img/bg-line-game-egypt.png",
                "cc.Texture2D"
            ],
            "2dvwct9uFEt49pXo2TWSig": [
                "4_gameScene/1_Slot1/img/coin-egypt-ingame",
                "cc.SpriteFrame",
                1
            ],
            "aeEdhyN4RPqZHZO5+3sXaD": [
                "4_gameScene/1_Slot1/img/coin-egypt-ingame.png",
                "cc.Texture2D"
            ],
            f5MetkAohKRpe9DiEvDicR: [
                "4_gameScene/1_Slot1/img/item/egypt-coin-1",
                "cc.SpriteFrame",
                1
            ],
            "45kNrtbstPV6VJpYXmpBrC": [
                "4_gameScene/1_Slot1/img/item/egypt-coin-1.png",
                "cc.Texture2D"
            ],
            "66zQbfhKBIfobImA5N1srQ": [
                "4_gameScene/1_Slot1/img/item/egypt-coin-2",
                "cc.SpriteFrame",
                1
            ],
            "33z8EX8QNLKKKm9XqXncIP": [
                "4_gameScene/1_Slot1/img/item/egypt-coin-2.png",
                "cc.Texture2D"
            ],
            f2RGr3OOZAi7G1szIpwJPc: [
                "4_gameScene/1_Slot1/img/item/egypt-coin-3",
                "cc.SpriteFrame",
                1
            ],
            ff8C4iykVK9KokfHrXMqRh: [
                "4_gameScene/1_Slot1/img/item/egypt-coin-3.png",
                "cc.Texture2D"
            ],
            "cduI/oEW1IF7zkFDoQA00L": [
                "4_gameScene/1_Slot1/img/item/item-egypt-1",
                "cc.SpriteFrame",
                1
            ],
            "1bx3yDhlRD6YPrhxyA7wE7": [
                "4_gameScene/1_Slot1/img/item/item-egypt-1.png",
                "cc.Texture2D"
            ],
            "32oTJlgk9Pa4/U2Bkv5SLh": [
                "4_gameScene/1_Slot1/img/item/item-egypt-11",
                "cc.SpriteFrame",
                1
            ],
            "92eZW7SMtOsqRHkLb5US9C": [
                "4_gameScene/1_Slot1/img/item/item-egypt-11.png",
                "cc.Texture2D"
            ],
            "24pit3a0ZDxKLus9Ue4fba": [
                "4_gameScene/1_Slot1/img/item/item-egypt-12",
                "cc.SpriteFrame",
                1
            ],
            "89fjkCY7JErYC2SW7bGAK+": [
                "4_gameScene/1_Slot1/img/item/item-egypt-12.png",
                "cc.Texture2D"
            ],
            "41SaKRfqNKV4u80bKMlyyW": [
                "4_gameScene/1_Slot1/img/item/item-egypt-13",
                "cc.SpriteFrame",
                1
            ],
            "3f9jVqON1J7YL33Gd5mcNG": [
                "4_gameScene/1_Slot1/img/item/item-egypt-13.png",
                "cc.Texture2D"
            ],
            "d8hFD+lflMaIBLEQ284D03": [
                "4_gameScene/1_Slot1/img/item/item-egypt-14",
                "cc.SpriteFrame",
                1
            ],
            "ebGaytud1G6aiJ+niwIGbe": [
                "4_gameScene/1_Slot1/img/item/item-egypt-14.png",
                "cc.Texture2D"
            ],
            "9fS0p5JWpGi5TikZNTHckM": [
                "4_gameScene/1_Slot1/img/item/item-egypt-2",
                "cc.SpriteFrame",
                1
            ],
            "3dXO2LHnJM4ph4VNPeM+Jc": [
                "4_gameScene/1_Slot1/img/item/item-egypt-2.png",
                "cc.Texture2D"
            ],
            d8WQjlJ0VLT6iT0cnvoYmY: [
                "4_gameScene/1_Slot1/img/item/item-egypt-3",
                "cc.SpriteFrame",
                1
            ],
            d4eCV7DxNIS6pzl67hcvtE: [
                "4_gameScene/1_Slot1/img/item/item-egypt-3.png",
                "cc.Texture2D"
            ],
            "15SLyjn5VB4payVXGJTlLL": [
                "4_gameScene/1_Slot1/img/item/item-egypt-4",
                "cc.SpriteFrame",
                1
            ],
            "b4oj/XU6NCVYX315gd9WRc": [
                "4_gameScene/1_Slot1/img/item/item-egypt-4.png",
                "cc.Texture2D"
            ],
            "b4nJAOOGxPMYcti0+CxzP3": [
                "4_gameScene/1_Slot1/img/item/item-egypt-5",
                "cc.SpriteFrame",
                1
            ],
            "64pcDK7hJBpqxV7LN5/xvI": [
                "4_gameScene/1_Slot1/img/item/item-egypt-5.png",
                "cc.Texture2D"
            ],
            "8bCFRzl5dNQY/CghYpq98m": [
                "4_gameScene/1_Slot1/img/item/item-egypt-6",
                "cc.SpriteFrame",
                1
            ],
            "25fsqgpQJPGoe+lNyZ/ls7": [
                "4_gameScene/1_Slot1/img/item/item-egypt-6.png",
                "cc.Texture2D"
            ],
            "71TQAJz35Mjq7QQAELBGRA": [
                "4_gameScene/1_Slot1/img/item/item-egypt-7",
                "cc.SpriteFrame",
                1
            ],
            "bddhWIn7FDMaVgA+XoXCE1": [
                "4_gameScene/1_Slot1/img/item/item-egypt-7.png",
                "cc.Texture2D"
            ],
            "79NbOiDxtIF7OeFfk3EJoZ": [
                "4_gameScene/1_Slot1/img/item/item-egypt-8",
                "cc.SpriteFrame",
                1
            ],
            "a9cjbO+uNHHYfo7mNsafuK": [
                "4_gameScene/1_Slot1/img/item/item-egypt-8.png",
                "cc.Texture2D"
            ],
            e51PlzfKJAELyuMvG5jt2l: [
                "4_gameScene/1_Slot1/img/item/item-egypt-9",
                "cc.SpriteFrame",
                1
            ],
            c32oc4QHNNaaKK23m01TTO: [
                "4_gameScene/1_Slot1/img/item/item-egypt-9.png",
                "cc.Texture2D"
            ],
            "b22AXQy+9NbrrUM5LTy/bv": [
                "4_gameScene/1_Slot1/img/item/wild-border",
                "cc.SpriteFrame",
                1
            ],
            e1jmpxrbVKMrXEDVp3fzFU: [
                "4_gameScene/1_Slot1/img/item/wild-border-1",
                "cc.SpriteFrame",
                1
            ],
            "70y9rXjYlDFoIPqealc67k": [
                "4_gameScene/1_Slot1/img/item/wild-border-1.png",
                "cc.Texture2D"
            ],
            "24yCfTMCtLIbWcR22qnb0d": [
                "4_gameScene/1_Slot1/img/item/wild-border-2",
                "cc.SpriteFrame",
                1
            ],
            "1ddixse41PYZnVFRB10neS": [
                "4_gameScene/1_Slot1/img/item/wild-border-2.png",
                "cc.Texture2D"
            ],
            "96xclc6t1ON63whepU2MH6": [
                "4_gameScene/1_Slot1/img/item/wild-border.png",
                "cc.Texture2D"
            ],
            "94xtaS8nFHsLtJtyhiEkAu": [
                "4_gameScene/1_Slot1/img/line-game-egypt",
                "cc.SpriteFrame",
                1
            ],
            "2bZc8rxG1LJ41EMClQzd4o": [
                "4_gameScene/1_Slot1/img/line-game-egypt.png",
                "cc.Texture2D"
            ],
            "50oB1XVztOQJpE6RV9JbXq": [
                "4_gameScene/1_Slot1/img/number-line-game-egypt",
                "cc.SpriteFrame",
                1
            ],
            e0MpfORuJK5aVa7ebp6JrD: [
                "4_gameScene/1_Slot1/img/number-line-game-egypt.png",
                "cc.Texture2D"
            ],
            "48ZiaNWm5Fxp/TOBJ1ydeJ": [
                "4_gameScene/1_Slot1/minigame/Egypt-Screen-bonus-game-",
                "cc.SpriteFrame",
                1
            ],
            "11gkc7LM1EeLcBsDv5tUjs": [
                "4_gameScene/1_Slot1/minigame/Egypt-Screen-bonus-game-.png",
                "cc.Texture2D"
            ],
            c7F2CQeGBBpayPfXNasD5v: [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-bg-ingame",
                "cc.SpriteFrame",
                1
            ],
            "a7+vTgS5NLKJjpbZVft/c+": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-bg-ingame.png",
                "cc.Texture2D"
            ],
            "0a3tyeIthCRLL0PaBJw3DL": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-coin-active",
                "cc.SpriteFrame",
                1
            ],
            d2HrR2T4FATrF7pwAXNClz: [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-coin-active.png",
                "cc.Texture2D"
            ],
            "e7rGNNxD1F6qx3KiDv6x+w": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-hide",
                "cc.SpriteFrame",
                1
            ],
            "edZvoGhKlMb6mCwG5jhg/4": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-hide.png",
                "cc.Texture2D"
            ],
            "cbj/89fQJKoIb5XC1OrqZ7": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-normal",
                "cc.SpriteFrame",
                1
            ],
            "91WIqy5zxBqLJ1gZ6O+IVj": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-normal.png",
                "cc.Texture2D"
            ],
            "54Mw0y5X5LTp/1ZAzkpbaX": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-paper-money",
                "cc.SpriteFrame",
                1
            ],
            fa8FJAyUpL4oee7jJ1PjJi: [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-paper-money.png",
                "cc.Texture2D"
            ],
            "7cSdY5oVBFBoztL1Cne6JY": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-txt-pick",
                "cc.SpriteFrame",
                1
            ],
            "80W2w9fFFDWJqduhUnXeJH": [
                "4_gameScene/1_Slot1/minigame/egypt-bonus-txt-pick.png",
                "cc.Texture2D"
            ],
            "c1NpHWGpZJpYVPwaQ7k+Dn": [
                "4_gameScene/1_Slot1/minigame/egypt-txt-congrat",
                "cc.SpriteFrame",
                1
            ],
            "93vUdxacVLZrtZrq2zAEP/": [
                "4_gameScene/1_Slot1/minigame/egypt-txt-congrat.png",
                "cc.Texture2D"
            ],
            "4e7UediEhAVZC8YAWCeLwn": [
                "4_gameScene/2_Slot2/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "24s7S2zU1ByqMuyC1QKyoh": [
                "4_gameScene/2_Slot2/img/bg-big.png",
                "cc.Texture2D"
            ],
            "97rihWgVBAe6h7ugWVWFgP": [
                "4_gameScene/2_Slot2/img/btn-dragon",
                "cc.SpriteFrame",
                1
            ],
            efpXy4TjdCp5Jvl5Y4W7l9: [
                "4_gameScene/2_Slot2/img/btn-dragon.png",
                "cc.Texture2D"
            ],
            "5c5h1k+sBHTY7rGdPtMwH+": [
                "4_gameScene/2_Slot2/img/game-dra-bg-gold",
                "cc.SpriteFrame",
                1
            ],
            f1HLJgt1VK94c01lk2gyL3: [
                "4_gameScene/2_Slot2/img/game-dra-bg-gold.png",
                "cc.Texture2D"
            ],
            "60jWS9FOxAObYFe6fUWl5t": [
                "4_gameScene/2_Slot2/img/game-dra-bg-score-blue",
                "cc.SpriteFrame",
                1
            ],
            "b3SlWCGbRLmIHnp+i0TutL": [
                "4_gameScene/2_Slot2/img/game-dra-bg-score-blue.png",
                "cc.Texture2D"
            ],
            "52HrDKspdOtYu66G9VufWa": [
                "4_gameScene/2_Slot2/img/game-dra-bg-time-line",
                "cc.SpriteFrame",
                1
            ],
            "18UjvXe/dG+aR5MWbwbBaa": [
                "4_gameScene/2_Slot2/img/game-dra-bg-time-line.png",
                "cc.Texture2D"
            ],
            "848PfEpkpJaq30lI1oSAks": [
                "4_gameScene/2_Slot2/img/game-dra-bg-violet",
                "cc.SpriteFrame",
                1
            ],
            dbqGnzsldAmYMdoMH2WbBi: [
                "4_gameScene/2_Slot2/img/game-dra-bg-violet.png",
                "cc.Texture2D"
            ],
            "21WhUJXgJFm4AFhO1J7QLP": [
                "4_gameScene/2_Slot2/img/game-dra-coin-gold-score",
                "cc.SpriteFrame",
                1
            ],
            "a635wr70VNZKHTf+0tYXcr": [
                "4_gameScene/2_Slot2/img/game-dra-coin-gold-score.png",
                "cc.Texture2D"
            ],
            "7fqQIHoJtAP4UZLIVwLwTM": [
                "4_gameScene/2_Slot2/img/game-dra-time-line-score",
                "cc.SpriteFrame",
                1
            ],
            "62dV6IDzJKx6A3bB3175+V": [
                "4_gameScene/2_Slot2/img/game-dra-time-line-score.png",
                "cc.Texture2D"
            ],
            "8cU+UNTitHe5ClEbUTGODC": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-1",
                "cc.SpriteFrame",
                1
            ],
            "20CMmFTNpO1Jz6Diu5A/Ba": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-1.png",
                "cc.Texture2D"
            ],
            "935Qs2tB1G7pX2jgvHytf9": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-2",
                "cc.SpriteFrame",
                1
            ],
            "17CZPlAU9NuLNWXSQdiLxv": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-2.png",
                "cc.Texture2D"
            ],
            "93c3hKUXxCsod50QvAUS2A": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-3",
                "cc.SpriteFrame",
                1
            ],
            "7cKraiSJtJo4XZFptOKD5P": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-3.png",
                "cc.Texture2D"
            ],
            "85RuLZJ5lA7LbJGuDfTta9": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-4",
                "cc.SpriteFrame",
                1
            ],
            "7bat62Zn5LiIppOQtGisFa": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-4.png",
                "cc.Texture2D"
            ],
            "f9BII3eHhLIIkk7p/d2852": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-5",
                "cc.SpriteFrame",
                1
            ],
            "94ohv0nMRDVIgKu61VE6EP": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-5.png",
                "cc.Texture2D"
            ],
            "b5Gsv5m/NCMoASOb/o0lC0": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-6",
                "cc.SpriteFrame",
                1
            ],
            "01jyjxi8JIOYJNZaIM4X2n": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-6.png",
                "cc.Texture2D"
            ],
            "58DZrpApFOIoENqaDpYl/A": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-7",
                "cc.SpriteFrame",
                1
            ],
            "496Wqq0f1Eep+57MPbHQew": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-7.png",
                "cc.Texture2D"
            ],
            "ef/yomYVtNkr6GH1IRxlI2": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-bg-money-coin",
                "cc.SpriteFrame",
                1
            ],
            "77AiGGXoNDKbFPYEBrIokJ": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-bg-money-coin.png",
                "cc.Texture2D"
            ],
            "03L/8hkcdLRrbKJM0H7wvd": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gold",
                "cc.SpriteFrame",
                1
            ],
            "9bd1McU3FITL/fzM7yGFc1": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gold-mon",
                "cc.SpriteFrame",
                1
            ],
            "fa/Sawa4xMsKrG9/BLILr4": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gold-mon.png",
                "cc.Texture2D"
            ],
            "8fiEO56MRA5oEm86IKHR2Q": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gold.png",
                "cc.Texture2D"
            ],
            "75t+o6JAxCdJ9qTENgJSQy": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gray",
                "cc.SpriteFrame",
                1
            ],
            "76Xi2tp6VGt4NQO2wNu7Yn": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gray-mon",
                "cc.SpriteFrame",
                1
            ],
            "b0zgk4MT5Ixa+VrExrjQ7v": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gray-mon.png",
                "cc.Texture2D"
            ],
            "4eSlSU9ZtPRo0aAMNglhPx": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-coin-gray.png",
                "cc.Texture2D"
            ],
            "98AOWW82RF4Lo4uL2oVfTK": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-dra-wild",
                "cc.SpriteFrame",
                1
            ],
            "35BkNZE5dMIr3DETDrUKj3": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-dra-wild-update",
                "cc.SpriteFrame",
                1
            ],
            "86qvdVrfVLzY5I5BaWEqEQ": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-dra-wild-update.png",
                "cc.Texture2D"
            ],
            e16bKOYp5GcrktpO4OwWN9: [
                "4_gameScene/2_Slot2/img/item/game-dra-item-dra-wild.png",
                "cc.Texture2D"
            ],
            "13y6bi1YZO85PhNQ3Sa37D": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-free-spin-1",
                "cc.SpriteFrame",
                1
            ],
            "67E0ZqfpFLqoaE5I8lmPX8": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-free-spin-1.png",
                "cc.Texture2D"
            ],
            "4aHxoRoNFHAYPZHfariLkr": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-freespin-2",
                "cc.SpriteFrame",
                1
            ],
            "4e8W41F2RFYplOHhOa8Zw/": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-freespin-2.png",
                "cc.Texture2D"
            ],
            e0xJkEwlxBqqTFOriCrCmf: [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-1",
                "cc.SpriteFrame",
                1
            ],
            "98QdZX/UpCab6+pAJoEWI+": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-1-bg",
                "cc.SpriteFrame",
                1
            ],
            "09iphQ4g1HqpJJr8bH+ESm": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-1-bg.png",
                "cc.Texture2D"
            ],
            f1MfJGFiRD3Z9grfDUkrLV: [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-1.png",
                "cc.Texture2D"
            ],
            c4CfizIBVGZ6PZVhLiEenw: [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-2",
                "cc.SpriteFrame",
                1
            ],
            "18SYQfI1pG5p6LM5ctN51j": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-2-bg",
                "cc.SpriteFrame",
                1
            ],
            "32T9e0K19HNIo1WxCriVlP": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-2-bg.png",
                "cc.Texture2D"
            ],
            "94mOPxeLtFgrh28Cky6NRP": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-2.png",
                "cc.Texture2D"
            ],
            "cd4jf5kwhJ+Zuf0X6IN7+i": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-3",
                "cc.SpriteFrame",
                1
            ],
            b7DccAxqBFfJdw5yvROpTJ: [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-3-bg",
                "cc.SpriteFrame",
                1
            ],
            "74LVKfDvpLE7ZCXdIwE7NP": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-3-bg.png",
                "cc.Texture2D"
            ],
            "9a8rfkbSdKUYym/lQRb2cI": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-special-3.png",
                "cc.Texture2D"
            ],
            "3aAj/PaXVNpbMvoREvcjXw": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-text-wild",
                "cc.SpriteFrame",
                1
            ],
            "5fW+OkotRCApArKBDABlLa": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-text-wild.png",
                "cc.Texture2D"
            ],
            "85Z6LufHtB/4fjYJCOQT6x": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-wild-bg",
                "cc.SpriteFrame",
                1
            ],
            "31Bg7q5NNEl5AM6UWi61LC": [
                "4_gameScene/2_Slot2/img/item/game-dra-item-wild-bg.png",
                "cc.Texture2D"
            ],
            "77kaevs7pBio7XJ2D2pDNk": [
                "4_gameScene/2_Slot2/img/item/game-dragon",
                "cc.SpriteFrame",
                1
            ],
            "0aH5MSdexFZLQY/Fl2hSKz": [
                "4_gameScene/2_Slot2/img/item/game-dragon.jpg",
                "cc.Texture2D"
            ],
            "b7H0FxaWlEprpv+xpE+MyH": [
                "4_gameScene/2_Slot2/img/light-dragon",
                "cc.SpriteFrame",
                1
            ],
            "1erOcVfH5EEpEGQYBw+hF2": [
                "4_gameScene/2_Slot2/img/light-dragon.png",
                "cc.Texture2D"
            ],
            bcW0bl2INEGYjpy8r7VA2E: [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-btn-start-stop",
                "cc.SpriteFrame",
                1
            ],
            "03+Esr3H1KNK+wwOaQ/SzA": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-btn-start-stop.png",
                "cc.Texture2D"
            ],
            eaTV1Kn39LuJE1Qq3DyxK9: [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-btn-stop",
                "cc.SpriteFrame",
                1
            ],
            b7VJCCf7BNUp7cpEkymp78: [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-btn-stop.png",
                "cc.Texture2D"
            ],
            "a8IfJxkh9NToZ+ha/H7DAN": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-chess",
                "cc.SpriteFrame",
                1
            ],
            d2khOjLRVDwbBxcNjxZScA: [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-chess.png",
                "cc.Texture2D"
            ],
            "1cXNRS3rNJwqy41ddFM7mm": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-gold",
                "cc.SpriteFrame",
                1
            ],
            "70mD11FPdCr4+5Isbw61Pb": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-gold.png",
                "cc.Texture2D"
            ],
            "a9g7oBtTxEwZR+wiOG6rTo": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-img-dragon",
                "cc.SpriteFrame",
                1
            ],
            "78S3Y2zaBFz4znE5XqaUdi": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-img-dragon.png",
                "cc.Texture2D"
            ],
            "d0BjHc+tVI7Lpebs9lztQj": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-monney",
                "cc.SpriteFrame",
                1
            ],
            "71LTdpz+9LFq6w31NfrX88": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-monney.png",
                "cc.Texture2D"
            ],
            "3bX2tsJDFIsrUSv+A5dPkf": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-tip",
                "cc.SpriteFrame",
                1
            ],
            f9sbMGGmtFYbIwegiGzncw: [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-tip.png",
                "cc.Texture2D"
            ],
            "6ckqgjOWVFb6vf/cdAOGN9": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-trans-chess",
                "cc.SpriteFrame",
                1
            ],
            "41GpJGWx9LM4u/MKbMkRyb": [
                "4_gameScene/2_Slot2/minigame/dragon-bonus-bg-trans-chess.png",
                "cc.Texture2D"
            ],
            "b19W5zEzBEZpf9wONSp/7R": [
                "4_gameScene/3_Slot3/img/Item-Recovered_cutitem_15",
                "cc.SpriteFrame",
                1
            ],
            "b5H1u+rS9G3IxNcN3p91mf": [
                "4_gameScene/3_Slot3/img/Item-Recovered_cutitem_15.png",
                "cc.Texture2D"
            ],
            "3bxpB7BZhI/6sb216TZKGP": [
                "4_gameScene/3_Slot3/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "4e6mW2G39M0LzRBUNKfPYN": [
                "4_gameScene/3_Slot3/img/bg-big.png",
                "cc.Texture2D"
            ],
            "70jKiMRe9DlJbf/8tUBALP": [
                "4_gameScene/3_Slot3/img/bg_number",
                "cc.SpriteFrame",
                1
            ],
            "7ez9qu595Oz5p085SLWI4m": [
                "4_gameScene/3_Slot3/img/bg_number.png",
                "cc.Texture2D"
            ],
            "387xPEgqpK6qpFX3qOwWrX": [
                "4_gameScene/3_Slot3/img/outline_bg",
                "cc.SpriteFrame",
                1
            ],
            "30VKeyaMZGELNpGFVe5A12": [
                "4_gameScene/3_Slot3/img/outline_bg.png",
                "cc.Texture2D"
            ],
            b5I1PESjNBnpnj32FsmzuZ: [
                "4_gameScene/3_Slot3/minigame/Bonus_farm_view",
                "cc.SpriteFrame",
                1
            ],
            "78Y0JzQkRDmJqZDGsG28kq": [
                "4_gameScene/3_Slot3/minigame/Bonus_farm_view.png",
                "cc.Texture2D"
            ],
            b53zDt1A5F9KOAIF9Z3hTG: [
                "4_gameScene/3_Slot3/minigame/bap_cai_alpha",
                "cc.SpriteFrame",
                1
            ],
            "8eZJqNvFxFQJfZoDfRarWI": [
                "4_gameScene/3_Slot3/minigame/bap_cai_alpha.png",
                "cc.Texture2D"
            ],
            "87M1G4Mr1JW5w+jSTgLwDK": [
                "4_gameScene/3_Slot3/minigame/bap_cai_dark",
                "cc.SpriteFrame",
                1
            ],
            d2H6TLRaJAJ4QTuhrLTRRR: [
                "4_gameScene/3_Slot3/minigame/bap_cai_dark.png",
                "cc.Texture2D"
            ],
            "caKuI6GWVAZouPMBV1E/i1": [
                "4_gameScene/3_Slot3/minigame/bap_cai_final",
                "cc.SpriteFrame",
                1
            ],
            e6yM83ByFJU5qtFmVFfh7N: [
                "4_gameScene/3_Slot3/minigame/bap_cai_final.png",
                "cc.Texture2D"
            ],
            adn5uBMINAmrLamZNVyS9C: [
                "4_gameScene/3_Slot3/minigame/bg",
                "cc.SpriteFrame",
                1
            ],
            dcDMuBQZRA56b3RRM9KnlD: [
                "4_gameScene/3_Slot3/minigame/bg-minigame",
                "cc.SpriteFrame",
                1
            ],
            "8cTrWD8MNHmYL8eUMuidVY": [
                "4_gameScene/3_Slot3/minigame/bg-minigame.png",
                "cc.Texture2D"
            ],
            "ddRPq0j/VNJrHk5b3bN53y": [
                "4_gameScene/3_Slot3/minigame/bg.png",
                "cc.Texture2D"
            ],
            "343M03mdxCV73/RfX7xNaI": [
                "4_gameScene/3_Slot3/minigame/ca_rot_alpha",
                "cc.SpriteFrame",
                1
            ],
            "beD/iHpwJBOJoi/qnYBXC8": [
                "4_gameScene/3_Slot3/minigame/ca_rot_alpha.png",
                "cc.Texture2D"
            ],
            "303WFS9aZPBZSzHXY56ym3": [
                "4_gameScene/3_Slot3/minigame/ca_rot_dark",
                "cc.SpriteFrame",
                1
            ],
            "ebmFx/rK5MlKx0L6jsGUjX": [
                "4_gameScene/3_Slot3/minigame/ca_rot_dark.png",
                "cc.Texture2D"
            ],
            "57a+2ju29FKIC3xL6SwKai": [
                "4_gameScene/3_Slot3/minigame/ca_rot_final",
                "cc.SpriteFrame",
                1
            ],
            "2bAuUpRc1NmaS+Gi6INThD": [
                "4_gameScene/3_Slot3/minigame/ca_rot_final.png",
                "cc.Texture2D"
            ],
            "84XlyvXHNBVbwEKHJPWfrB": [
                "4_gameScene/3_Slot3/minigame/cu_hanh_alpha",
                "cc.SpriteFrame",
                1
            ],
            "8ero+b/TVOYqIuU7EM8dOt": [
                "4_gameScene/3_Slot3/minigame/cu_hanh_alpha.png",
                "cc.Texture2D"
            ],
            "56dkNtTQRBkZ8XXGBAri7J": [
                "4_gameScene/3_Slot3/minigame/cu_hanh_dark",
                "cc.SpriteFrame",
                1
            ],
            "60KQBBqZVP2LbVL3X72fMe": [
                "4_gameScene/3_Slot3/minigame/cu_hanh_dark.png",
                "cc.Texture2D"
            ],
            "4cRoClK/VOb6e51FDlikTM": [
                "4_gameScene/3_Slot3/minigame/cu_hanh_final",
                "cc.SpriteFrame",
                1
            ],
            edhTFYGctEAojHkCeFnzhu: [
                "4_gameScene/3_Slot3/minigame/cu_hanh_final.png",
                "cc.Texture2D"
            ],
            aa1iOhRbpDFqLyide5F1la: [
                "4_gameScene/3_Slot3/minigame/gold_bap_cai",
                "cc.SpriteFrame",
                1
            ],
            a2zgKDcdhHCZGay9hie2hY: [
                "4_gameScene/3_Slot3/minigame/gold_bap_cai.png",
                "cc.Texture2D"
            ],
            "2eEAID23lOyaSsUy2RoA1Q": [
                "4_gameScene/3_Slot3/minigame/gold_ca_rot",
                "cc.SpriteFrame",
                1
            ],
            "9fOAsas/pHpbTvjWGW7vYg": [
                "4_gameScene/3_Slot3/minigame/gold_ca_rot.png",
                "cc.Texture2D"
            ],
            "1dDfQQJkRAoKFbLYjK0TCQ": [
                "4_gameScene/3_Slot3/minigame/gold_cu_hanh",
                "cc.SpriteFrame",
                1
            ],
            df1rXz03dAGYPEW45ULEft: [
                "4_gameScene/3_Slot3/minigame/gold_cu_hanh.png",
                "cc.Texture2D"
            ],
            "59+RlDAsFDJoiQqj2fNEfP": [
                "4_gameScene/3_Slot3/minigame/gold_huong_duong",
                "cc.SpriteFrame",
                1
            ],
            "43Behh94lFhblBEwI9+13m": [
                "4_gameScene/3_Slot3/minigame/gold_huong_duong.png",
                "cc.Texture2D"
            ],
            "c9KczMV1ZORJY75thA5/0F": [
                "4_gameScene/3_Slot3/minigame/golden_pumpkin",
                "cc.SpriteFrame",
                1
            ],
            "1a3E/X1SVNIbkPmWEBdOq4": [
                "4_gameScene/3_Slot3/minigame/golden_pumpkin.png",
                "cc.Texture2D"
            ],
            "03LsKufiNHI4jPbXkV4qpX": [
                "4_gameScene/3_Slot3/minigame/grub",
                "cc.SpriteFrame",
                1
            ],
            "05GvY/GV9GmIFYvObaZZZq": [
                "4_gameScene/3_Slot3/minigame/grub.png",
                "cc.Texture2D"
            ],
            "26mE5KgFJDdoPpxjoK64AI": [
                "4_gameScene/3_Slot3/minigame/huong_duong_alpha",
                "cc.SpriteFrame",
                1
            ],
            "32H/xDQWpHg4k+FJ6MKiIQ": [
                "4_gameScene/3_Slot3/minigame/huong_duong_alpha-13",
                "cc.SpriteFrame",
                1
            ],
            "bcydtulL1HH7e+4SDCxJPd": [
                "4_gameScene/3_Slot3/minigame/huong_duong_alpha-13.png",
                "cc.Texture2D"
            ],
            "6bGYYfhxRNc75jE1SOZibm": [
                "4_gameScene/3_Slot3/minigame/huong_duong_alpha.png",
                "cc.Texture2D"
            ],
            fer01wXtRJV7s31p6mUVD2: [
                "4_gameScene/3_Slot3/minigame/huong_duong_dark",
                "cc.SpriteFrame",
                1
            ],
            "93vT8NDl1ONKBKaBR3WDGC": [
                "4_gameScene/3_Slot3/minigame/huong_duong_dark.png",
                "cc.Texture2D"
            ],
            "69OUrTGEZAB47OzJc+nev0": [
                "4_gameScene/3_Slot3/minigame/o_dat",
                "cc.SpriteFrame",
                1
            ],
            "48OQwQ5e9IS6TmM24Njf8A": [
                "4_gameScene/3_Slot3/minigame/o_dat.png",
                "cc.Texture2D"
            ],
            "98HruYw9JG5IQdK7IsmRHB": [
                "4_gameScene/3_Slot3/minigame/snake",
                "cc.SpriteFrame",
                1
            ],
            d6PE5w0XxKU6H1i5RZsiFM: [
                "4_gameScene/3_Slot3/minigame/snake.png",
                "cc.Texture2D"
            ],
            "8fNQ3vL81HsqTlJXL4S8ER": [
                "4_gameScene/3_Slot3/minigame/start_minigame",
                "cc.SpriteFrame",
                1
            ],
            "aam0g+LTBBm7Jd2ZgEKI2S": [
                "4_gameScene/3_Slot3/minigame/start_minigame.png",
                "cc.Texture2D"
            ],
            "85aNfebfFICK5DI4wLh1Ap": [
                "4_gameScene/3_Slot3/minigame/start_txt",
                "cc.SpriteFrame",
                1
            ],
            "66M4PjUpRJSLaqLZhsFW+8": [
                "4_gameScene/3_Slot3/minigame/start_txt.png",
                "cc.Texture2D"
            ],
            "ad/TMUJP5PO5nzEOFaUXlA": [
                "4_gameScene/3_Slot3/minigame/total",
                "cc.SpriteFrame",
                1
            ],
            "0egGmeayNKXZaOMvCFJL8d": [
                "4_gameScene/3_Slot3/minigame/total.png",
                "cc.Texture2D"
            ],
            a1GtQlocJIDbsn4hEm3wiE: [
                "4_gameScene/3_Slot3/skeleton/item_spine/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "c6hDegwHxERLX+Ftl+fxA6": [
                "4_gameScene/3_Slot3/skeleton/item_spine/skeleton.atlas",
                "cc.Asset"
            ],
            "7f8Llaj7dBiqY7Mim/fTMP": [
                "4_gameScene/3_Slot3/skeleton/item_spine/skeleton.json",
                "sp.SkeletonData"
            ],
            f3r5YJx7dLwr6Van0Buaub: [
                "4_gameScene/3_Slot3/skeleton/item_spine/skeleton.png",
                "cc.Texture2D"
            ],
            b5qdjKi7BKNL6EV69aszk1: [
                "4_gameScene/4_Slot4/img/Item/item-casino-1",
                "cc.SpriteFrame",
                1
            ],
            "28aEhfRf9JBLTBrjhvACmK": [
                "4_gameScene/4_Slot4/img/Item/item-casino-1.png",
                "cc.Texture2D"
            ],
            "0c8BNoWrVKb7oipRnwjDT3": [
                "4_gameScene/4_Slot4/img/Item/item-casino-10",
                "cc.SpriteFrame",
                1
            ],
            bcsBGvv7dFqZiU6ruQmZ5O: [
                "4_gameScene/4_Slot4/img/Item/item-casino-10-gold",
                "cc.SpriteFrame",
                1
            ],
            "30D+09KThEpaIIGLdbb3pU": [
                "4_gameScene/4_Slot4/img/Item/item-casino-10-gold.png",
                "cc.Texture2D"
            ],
            "adX7+6IyhLUJUw6GqreKpS": [
                "4_gameScene/4_Slot4/img/Item/item-casino-10.png",
                "cc.Texture2D"
            ],
            aeJCkK6TJE0bXLuxitbC5p: [
                "4_gameScene/4_Slot4/img/Item/item-casino-11",
                "cc.SpriteFrame",
                1
            ],
            "f02mFacINGVLc6Hfi+7zaT": [
                "4_gameScene/4_Slot4/img/Item/item-casino-11-gold",
                "cc.SpriteFrame",
                1
            ],
            "56MKm/QRVNzpIX2EDlj3Tf": [
                "4_gameScene/4_Slot4/img/Item/item-casino-11-gold.png",
                "cc.Texture2D"
            ],
            "6aMQze1dtD0r/K30zBD3bO": [
                "4_gameScene/4_Slot4/img/Item/item-casino-11.png",
                "cc.Texture2D"
            ],
            "d6389CybdB/6L2d2CPKJzi": [
                "4_gameScene/4_Slot4/img/Item/item-casino-2",
                "cc.SpriteFrame",
                1
            ],
            f5pvbSshhFi5bahm1ZT44J: [
                "4_gameScene/4_Slot4/img/Item/item-casino-2.png",
                "cc.Texture2D"
            ],
            "02+arI+S9PB7FcYcuOq4AM": [
                "4_gameScene/4_Slot4/img/Item/item-casino-3",
                "cc.SpriteFrame",
                1
            ],
            "7dVcZjCjtHcb4zc3dlu2uf": [
                "4_gameScene/4_Slot4/img/Item/item-casino-3.png",
                "cc.Texture2D"
            ],
            "98v+vpWJpEE7KllOD7Nwig": [
                "4_gameScene/4_Slot4/img/Item/item-casino-4",
                "cc.SpriteFrame",
                1
            ],
            a3REYkUnJAloHQu3xXMCYy: [
                "4_gameScene/4_Slot4/img/Item/item-casino-4.png",
                "cc.Texture2D"
            ],
            "0cnopVsKJODZ1L/CwIBVDs": [
                "4_gameScene/4_Slot4/img/Item/item-casino-5",
                "cc.SpriteFrame",
                1
            ],
            "407MN6wCpONq+Zk49apXBD": [
                "4_gameScene/4_Slot4/img/Item/item-casino-5.png",
                "cc.Texture2D"
            ],
            "9bmfmzfjxJkZrtM8qPjTXF": [
                "4_gameScene/4_Slot4/img/Item/item-casino-6",
                "cc.SpriteFrame",
                1
            ],
            "13l9RRtxhB5JIHIak8vIRI": [
                "4_gameScene/4_Slot4/img/Item/item-casino-6.png",
                "cc.Texture2D"
            ],
            "berzHuHwdC+5iRvGp0rqiv": [
                "4_gameScene/4_Slot4/img/Item/item-casino-7",
                "cc.SpriteFrame",
                1
            ],
            a5HgxXISlBVpVpbRy6GG2F: [
                "4_gameScene/4_Slot4/img/Item/item-casino-7.png",
                "cc.Texture2D"
            ],
            "94DEztTiRJWLF7LyEWTUDM": [
                "4_gameScene/4_Slot4/img/Item/item-casino-8",
                "cc.SpriteFrame",
                1
            ],
            a0ayNMAc9AuLnOf6WLn4SM: [
                "4_gameScene/4_Slot4/img/Item/item-casino-8-gold",
                "cc.SpriteFrame",
                1
            ],
            "0eIA8p32VNnKhDM5IBskt9": [
                "4_gameScene/4_Slot4/img/Item/item-casino-8-gold.png",
                "cc.Texture2D"
            ],
            "53oW8LpHFMeZ1yTqdv5owP": [
                "4_gameScene/4_Slot4/img/Item/item-casino-8.png",
                "cc.Texture2D"
            ],
            "f71L/NLwxAGqeU7iYQ+99K": [
                "4_gameScene/4_Slot4/img/Item/item-casino-9",
                "cc.SpriteFrame",
                1
            ],
            a6oDJ2kupBU4QrzpzFbXZi: [
                "4_gameScene/4_Slot4/img/Item/item-casino-9-gold",
                "cc.SpriteFrame",
                1
            ],
            ed5CdPyNxKNq4j3Sr6Uqg5: [
                "4_gameScene/4_Slot4/img/Item/item-casino-9-gold.png",
                "cc.Texture2D"
            ],
            dbykHjdoJCL4HPMRmv5B27: [
                "4_gameScene/4_Slot4/img/Item/item-casino-9.png",
                "cc.Texture2D"
            ],
            abmkciit1DxqLNIwlWmhFK: [
                "4_gameScene/4_Slot4/img/Item/item-casino-scatter",
                "cc.SpriteFrame",
                1
            ],
            "5cr1Tl5mFOxqO6tLlGNKnN": [
                "4_gameScene/4_Slot4/img/Item/item-casino-scatter.png",
                "cc.Texture2D"
            ],
            "6bB6MWJ0NBir0T7RmH4oY6": [
                "4_gameScene/4_Slot4/img/Item/item-casino-wild",
                "cc.SpriteFrame",
                1
            ],
            bdZ4wwUhVOg59DA4eTj2VC: [
                "4_gameScene/4_Slot4/img/Item/item-casino-wild.png",
                "cc.Texture2D"
            ],
            df39EVZU1NPKV5Ltj9lWKe: [
                "4_gameScene/4_Slot4/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "57nSwB4lRLoaMoqdXc/Dz8": [
                "4_gameScene/4_Slot4/img/bg-big.jpg",
                "cc.Texture2D"
            ],
            "7ePrGUnxJPLrf6XG7s8BCl": [
                "4_gameScene/4_Slot4/img/bg-dark-ingame-casino",
                "cc.SpriteFrame",
                1
            ],
            "65fMRrHdJJB7eBzLaoiokE": [
                "4_gameScene/4_Slot4/img/bg-dark-ingame-casino.png",
                "cc.Texture2D"
            ],
            "72JlQsoAtKB5HH5cB2xQg3": [
                "4_gameScene/4_Slot4/img/bg-item-casino-10",
                "cc.SpriteFrame",
                1
            ],
            "8bgsj+idJCYYm4FcgdyeFJ": [
                "4_gameScene/4_Slot4/img/bg-item-casino-10.png",
                "cc.Texture2D"
            ],
            "529kBp8ApDp4U1tA+mkXAp": [
                "4_gameScene/4_Slot4/img/bg-item-casino-11",
                "cc.SpriteFrame",
                1
            ],
            "87pz+NpYxNdb7jJ0qMvn1Z": [
                "4_gameScene/4_Slot4/img/bg-item-casino-11.png",
                "cc.Texture2D"
            ],
            "4bwltjwZpHlo7ArU8adLGB": [
                "4_gameScene/4_Slot4/img/bg-item-casino-8",
                "cc.SpriteFrame",
                1
            ],
            "5dfGp7UQZEa4PNwAHXhyfY": [
                "4_gameScene/4_Slot4/img/bg-item-casino-8.png",
                "cc.Texture2D"
            ],
            "da4g97clJF/75T9rQYzzR8": [
                "4_gameScene/4_Slot4/img/bg-item-casino-9",
                "cc.SpriteFrame",
                1
            ],
            "995W0xImdDMa/Pq6Uipica": [
                "4_gameScene/4_Slot4/img/bg-item-casino-9.png",
                "cc.Texture2D"
            ],
            "9dlPAwpCFHz4x4BbWQqw21": [
                "4_gameScene/4_Slot4/img/bg-item-casino-scatter",
                "cc.SpriteFrame",
                1
            ],
            bbp8FbiDBM0aFdHPv5Lp3O: [
                "4_gameScene/4_Slot4/img/bg-item-casino-scatter.png",
                "cc.Texture2D"
            ],
            "01+HqHe29HAp8mrwdb8559": [
                "4_gameScene/4_Slot4/img/bg-item-casino-wild",
                "cc.SpriteFrame",
                1
            ],
            "86z5YZ81lDXItiJqHgmEEQ": [
                "4_gameScene/4_Slot4/img/bg-item-casino-wild.png",
                "cc.Texture2D"
            ],
            "7e2X7N/8BHapEiCwuQgkK3": [
                "4_gameScene/4_Slot4/img/bg-select-option-game-casino",
                "cc.SpriteFrame",
                1
            ],
            "41zqt3hENGXZZbfDZR1JoX": [
                "4_gameScene/4_Slot4/img/bg-select-option-game-casino.png",
                "cc.Texture2D"
            ],
            "a5tyuIUsxLRYZXcFph9Z/z": [
                "4_gameScene/4_Slot4/img/borderBet",
                "cc.SpriteFrame",
                1
            ],
            a9YqmCV9hNP7iqKMBx7Xrx: [
                "4_gameScene/4_Slot4/img/borderBet.png",
                "cc.Texture2D"
            ],
            "63QAFkNmhLvJf0BRLj/1at": [
                "4_gameScene/4_Slot4/img/btn-help-ingame-casino",
                "cc.SpriteFrame",
                1
            ],
            "f2fIYum0xJeIfmZ+ih3DP6": [
                "4_gameScene/4_Slot4/img/btn-help-ingame-casino.png",
                "cc.Texture2D"
            ],
            a74I2oMTlERav2d03fATtH: [
                "4_gameScene/4_Slot4/img/casino-slot-bg-coin-colect-money",
                "cc.SpriteFrame",
                1
            ],
            "4cw91KuEZH04jZvtz6JoBD": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-coin-colect-money.png",
                "cc.Texture2D"
            ],
            "43+Qk4h2NLibjnuHcrPbze": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-grand-money",
                "cc.SpriteFrame",
                1
            ],
            "55RZNLuipGLIlU0uqUiAgN": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-grand-money.png",
                "cc.Texture2D"
            ],
            "15oCrelwhBcaM3cTayGKVC": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-major-money",
                "cc.SpriteFrame",
                1
            ],
            "9b7hs8RN1Ggqml98DebcM2": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-major-money.png",
                "cc.Texture2D"
            ],
            "c9nPh81HNM6pvH0DiUH/mT": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-mini-money",
                "cc.SpriteFrame",
                1
            ],
            "c0p+hluN9F2bwEhJgRQ4Ji": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-mini-money.png",
                "cc.Texture2D"
            ],
            "65GTw1ZCNOJIBOgyHaPYmN": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-minor-money",
                "cc.SpriteFrame",
                1
            ],
            "12iL7m7KxPv4YszpyPWxNI": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-minor-money.png",
                "cc.Texture2D"
            ],
            "0eM4wU4M5GzYE+xPYyIbns": [
                "4_gameScene/4_Slot4/img/casino-slot-bg-play-game",
                "cc.SpriteFrame",
                1
            ],
            b3CcWyAgVE4pSobpvWtvfc: [
                "4_gameScene/4_Slot4/img/casino-slot-bg-play-game.png",
                "cc.Texture2D"
            ],
            deXhfbNwtIfahJLrkeh42w: [
                "4_gameScene/4_Slot4/img/casino-slot-stroke-win",
                "cc.SpriteFrame",
                1
            ],
            "3dzuQIW/lHs5UQ706br1kQ": [
                "4_gameScene/4_Slot4/img/casino-slot-stroke-win.png",
                "cc.Texture2D"
            ],
            "43c53CzdRBKrwwNxCVLE/b": [
                "4_gameScene/4_Slot4/img/game-3",
                "cc.SpriteFrame",
                1
            ],
            "6a5hgjbFhNPI9mdVqp28sy": [
                "4_gameScene/4_Slot4/img/game-3-select-bet",
                "cc.SpriteFrame",
                1
            ],
            "1bWPwVWeNBMLH0o9DggUQv": [
                "4_gameScene/4_Slot4/img/game-3-select-bet.png",
                "cc.Texture2D"
            ],
            "39/wCKZVZEJp9Rgrcjoux6": [
                "4_gameScene/4_Slot4/img/game-3.png",
                "cc.Texture2D"
            ],
            "19RDqNG3JLPp1TMV57yfZW": [
                "4_gameScene/4_Slot4/img/overlay",
                "cc.SpriteFrame",
                1
            ],
            "78HTqiNr1ODorfuQVLfqhW": [
                "4_gameScene/4_Slot4/img/overlay.png",
                "cc.Texture2D"
            ],
            fbpu9RH1pLk6iHWD55V3I4: [
                "4_gameScene/4_Slot4/img/select-bet-5-of-kind",
                "cc.SpriteFrame",
                1
            ],
            "62LD1XeLtCQJj72Wv3GCqX": [
                "4_gameScene/4_Slot4/img/select-bet-5-of-kind.png",
                "cc.Texture2D"
            ],
            e6tJ0SVBBNULGDBkZIiLMG: [
                "4_gameScene/4_Slot4/img/select-bet-bg",
                "cc.SpriteFrame",
                1
            ],
            "72uBcdD5lFL4QL3ba6Im2y": [
                "4_gameScene/4_Slot4/img/select-bet-bg.png",
                "cc.Texture2D"
            ],
            "21Kg/joGpCHbFefsFedl9p": [
                "4_gameScene/4_Slot4/img/select-bet-play-1",
                "cc.SpriteFrame",
                1
            ],
            "33tE13j7JFmbmPlKZst6ZA": [
                "4_gameScene/4_Slot4/img/select-bet-play-1.png",
                "cc.Texture2D"
            ],
            "74AlWFZSdNFZTTJ2tLUGCC": [
                "4_gameScene/4_Slot4/img/select-bet-play-2",
                "cc.SpriteFrame",
                1
            ],
            e0ainAVXZCcKhWaJ9oSRnN: [
                "4_gameScene/4_Slot4/img/select-bet-play-2.png",
                "cc.Texture2D"
            ],
            "12g1lDhVBBmbWiR07GJZS1": [
                "4_gameScene/4_Slot4/img/select-bet-play-3",
                "cc.SpriteFrame",
                1
            ],
            "91ZeR9Dg1Dp5jBcE+wq4E9": [
                "4_gameScene/4_Slot4/img/select-bet-play-3.png",
                "cc.Texture2D"
            ],
            "731RUzPCRFUa+dARv6eTB5": [
                "4_gameScene/4_Slot4/img/select-bet-play-4",
                "cc.SpriteFrame",
                1
            ],
            "16DHsfl6lNJ46SwcsRgoLZ": [
                "4_gameScene/4_Slot4/img/select-bet-play-4.png",
                "cc.Texture2D"
            ],
            "b0+lo0jL9Fg5K4HGo4ruBk": [
                "4_gameScene/4_Slot4/img/select-bet-play-5",
                "cc.SpriteFrame",
                1
            ],
            "74fSLPCvJJUKOkZmzRVq9h": [
                "4_gameScene/4_Slot4/img/select-bet-play-5.png",
                "cc.Texture2D"
            ],
            "38I93ng+BCoKHdp8caM5Y2": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-1",
                "cc.SpriteFrame",
                1
            ],
            c02yiOSE1B8qlJK4SLQjeG: [
                "4_gameScene/4_Slot4/img/select-bet-play-item-1.png",
                "cc.Texture2D"
            ],
            "1fpi9jcQZEpb2k9BuokAj5": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-2",
                "cc.SpriteFrame",
                1
            ],
            "b4YnSH8HVEz4aJk1B8oL0+": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-2.png",
                "cc.Texture2D"
            ],
            "0au0alIEhHpKZrgQCFgO8K": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-3",
                "cc.SpriteFrame",
                1
            ],
            "960SWOJVREU4xaUwfd19kz": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-3.png",
                "cc.Texture2D"
            ],
            "58x2XEAg9OgoEx0kQr98+g": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-4",
                "cc.SpriteFrame",
                1
            ],
            "a2XNTQiZ1F+bc4GRs958rW": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-4.png",
                "cc.Texture2D"
            ],
            "83E93A271IR4u7zxIpq22c": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-5",
                "cc.SpriteFrame",
                1
            ],
            "30VKrYllNEc5Iuckk1oC8D": [
                "4_gameScene/4_Slot4/img/select-bet-play-item-5.png",
                "cc.Texture2D"
            ],
            "0a0/RWArZO84uzVPGDxkH6": [
                "4_gameScene/4_Slot4/img/select-bet-play-text-bet",
                "cc.SpriteFrame",
                1
            ],
            "7cQTiYqRBOPqJvaGQFI2tn": [
                "4_gameScene/4_Slot4/img/select-bet-play-text-bet.png",
                "cc.Texture2D"
            ],
            "6cnGdnW9BGuJuIG4DPJsj3": [
                "4_gameScene/4_Slot4/img/select-bet-wild-scatter",
                "cc.SpriteFrame",
                1
            ],
            "efD0VP+H1J6YfdtTBLxWQT": [
                "4_gameScene/4_Slot4/img/select-bet-wild-scatter.png",
                "cc.Texture2D"
            ],
            "a6n2kB/WJKZqz7NVwzGZDm": [
                "4_gameScene/4_Slot4/minigame/Casino-bg-congrat-bg-money-win",
                "cc.SpriteFrame",
                1
            ],
            "dfg7+pAjdLY7o4Jj+SP6ZP": [
                "4_gameScene/4_Slot4/minigame/Casino-bg-congrat-bg-money-win.png",
                "cc.Texture2D"
            ],
            "62ogOr4vRMg7stsj5q+GsJ": [
                "4_gameScene/4_Slot4/minigame/Casino-bg-congrat-txt-coin",
                "cc.SpriteFrame",
                1
            ],
            "3a9TIdMulJYoigKgxMq5Ri": [
                "4_gameScene/4_Slot4/minigame/Casino-bg-congrat-txt-coin.png",
                "cc.Texture2D"
            ],
            dfPLDlythIb5l87f5VowFl: [
                "4_gameScene/4_Slot4/minigame/Casino-bg-congrat-txt-youwin",
                "cc.SpriteFrame",
                1
            ],
            "8c8mIQMQxJy4VTGr7Za0yz": [
                "4_gameScene/4_Slot4/minigame/Casino-bg-congrat-txt-youwin.png",
                "cc.Texture2D"
            ],
            fciufAWkJMxpvkxhssOvu8: [
                "4_gameScene/4_Slot4/minigame/bg-big-result",
                "cc.SpriteFrame",
                1
            ],
            "b9cS/l4b1IpZF2L5m7XNGJ": [
                "4_gameScene/4_Slot4/minigame/bg-big-result.png",
                "cc.Texture2D"
            ],
            "7cyL4gmHJPdan1JMjVqnV5": [
                "4_gameScene/4_Slot4/minigame/bonus-coin-2",
                "cc.SpriteFrame",
                1
            ],
            "9cjTeBmi9C1YtjllSGFpal": [
                "4_gameScene/4_Slot4/minigame/bonus-coin-2.jpg",
                "cc.Texture2D"
            ],
            "06XKvnsoNIXKCBVs6dN/G1": [
                "4_gameScene/4_Slot4/minigame/casino-slot-bg-txt-bonus-coin",
                "cc.SpriteFrame",
                1
            ],
            "0bctc7VSdPcbvNyvAwTK09": [
                "4_gameScene/4_Slot4/minigame/casino-slot-bg-txt-bonus-coin.png",
                "cc.Texture2D"
            ],
            "51euBuIRpOmaaXKHVlTXLF": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-bg-bonus-coin",
                "cc.SpriteFrame",
                1
            ],
            "8ftZy5rmZCaoWj89JFEANI": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-bg-bonus-coin.png",
                "cc.Texture2D"
            ],
            "68X1YRuxdCrKfGMkC1ud2L": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-grand",
                "cc.SpriteFrame",
                1
            ],
            "c07R05d9xEPYoiVFHO8G/N": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-grand.png",
                "cc.Texture2D"
            ],
            "77EpvnBXRBV5HAHTgmbO5s": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-major",
                "cc.SpriteFrame",
                1
            ],
            ef9FrFrwJDeoEdjCXSg7MY: [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-major.png",
                "cc.Texture2D"
            ],
            "c8P3pRk/ROT5VqriuBOVqt": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-mini",
                "cc.SpriteFrame",
                1
            ],
            "7aROZK60ZHFKiW+x5MZ4Lo": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-mini.png",
                "cc.Texture2D"
            ],
            "52zRf64RBEs4SYy45F1wKu": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-minor",
                "cc.SpriteFrame",
                1
            ],
            "fe4Gy/0sBNf7Bnj31xubM0": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-minor.png",
                "cc.Texture2D"
            ],
            "66xpYPrslJPqY/2Mug2jyB": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-normal",
                "cc.SpriteFrame",
                1
            ],
            "25r9y9BN1L+pUyCl5Hld41": [
                "4_gameScene/4_Slot4/minigame/casino-slot-coin-normal.png",
                "cc.Texture2D"
            ],
            "8c3aJRv/hMa7t9QbsR3d8Z": [
                "4_gameScene/4_Slot4/minigame/popup-bonusgame-casino",
                "cc.SpriteFrame",
                1
            ],
            "6dPWUEB/dLT4bv/6/1duVP": [
                "4_gameScene/4_Slot4/minigame/popup-bonusgame-casino.png",
                "cc.Texture2D"
            ],
            "3bO7GErR9Eiqd/S8BxiKS+": [
                "4_gameScene/5_Slot5/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "472Vhy/eJBuoBWmXtqen98": [
                "4_gameScene/5_Slot5/img/bg-big.png",
                "cc.Texture2D"
            ],
            "f3+gSMaFxNLbHg/LQ5+c9J": [
                "4_gameScene/5_Slot5/img/bg-gem-ingame",
                "cc.SpriteFrame",
                1
            ],
            "98S8nZu99GTL7pws2foX3n": [
                "4_gameScene/5_Slot5/img/bg-gem-ingame.png",
                "cc.Texture2D"
            ],
            "52uYDtzfNMEIT7RzoIDacd": [
                "4_gameScene/5_Slot5/img/gem-bg-number-line",
                "cc.SpriteFrame",
                1
            ],
            "66Y063HdZOLZtqk8qUrLyN": [
                "4_gameScene/5_Slot5/img/gem-bg-number-line.png",
                "cc.Texture2D"
            ],
            "810g3tBR1Fa4U23iAvqDwj": [
                "4_gameScene/5_Slot5/img/item/bg-bonus-gem",
                "cc.SpriteFrame",
                1
            ],
            "76KOC8hBZK7oNfUPRZulyQ": [
                "4_gameScene/5_Slot5/img/item/bg-bonus-gem.png",
                "cc.Texture2D"
            ],
            "ca4qvYSOFGmL6hBvEBgf/9": [
                "4_gameScene/5_Slot5/img/item/bg-scatter-gem",
                "cc.SpriteFrame",
                1
            ],
            "9afARNl1ZJTrNcYUV+3G6z": [
                "4_gameScene/5_Slot5/img/item/bg-scatter-gem.png",
                "cc.Texture2D"
            ],
            "645Ia7NeNNabzHLa7FOszD": [
                "4_gameScene/5_Slot5/img/item/bg-wild-gem",
                "cc.SpriteFrame",
                1
            ],
            "0cW01uCEdOkrN8oEh5wcLJ": [
                "4_gameScene/5_Slot5/img/item/bg-wild-gem.png",
                "cc.Texture2D"
            ],
            "1bqyAq6IhOhZ4FkWEZWK2Y": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-1",
                "cc.SpriteFrame",
                1
            ],
            "82DHLl95lHq4FKsctx5Fnd": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-1.png",
                "cc.Texture2D"
            ],
            b6aZSQE2BBUJb3LsOkaoWQ: [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-2",
                "cc.SpriteFrame",
                1
            ],
            "ef2yFzakRHF7C3Y4Z24uS+": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-2.png",
                "cc.Texture2D"
            ],
            "0bd6zgS6RP5paDQ/TPCqgJ": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-3",
                "cc.SpriteFrame",
                1
            ],
            "adm02n+1NJ06jy9jgHZagJ": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-3.png",
                "cc.Texture2D"
            ],
            "9aJDMG5ppIiYWRPSfV/O7v": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-4",
                "cc.SpriteFrame",
                1
            ],
            "a7pT+PXUhL6LMUsuLCQTpx": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-4.png",
                "cc.Texture2D"
            ],
            "fa/b2bxfVOXI17YHFBZ8BZ": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-5",
                "cc.SpriteFrame",
                1
            ],
            "e5m8VmOahCc5Nik4+gQE7m": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-5.png",
                "cc.Texture2D"
            ],
            "12Jl2ZRENNt57h7ixZPmrd": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-6",
                "cc.SpriteFrame",
                1
            ],
            "9b4e5O0atHFq2BlwQrLfAx": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-6.png",
                "cc.Texture2D"
            ],
            "42ikTrEA5Px4E3lGF9opEG": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-7",
                "cc.SpriteFrame",
                1
            ],
            "11eN7pyIVBDorDdvdFqp39": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-7.png",
                "cc.Texture2D"
            ],
            "5ee9yIdclBr77NRawEuL8S": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-8",
                "cc.SpriteFrame",
                1
            ],
            "7cefBqmUdEgr5ZJHU1LvT7": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-8.png",
                "cc.Texture2D"
            ],
            "88y3qp6ENLxYUG7GmvH8zD": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-9",
                "cc.SpriteFrame",
                1
            ],
            "efSTl/0xZO9adzy5i01fHq": [
                "4_gameScene/5_Slot5/img/item/gem-item-ingame-9.png",
                "cc.Texture2D"
            ],
            darZTNEpVLD5TBqD90eOtq: [
                "4_gameScene/5_Slot5/img/item/item-bonus-gem",
                "cc.SpriteFrame",
                1
            ],
            "1cHYzCPf9ICoaRCfKHwcJL": [
                "4_gameScene/5_Slot5/img/item/item-bonus-gem.png",
                "cc.Texture2D"
            ],
            "efp4UJL4ZIX4Kzaqmsq/1p": [
                "4_gameScene/5_Slot5/img/item/item-scatter-gem",
                "cc.SpriteFrame",
                1
            ],
            "4205AwX5BJBqTcr70i3vF/": [
                "4_gameScene/5_Slot5/img/item/item-scatter-gem.png",
                "cc.Texture2D"
            ],
            "122jSf/mlHL4PsPoXvOF5N": [
                "4_gameScene/5_Slot5/img/item/item-wild-gem",
                "cc.SpriteFrame",
                1
            ],
            "861xiFbphOnY/l0Qy1xZET": [
                "4_gameScene/5_Slot5/img/item/item-wild-gem.png",
                "cc.Texture2D"
            ],
            "51GoSJ0uhKcJ3TuAnJXCl5": [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-bg",
                "cc.SpriteFrame",
                1
            ],
            "f8IrzToSVNK5+V/G22i+VZ": [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-bg.png",
                "cc.Texture2D"
            ],
            ecFxqzuvRBlYkm8848x5D6: [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-bonus-1",
                "cc.SpriteFrame",
                1
            ],
            "5etiYXNUVGQpSDTRNYrpta": [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-bonus-1.png",
                "cc.Texture2D"
            ],
            "61f5lCwJdKJbxpQPbSC/+x": [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-bonus-2",
                "cc.SpriteFrame",
                1
            ],
            "3eyV2YKZxN26JR1mEyYs2B": [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-bonus-2.png",
                "cc.Texture2D"
            ],
            "1c1JF3YJFDwJ3q2Rj2v/RR": [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-txt-title",
                "cc.SpriteFrame",
                1
            ],
            "0bO6ZMbENAtLHWq6AIT+Uu": [
                "4_gameScene/5_Slot5/minigame/gem-minigame-select-txt-title.png",
                "cc.Texture2D"
            ],
            "c0JnJJUJVD/Ibd/G2IKTXA": [
                "4_gameScene/5_Slot5/minigame/gem_bonus_minigame_txt",
                "cc.SpriteFrame",
                1
            ],
            e4yk9oE8lPnqsImXX0B5bZ: [
                "4_gameScene/5_Slot5/minigame/gem_bonus_minigame_txt.png",
                "cc.Texture2D"
            ],
            c10ZJPFQVNKJeuVeZVmTI5: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_bg",
                "cc.SpriteFrame",
                1
            ],
            "fbtCH5A9xJw6y2+N4LS640": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_bg.png",
                "cc.Texture2D"
            ],
            "8dfWzInCpEra14YGUeo00S": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_01",
                "cc.SpriteFrame",
                1
            ],
            efJJoGmJZJPaALj8klFlNY: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_01.png",
                "cc.Texture2D"
            ],
            "7be8TZhQZI9JrA997srxHt": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_02",
                "cc.SpriteFrame",
                1
            ],
            "e8UFe6H/tLxK77sNvMs0iJ": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_02.png",
                "cc.Texture2D"
            ],
            "e2v8oH2GtJN7aI+Elqe8wj": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_03",
                "cc.SpriteFrame",
                1
            ],
            "c2JY31bjpMfLLVfGb+BiDQ": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_03.png",
                "cc.Texture2D"
            ],
            "94WM6N/rFNBYPdhht4ZIm4": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_04",
                "cc.SpriteFrame",
                1
            ],
            "4ar3E+b3NIQq3D1BbkKGPo": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_04.png",
                "cc.Texture2D"
            ],
            "a482WHev1AD5j67vCZvrX/": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_05",
                "cc.SpriteFrame",
                1
            ],
            "a88JHWlVhHUo2h/r2PgjOg": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_05.png",
                "cc.Texture2D"
            ],
            "1dBBcZzSlLsJOswfjzsxSt": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_06",
                "cc.SpriteFrame",
                1
            ],
            "062TEqOH9KMbE6PPMrARRd": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_06.png",
                "cc.Texture2D"
            ],
            b1PEHC1hZApJhmFH1MLRAM: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_07",
                "cc.SpriteFrame",
                1
            ],
            c7rp1t6edD0oT2Gb7uXSeq: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_07.png",
                "cc.Texture2D"
            ],
            ddzjaHzvlFhZ9KkNVwzizW: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_08",
                "cc.SpriteFrame",
                1
            ],
            "099pv/en5Ov7EOuWw4oYV/": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_08.png",
                "cc.Texture2D"
            ],
            "0ezhmkfVlFt6wUqTUPdw6O": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_09",
                "cc.SpriteFrame",
                1
            ],
            "60K7Y9xLpOz6G6ePuHDRVt": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_09.png",
                "cc.Texture2D"
            ],
            "64qUJdrwxBk6qdR9ImlsPp": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_10",
                "cc.SpriteFrame",
                1
            ],
            "6bHmENJudH+r4LylUWz2Zg": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_10.png",
                "cc.Texture2D"
            ],
            dc9PaezVxNSo0g7lrvBEmG: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_11",
                "cc.SpriteFrame",
                1
            ],
            "96X8xrvqRGoKbMD+va2P9j": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_11.png",
                "cc.Texture2D"
            ],
            "3eHXZXsB5OhpSQmjv73+b1": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_12",
                "cc.SpriteFrame",
                1
            ],
            "19byRHGJpMPbTB7infNmIw": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_12.png",
                "cc.Texture2D"
            ],
            "24vYJnIt1OSb5/X13Rd3cd": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_13",
                "cc.SpriteFrame",
                1
            ],
            "5aXf7QfqdNIZtgkwRf1VXD": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_13.png",
                "cc.Texture2D"
            ],
            "08IdKQ0xlAjrhR0AcbsTer": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_14",
                "cc.SpriteFrame",
                1
            ],
            "174zjcIABOBbezzEa6cwri": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_14.png",
                "cc.Texture2D"
            ],
            fdKpoC0JdHDb1QzHPSCK1M: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_15",
                "cc.SpriteFrame",
                1
            ],
            "51bCGvbDBOgYlNao1aJs9p": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_15.png",
                "cc.Texture2D"
            ],
            "993dlUOdlJuYgpEMP0UsS0": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_fail_01",
                "cc.SpriteFrame",
                1
            ],
            "ebPQl+DjdATqUTdf7CJGce": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_fail_01.png",
                "cc.Texture2D"
            ],
            "39SqZ0WVdCfrmHGL/CuMsn": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_fail_02",
                "cc.SpriteFrame",
                1
            ],
            b8C0ozERhB6aBOE7EqKV7B: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_fail_02.png",
                "cc.Texture2D"
            ],
            "felX2q/UNEHpGzg5NstdQ2": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_total",
                "cc.SpriteFrame",
                1
            ],
            b8SG93lmFDpIkc0Q6WNnCd: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_total.png",
                "cc.Texture2D"
            ],
            "28pzHOA2tAbozLv965Qsne": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_01",
                "cc.SpriteFrame",
                1
            ],
            "42Z6JA3iRCXLvzf9da+mAU": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_01.png",
                "cc.Texture2D"
            ],
            "21pu5FZslB2rTLuSd97Q3Q": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_02",
                "cc.SpriteFrame",
                1
            ],
            "aeu+GvjvdDDoyJvSX1f8Ce": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_02.png",
                "cc.Texture2D"
            ],
            a0KhYqvH1McqqIf8jVxbri: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_03",
                "cc.SpriteFrame",
                1
            ],
            "b2r+x6qJJC/bpO4NTF5Ip7": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_03.png",
                "cc.Texture2D"
            ],
            "3dcFUhm0BPqIh8gjmBgfYX": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_04",
                "cc.SpriteFrame",
                1
            ],
            "32xWOJEeRLfqO2zxRFPmmm": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_04.png",
                "cc.Texture2D"
            ],
            "f65eXnCAtBwIXZQoVs/nwh": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_05",
                "cc.SpriteFrame",
                1
            ],
            "4dQtoV/gJNsLRSktyYY9e9": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_05.png",
                "cc.Texture2D"
            ],
            "61HIjLqqNG97KXkL8CVKPu": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_06",
                "cc.SpriteFrame",
                1
            ],
            "41PV/pM69IMYLD36hooeM5": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_06.png",
                "cc.Texture2D"
            ],
            "03eAWnwPZAfYSdkgGBCHn/": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_07",
                "cc.SpriteFrame",
                1
            ],
            e9IO69j4NB5IQnK1Kf8omN: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_07.png",
                "cc.Texture2D"
            ],
            "3a+yrvTNJO0IhKyalkpK22": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_08",
                "cc.SpriteFrame",
                1
            ],
            f959Y9p3ZPYZVyi5F83ABb: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_08.png",
                "cc.Texture2D"
            ],
            "2aRDjQV1ZOX5b0tBubIHNv": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_09",
                "cc.SpriteFrame",
                1
            ],
            "49nlgrpKVBvpd9FeAc2Um2": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_09.png",
                "cc.Texture2D"
            ],
            "ecrV/UoptPWJQCicFuJowk": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_10",
                "cc.SpriteFrame",
                1
            ],
            "96nQv9EUBLIpGUgCdk8tzo": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_10.png",
                "cc.Texture2D"
            ],
            "b5e2Ly/MlOibBpDz7qs/A/": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_11",
                "cc.SpriteFrame",
                1
            ],
            bcfPXzPwZI2bTJMvmsTBBa: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_11.png",
                "cc.Texture2D"
            ],
            d0TPNqGF1Ku6Mw4VjR4BHt: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_12",
                "cc.SpriteFrame",
                1
            ],
            a5cBSlYnpA3bJ4j2t10Cpe: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_12.png",
                "cc.Texture2D"
            ],
            d7p1wkHaRH2ILX7N3stox7: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_13",
                "cc.SpriteFrame",
                1
            ],
            c6gJ9zuSpOFJRsPeGCNHly: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_13.png",
                "cc.Texture2D"
            ],
            "feAm53sk5LmqaPJbB+iTK5": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_14",
                "cc.SpriteFrame",
                1
            ],
            "861DyqXLJMOpJzuujY9EEz": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_14.png",
                "cc.Texture2D"
            ],
            c76WAFUH9DBo74fsPas89w: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_15",
                "cc.SpriteFrame",
                1
            ],
            "3dnws6gztLL6LLmeqAiZyF": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_15.png",
                "cc.Texture2D"
            ],
            "1d6id+xfBN2qbmfaFz24Te": [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_fail",
                "cc.SpriteFrame",
                1
            ],
            ecIY4l6NFB5LZT6E7fhAQt: [
                "4_gameScene/5_Slot5/minigame/gem_minigame_bonus_item_white_fail.png",
                "cc.Texture2D"
            ],
            "2aRp26BYxKS4zymQ/T14Nr": [
                "4_gameScene/6_Slot6/audio/Amazing.ogg",
                "cc.AudioClip"
            ],
            "15qqsaqxpKp56H49vf1QLp": [
                "4_gameScene/6_Slot6/audio/Awesome.ogg",
                "cc.AudioClip"
            ],
            "dbTa6e+YRGxawND6EhZgDU": [
                "4_gameScene/6_Slot6/audio/Excellent.ogg",
                "cc.AudioClip"
            ],
            "b1/wJKUHZPJq3ELedGbYGO": [
                "4_gameScene/6_Slot6/audio/Nice.ogg",
                "cc.AudioClip"
            ],
            b4D0gp38pBcqKaSyamzV4r: [
                "4_gameScene/6_Slot6/audio/Remarkable.ogg",
                "cc.AudioClip"
            ],
            "25nZecWYxGz6jqNNo4kYJe": [
                "4_gameScene/6_Slot6/audio/xinfu_bang_up0.ogg",
                "cc.AudioClip"
            ],
            cbc6WQbn5HzqO17YKfoAlP: [
                "4_gameScene/6_Slot6/audio/xinfu_bang_up1.ogg",
                "cc.AudioClip"
            ],
            cbWAAxVsRMrZT2ohAVGqqQ: [
                "4_gameScene/6_Slot6/audio/xinfu_bang_up2.ogg",
                "cc.AudioClip"
            ],
            "0cqupsVZ1OqpXGIx2fDM0E": [
                "4_gameScene/6_Slot6/audio/xinfu_free_spins_pop_up.ogg",
                "cc.AudioClip"
            ],
            "1coXUChOtPkqnVJ3aVaJJy": [
                "4_gameScene/6_Slot6/audio/xinfu_intro_loop.ogg",
                "cc.AudioClip"
            ],
            a0Gx96mlZG35FlUPBVsHpk: [
                "4_gameScene/6_Slot6/audio/xinfu_scatter_stop_3.ogg",
                "cc.AudioClip"
            ],
            "82AQmtG5RMLIkpJj2er0/m": [
                "4_gameScene/6_Slot6/audio/xinfu_welcome.ogg",
                "cc.AudioClip"
            ],
            dcbsmLiolFcaGkOeRBQBku: [
                "4_gameScene/6_Slot6/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "66tQ4+hBBMAZi1ir/366uc": [
                "4_gameScene/6_Slot6/img/bg-big.png",
                "cc.Texture2D"
            ],
            "5akPNrdnRD0Z9Ss8QTG/zV": [
                "4_gameScene/6_Slot6/img/bg_game_02",
                "cc.SpriteFrame",
                1
            ],
            "57pzRxz0tDvJ3P7vs6x6SV": [
                "4_gameScene/6_Slot6/img/bg_game_02.png",
                "cc.Texture2D"
            ],
            "1aPIMrqRFEP7gTGN2OPSVj": [
                "4_gameScene/6_Slot6/img/bonus_game_bg",
                "cc.SpriteFrame",
                1
            ],
            "38TPDYYhBHDYip2Mqloxni": [
                "4_gameScene/6_Slot6/img/bonus_game_bg.png",
                "cc.Texture2D"
            ],
            "779dAa57ROUpdSJr6TJNBR": [
                "4_gameScene/6_Slot6/img/number_spin_bg",
                "cc.SpriteFrame",
                1
            ],
            d6QlVPbd5K3KRJFiVaYYt8: [
                "4_gameScene/6_Slot6/img/number_spin_bg.png",
                "cc.Texture2D"
            ],
            "28WmjVrwhNlJQzra/lI/lb": [
                "4_gameScene/6_Slot6/minigame/bg",
                "cc.SpriteFrame",
                1
            ],
            "335TX2VB9M6Y9G56ZJqRQY": [
                "4_gameScene/6_Slot6/minigame/bg.png",
                "cc.Texture2D"
            ],
            "60i0adcj9HnIxztsT32NyB": [
                "4_gameScene/6_Slot6/minigame/black_over",
                "cc.SpriteFrame",
                1
            ],
            "d6wne+kwFJMpd8hjEHoXQ4": [
                "4_gameScene/6_Slot6/minigame/black_over.png",
                "cc.Texture2D"
            ],
            "13h4X8j6dFCbtMS90W5yfj": [
                "4_gameScene/6_Slot6/minigame/circle01",
                "cc.SpriteFrame",
                1
            ],
            "3cEtua8EVAhaWvVYEh//pI": [
                "4_gameScene/6_Slot6/minigame/circle01.png",
                "cc.Texture2D"
            ],
            "98GTw4VxtEGKEDnKgbhE+I": [
                "4_gameScene/6_Slot6/minigame/circle02",
                "cc.SpriteFrame",
                1
            ],
            "93FkcHIaBCjKwzs8/vWgl1": [
                "4_gameScene/6_Slot6/minigame/circle02.png",
                "cc.Texture2D"
            ],
            "16M4UWiuhALaVJYj+OrssP": [
                "4_gameScene/6_Slot6/minigame/circle03",
                "cc.SpriteFrame",
                1
            ],
            "36t6oqY1xNWaysWct2NWHw": [
                "4_gameScene/6_Slot6/minigame/circle03.png",
                "cc.Texture2D"
            ],
            cdKsaBqDRGqIhcijEboZt8: [
                "4_gameScene/6_Slot6/minigame/circle_04",
                "cc.SpriteFrame",
                1
            ],
            "56W/NXvxxMUr1hHlRi+vTr": [
                "4_gameScene/6_Slot6/minigame/circle_04.png",
                "cc.Texture2D"
            ],
            "3flIdZx9xEAICC2ESoUbEo": [
                "4_gameScene/6_Slot6/minigame/daisen",
                "cc.SpriteFrame",
                1
            ],
            "4fkfwnV5tLzKSXq1PjLl4j": [
                "4_gameScene/6_Slot6/minigame/daisen.png",
                "cc.Texture2D"
            ],
            "97xH/GLYVCq7liddQN49fr": [
                "4_gameScene/6_Slot6/minigame/kilan",
                "cc.SpriteFrame",
                1
            ],
            "9c0meRgEhA3ag+/yP+TQOf": [
                "4_gameScene/6_Slot6/minigame/kilan.png",
                "cc.Texture2D"
            ],
            "51PQRvFH9PipmBstGkbP8C": [
                "4_gameScene/6_Slot6/minigame/light03_spin",
                "cc.SpriteFrame",
                1
            ],
            "a1yKIORCBC+Kamx0tz7JLI": [
                "4_gameScene/6_Slot6/minigame/light03_spin.png",
                "cc.Texture2D"
            ],
            e7BaE3ToJBkL93lTrB3jmO: [
                "4_gameScene/6_Slot6/minigame/light_circle03",
                "cc.SpriteFrame",
                1
            ],
            "1bKzbQMMZHBZ8vaTtkvR7j": [
                "4_gameScene/6_Slot6/minigame/light_circle03.png",
                "cc.Texture2D"
            ],
            "8apboBNPRJcp4M+e883OUJ": [
                "4_gameScene/6_Slot6/minigame/spin_black",
                "cc.SpriteFrame",
                1
            ],
            f1I4NisgtIn5aUPpM5keII: [
                "4_gameScene/6_Slot6/minigame/spin_black.png",
                "cc.Texture2D"
            ],
            a9NV7daUFAZbZXBSq13TN2: [
                "4_gameScene/6_Slot6/minigame/spin_color",
                "cc.SpriteFrame",
                1
            ],
            "70iWNpyQ9EQbvHdMqKCIv5": [
                "4_gameScene/6_Slot6/minigame/spin_color.png",
                "cc.Texture2D"
            ],
            c3WvFPl8tKM6P0zGfvrNyf: [
                "4_gameScene/6_Slot6/minigame/tree",
                "cc.SpriteFrame",
                1
            ],
            "fe6G5O/SJLQZ5g2RDJvoQs": [
                "4_gameScene/6_Slot6/minigame/tree.png",
                "cc.Texture2D"
            ],
            f4jmbbdaNAUabJrq1qD4RS: [
                "4_gameScene/6_Slot6/minigame/tree_02",
                "cc.SpriteFrame",
                1
            ],
            "9e9Xvcu2lHmouqd4S81MX9": [
                "4_gameScene/6_Slot6/minigame/tree_02.png",
                "cc.Texture2D"
            ],
            "55dJDQ7k5AsL3k311Mjd+F": [
                "4_gameScene/6_Slot6/skeleton/item_spine/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "1bub/r9bZGWLanPujHfONC": [
                "4_gameScene/6_Slot6/skeleton/item_spine/skeleton.atlas",
                "cc.Asset"
            ],
            "80tmjdgj1CUYAXZ7il5Qe9": [
                "4_gameScene/6_Slot6/skeleton/item_spine/skeleton.json",
                "sp.SkeletonData"
            ],
            "50YVyplg1AqrWx8/6kQAjU": [
                "4_gameScene/6_Slot6/skeleton/item_spine/skeleton.png",
                "cc.Texture2D"
            ],
            "85f2/2Iw9HYpBJ9IUh4nDW": [
                "4_gameScene/7_Slot7/img/active_reel",
                "cc.SpriteFrame",
                1
            ],
            "3885qT1whFMYvSd7eqSYA4": [
                "4_gameScene/7_Slot7/img/active_reel.png",
                "cc.Texture2D"
            ],
            "03d2FMla1Mj724iSbTjbgN": [
                "4_gameScene/7_Slot7/img/active_reels",
                "cc.SpriteFrame",
                1
            ],
            "1c4S5zXiVHxropIcxO9Kme": [
                "4_gameScene/7_Slot7/img/active_reels.png",
                "cc.Texture2D"
            ],
            "34Gfd3cCZCP7TFLVbSjKO5": [
                "4_gameScene/7_Slot7/img/active_reels_all",
                "cc.SpriteFrame",
                1
            ],
            "21VBXkzyBC/Y/Q5/Mw/ifr": [
                "4_gameScene/7_Slot7/img/active_reels_all.png",
                "cc.Texture2D"
            ],
            fa6Osxo1dIr6dCBxRRjpwJ: [
                "4_gameScene/7_Slot7/img/backtogame_center",
                "cc.SpriteFrame",
                1
            ],
            "5ed9jNycpOqJhSz+tLwTOy": [
                "4_gameScene/7_Slot7/img/backtogame_center.png",
                "cc.Texture2D"
            ],
            c43hg7cBFN6abOxbPRP97l: [
                "4_gameScene/7_Slot7/img/backtogame_left",
                "cc.SpriteFrame",
                1
            ],
            "94wdCm3UxDt5Cpsxm7lgrC": [
                "4_gameScene/7_Slot7/img/backtogame_left.png",
                "cc.Texture2D"
            ],
            "e7LWctIhJOxZZn8ll5BD+V": [
                "4_gameScene/7_Slot7/img/backtogame_right",
                "cc.SpriteFrame",
                1
            ],
            "65DohtKG5IlaMfZQtSrAr1": [
                "4_gameScene/7_Slot7/img/backtogame_right.png",
                "cc.Texture2D"
            ],
            bc5WFnw29EHK0GGCag3Qfy: [
                "4_gameScene/7_Slot7/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "7fEJTPKchPcbWRBU5HZksS": [
                "4_gameScene/7_Slot7/img/bg-big.png",
                "cc.Texture2D"
            ],
            "0dfrhcu+hE1Ypzo/YHSN7v": [
                "4_gameScene/7_Slot7/img/bg_payTabel",
                "cc.SpriteFrame",
                1
            ],
            e93hROETtEO77olSEfsfTz: [
                "4_gameScene/7_Slot7/img/bg_payTabel.png",
                "cc.Texture2D"
            ],
            "0dhE661pJNT5BT1eq9SCj0": [
                "4_gameScene/7_Slot7/img/color_reward",
                "cc.SpriteFrame",
                1
            ],
            "e0geupMCBM0JaKo+VzSPjW": [
                "4_gameScene/7_Slot7/img/color_reward.png",
                "cc.Texture2D"
            ],
            b2eFmp96JHAoTSTHb8Z9Qp: [
                "4_gameScene/7_Slot7/img/item/active_reels_button_01",
                "cc.SpriteFrame",
                1
            ],
            "d7de9+YDlHoqXX5EgA0gOQ": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_01.png",
                "cc.Texture2D"
            ],
            "7ftG/FU6ZHBbBw6ZVLH1RB": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_02",
                "cc.SpriteFrame",
                1
            ],
            "23EOrVrZBK2IKksmj9B31c": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_02.png",
                "cc.Texture2D"
            ],
            "8aJf/jQz9Lkruig6YZBo1t": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_03",
                "cc.SpriteFrame",
                1
            ],
            "991KNwbidM36glccJQHUsT": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_03.png",
                "cc.Texture2D"
            ],
            "6dVL7OYHNKI54RORk69LRT": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_04",
                "cc.SpriteFrame",
                1
            ],
            fc8nPBErBCRY7WYSFUD6Pc: [
                "4_gameScene/7_Slot7/img/item/active_reels_button_04.png",
                "cc.Texture2D"
            ],
            "23rEEvaTpD9pxjOICBesAN": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_05",
                "cc.SpriteFrame",
                1
            ],
            "160u0u2MlNYYXVAjouq++p": [
                "4_gameScene/7_Slot7/img/item/active_reels_button_05.png",
                "cc.Texture2D"
            ],
            "22jNRTkrhCfoKKGkFn4taX": [
                "4_gameScene/7_Slot7/img/item/free_spins_txt",
                "cc.SpriteFrame",
                1
            ],
            "37nUtLOXZB1I9L8XgzvMNh": [
                "4_gameScene/7_Slot7/img/item/free_spins_txt.png",
                "cc.Texture2D"
            ],
            ab7KDetc9Ab6AoeZJMxWjQ: [
                "4_gameScene/7_Slot7/img/item/khung_reels_1",
                "cc.SpriteFrame",
                1
            ],
            "97YtBto0lJepAWlngnG/O7": [
                "4_gameScene/7_Slot7/img/item/khung_reels_1.png",
                "cc.Texture2D"
            ],
            b2gO0MvP1PT71STwvSK5tk: [
                "4_gameScene/7_Slot7/img/item/khung_reels_2",
                "cc.SpriteFrame",
                1
            ],
            "85aH/wkVhC9rFCzdUpBsY0": [
                "4_gameScene/7_Slot7/img/item/khung_reels_2.png",
                "cc.Texture2D"
            ],
            "0eE88cHI5HM6+mqTDzHoOm": [
                "4_gameScene/7_Slot7/img/item/khung_reels_3",
                "cc.SpriteFrame",
                1
            ],
            f7dsu7NRFGqLOoLKitXgND: [
                "4_gameScene/7_Slot7/img/item/khung_reels_3.png",
                "cc.Texture2D"
            ],
            "9dCGt2BghMNpIIduGFEOtI": [
                "4_gameScene/7_Slot7/img/item/khung_reels_4",
                "cc.SpriteFrame",
                1
            ],
            "65I+9UgKVAE68pim5gYYRj": [
                "4_gameScene/7_Slot7/img/item/khung_reels_4.png",
                "cc.Texture2D"
            ],
            "09vvpy1FFPKaKepkVAmHAm": [
                "4_gameScene/7_Slot7/img/item/khung_reels_5",
                "cc.SpriteFrame",
                1
            ],
            aawxYuPyhORo8L0Dr9GigC: [
                "4_gameScene/7_Slot7/img/item/khung_reels_5.png",
                "cc.Texture2D"
            ],
            "06IIuY31BNiZi1fcgqPl1S": [
                "4_gameScene/7_Slot7/img/item/major",
                "cc.SpriteFrame",
                1
            ],
            "66+Wn7NCxFh6B81jW7T4Kh": [
                "4_gameScene/7_Slot7/img/item/major.png",
                "cc.Texture2D"
            ],
            "a3Kx7gPRdP5IDiaJZiU8N+": [
                "4_gameScene/7_Slot7/img/item/mega",
                "cc.SpriteFrame",
                1
            ],
            "d1xQtc58pASI57j3yNT/gv": [
                "4_gameScene/7_Slot7/img/item/mega.png",
                "cc.Texture2D"
            ],
            "8e2qKmWnpLXKt3Me1HLb1F": [
                "4_gameScene/7_Slot7/img/item/mini",
                "cc.SpriteFrame",
                1
            ],
            "82bW7qKbVN8aI4UfH9ELYO": [
                "4_gameScene/7_Slot7/img/item/mini.png",
                "cc.Texture2D"
            ],
            "47PVj9iRpE7qFgImUAyrHH": [
                "4_gameScene/7_Slot7/img/item/minor",
                "cc.SpriteFrame",
                1
            ],
            "448UI0NU1AyKgDQkTbUmtl": [
                "4_gameScene/7_Slot7/img/item/minor.png",
                "cc.Texture2D"
            ],
            "2biY1acPVOeqUic1f4XUmH": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_1",
                "cc.SpriteFrame",
                1
            ],
            "e7zgwuDH1DiaQMzvkvu7I/": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_1.png",
                "cc.Texture2D"
            ],
            "51Ufis8iVImJm9gge5XAuU": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_2",
                "cc.SpriteFrame",
                1
            ],
            "b8+UuYZpVGdIe3tOsdqp2e": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_2.png",
                "cc.Texture2D"
            ],
            "17Kwr8BstC1oaeVB3WakZG": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_3",
                "cc.SpriteFrame",
                1
            ],
            a4c0p7afZLZYsfz7RxaJhj: [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_3.png",
                "cc.Texture2D"
            ],
            "0caLmEMuJM+p3awnjm3vR1": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_4",
                "cc.SpriteFrame",
                1
            ],
            "37Z04wGANDmLUh6DUidOlR": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_4.png",
                "cc.Texture2D"
            ],
            "80Pu/qiclIfq7wNZnEGvph": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_5",
                "cc.SpriteFrame",
                1
            ],
            "39QeQn93tJmpH40G2QwOoq": [
                "4_gameScene/7_Slot7/img/item/power_reels_lock_5.png",
                "cc.Texture2D"
            ],
            "92Cp/O6u1HVIX//kwtw4Zv": [
                "4_gameScene/7_Slot7/img/item/rules_txt",
                "cc.SpriteFrame",
                1
            ],
            "44N4gHH3lE25lGr+XW9ut0": [
                "4_gameScene/7_Slot7/img/item/rules_txt.png",
                "cc.Texture2D"
            ],
            a5d64WiuVCVI2l7FKiLLdk: [
                "4_gameScene/7_Slot7/img/item/scatter-shields",
                "cc.SpriteFrame",
                1
            ],
            "15fpgoyn5GRKyejXuwLg1X": [
                "4_gameScene/7_Slot7/img/item/scatter-shields.png",
                "cc.Texture2D"
            ],
            "36IT6vEBJC+6KFDgJ9vDAF": [
                "4_gameScene/7_Slot7/img/item/scatter_shields_txt",
                "cc.SpriteFrame",
                1
            ],
            "2bBtffXNJG8ZeKAZjWbchM": [
                "4_gameScene/7_Slot7/img/item/scatter_shields_txt.png",
                "cc.Texture2D"
            ],
            "416EUiUS1H+JztJ4u3bY1p": [
                "4_gameScene/7_Slot7/img/item/title_free_spins",
                "cc.SpriteFrame",
                1
            ],
            "06V5EDyxtFZ7fvYR55BB1n": [
                "4_gameScene/7_Slot7/img/item/title_free_spins.png",
                "cc.Texture2D"
            ],
            "f0vgi7AYhKj7R+LCy6fliW": [
                "4_gameScene/7_Slot7/img/item/title_rules",
                "cc.SpriteFrame",
                1
            ],
            b7tdOrfoJKfaqoev4Y2y5c: [
                "4_gameScene/7_Slot7/img/item/title_rules.png",
                "cc.Texture2D"
            ],
            "5dHCiCI95ECrnx+Nurqb4F": [
                "4_gameScene/7_Slot7/img/item/title_wheel_spins",
                "cc.SpriteFrame",
                1
            ],
            "84cIXtauxCM44Nbp3AYcwn": [
                "4_gameScene/7_Slot7/img/item/title_wheel_spins.png",
                "cc.Texture2D"
            ],
            "15o0BDNYZCmpB28hNVbUMh": [
                "4_gameScene/7_Slot7/img/item/wheel_spins_txt",
                "cc.SpriteFrame",
                1
            ],
            cahFjQK4pIfrmbWK73lzjr: [
                "4_gameScene/7_Slot7/img/item/wheel_spins_txt.png",
                "cc.Texture2D"
            ],
            "a6///OxTNGrLiypwLPhYek": [
                "4_gameScene/7_Slot7/img/item_khung_bottom",
                "cc.SpriteFrame",
                1
            ],
            "c8DhfNUFBDwovDZl+ew4IJ": [
                "4_gameScene/7_Slot7/img/item_khung_bottom.png",
                "cc.Texture2D"
            ],
            "24Umr8ZD1DYoXiQb50bSZe": [
                "4_gameScene/7_Slot7/img/item_khung_top",
                "cc.SpriteFrame",
                1
            ],
            "40qIfx7AxDKqdvYhnvWmNi": [
                "4_gameScene/7_Slot7/img/item_khung_top.png",
                "cc.Texture2D"
            ],
            "61xhYRayJBkISTH+I/7p0a": [
                "4_gameScene/7_Slot7/img/khung",
                "cc.SpriteFrame",
                1
            ],
            "78UD5J7LpIcqIqvRFqGOQP": [
                "4_gameScene/7_Slot7/img/khung.png",
                "cc.Texture2D"
            ],
            "3cFiVnbzlFvaStT/fyfvP9": [
                "4_gameScene/7_Slot7/img/khung_bg",
                "cc.SpriteFrame",
                1
            ],
            "f3aP+Sr7BL/JY5pxGgP/+e": [
                "4_gameScene/7_Slot7/img/khung_bg.png",
                "cc.Texture2D"
            ],
            "64opHyH+pFWYRn810RJVvB": [
                "4_gameScene/7_Slot7/img/lock_reels",
                "cc.SpriteFrame",
                1
            ],
            "e6eVkTOqBOs79R+xQh3OEd": [
                "4_gameScene/7_Slot7/img/lock_reels.png",
                "cc.Texture2D"
            ],
            "96rFm2SctEqqskrPTDT4dh": [
                "4_gameScene/7_Slot7/img/overlay",
                "cc.SpriteFrame",
                1
            ],
            "378b4ytMBAVbaHUdtirijr": [
                "4_gameScene/7_Slot7/img/overlay.png",
                "cc.Texture2D"
            ],
            "9b6mJ8d0ZCdrZ/2M3GSu4j": [
                "4_gameScene/7_Slot7/img/review",
                "cc.SpriteFrame",
                1
            ],
            ee3drL0CdCZ72SLSfgg18i: [
                "4_gameScene/7_Slot7/img/review.png",
                "cc.Texture2D"
            ],
            "89qPvTqkNJSYpeX6uvxn37": [
                "4_gameScene/7_Slot7/img/reward",
                "cc.SpriteFrame",
                1
            ],
            "e2Azw+DUhOW6vH7ualHaDW": [
                "4_gameScene/7_Slot7/img/reward.png",
                "cc.Texture2D"
            ],
            "75DhAQrBBJCrKyT8PLs7WD": [
                "4_gameScene/7_Slot7/minigame/12_freespins_bonus",
                "cc.SpriteFrame",
                1
            ],
            c3QUqPRstKY6hgddeulkI4: [
                "4_gameScene/7_Slot7/minigame/12_freespins_bonus.png",
                "cc.Texture2D"
            ],
            "27LdMT1etOaL/ZngpeT6SI": [
                "4_gameScene/7_Slot7/minigame/8_freespins_bonus",
                "cc.SpriteFrame",
                1
            ],
            "b46IWLyGxDZqT/XOaiixyr": [
                "4_gameScene/7_Slot7/minigame/8_freespins_bonus.png",
                "cc.Texture2D"
            ],
            "2bPjhB9oBDE4bFDL1aRoKH": [
                "4_gameScene/7_Slot7/minigame/alpha_2",
                "cc.SpriteFrame",
                1
            ],
            "29QfFb3dFFXKXTrSLQwS9F": [
                "4_gameScene/7_Slot7/minigame/alpha_2.png",
                "cc.Texture2D"
            ],
            "55+IVL1QVLkKA8H7bQLRHl": [
                "4_gameScene/7_Slot7/minigame/bg",
                "cc.SpriteFrame",
                1
            ],
            "72m2Z/dXpATb88s3mpA+xh": [
                "4_gameScene/7_Slot7/minigame/bg.png",
                "cc.Texture2D"
            ],
            "750Vb0xAdEe5nWSpV33blc": [
                "4_gameScene/7_Slot7/minigame/button",
                "cc.SpriteFrame",
                1
            ],
            "76dnHQsolCfKgcsBVIaQBL": [
                "4_gameScene/7_Slot7/minigame/button.png",
                "cc.Texture2D"
            ],
            "42nZEta6VPxrRf97kX4fjv": [
                "4_gameScene/7_Slot7/minigame/button_false",
                "cc.SpriteFrame",
                1
            ],
            f9JLf0QchPDZrFeQNZitP2: [
                "4_gameScene/7_Slot7/minigame/button_false.png",
                "cc.Texture2D"
            ],
            "8780/udWlL7YVfgM5MXM0Z": [
                "4_gameScene/7_Slot7/minigame/free_spin_txt",
                "cc.SpriteFrame",
                1
            ],
            "f7/VTUJGhH5aAJse1g43co": [
                "4_gameScene/7_Slot7/minigame/free_spin_txt.png",
                "cc.Texture2D"
            ],
            f8XE2aC1dDeL9zvchybgoD: [
                "4_gameScene/7_Slot7/minigame/freespins_bonus_bg",
                "cc.SpriteFrame",
                1
            ],
            "264e5JEwJEU4EytJdpjehU": [
                "4_gameScene/7_Slot7/minigame/freespins_bonus_bg.png",
                "cc.Texture2D"
            ],
            "df6+d+yCZAPZOjcS8Yd0R0": [
                "4_gameScene/7_Slot7/minigame/item_khung_bottom_start",
                "cc.SpriteFrame",
                1
            ],
            "ebK5HZZiFA4qNL/9d/KVf8": [
                "4_gameScene/7_Slot7/minigame/item_khung_bottom_start.png",
                "cc.Texture2D"
            ],
            dcXDluMnJO8oBmgb59XdV3: [
                "4_gameScene/7_Slot7/minigame/jackpots_fs",
                "cc.SpriteFrame",
                1
            ],
            d9MwJ6is9AP5GqK0RudHwc: [
                "4_gameScene/7_Slot7/minigame/jackpots_fs.png",
                "cc.Texture2D"
            ],
            "40vonGPGdIy5WWMoNEgrjb": [
                "4_gameScene/7_Slot7/minigame/khien",
                "cc.SpriteFrame",
                1
            ],
            a1Q5hvBt9DxKswNkfdvCam: [
                "4_gameScene/7_Slot7/minigame/khien.png",
                "cc.Texture2D"
            ],
            a2HUGgysZMzJ8pFKtJ9Q2Z: [
                "4_gameScene/7_Slot7/minigame/khung",
                "cc.SpriteFrame",
                1
            ],
            "63M3WH53pPjpfBpReXS32k": [
                "4_gameScene/7_Slot7/minigame/khung.png",
                "cc.Texture2D"
            ],
            "9bL8ZriitKCKh1pzjnd4Zq": [
                "4_gameScene/7_Slot7/minigame/major",
                "cc.SpriteFrame",
                1
            ],
            "1dg0PkQktByZenLynQKouQ": [
                "4_gameScene/7_Slot7/minigame/major.png",
                "cc.Texture2D"
            ],
            d6TPUUJiZIJbV1yotAKwQm: [
                "4_gameScene/7_Slot7/minigame/mang_bang",
                "cc.SpriteFrame",
                1
            ],
            "34i0qPJKhM5r03NXOKi2aI": [
                "4_gameScene/7_Slot7/minigame/mang_bang.png",
                "cc.Texture2D"
            ],
            b5eig3hs9NUJexXNgtoQ5J: [
                "4_gameScene/7_Slot7/minigame/mega",
                "cc.SpriteFrame",
                1
            ],
            "afR4PODzhF8qdlzci5WIV/": [
                "4_gameScene/7_Slot7/minigame/mega.png",
                "cc.Texture2D"
            ],
            "6469IkdzdE4IYcE0Co+XZM": [
                "4_gameScene/7_Slot7/minigame/mini",
                "cc.SpriteFrame",
                1
            ],
            f5Ac28kNtAWa1MxHvCxI6K: [
                "4_gameScene/7_Slot7/minigame/mini.png",
                "cc.Texture2D"
            ],
            a6E3YqjIRIDaX6vlcZKl4V: [
                "4_gameScene/7_Slot7/minigame/minor",
                "cc.SpriteFrame",
                1
            ],
            "6fH+FXDa1J1ZSnXT8eF6n7": [
                "4_gameScene/7_Slot7/minigame/minor.png",
                "cc.Texture2D"
            ],
            faYlSSsptBv7Ps87UFA4cP: [
                "4_gameScene/7_Slot7/minigame/nuttrenkhien",
                "cc.SpriteFrame",
                1
            ],
            "03SgskRbJGLpxTQVThpk82": [
                "4_gameScene/7_Slot7/minigame/nuttrenkhien.png",
                "cc.Texture2D"
            ],
            "4bjuR6XNhHVarWukRjI1Mb": [
                "4_gameScene/7_Slot7/minigame/overlay",
                "cc.SpriteFrame",
                1
            ],
            "22XrNMbYRA0LHj5jYvW3oR": [
                "4_gameScene/7_Slot7/minigame/overlay.png",
                "cc.Texture2D"
            ],
            "fdXvOYUoRGE7aPBdsYM+5Q": [
                "4_gameScene/7_Slot7/minigame/reward",
                "cc.SpriteFrame",
                1
            ],
            "81EOCgG7VIlauRlhKR6Axm": [
                "4_gameScene/7_Slot7/minigame/reward.png",
                "cc.Texture2D"
            ],
            "5d9K/E7Z9DmIap0eZ8D5Gy": [
                "4_gameScene/7_Slot7/minigame/start",
                "cc.SpriteFrame",
                1
            ],
            fay42DH8xFRr12kZ0XrQRm: [
                "4_gameScene/7_Slot7/minigame/start.png",
                "cc.Texture2D"
            ],
            b2MMgUbW9BWZhuJhIsetxG: [
                "4_gameScene/7_Slot7/minigame/title_free_spins",
                "cc.SpriteFrame",
                1
            ],
            "33FzkL8WVJw419/IPEvZjR": [
                "4_gameScene/7_Slot7/minigame/title_free_spins.png",
                "cc.Texture2D"
            ],
            "199PrpAPxArYPxm/1XajtE": [
                "4_gameScene/7_Slot7/minigame/total",
                "cc.SpriteFrame",
                1
            ],
            a8o5XQkx9Nb4T0rzCM9P5P: [
                "4_gameScene/7_Slot7/minigame/total.png",
                "cc.Texture2D"
            ],
            "60eP05eEBEkZKPVc3Lyo59": [
                "4_gameScene/7_Slot7/minigame/txt",
                "cc.SpriteFrame",
                1
            ],
            "43JkAppdtLdagpxlGCyMtt": [
                "4_gameScene/7_Slot7/minigame/txt.png",
                "cc.Texture2D"
            ],
            "278eummu5OfJQ5S+KK+yog": [
                "4_gameScene/7_Slot7/minigame/x2_double",
                "cc.SpriteFrame",
                1
            ],
            "d2fv9ug+RIhJmRb34+CpHD": [
                "4_gameScene/7_Slot7/minigame/x2_double.png",
                "cc.Texture2D"
            ],
            "58W5OhNHhDopsW4db0F+5P": [
                "4_gameScene/7_Slot7/skeleton/bird_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "dcNFQ+rAhHIbqkNN9SSu31": [
                "4_gameScene/7_Slot7/skeleton/bird_ani/skeleton.atlas",
                "cc.Asset"
            ],
            "14VXhxSCdC7IHP1T6yD8P/": [
                "4_gameScene/7_Slot7/skeleton/bird_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            d0GkN5vtRCm7M5WhB3RgR8: [
                "4_gameScene/7_Slot7/skeleton/bird_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "71+mFvA69LJJBqKHAf9tMa": [
                "4_gameScene/7_Slot7/skeleton/chem_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "ac+NNZSrVFj5WW42C80B7a": [
                "4_gameScene/7_Slot7/skeleton/chem_ani/skeleton.atlas",
                "cc.Asset"
            ],
            c6OXT1KYlJX4j1vvwIVcyy: [
                "4_gameScene/7_Slot7/skeleton/chem_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            bc5peOZVdCc7OdJNrYwi1r: [
                "4_gameScene/7_Slot7/skeleton/chem_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "aaFr7B80lDdI+QZ6lSCas6": [
                "4_gameScene/7_Slot7/skeleton/cut_item_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "08X7IisPlATLNbXTeHOZtx": [
                "4_gameScene/7_Slot7/skeleton/cut_item_ani/skeleton.atlas",
                "cc.Asset"
            ],
            "25EtKzNTpOnbUCsUqM0hqw": [
                "4_gameScene/7_Slot7/skeleton/cut_item_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            "ec/uij3PBKuq6KWnMRR07s": [
                "4_gameScene/7_Slot7/skeleton/cut_item_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "09bNtaGGxPr6poq15GGrxt": [
                "4_gameScene/7_Slot7/skeleton/khung_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "2eJ5g9jvlMdbmxsHktog8o": [
                "4_gameScene/7_Slot7/skeleton/khung_ani/skeleton.atlas",
                "cc.Asset"
            ],
            ac9Cos42dKYYuTXuRWmRwB: [
                "4_gameScene/7_Slot7/skeleton/khung_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            "45SuvtTZdBdoHdue3IpPVi": [
                "4_gameScene/7_Slot7/skeleton/khung_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "ec6FjYJSpO2JeVM2/N8yCi": [
                "4_gameScene/7_Slot7/skeleton/kiemset_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "76zxsxA/5ENJH+/2GRLo2U": [
                "4_gameScene/7_Slot7/skeleton/kiemset_ani/skeleton.atlas",
                "cc.Asset"
            ],
            f7pCwTQ61ALofNZcKnWVuw: [
                "4_gameScene/7_Slot7/skeleton/kiemset_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            "aaps8oVYdLkIQIue+adxO0": [
                "4_gameScene/7_Slot7/skeleton/kiemset_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "a1ZOh6xVhOYrOZlNV+2fRi": [
                "4_gameScene/7_Slot7/skeleton/locxoay/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "1dvX9lT9NJC7/CFiKR1nKh": [
                "4_gameScene/7_Slot7/skeleton/locxoay/skeleton.atlas",
                "cc.Asset"
            ],
            bcu50XOcdJc4IKOG2NF8SU: [
                "4_gameScene/7_Slot7/skeleton/locxoay/skeleton.json",
                "sp.SkeletonData"
            ],
            "6dgLItBRxGYL7S/Q4C+Frc": [
                "4_gameScene/7_Slot7/skeleton/locxoay/skeleton.png",
                "cc.Texture2D"
            ],
            "17F2DQHHdDc5DwEjzrfswq": [
                "4_gameScene/7_Slot7/skeleton/thunder_active_button/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "74bU/+vmNOz4yFo9TmuRoi": [
                "4_gameScene/7_Slot7/skeleton/thunder_active_button/skeleton.atlas",
                "cc.Asset"
            ],
            "4b0aoQ81VFFbgKTNbNgf7w": [
                "4_gameScene/7_Slot7/skeleton/thunder_active_button/skeleton.json",
                "sp.SkeletonData"
            ],
            "7f0oKSGu9FTrYmu10LUrLb": [
                "4_gameScene/7_Slot7/skeleton/thunder_active_button/skeleton.png",
                "cc.Texture2D"
            ],
            "6d9iRbBvZPboD1s0BhjAM2": [
                "4_gameScene/7_Slot7/skeleton/thunder_button_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "1bXx6nljJGb7SFVjIjHwfN": [
                "4_gameScene/7_Slot7/skeleton/thunder_button_ani/skeleton.atlas",
                "cc.Asset"
            ],
            "47UyOPW6lNFLFwtLDIb/R2": [
                "4_gameScene/7_Slot7/skeleton/thunder_button_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            "7fmBNZNJ5Nz6h1xu4XINqv": [
                "4_gameScene/7_Slot7/skeleton/thunder_button_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "d5ODUIqeVMg7gp+FIc9GTD": [
                "4_gameScene/7_Slot7/skeleton/vongtron2_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "f6qmP+2cdKP4tjbjNSh1h+": [
                "4_gameScene/7_Slot7/skeleton/vongtron2_ani/skeleton.atlas",
                "cc.Asset"
            ],
            "b9d6BpNCdA8KfoGzoxrTn/": [
                "4_gameScene/7_Slot7/skeleton/vongtron2_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            "28BbjPttZGRZ1yhTkl0Atq": [
                "4_gameScene/7_Slot7/skeleton/vongtron2_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "47xN+iOexJf4/DQHt9CX04": [
                "4_gameScene/7_Slot7/skeleton/vontron_ani/skeleton",
                "cc.SpriteFrame",
                1
            ],
            "d7zuoxlfxJnrfwWRan1/t2": [
                "4_gameScene/7_Slot7/skeleton/vontron_ani/skeleton.atlas",
                "cc.Asset"
            ],
            e03W3qIOlFvoYBJTKJpeWC: [
                "4_gameScene/7_Slot7/skeleton/vontron_ani/skeleton.json",
                "sp.SkeletonData"
            ],
            "00ih44kXJFP4bPlUBLyuXA": [
                "4_gameScene/7_Slot7/skeleton/vontron_ani/skeleton.png",
                "cc.Texture2D"
            ],
            "1cF1+LP/dG6o8G5/Zd/zFM": [
                "4_gameScene/8_Slot8/img/backtogame_center",
                "cc.SpriteFrame",
                1
            ],
            "91J0cgCXlAP5S1lwmgQC/P": [
                "4_gameScene/8_Slot8/img/backtogame_center.png",
                "cc.Texture2D"
            ],
            "8fNHRAMPFAxIIP6xTLs5dw": [
                "4_gameScene/8_Slot8/img/backtogame_left",
                "cc.SpriteFrame",
                1
            ],
            "60+AYdT4dLW4OaMw2J/pd3": [
                "4_gameScene/8_Slot8/img/backtogame_left.png",
                "cc.Texture2D"
            ],
            "f6coICEJxDhY+tfZrWYu0g": [
                "4_gameScene/8_Slot8/img/backtogame_right",
                "cc.SpriteFrame",
                1
            ],
            "65SVwfK1xOF7YWyGxacQlK": [
                "4_gameScene/8_Slot8/img/backtogame_right.png",
                "cc.Texture2D"
            ],
            "10nnL6HaRGV4PnXX/O6VYF": [
                "4_gameScene/8_Slot8/img/bg-big",
                "cc.SpriteFrame",
                1
            ],
            "a2uC/IobxG4ZK9MR8JmGqr": [
                "4_gameScene/8_Slot8/img/bg-big.png",
                "cc.Texture2D"
            ],
            "81hBsq8o1DyqOdet+nFEfJ": [
                "4_gameScene/8_Slot8/img/bg_paytable",
                "cc.SpriteFrame",
                1
            ],
            "8f51TzMAtKw5pLPbOI78cX": [
                "4_gameScene/8_Slot8/img/bg_paytable.png",
                "cc.Texture2D"
            ],
            "b4VQcBub9Iopgw1Kyac/5K": [
                "4_gameScene/8_Slot8/img/halloween_review",
                "cc.SpriteFrame",
                1
            ],
            "7eXmzdZT1DM5zO0qK2Q2cq": [
                "4_gameScene/8_Slot8/img/halloween_review.png",
                "cc.Texture2D"
            ],
            "80Vajds4JK55V97W01CdY2": [
                "4_gameScene/8_Slot8/img/item/1",
                "cc.SpriteFrame",
                1
            ],
            "7bvfMXXghKlIE+Dj2bhs8c": [
                "4_gameScene/8_Slot8/img/item/1.png",
                "cc.Texture2D"
            ],
            "29JSwqqbFFSb6+s7X7/Arv": [
                "4_gameScene/8_Slot8/img/item/10",
                "cc.SpriteFrame",
                1
            ],
            "balr336JBC4pl90nALW+q2": [
                "4_gameScene/8_Slot8/img/item/10.png",
                "cc.Texture2D"
            ],
            ea0Acl9yBILpusZfjVBfSz: [
                "4_gameScene/8_Slot8/img/item/2",
                "cc.SpriteFrame",
                1
            ],
            e57owlRLtC76CLSzWHlAvn: [
                "4_gameScene/8_Slot8/img/item/2.png",
                "cc.Texture2D"
            ],
            "43GlsMUEtADoHiC5KU1zMU": [
                "4_gameScene/8_Slot8/img/item/3",
                "cc.SpriteFrame",
                1
            ],
            "c6g/vI7+JDFL9b/KmA1UA2": [
                "4_gameScene/8_Slot8/img/item/3.png",
                "cc.Texture2D"
            ],
            "2bPGNTxclNKrpci5aG1kCx": [
                "4_gameScene/8_Slot8/img/item/4",
                "cc.SpriteFrame",
                1
            ],
            "37m2s5lwZIv5pd1TDwngnG": [
                "4_gameScene/8_Slot8/img/item/4.png",
                "cc.Texture2D"
            ],
            "b20nX9/FdNBoDFcu49PDM0": [
                "4_gameScene/8_Slot8/img/item/bingo",
                "cc.SpriteFrame",
                1
            ],
            "75/UuM3UdJaISwz7F8AtAg": [
                "4_gameScene/8_Slot8/img/item/bingo.png",
                "cc.Texture2D"
            ],
            "39kTwgmOZHE5R5Vf+eBFfJ": [
                "4_gameScene/8_Slot8/img/item/bonus",
                "cc.SpriteFrame",
                1
            ],
            "9bOnDny41IKLc98du0WW8z": [
                "4_gameScene/8_Slot8/img/item/bonus.png",
                "cc.Texture2D"
            ],
            "443TEoavNPqpJBbrOZaova": [
                "4_gameScene/8_Slot8/img/item/daulau",
                "cc.SpriteFrame",
                1
            ],
            "72yi6d2y5BNoBC/q2cbJOm": [
                "4_gameScene/8_Slot8/img/item/daulau.png",
                "cc.Texture2D"
            ],
            "11Ofu4qZxK1YfrHtn6QiHs": [
                "4_gameScene/8_Slot8/img/item/gieng",
                "cc.SpriteFrame",
                1
            ],
            "23BqBOfENOWotyBT98jWIL": [
                "4_gameScene/8_Slot8/img/item/gieng.png",
                "cc.Texture2D"
            ],
            "20UFIX67pNKKvmEggYj4jD": [
                "4_gameScene/8_Slot8/img/item/j",
                "cc.SpriteFrame",
                1
            ],
            "33VAqMcZxMx6rEIxqyAwsk": [
                "4_gameScene/8_Slot8/img/item/j.png",
                "cc.Texture2D"
            ],
            "52443X1J1C/paFAPr6ri/v": [
                "4_gameScene/8_Slot8/img/item/k",
                "cc.SpriteFrame",
                1
            ],
            "5eARtjvEVE/YaSkukdzlYt": [
                "4_gameScene/8_Slot8/img/item/k.png",
                "cc.Texture2D"
            ],
            "22c+QibQ9DVpsaFO+6e7ZO": [
                "4_gameScene/8_Slot8/img/item/noi",
                "cc.SpriteFrame",
                1
            ],
            f4eyNdEPFIIaKCytJvsl9v: [
                "4_gameScene/8_Slot8/img/item/noi.png",
                "cc.Texture2D"
            ],
            "48S9UN3LBEe58kBlDnx/l0": [
                "4_gameScene/8_Slot8/img/item/q",
                "cc.SpriteFrame",
                1
            ],
            "0e3VJaJ9dGQbR0KHw5H/uT": [
                "4_gameScene/8_Slot8/img/item/q.png",
                "cc.Texture2D"
            ],
            "54vr6oAWlOGqssK+YkfGXt": [
                "4_gameScene/8_Slot8/img/item/scatter",
                "cc.SpriteFrame",
                1
            ],
            "64nrzxkzVPnqhQb0JO6Q1O": [
                "4_gameScene/8_Slot8/img/item/scatter.png",
                "cc.Texture2D"
            ],
            e4Jayw9RxBrLTZi0RE3tAo: [
                "4_gameScene/8_Slot8/img/item/wild",
                "cc.SpriteFrame",
                1
            ],
            "97rTWWXO1GxbZt0AVv0ugW": [
                "4_gameScene/8_Slot8/img/item/wild.png",
                "cc.Texture2D"
            ],
            "74cAconLdH2p2a44WFnjBH": [
                "4_gameScene/8_Slot8/img/item/wild_special",
                "cc.SpriteFrame",
                1
            ],
            d6uQ6WQtxFdYVlsja0aCru: [
                "4_gameScene/8_Slot8/img/item/wild_special.png",
                "cc.Texture2D"
            ],
            "60aqATShJEcZ4F7B59jGho": [
                "4_gameScene/8_Slot8/img/khung-bg",
                "cc.SpriteFrame",
                1
            ],
            "c8uMoavoRC4IcbUYfO/Ew9": [
                "4_gameScene/8_Slot8/img/khung-bg.png",
                "cc.Texture2D"
            ],
            f5ZlExiU9Jyawh2g1LKOPE: [
                "4_gameScene/8_Slot8/img/nen_bg",
                "cc.SpriteFrame",
                1
            ],
            "84GFS5HP1N8KH1tU3/Cjow": [
                "4_gameScene/8_Slot8/img/nen_bg.png",
                "cc.Texture2D"
            ],
            "1cg7SgfpxDEbuMbAnSgY+g": [
                "4_gameScene/8_Slot8/img/paytable01_review",
                "cc.SpriteFrame",
                1
            ],
            "0fnjsYXwNG6rmI/OQ+o4Bi": [
                "4_gameScene/8_Slot8/img/paytable01_review.png",
                "cc.Texture2D"
            ],
            bcCvggEq5LiK0qjSzAy0sC: [
                "4_gameScene/8_Slot8/minigame/active_effect_white",
                "cc.SpriteFrame",
                1
            ],
            dcwgdzTxJNUoho4USiekC4: [
                "4_gameScene/8_Slot8/minigame/active_effect_white.png",
                "cc.Texture2D"
            ],
            "9bf5iZh2lPsqSryj22Rv6a": [
                "4_gameScene/8_Slot8/minigame/backtogame_center",
                "cc.SpriteFrame",
                1
            ],
            "0ejBiEdUBGLoq8JoTECwze": [
                "4_gameScene/8_Slot8/minigame/backtogame_center.png",
                "cc.Texture2D"
            ],
            "0fIMl0nbhDL79VphP1WHZj": [
                "4_gameScene/8_Slot8/minigame/bg_effect_win",
                "cc.SpriteFrame",
                1
            ],
            cfABlWWs5KDLMd3Yr3IVeF: [
                "4_gameScene/8_Slot8/minigame/bg_effect_win.png",
                "cc.Texture2D"
            ],
            "8aN8cHIZJElLqq/PPg69ZG": [
                "4_gameScene/8_Slot8/minigame/bg_fail",
                "cc.SpriteFrame",
                1
            ],
            "2cXu5qhQVOtJkTIvUymbMU": [
                "4_gameScene/8_Slot8/minigame/bg_fail.png",
                "cc.Texture2D"
            ],
            "54nV91asFHZLj+HwbjiJZF": [
                "4_gameScene/8_Slot8/minigame/bg_paytable",
                "cc.SpriteFrame",
                1
            ],
            "4bPAYefyFCi5DqWE7tU4jL": [
                "4_gameScene/8_Slot8/minigame/bg_paytable.png",
                "cc.Texture2D"
            ],
            "73gxvySshFBLD+qY9sXRdn": [
                "4_gameScene/8_Slot8/minigame/bg_power_win",
                "cc.SpriteFrame",
                1
            ],
            "30E/yXFzpI34U23ruYkByK": [
                "4_gameScene/8_Slot8/minigame/bg_power_win-02",
                "cc.SpriteFrame",
                1
            ],
            "ebkTuxm8pP55/AAeWUgXjY": [
                "4_gameScene/8_Slot8/minigame/bg_power_win-02.png",
                "cc.Texture2D"
            ],
            "62RmY+cyxCRZiG+dYyoPcc": [
                "4_gameScene/8_Slot8/minigame/bg_power_win.png",
                "cc.Texture2D"
            ],
            "94r/yUm9BLXLQkc8V3MAMv": [
                "4_gameScene/8_Slot8/minigame/bonus_active",
                "cc.SpriteFrame",
                1
            ],
            "6b0IoVSrtIE4PHDtCHZ6Lv": [
                "4_gameScene/8_Slot8/minigame/bonus_active.png",
                "cc.Texture2D"
            ],
            "5f+P86021Jt5qwjODu1DEa": [
                "4_gameScene/8_Slot8/minigame/bonus_active_and_total",
                "cc.SpriteFrame",
                1
            ],
            "2f6WuaCTRLsa1wbp6FsMfZ": [
                "4_gameScene/8_Slot8/minigame/bonus_active_and_total.png",
                "cc.Texture2D"
            ],
            ca2oUKfodF1Ix2ieduazMy: [
                "4_gameScene/8_Slot8/minigame/bonus_active_txt",
                "cc.SpriteFrame",
                1
            ],
            b3NOsAwSZPs7HpJLXXS9ni: [
                "4_gameScene/8_Slot8/minigame/bonus_active_txt.png",
                "cc.Texture2D"
            ],
            "b4FU+uH/ZLyLn4mUg8CMv7": [
                "4_gameScene/8_Slot8/minigame/bonus_active_view",
                "cc.SpriteFrame",
                1
            ],
            "fcQuB8EcZE+IHEqBI21SPc": [
                "4_gameScene/8_Slot8/minigame/bonus_active_view.png",
                "cc.Texture2D"
            ],
            e6YjwDI8lMcZQqxoLEaNOF: [
                "4_gameScene/8_Slot8/minigame/daulau_red",
                "cc.SpriteFrame",
                1
            ],
            "5eZ7SRZh5L4JuarNUSnFPy": [
                "4_gameScene/8_Slot8/minigame/daulau_red.png",
                "cc.Texture2D"
            ],
            "9f7tHOMRVJD6Gvbz/Ulp0Y": [
                "4_gameScene/8_Slot8/minigame/daulau_white",
                "cc.SpriteFrame",
                1
            ],
            "2a0qwD/PtIAKNLE3vyJtY8": [
                "4_gameScene/8_Slot8/minigame/daulau_white.png",
                "cc.Texture2D"
            ],
            "b3dKFO66VGP4SBeHsGa89+": [
                "4_gameScene/8_Slot8/minigame/minibonus_txt",
                "cc.SpriteFrame",
                1
            ],
            fcpr27EuxBTIOb1uk42gBX: [
                "4_gameScene/8_Slot8/minigame/minibonus_txt.png",
                "cc.Texture2D"
            ],
            b41cqLNjNMJZGD90oNsLgl: [
                "4_gameScene/8_Slot8/minigame/minigame_bonus_view",
                "cc.SpriteFrame",
                1
            ],
            "80Aws2KVZCaaAB+DBRclo4": [
                "4_gameScene/8_Slot8/minigame/minigame_bonus_view.png",
                "cc.Texture2D"
            ],
            "57bWxSeFdGU6dglDBksit5": [
                "4_gameScene/8_Slot8/minigame/power_win_full",
                "cc.SpriteFrame",
                1
            ],
            "20RM9dQiFJ3oBh5bKAwHGC": [
                "4_gameScene/8_Slot8/minigame/power_win_full.png",
                "cc.Texture2D"
            ],
            "57g6IeAjhHT7zn+QADrOOX": [
                "4_gameScene/8_Slot8/minigame/power_win_run",
                "cc.SpriteFrame",
                1
            ],
            "9dbg70VNdLYaXQziRsa8IO": [
                "4_gameScene/8_Slot8/minigame/power_win_run.png",
                "cc.Texture2D"
            ],
            "a5uJl2yflB/IY293vEP0B9": [
                "4_gameScene/8_Slot8/minigame/quantai_boxuong",
                "cc.SpriteFrame",
                1
            ],
            "e32/j0t9tLN7TN24vHAjGA": [
                "4_gameScene/8_Slot8/minigame/quantai_boxuong.png",
                "cc.Texture2D"
            ],
            "923TUNdCVLvJbLejUuv1GY": [
                "4_gameScene/8_Slot8/minigame/quantai_boxuong_gray",
                "cc.SpriteFrame",
                1
            ],
            ebdTSKHcVCL64cG1FPLEdi: [
                "4_gameScene/8_Slot8/minigame/quantai_boxuong_gray.png",
                "cc.Texture2D"
            ],
            a50RavuE1BGrmnt3SaELMg: [
                "4_gameScene/8_Slot8/minigame/quantai_off",
                "cc.SpriteFrame",
                1
            ],
            cfJSrc1qVIQ6zVkesUEAh4: [
                "4_gameScene/8_Slot8/minigame/quantai_off.png",
                "cc.Texture2D"
            ],
            "7cd+r/x6JJlLrx/SkJHATO": [
                "4_gameScene/8_Slot8/minigame/quantai_open",
                "cc.SpriteFrame",
                1
            ],
            "68MPp5IPhKTpyOWzAuaNKR": [
                "4_gameScene/8_Slot8/minigame/quantai_open.png",
                "cc.Texture2D"
            ],
            "2eqkiAJrdNqZeFHpXgY7qj": [
                "4_gameScene/8_Slot8/minigame/sach_gray",
                "cc.SpriteFrame",
                1
            ],
            "62/7Yh9AtK/bT+/bkdYCLk": [
                "4_gameScene/8_Slot8/minigame/sach_gray.png",
                "cc.Texture2D"
            ],
            c3pfqqQwhLYqMo11clXIjS: [
                "4_gameScene/8_Slot8/minigame/sach_green",
                "cc.SpriteFrame",
                1
            ],
            "48SykvPU1FXpRjYOuj8e9/": [
                "4_gameScene/8_Slot8/minigame/sach_green.png",
                "cc.Texture2D"
            ],
            "418ku/fVlDbIF9OJ58NBac": [
                "4_gameScene/8_Slot8/minigame/sach_pink",
                "cc.SpriteFrame",
                1
            ],
            "8dIJUxVcBB05xdrsz3V09q": [
                "4_gameScene/8_Slot8/minigame/sach_pink.png",
                "cc.Texture2D"
            ],
            "89lCZ28ClC/IE31ZgwSIA0": [
                "4_gameScene/8_Slot8/minigame/sach_red",
                "cc.SpriteFrame",
                1
            ],
            "005nnA1QxNkrM8HANCeEgT": [
                "4_gameScene/8_Slot8/minigame/sach_red.png",
                "cc.Texture2D"
            ],
            "8crYhRpiBKeYtGQU0j4E0w": [
                "4_gameScene/8_Slot8/minigame/thienthan",
                "cc.SpriteFrame",
                1
            ],
            dfLPxh4mFFBINMIIO5TWeG: [
                "4_gameScene/8_Slot8/minigame/thienthan.png",
                "cc.Texture2D"
            ],
            "d0RymBV6tE0o21W6PGAxW/": [
                "4_gameScene/8_Slot8/minigame/total_win_txt",
                "cc.SpriteFrame",
                1
            ],
            "59MGzOVe9J04TQyMLiwk4h": [
                "4_gameScene/8_Slot8/minigame/total_win_txt.png",
                "cc.Texture2D"
            ],
            "edhBd9UNlI1LfwWWkM+M9F": [
                "5_Games/1_Slot1/Minigame1.prefab",
                "cc.Prefab"
            ],
            "73/QZLAq5JZbyFhsP9cwjm": [
                "5_Games/1_Slot1/Slot1.prefab",
                "cc.Prefab"
            ],
            d40ienE9ZIBqLqVc55zDSx: [
                "5_Games/1_Slot1/top-down.anim",
                "cc.AnimationClip"
            ],
            "839bovB+pJYYwW78cDraUI": [
                "5_Games/2_Slot2/Minigame2.prefab",
                "cc.Prefab"
            ],
            "e99aqJi9FDWrlqTT/OacmQ": [
                "5_Games/2_Slot2/Slot2.prefab",
                "cc.Prefab"
            ],
            f5a1I2hM1Jf7s5HtWWfOHx: [
                "5_Games/2_Slot2/animation/light-dragon.anim",
                "cc.AnimationClip"
            ],
            "0d3TzOnylF4oZyuXpUG9+u": [
                "5_Games/2_Slot2/animation/mask",
                "cc.SpriteFrame",
                1
            ],
            ddPYBmb0RM0YiTmXmRekVl: [
                "5_Games/2_Slot2/animation/mask.png",
                "cc.Texture2D"
            ],
            "74atjwh4dF2r6AP7gyvGDh": [
                "5_Games/2_Slot2/partical/particle_coinUpdate.plist",
                "cc.ParticleAsset"
            ],
            e1B7coh3BEUJnUkcRh1u0Y: [
                "5_Games/2_Slot2/partical/particle_texture.plist",
                "cc.ParticleAsset"
            ],
            "4a3gDs+ldJaKY1QwfmIT7E": [
                "5_Games/3_Slot3/Minigame3.prefab",
                "cc.Prefab"
            ],
            "77U0ziAF9ISKZ22ByHQjiv": [
                "5_Games/3_Slot3/Slot3.prefab",
                "cc.Prefab"
            ],
            "ecWWHpBI1Cl6AoXq+bba2w": [
                "5_Games/4_Slot4/MiniGame4.prefab",
                "cc.Prefab"
            ],
            "b0yO/Ic4hJJ4WBrK3JvgAj": [
                "5_Games/4_Slot4/Slot4.prefab",
                "cc.Prefab"
            ],
            d4cSGwYRdJG5NZXaOoklZx: [
                "5_Games/4_Slot4/partical/particle_coin.plist",
                "cc.ParticleAsset"
            ],
            "beeh9NrKhOEZlEcrh+Zid2": [
                "5_Games/5_Slot5/Minigame5.prefab",
                "cc.Prefab"
            ],
            "6bMpW//HNJkoPYtsEc+Gb7": [
                "5_Games/5_Slot5/Particle/LightStar.plist",
                "cc.ParticleAsset"
            ],
            "10Q2/qfwxE2Y4D8Df4gByf": [
                "5_Games/5_Slot5/Particle/StarBigWin.plist",
                "cc.ParticleAsset"
            ],
            "8djV/i5LtLbK2HICWPU6Rq": [
                "5_Games/5_Slot5/Particle/particle_texture.plist",
                "cc.ParticleAsset"
            ],
            "d7tjYClfJOmLakVEh1E/Ji": [
                "5_Games/5_Slot5/Slot5.prefab",
                "cc.Prefab"
            ],
            ebHBgr6hpNyqyTveJZXsmE: [
                "5_Games/6_Slot6/Minigame6.prefab",
                "cc.Prefab"
            ],
            "30Z1hEJ0JCaZ+mRR1fx/2k": [
                "5_Games/6_Slot6/Particle/canh2",
                "cc.SpriteFrame",
                1
            ],
            "43W3UeQmRGHZf5EtstT4WU": [
                "5_Games/6_Slot6/Particle/canh2.png",
                "cc.Texture2D"
            ],
            "f8/0pOuSRLj5n7Ix8A8a12": [
                "5_Games/6_Slot6/Particle/particle_texture.plist",
                "cc.ParticleAsset"
            ],
            "faH+PiYf1P3qDgi13QlOB9": [
                "5_Games/6_Slot6/Particle/rain.plist",
                "cc.ParticleAsset"
            ],
            "4aIiNzONJEJYFoU7QSGYGE": [
                "5_Games/6_Slot6/Slot6.prefab",
                "cc.Prefab"
            ],
            c80gxoIpdG4rakbQkYbpfk: [
                "5_Games/7_Slot7/Minigame7.prefab",
                "cc.Prefab"
            ],
            dc9SelZBxKt4ejqiwpSxW1: [
                "5_Games/7_Slot7/Slot7.prefab",
                "cc.Prefab"
            ],
            "87XDdkoY1E15tQsRfiCwpF": [
                "5_Games/7_Slot7/animation/animation_kiem.anim",
                "cc.AnimationClip"
            ],
            "3czDQmm+FOnoI8Epvvv3UP": [
                "5_Games/7_Slot7/partical/particle_shi.plist",
                "cc.ParticleAsset"
            ],
            c8PfP81YtErID57TaauonY: [
                "5_Games/7_Slot7/partical/particle_shi2.plist",
                "cc.ParticleAsset"
            ],
            cc9imQGsRMyZ2GtWN5cA7V: [
                "5_Games/8_Slot8/Minigame8.prefab",
                "cc.Prefab"
            ],
            "76Qv0xcPZOm46446EmF/ZO": [
                "5_Games/8_Slot8/Particle/LightStar.plist",
                "cc.ParticleAsset"
            ],
            "c1XEVBeqFG2Iyv+4D3qeHZ": [
                "5_Games/8_Slot8/Particle/StarBigWin.plist",
                "cc.ParticleAsset"
            ],
            "3dWOB+RahMboNaGMt2ydQh": [
                "5_Games/8_Slot8/Particle/bongbong_water",
                "cc.SpriteFrame",
                1
            ],
            e6sWWV4CJLpooZqu1ZNGP4: [
                "5_Games/8_Slot8/Particle/bongbong_water.png",
                "cc.Texture2D"
            ],
            d6h7HDkxlE47nUurbgs7qH: [
                "5_Games/8_Slot8/Particle/particle.plist",
                "cc.ParticleAsset"
            ],
            "2bQ2H6gk5AV6968I2u7AOp": [
                "5_Games/8_Slot8/Slot8.prefab",
                "cc.Prefab"
            ]
        }
    },
    jsList: [
        "assets/scripts/lib/socketcluster.js"
    ],
    launchScene: "db://assets/Scenes/Start.fire",
    scenes: [
        {
            url: "db://assets/Scenes/Start.fire",
            uuid: "6czYMWTwpEQoAxxs+CVLIY"
        }
    ],
    packedAssets: {
        "02eb6482d": [
            "29FYIk+N1GYaeWH/q1NxQO",
            "30Z1hEJ0JCaZ+mRR1fx/2k",
            "48aI31qb9F5IQ49aCJb0VK",
            "7c8AJX0e1Pz6gfa5C3AVke",
            "e1h1A5QChJSrKLpzWdzSQB",
            "e9YSqke7RKR6DWAr4h0ERl",
            "e97GVMl6JHh5Ml5qEDdSGa",
            "ebHBgr6hpNyqyTveJZXsmE",
            "f0BIwQ8D5Ml7nTNQbh1YlS",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "0307b7fcc": [
            "2bQ2H6gk5AV6968I2u7AOp",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "04200ad20": [
            "a9HT2hGqRCPLT1KXId+yWD",
            "c1XEVBeqFG2Iyv+4D3qeHZ"
        ],
        "04f653094": [
            "30Z1hEJ0JCaZ+mRR1fx/2k",
            "faH+PiYf1P3qDgi13QlOB9"
        ],
        "051d0baa1": [
            "010i+2VeJNLqBC4rgFc4LP",
            "06E7ssTAhKsr2u76Q4paM9",
            "0cYAVLV3hLOrnqdVC6m+94",
            "13pI23QWtHzJ1OTBglys+D",
            "1523BBo3tOy6AjgdvD7AsQ",
            "163ciWemNOkqZ/iPB1WcYl",
            "27AqCbJYtIML5b7bFAUduM",
            "29FYIk+N1GYaeWH/q1NxQO",
            "29Y5ia2TxEipapWTsqwjwn",
            "2cD+iXLkdGhof+KxG74Vhx",
            "2fy9Lg5B5Goq8tLT6Clvhz",
            "30fjdZnQtDrpRQa12XCcqi",
            "33mcNug8tOlZ6OH8RPiNYX",
            "36BdEYFttAFIwneeEBRiu7",
            "38K88QR3VNKqBYD2BJBCKE",
            "3eI+JZpBxCaax+2GX1adCg",
            "3eSr+J3dxE3rW9G9pU8lCQ",
            "41YaTxk6FBT7NCdPnXks5y",
            "429SWZnyhPa51bYeKJzasc",
            "48AQN1Cc9ODbMJuc7fu/lp",
            "4awwR1ETpDb6dUIaNR599t",
            "4eSvH7fwpOsJlCSuYnbCvU",
            "51rNQNfJBNVLyPzmMuYC9T",
            "52hP76EahKMK9gQJpvR81k",
            "53LespqTxK0rQNvqmmK5AZ",
            "57JgDKJ/1Esop62s9W+M7h",
            "59KisoI8FEKL1cgpe4FxnL",
            "5cO7kybDxGj4ipyMYdRYZB",
            "5czFmgVgBEa7XeKHjm+Skz",
            "5f5dyqtRNNxaFmVzYns6FZ",
            "61YxvObDdD540zeO4ag6XR",
            "66AtMglxFM1osOZ+5FQTSf",
            "69dO8fjOhCHoRVNIB/mePg",
            "6czYMWTwpEQoAxxs+CVLIY",
            "71meXvlT5KJKVQgvwWpKBO",
            "74Ig4rlThA4q4SIaRgLvIb",
            "78kkR29PRK67hMBZlX6oS/",
            "7bydhlBlVJ2ICXKxWb6Spc",
            "831c1UqxpACb6oO07YIwcd",
            "86D3mOsrJJHbfuA147slVM",
            "8bplBnhcRDZbSZ4hOiheNg",
            "8d/o6qTTFLpKYfzc7jxnJK",
            "91q3bs2NtCdpkNFObUtrcm",
            "99XNeeNdhIEaPTeChBJavl",
            "9a7ULqD31BC59rUl49AWHW",
            "9bvaMerUlDyary99mJa6xp",
            "9fLlU2MoNFA7UD5M2Wfz11",
            "a0wREYAb1GFLd5+2MsgC84",
            "a18Jj0cyJK17LF2pgR6vUl",
            "a4Vidk9SdLHL9QL0gEHkre",
            "a9p9D+J3FCGLBKAY+LE+SQ",
            "acCVK2A3lL9ogy1T41eovR",
            "b0w5fmdqZG7YBBwwBXdS2H",
            "b3Pzs7eFNI95y7mDr3PBfk",
            "b7Z0ca/OhEn5hvoY9QR9+O",
            "b7hxjzixJCVIpF0zJTi7K2",
            "bcrBicFttGmLj5jbg43boe",
            "bc78wiLiVChbl88NMlNm95",
            "c0GS2dpq1GOr+qeHk88bX+",
            "c6G7Bnwc9BiI2TAa7iZ5Xk",
            "d1FQ+vqZJM8bUdEcoBOT5e",
            "d4dX0GOZVEYKBMi92G5xMn",
            "d7IfRsBdtHqJSy1Qh8WobL",
            "e47A0XGwdMNKIe18fAYZIu",
            "e97GVMl6JHh5Ml5qEDdSGa",
            "ebUIK6KHRPe6fmjmVwOix4",
            "eeD4YcIWxOta+8p4LOn2bX",
            "f0BIwQ8D5Ml7nTNQbh1YlS",
            "f2V8wZFLxGJKMCsYOQO3XB",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "0528a33e4": [
            "70lC8aV+tBXp55tJhaTiWY",
            "aaWWHdlCRO9JaQOOeEkOnH"
        ],
        "061199536": [
            "3dWOB+RahMboNaGMt2ydQh",
            "48aI31qb9F5IQ49aCJb0VK",
            "cc9imQGsRMyZ2GtWN5cA7V",
            "d6h7HDkxlE47nUurbgs7qH",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "0634f6037": [
            "29FYIk+N1GYaeWH/q1NxQO",
            "4cJxyzJIZD8Y31QWz9kbm9",
            "c8PfP81YtErID57TaauonY",
            "dc9SelZBxKt4ejqiwpSxW1",
            "dd8/Q5j/JNIIC/cQmoxk3E",
            "e97GVMl6JHh5Ml5qEDdSGa",
            "f0BIwQ8D5Ml7nTNQbh1YlS",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "0675d6fe0": [
            "16ghaQKjJOfLDvTizedZAV",
            "53snJ90A5LFJXxplp3ARES",
            "5a8qsGXPZB7a3N/Hg1rpTQ",
            "a4T8VnQwhPsKBaxjv1xMsC"
        ],
        "077f7381a": [
            "13pI23QWtHzJ1OTBglys+D",
            "73/QZLAq5JZbyFhsP9cwjm"
        ],
        "084ba2414": [
            "75ojflVmlBy7zjwBz6I9wi",
            "87XDdkoY1E15tQsRfiCwpF",
            "97kPzCYMhHGLtlQ/zjS4EU",
            "9e1s1Y2PxHWKhL6iVfhLNL",
            "c80gxoIpdG4rakbQkYbpfk",
            "dfsBzuyeNKU4Hls1cPZFNz"
        ],
        "09909c6e3": [
            "00ih44kXJFP4bPlUBLyuXA",
            "005nnA1QxNkrM8HANCeEgT",
            "01jyjxi8JIOYJNZaIM4X2n",
            "03SgskRbJGLpxTQVThpk82",
            "03+Esr3H1KNK+wwOaQ/SzA",
            "05GvY/GV9GmIFYvObaZZZq",
            "06V5EDyxtFZ7fvYR55BB1n",
            "060ugkxFFC4oLu+uIrG7c8",
            "062TEqOH9KMbE6PPMrARRd",
            "09iphQ4g1HqpJJr8bH+ESm",
            "099pv/en5Ov7EOuWw4oYV/",
            "0aH5MSdexFZLQY/Fl2hSKz",
            "0bO6ZMbENAtLHWq6AIT+Uu",
            "0bctc7VSdPcbvNyvAwTK09",
            "0cW01uCEdOkrN8oEh5wcLJ",
            "0dC2BgMixJiZ+dojBvyck3",
            "0eIA8p32VNnKhDM5IBskt9",
            "0eJmpCHeVLpLSEYOu9GD/W",
            "0egGmeayNKXZaOMvCFJL8d",
            "0ejBiEdUBGLoq8JoTECwze",
            "0e3VJaJ9dGQbR0KHw5H/uT",
            "0fRQyckVZNiIgkSpYOsPpv",
            "0fnjsYXwNG6rmI/OQ+o4Bi",
            "11eN7pyIVBDorDdvdFqp39",
            "11gkc7LM1EeLcBsDv5tUjs",
            "12iL7m7KxPv4YszpyPWxNI",
            "12uraOYolPyoYsYg5SajC/",
            "13aeHwhSJBFrP2P/nJvTIt",
            "13l9RRtxhB5JIHIak8vIRI",
            "139ajMHxxOIKbI2zfCZr8Q",
            "15fpgoyn5GRKyejXuwLg1X",
            "16DHsfl6lNJ46SwcsRgoLZ",
            "160u0u2MlNYYXVAjouq++p",
            "17CZPlAU9NuLNWXSQdiLxv",
            "174zjcIABOBbezzEa6cwri",
            "181382b81",
            "18UjvXe/dG+aR5MWbwbBaa",
            "19byRHGJpMPbTB7infNmIw",
            "1a3E/X1SVNIbkPmWEBdOq4",
            "1bKzbQMMZHBZ8vaTtkvR7j",
            "1bWPwVWeNBMLH0o9DggUQv",
            "1bx3yDhlRD6YPrhxyA7wE7",
            "1cHYzCPf9ICoaRCfKHwcJL",
            "1cdkkRlDZFo6XTHJ0gaBop",
            "1c4S5zXiVHxropIcxO9Kme",
            "1ddixse41PYZnVFRB10neS",
            "1dg0PkQktByZenLynQKouQ",
            "1dmfH47w5PJ4oeFmOJtEGK",
            "1e58310e3",
            "1erOcVfH5EEpEGQYBw+hF2",
            "20CCcyb7pO7K+SOHoX7QYQ",
            "20CMmFTNpO1Jz6Diu5A/Ba",
            "20RM9dQiFJ3oBh5bKAwHGC",
            "21VBXkzyBC/Y/Q5/Mw/ifr",
            "21ezNVZwlDCLwpJmx1rrzi",
            "22XrNMbYRA0LHj5jYvW3oR",
            "22rA9j9M5Ej4iZniNc/6B4",
            "23BqBOfENOWotyBT98jWIL",
            "23EOrVrZBK2IKksmj9B31c",
            "24s7S2zU1ByqMuyC1QKyoh",
            "24+0SS8ORMZr1CCaMMEeLb",
            "25fsqgpQJPGoe+lNyZ/ls7",
            "25r9y9BN1L+pUyCl5Hld41",
            "25ulgrrGdAEJgDAJcuDAxY",
            "264e5JEwJEU4EytJdpjehU",
            "27FHIZUbpPnIQoVZ9hjrYV",
            "27fnkWrANNW5p4wTC8ceBW",
            "28BbjPttZGRZ1yhTkl0Atq",
            "28aEhfRf9JBLTBrjhvACmK",
            "29QfFb3dFFXKXTrSLQwS9F",
            "29vMvznXBJAbHjH7zFstFp",
            "2apS7RYORK+5lLpBTvH2az",
            "2a0qwD/PtIAKNLE3vyJtY8",
            "2bAuUpRc1NmaS+Gi6INThD",
            "2bBtffXNJG8ZeKAZjWbchM",
            "2bZc8rxG1LJ41EMClQzd4o",
            "2cXu5qhQVOtJkTIvUymbMU",
            "2eWrj7lTNNfqT0nKB17F2Q",
            "2f6WuaCTRLsa1wbp6FsMfZ",
            "30D+09KThEpaIIGLdbb3pU",
            "30VKeyaMZGELNpGFVe5A12",
            "30VKrYllNEc5Iuckk1oC8D",
            "30wUtKkb9KcZJfpTj2zz3I",
            "31Bg7q5NNEl5AM6UWi61LC",
            "31wWwgtBBMKq9lQFKNBk+X",
            "32T9e0K19HNIo1WxCriVlP",
            "32xWOJEeRLfqO2zxRFPmmm",
            "33FzkL8WVJw419/IPEvZjR",
            "33VAqMcZxMx6rEIxqyAwsk",
            "33tE13j7JFmbmPlKZst6ZA",
            "33z8EX8QNLKKKm9XqXncIP",
            "335TX2VB9M6Y9G56ZJqRQY",
            "34i0qPJKhM5r03NXOKi2aI",
            "36bGgMgT5PU7u1fZ/gMAUB",
            "36t6oqY1xNWaysWct2NWHw",
            "37Z04wGANDmLUh6DUidOlR",
            "37m2s5lwZIv5pd1TDwngnG",
            "37nUtLOXZB1I9L8XgzvMNh",
            "378b4ytMBAVbaHUdtirijr",
            "38TPDYYhBHDYip2Mqloxni",
            "3885qT1whFMYvSd7eqSYA4",
            "39QeQn93tJmpH40G2QwOoq",
            "39/wCKZVZEJp9Rgrcjoux6",
            "3aBymgHuBJLqnGlaAV1eMr",
            "3a9TIdMulJYoigKgxMq5Ri",
            "3cEtua8EVAhaWvVYEh//pI",
            "3cwLffwb5K/aH87+TLq+1k",
            "3dXO2LHnJM4ph4VNPeM+Jc",
            "3dnws6gztLL6LLmeqAiZyF",
            "3dzuQIW/lHs5UQ706br1kQ",
            "3eyV2YKZxN26JR1mEyYs2B",
            "3f9jVqON1J7YL33Gd5mcNG",
            "40qIfx7AxDKqdvYhnvWmNi",
            "40rAsZJ/JHh4LZxJl+SuCI",
            "407MN6wCpONq+Zk49apXBD",
            "41GpJGWx9LM4u/MKbMkRyb",
            "41PV/pM69IMYLD36hooeM5",
            "41TeJbgXRDhaKHzUuaVLsD",
            "41zqt3hENGXZZbfDZR1JoX",
            "42Z6JA3iRCXLvzf9da+mAU",
            "4205AwX5BJBqTcr70i3vF/",
            "43Behh94lFhblBEwI9+13m",
            "43JkAppdtLdagpxlGCyMtt",
            "43W3UeQmRGHZf5EtstT4WU",
            "43w7FVWOlKFJAIw6LQSc0C",
            "44N4gHH3lE25lGr+XW9ut0",
            "448UI0NU1AyKgDQkTbUmtl",
            "45SuvtTZdBdoHdue3IpPVi",
            "45kNrtbstPV6VJpYXmpBrC",
            "46MDNNzGBC1ZktAuh11ppK",
            "472Vhy/eJBuoBWmXtqen98",
            "48OQwQ5e9IS6TmM24Njf8A",
            "48SykvPU1FXpRjYOuj8e9/",
            "49nlgrpKVBvpd9FeAc2Um2",
            "496Wqq0f1Eep+57MPbHQew",
            "4ar3E+b3NIQq3D1BbkKGPo",
            "4bPAYefyFCi5DqWE7tU4jL",
            "4cw91KuEZH04jZvtz6JoBD",
            "4dQtoV/gJNsLRSktyYY9e9",
            "4d6UIW8VBMw7qrCUHDrC7w",
            "4eSlSU9ZtPRo0aAMNglhPx",
            "4e6mW2G39M0LzRBUNKfPYN",
            "4e8W41F2RFYplOHhOa8Zw/",
            "4fkfwnV5tLzKSXq1PjLl4j",
            "50YVyplg1AqrWx8/6kQAjU",
            "50cvkwlzVPEbDV+1sv8rbC",
            "51bCGvbDBOgYlNao1aJs9p",
            "518or4kNRFrJWMz/ccgXbh",
            "51/XILsltLDI+KXgHNYcmi",
            "53oW8LpHFMeZ1yTqdv5owP",
            "53qAh3fYJFjKQHTm0picXK",
            "55BBPgDGlPRbPTuWR4XHT2",
            "55RZNLuipGLIlU0uqUiAgN",
            "55+srNvCFJR6F4mz1Rcpk7",
            "56MKm/QRVNzpIX2EDlj3Tf",
            "56W/NXvxxMUr1hHlRi+vTr",
            "57nSwB4lRLoaMoqdXc/Dz8",
            "57pzRxz0tDvJ3P7vs6x6SV",
            "58FmBKOnBBv41Kbt9v48Uj",
            "585P8ZvDJPUrM6MXTgqBP5",
            "59MGzOVe9J04TQyMLiwk4h",
            "5aXf7QfqdNIZtgkwRf1VXD",
            "5cr1Tl5mFOxqO6tLlGNKnN",
            "5dfGp7UQZEa4PNwAHXhyfY",
            "5d9jfiwcRJQ7hTC6NoOLNZ",
            "5eARtjvEVE/YaSkukdzlYt",
            "5eZ7SRZh5L4JuarNUSnFPy",
            "5ed9jNycpOqJhSz+tLwTOy",
            "5etiYXNUVGQpSDTRNYrpta",
            "5fW+OkotRCApArKBDABlLa",
            "60KQBBqZVP2LbVL3X72fMe",
            "60K7Y9xLpOz6G6ePuHDRVt",
            "60+AYdT4dLW4OaMw2J/pd3",
            "61cyPdEfRN047sDK9rO0W5",
            "62LD1XeLtCQJj72Wv3GCqX",
            "62RmY+cyxCRZiG+dYyoPcc",
            "62ahVYKp1AvozTQy7cXMMf",
            "62dV6IDzJKx6A3bB3175+V",
            "62/7Yh9AtK/bT+/bkdYCLk",
            "63M3WH53pPjpfBpReXS32k",
            "63isKmft1HhKo5dkwd4JZy",
            "64nrzxkzVPnqhQb0JO6Q1O",
            "64pcDK7hJBpqxV7LN5/xvI",
            "65DohtKG5IlaMfZQtSrAr1",
            "65EtOIoFVL4bL/XOVRFb3f",
            "65I+9UgKVAE68pim5gYYRj",
            "65SVwfK1xOF7YWyGxacQlK",
            "65fMRrHdJJB7eBzLaoiokE",
            "66M4PjUpRJSLaqLZhsFW+8",
            "66Y063HdZOLZtqk8qUrLyN",
            "66tQ4+hBBMAZi1ir/366uc",
            "66+Wn7NCxFh6B81jW7T4Kh",
            "67B4U0gL1NZ5uxmGRkV5ZW",
            "67E0ZqfpFLqoaE5I8lmPX8",
            "68MPp5IPhKTpyOWzAuaNKR",
            "6aMQze1dtD0r/K30zBD3bO",
            "6bGYYfhxRNc75jE1SOZibm",
            "6bHmENJudH+r4LylUWz2Zg",
            "6b0IoVSrtIE4PHDtCHZ6Lv",
            "6b8ZLwaqVMg5RFBNvP03ot",
            "6dPWUEB/dLT4bv/6/1duVP",
            "6dgLItBRxGYL7S/Q4C+Frc",
            "6fH+FXDa1J1ZSnXT8eF6n7",
            "70iWNpyQ9EQbvHdMqKCIv5",
            "70mD11FPdCr4+5Isbw61Pb",
            "70y9rXjYlDFoIPqealc67k",
            "71LTdpz+9LFq6w31NfrX88",
            "71VhFCTINJM6/Ky3oX9nBT",
            "72m2Z/dXpATb88s3mpA+xh",
            "72uBcdD5lFL4QL3ba6Im2y",
            "72yi6d2y5BNoBC/q2cbJOm",
            "73HOyAsl5CPJnuM8ql8e/N",
            "74LVKfDvpLE7ZCXdIwE7NP",
            "74fSLPCvJJUKOkZmzRVq9h",
            "75ty/AxiZM0bqDcmClfWel",
            "75zjIh6U5Eb6duH10A/mKL",
            "75/UuM3UdJaISwz7F8AtAg",
            "76KA2cCZpDvrLlUID3yStF",
            "76KOC8hBZK7oNfUPRZulyQ",
            "76dnHQsolCfKgcsBVIaQBL",
            "77AiGGXoNDKbFPYEBrIokJ",
            "78HTqiNr1ODorfuQVLfqhW",
            "78S3Y2zaBFz4znE5XqaUdi",
            "78UD5J7LpIcqIqvRFqGOQP",
            "78Y0JzQkRDmJqZDGsG28kq",
            "7aROZK60ZHFKiW+x5MZ4Lo",
            "7bX3UYNiJGergCPlIUfqp8",
            "7bat62Zn5LiIppOQtGisFa",
            "7bvfMXXghKlIE+Dj2bhs8c",
            "7cKraiSJtJo4XZFptOKD5P",
            "7cQTiYqRBOPqJvaGQFI2tn",
            "7cdEf41VhNt53UU2qKc/aS",
            "7cefBqmUdEgr5ZJHU1LvT7",
            "7dVcZjCjtHcb4zc3dlu2uf",
            "7eMllcwY1KEbk24RHnZtn9",
            "7eXmzdZT1DM5zO0qK2Q2cq",
            "7ez9qu595Oz5p085SLWI4m",
            "7fEJTPKchPcbWRBU5HZksS",
            "7fmBNZNJ5Nz6h1xu4XINqv",
            "7f0oKSGu9FTrYmu10LUrLb",
            "7f2aKV8dNGm5aCS5soIhYd",
            "80Aws2KVZCaaAB+DBRclo4",
            "80W2w9fFFDWJqduhUnXeJH",
            "80krSyDNdEOJjaNY/GIyWz",
            "81EOCgG7VIlauRlhKR6Axm",
            "81kEpCQClJcJYjDvndV8oH",
            "81k/777jNLsIHEfAs6pynO",
            "82DHLl95lHq4FKsctx5Fnd",
            "82bW7qKbVN8aI4UfH9ELYO",
            "84GFS5HP1N8KH1tU3/Cjow",
            "84cIXtauxCM44Nbp3AYcwn",
            "85aH/wkVhC9rFCzdUpBsY0",
            "86qvdVrfVLzY5I5BaWEqEQ",
            "86z5YZ81lDXItiJqHgmEEQ",
            "861DyqXLJMOpJzuujY9EEz",
            "861xiFbphOnY/l0Qy1xZET",
            "87pz+NpYxNdb7jJ0qMvn1Z",
            "89fjkCY7JErYC2SW7bGAK+",
            "8bgsj+idJCYYm4FcgdyeFJ",
            "8cTrWD8MNHmYL8eUMuidVY",
            "8c8mIQMQxJy4VTGr7Za0yz",
            "8dIJUxVcBB05xdrsz3V09q",
            "8d6faQNUhGObaLdudeXuPp",
            "8eZJqNvFxFQJfZoDfRarWI",
            "8ero+b/TVOYqIuU7EM8dOt",
            "8fiEO56MRA5oEm86IKHR2Q",
            "8ftZy5rmZCaoWj89JFEANI",
            "8f51TzMAtKw5pLPbOI78cX",
            "90F0whFgNIcpgGVhSbh01l",
            "90vBxT2CRMvrcoJtVclXd9",
            "9080S2Mm1Dso4gSoj8dlUQ",
            "91J0cgCXlAP5S1lwmgQC/P",
            "91WIqy5zxBqLJ1gZ6O+IVj",
            "91ZeR9Dg1Dp5jBcE+wq4E9",
            "92eZW7SMtOsqRHkLb5US9C",
            "93FkcHIaBCjKwzs8/vWgl1",
            "93vT8NDl1ONKBKaBR3WDGC",
            "93vUdxacVLZrtZrq2zAEP/",
            "94KG+IW6FPJYqI1l39j1Xv",
            "94mOPxeLtFgrh28Cky6NRP",
            "94ohv0nMRDVIgKu61VE6EP",
            "94wdCm3UxDt5Cpsxm7lgrC",
            "96MhGe4mhMVIBlbQIqfs5c",
            "96X8xrvqRGoKbMD+va2P9j",
            "96nQv9EUBLIpGUgCdk8tzo",
            "96xclc6t1ON63whepU2MH6",
            "960SWOJVREU4xaUwfd19kz",
            "962HOFhZVLaYLNe9khtgzC",
            "97YtBto0lJepAWlngnG/O7",
            "97rTWWXO1GxbZt0AVv0ugW",
            "98S8nZu99GTL7pws2foX3n",
            "98zTHSLlFOdbR0gTZfilg1",
            "99XILorW1FxLIvZfHJpYIY",
            "99wsjufQtHy5D52aEPZY+L",
            "991KNwbidM36glccJQHUsT",
            "995W0xImdDMa/Pq6Uipica",
            "9afARNl1ZJTrNcYUV+3G6z",
            "9a8rfkbSdKUYym/lQRb2cI",
            "9bOnDny41IKLc98du0WW8z",
            "9bv3ONbMhAdba2ui24gBFo",
            "9b4e5O0atHFq2BlwQrLfAx",
            "9b7hs8RN1Ggqml98DebcM2",
            "9cjTeBmi9C1YtjllSGFpal",
            "9cutVPmHBEtJe5zw0Uw2Xf",
            "9c0meRgEhA3ag+/yP+TQOf",
            "9dbg70VNdLYaXQziRsa8IO",
            "9e9Xvcu2lHmouqd4S81MX9",
            "9fOAsas/pHpbTvjWGW7vYg",
            "a1Q5hvBt9DxKswNkfdvCam",
            "a1yKIORCBC+Kamx0tz7JLI",
            "a2CWdR/+tBILr+gG6Dtx+7",
            "a2XNTQiZ1F+bc4GRs958rW",
            "a2uC/IobxG4ZK9MR8JmGqr",
            "a2zgKDcdhHCZGay9hie2hY",
            "a3CbHeUdVGh4m5QCDQ0Rit",
            "a3REYkUnJAloHQu3xXMCYy",
            "a4c0p7afZLZYsfz7RxaJhj",
            "a45Ptgcf9O7JyB7xst0ZzU",
            "a5HgxXISlBVpVpbRy6GG2F",
            "a5cBSlYnpA3bJ4j2t10Cpe",
            "a5myBSC2ZP/rJGDNHCImrx",
            "a635wr70VNZKHTf+0tYXcr",
            "a6+coWhRxP8Znn2bxVihrH",
            "a7WGeJj8FGs6hIeAAGY50A",
            "a7pT+PXUhL6LMUsuLCQTpx",
            "a7+vTgS5NLKJjpbZVft/c+",
            "a8YiZzpKxM+r2szBANdYbW",
            "a8o5XQkx9Nb4T0rzCM9P5P",
            "a88JHWlVhHUo2h/r2PgjOg",
            "a9YqmCV9hNP7iqKMBx7Xrx",
            "a9cjbO+uNHHYfo7mNsafuK",
            "aam0g+LTBBm7Jd2ZgEKI2S",
            "aaps8oVYdLkIQIue+adxO0",
            "aawxYuPyhORo8L0Dr9GigC",
            "acKsqCo4NKgqqA3ClZ+yAx",
            "adHZS2SglPT6cOGT7UAw+4",
            "adJljPhZxPbbUctUhF0zSD",
            "adX7+6IyhLUJUw6GqreKpS",
            "adm02n+1NJ06jy9jgHZagJ",
            "aeEdhyN4RPqZHZO5+3sXaD",
            "aeu+GvjvdDDoyJvSX1f8Ce",
            "afR4PODzhF8qdlzci5WIV/",
            "b0zgk4MT5Ixa+VrExrjQ7v",
            "b2r+x6qJJC/bpO4NTF5Ip7",
            "b3CcWyAgVE4pSobpvWtvfc",
            "b3NOsAwSZPs7HpJLXXS9ni",
            "b3SlWCGbRLmIHnp+i0TutL",
            "b4P/PCArtIdIH38t6mlw8Y",
            "b4YnSH8HVEz4aJk1B8oL0+",
            "b4oj/XU6NCVYX315gd9WRc",
            "b46IWLyGxDZqT/XOaiixyr",
            "b5H1u+rS9G3IxNcN3p91mf",
            "b7VJCCf7BNUp7cpEkymp78",
            "b7tdOrfoJKfaqoev4Y2y5c",
            "b8C0ozERhB6aBOE7EqKV7B",
            "b8SG93lmFDpIkc0Q6WNnCd",
            "b8+UuYZpVGdIe3tOsdqp2e",
            "b9GSobCydAoKkZtcueQ7gO",
            "b9cS/l4b1IpZF2L5m7XNGJ",
            "balr336JBC4pl90nALW+q2",
            "bars78ugdLrp9v01kB1pne",
            "bbHAtyT9FBs6nGVns5nESH",
            "bbp8FbiDBM0aFdHPv5Lp3O",
            "bcfPXzPwZI2bTJMvmsTBBa",
            "bcydtulL1HH7e+4SDCxJPd",
            "bc5mmpNwxOh5MJEKrb1fBg",
            "bc5peOZVdCc7OdJNrYwi1r",
            "bdZ4wwUhVOg59DA4eTj2VC",
            "bddhWIn7FDMaVgA+XoXCE1",
            "beD/iHpwJBOJoi/qnYBXC8",
            "c0p+hluN9F2bwEhJgRQ4Ji",
            "c02yiOSE1B8qlJK4SLQjeG",
            "c07R05d9xEPYoiVFHO8G/N",
            "c2JY31bjpMfLLVfGb+BiDQ",
            "c2qgVe+QRFqqTAfcTHv/6v",
            "c3G2mIaW1HnaLFeoRilV6J",
            "c3PXyl6wREUaeFN7Rb0YNG",
            "c3QUqPRstKY6hgddeulkI4",
            "c32oc4QHNNaaKK23m01TTO",
            "c6gJ9zuSpOFJRsPeGCNHly",
            "c6g/vI7+JDFL9b/KmA1UA2",
            "c7rp1t6edD0oT2Gb7uXSeq",
            "c8DhfNUFBDwovDZl+ew4IJ",
            "c8uMoavoRC4IcbUYfO/Ew9",
            "caPNqks25KgoxFqaXm9d7v",
            "cahFjQK4pIfrmbWK73lzjr",
            "cakM6VR6tNFZTlJuN4xmGg",
            "cbCSnciEREkJ5QaOp9bwhu",
            "cd44h3qFdE9bzGtDdOAQQG",
            "cfABlWWs5KDLMd3Yr3IVeF",
            "cfJSrc1qVIQ6zVkesUEAh4",
            "d0EQ6+x+lCYrupjIfuVch3",
            "d0GkN5vtRCm7M5WhB3RgR8",
            "d0JaosdIdDUocGODPcMGHr",
            "d1xQtc58pASI57j3yNT/gv",
            "d2HrR2T4FATrF7pwAXNClz",
            "d2H6TLRaJAJ4QTuhrLTRRR",
            "d2fv9ug+RIhJmRb34+CpHD",
            "d2khOjLRVDwbBxcNjxZScA",
            "d3d9SzvctAFI92RsmojcDl",
            "d4eCV7DxNIS6pzl67hcvtE",
            "d6PE5w0XxKU6H1i5RZsiFM",
            "d6QlVPbd5K3KRJFiVaYYt8",
            "d6uQ6WQtxFdYVlsja0aCru",
            "d6wne+kwFJMpd8hjEHoXQ4",
            "d608qFRoFHwbXd0Dap056i",
            "d64VLoML5Cs7btSMVj8Q2/",
            "d7de9+YDlHoqXX5EgA0gOQ",
            "d8HsitJHxOYqo801xBk8ev",
            "d9MwJ6is9AP5GqK0RudHwc",
            "dawFXXvCxGE5OjEy/ybAJl",
            "dbqGnzsldAmYMdoMH2WbBi",
            "dbykHjdoJCL4HPMRmv5B27",
            "dcwLot2DBItrSvK3t+nyWV",
            "dcwgdzTxJNUoho4USiekC4",
            "dc2JBAc+NHn4rH/tz3NByJ",
            "ddPYBmb0RM0YiTmXmRekVl",
            "ddRPq0j/VNJrHk5b3bN53y",
            "dfLPxh4mFFBINMIIO5TWeG",
            "dfg7+pAjdLY7o4Jj+SP6ZP",
            "df1rXz03dAGYPEW45ULEft",
            "e0MpfORuJK5aVa7ebp6JrD",
            "e0ainAVXZCcKhWaJ9oSRnN",
            "e0geupMCBM0JaKo+VzSPjW",
            "e1bGJbWKZKxr/PHVGhxMWt",
            "e16bKOYp5GcrktpO4OwWN9",
            "e2Azw+DUhOW6vH7ualHaDW",
            "e32/j0t9tLN7TN24vHAjGA",
            "e4NZbOt9tD260Y4vqHrHVS",
            "e4PO0jFG5FSbsoodCteVIa",
            "e4cpgk8o1Kl7KKX53NAns/",
            "e4yk9oE8lPnqsImXX0B5bZ",
            "e5m8VmOahCc5Nik4+gQE7m",
            "e57owlRLtC76CLSzWHlAvn",
            "e6eVkTOqBOs79R+xQh3OEd",
            "e6m8VCgGpKPKjn1XjTe5iu",
            "e6sWWV4CJLpooZqu1ZNGP4",
            "e6yM83ByFJU5qtFmVFfh7N",
            "e7r5ZMmSpAEbEF/tu/roc4",
            "e7zgwuDH1DiaQMzvkvu7I/",
            "e8UFe6H/tLxK77sNvMs0iJ",
            "e8Ueib+qJEhL6mXAHdnwbi",
            "e9IO69j4NB5IQnK1Kf8omN",
            "e93hROETtEO77olSEfsfTz",
            "ebF6Kq7AFGtJ7aikFJITQg",
            "ebGaytud1G6aiJ+niwIGbe",
            "ebK5HZZiFA4qNL/9d/KVf8",
            "ebPQl+DjdATqUTdf7CJGce",
            "ebdTSKHcVCL64cG1FPLEdi",
            "ebkTuxm8pP55/AAeWUgXjY",
            "ebmFx/rK5MlKx0L6jsGUjX",
            "eb2vewiCtGRbiXKwMcNLaV",
            "ecIY4l6NFB5LZT6E7fhAQt",
            "ec/uij3PBKuq6KWnMRR07s",
            "edZvoGhKlMb6mCwG5jhg/4",
            "edhTFYGctEAojHkCeFnzhu",
            "ed5CdPyNxKNq4j3Sr6Uqg5",
            "ee3drL0CdCZ72SLSfgg18i",
            "efD0VP+H1J6YfdtTBLxWQT",
            "efJJoGmJZJPaALj8klFlNY",
            "efSTl/0xZO9adzy5i01fHq",
            "efpXy4TjdCp5Jvl5Y4W7l9",
            "ef2yFzakRHF7C3Y4Z24uS+",
            "ef9FrFrwJDeoEdjCXSg7MY",
            "f1HLJgt1VK94c01lk2gyL3",
            "f1I4NisgtIn5aUPpM5keII",
            "f1MfJGFiRD3Z9grfDUkrLV",
            "f2fIYum0xJeIfmZ+ih3DP6",
            "f3aP+Sr7BL/JY5pxGgP/+e",
            "f3r5YJx7dLwr6Van0Buaub",
            "f4eyNdEPFIIaKCytJvsl9v",
            "f5Ac28kNtAWa1MxHvCxI6K",
            "f5pvbSshhFi5bahm1ZT44J",
            "f60x2DBT9JKYW4IqPo6bmW",
            "f7dsu7NRFGqLOoLKitXgND",
            "f7/VTUJGhH5aAJse1g43co",
            "f8IrzToSVNK5+V/G22i+VZ",
            "f9JLf0QchPDZrFeQNZitP2",
            "f9sbMGGmtFYbIwegiGzncw",
            "f9vTBXQihMU462iOVmOdz2",
            "f959Y9p3ZPYZVyi5F83ABb",
            "fay42DH8xFRr12kZ0XrQRm",
            "fa8FJAyUpL4oee7jJ1PjJi",
            "fa/Sawa4xMsKrG9/BLILr4",
            "fbfyDexoBC9KCkIPyNxBMW",
            "fbtCH5A9xJw6y2+N4LS640",
            "fbtVpapbZLOICIiotj7yYt",
            "fcQuB8EcZE+IHEqBI21SPc",
            "fcpr27EuxBTIOb1uk42gBX",
            "fc8nPBErBCRY7WYSFUD6Pc",
            "fdWXgvHb9JOqOC8jLKjonW",
            "fe2Gb4JLRMO7F+KmkgFcIA",
            "fe4Gy/0sBNf7Bnj31xubM0",
            "fe6G5O/SJLQZ5g2RDJvoQs",
            "ff8C4iykVK9KokfHrXMqRh"
        ],
        "09d51f29a": [
            "b0yO/Ic4hJJ4WBrK3JvgAj",
            "d4cSGwYRdJG5NZXaOoklZx"
        ],
        "0a0fab049": [
            "48aI31qb9F5IQ49aCJb0VK",
            "7c8AJX0e1Pz6gfa5C3AVke",
            "8djV/i5LtLbK2HICWPU6Rq",
            "beeh9NrKhOEZlEcrh+Zid2",
            "e1h1A5QChJSrKLpzWdzSQB",
            "e9YSqke7RKR6DWAr4h0ERl",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "0a36c0558": [
            "48aI31qb9F5IQ49aCJb0VK",
            "9bV14S70RAzbHdlagTTLg+",
            "dbApQIXIZFHoF3C7Gf6NxV",
            "e9YSqke7RKR6DWAr4h0ERl",
            "ecWWHpBI1Cl6AoXq+bba2w"
        ],
        "0cfdb76a4": [
            "13pI23QWtHzJ1OTBglys+D",
            "8b68L2P7tFUrHGXnCkSwwF"
        ],
        "0d4958486": [
            "10Q2/qfwxE2Y4D8Df4gByf",
            "a9HT2hGqRCPLT1KXId+yWD"
        ],
        "0da83b46d": [
            "08+xXqvs1MUq/dIBFqvUzw",
            "19t+Nvk2VEbpHwGOqyOUL6"
        ],
        "0e2e71634": [
            "0d3TzOnylF4oZyuXpUG9+u",
            "13pI23QWtHzJ1OTBglys+D",
            "74atjwh4dF2r6AP7gyvGDh",
            "e99aqJi9FDWrlqTT/OacmQ",
            "f5a1I2hM1Jf7s5HtWWfOHx"
        ],
        "0e324a144": [
            "48aI31qb9F5IQ49aCJb0VK",
            "7c8AJX0e1Pz6gfa5C3AVke",
            "839bovB+pJYYwW78cDraUI",
            "e1h1A5QChJSrKLpzWdzSQB",
            "e9YSqke7RKR6DWAr4h0ERl",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "0e4a6afab": [
            "3cgFTtL2RGlpX1HRRPF9ZQ",
            "48aI31qb9F5IQ49aCJb0VK",
            "4a3gDs+ldJaKY1QwfmIT7E",
            "7c8AJX0e1Pz6gfa5C3AVke",
            "a5ldCKodJFla+y2GqbIHaW",
            "e1h1A5QChJSrKLpzWdzSQB",
            "e9YSqke7RKR6DWAr4h0ERl",
            "f38gzWUbZCIrJsjc0TrFDp"
        ],
        "0f8cbe9cb": [
            "48aI31qb9F5IQ49aCJb0VK",
            "7c8AJX0e1Pz6gfa5C3AVke",
            "e1h1A5QChJSrKLpzWdzSQB",
            "e9YSqke7RKR6DWAr4h0ERl",
            "edhBd9UNlI1LfwWWkM+M9F",
            "f38gzWUbZCIrJsjc0TrFDp"
        ]
    },
    md5AssetsMap: {},
    orientation: "landscape",
    debug: true,
    subpackages: {}
};
